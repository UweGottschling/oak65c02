; (C) 2021 Uwe Gottschling
; Moon lande demo
; Graphic mode 1, TMS9918 sprites
; Interrupt controlled vertical sync

!src "../Kernel/include/label_definition.asm"
!src "../Kernel/include/Jumptable.asm"
!src "../Kernel/DOS/DOS_FAT_constant.asm"

!cpu 65c02

;Zero page

SPRITENAME	=$54
SAVEIRQVECT_L	=$55
SAVEIRQVECT_H	=$56
VSYNCFLAG	=$57
JOYSTICK	=$58
VDPSTATUS	=$59

; Zero page fixed point 8.8 
XPOS 		= $60	
YPOS 		= $62
SPEEDX		= $64
SPEEDY		= $66
FUEL		= $68

; Zero page bytes
FRAMECTR	= $70	; 1 byte
INDICATOR	= $71	; 2 bytes
LANDERCOLOR 	= $73	; 1 byte
TILEOFFSET 	= $74	; 2 bytes
SCORE		= $76	; 2 bytes
HIGHSCORE	= $78	; 2 bytes
LANDINGPLATFORM	= $80	; 1 byte
LANDINGPOINTS 	= $81	; 1 byte
SUMEOFPOINTS	= $82	; 1 byte
THRUSTSOUND_OLD	= $83	; 1 byte
; Constants

Gravity 	= $0003		; dez = 0.1/256*x
ThrustUp	= $FFFA
ThrustLeft	= $FFFE
ThrustRight	= $0004
FuelConsump	= $FFC0
MaximumFuel 	= $78


; Sprite name offset
SpriteShip		= $00
SpriteThrustBelow 	= $04
SpriteThrustLeft 	= $08
SpriteThrustRight	= $0C
SpriteArrow		= $10
SpriteExplode1		= $14

; Sprite numbers	
SpriteShipNo		= $00


;VDC bits
EarlyClockBit		= $80

; Register 2 Name Table Base --> $1800
; Register 3 Color table base --> Value/$400 --> $2000
; Register 4 Pattern generator base --> Value/$800 -->$000
; Register 5 Sprite Attribute Table Base Address --> $1B00	
; Register 6 Sprite Pattern Generator Base Address --> $3800
NameTableBase		= $1800 ; 32*24=768 bytes
ColorTableBase		= $2000 ; 256/8=32 bytes; each byte set the color of 8 characters
PatternBase		= $0000 ; 256*8=2048 bytes
SpriteAttrBase		= $1B00	; max. 128 bytes
SpritePattBase		= $3800	; max. 2048 byes

*=$0FFE				; Programme start address header
!word $1000		

Main
		stz VSYNCFLAG
		stz VIA2DDRB	; input for joystick interface
		stz VIA2ACR	; input latch disable
		stz FRAMECTR
		lda #$FF
		sta VIA2ORB 
		sei
		lda IRQVector_L
		sta SAVEIRQVECT_L
		lda IRQVector_H
		sta SAVEIRQVECT_H	
		lda #<Interrupt_Service
		sta IRQVector_L	
		lda #>Interrupt_Service
		sta IRQVector_H
		jsr TMS9918InitMode
		jsr YMF262Init
		jsr TMS9918InitTiles
		jsr TMS9918Init_Sprite_Pattern
		jsr InitalInit
Main1
		jsr DisableAllSprites
		jsr InitStartScreen
		ldx #PS2_GetKey_FN
		jsr PS2_Function_Call
		jsr StartInit
;		ldx #VDP_Clear_Text_Screen_FN
;		jsr VDP_Function_Call		
;		lda #<IndicatorText	; < low byte of expression
;		sta PTR_String_L
;		lda #>IndicatorText	; > high byte of expression
;		sta PTR_String_H
;		ldx #OS_Print_String_FN		
;		jsr OS_Function_Call
;		jsr TMS9918ColorTable
Main5
		jsr ShowSurface
		jsr GameInit
		jsr ShowLanderSprite
		lda #$FF
		sta JOYSTICK		; Disable all thrust sprites
		jsr GetJoystick1
		jsr ShowIndicators	
		jsr PlayIntroMusic
Main10		
		jsr VDCIRQEnable
		jsr GameLoop		; If carry set --> crash
		jsr VDCIRQDisable
		bcs Main22
		jsr CheckSpeedLimits
		bcs Main15
		jsr PlaySuccessMusic
		jsr CalcScore
		jsr DisableAllSprites
		bra Main5
Main15		
		jsr PlayFailMusic
		jsr VDCIRQEnable
		jsr Explode	
		jsr VDCIRQDisable		
Main22
		jsr DisableAllSprites
		jsr GameOver
		cmp #"x"
		bne Main1
		lda SAVEIRQVECT_L
		sta IRQVector_L
		lda SAVEIRQVECT_H
		sta IRQVector_H
		rts

;StartText
;!text "Moon lander demo",CR
;!text "Use joystick",CR,CR
;!text "Press any key to start / exit",CR,00

;IndicatorText
;     "12345678901234567890123456789012"
;!text " ------------>>|<<-------------",CR
;!text "       Vertical speed",CR
;!text " ------------>>|<<-------------",CR
;!text "     Horizontal speed",CR
;!text ">------------<",CR
;!text ".....Fuel.....",CR,CR,00

;***************************
; TMS9918 Sprite Pattern
;
;***************************

TMS9918Init_Sprite_Pattern
		lda #$00
		sta TMS9918REG		; set start address VRAM low Byte
		nop
		nop
		lda #$40 OR $38		; set address maker and high Byte --> Pattern Generator Sub-Block: $3800 
		sta TMS9918REG		; set start address VRAM
		lda #<Sprite_Data	; Low Byte
		sta PTR_String_L
		lda #>Sprite_Data	; High Byte
		sta PTR_String_H		
		ldy #13			; number of pattern 	
TMS9918Init_Sprite_Pattern1
		ldx #32			; Byte per pattern		
TMS9918Init_Sprite_Pattern2
		lda (PTR_String_L)
		jsr Inc_PTR1
		nop
		nop
		nop
		nop
		sta TMS9918RAM
		dex
		bne TMS9918Init_Sprite_Pattern2
		dey
		bne TMS9918Init_Sprite_Pattern1
		rts

;***************************
; Enable VDC Interrupt
;***************************
VDCIRQEnable
		lda #$42+$20+$80		; Enable VDC interrupt
		sta TMS9918REG
		lda #$81
		sta TMS9918REG
		rts

;***************************
; Disable VDC Interrupt
;***************************
VDCIRQDisable
		lda #$42+$80			; Disable VDC interrupt
		sta TMS9918REG
		lda #$81
		sta TMS9918REG
		rts
		
;***************************
; Init zero page variable for the first time
;***************************

GameOver
;		lda #8
;		sta VDP_CURSOR_V
;		lda #10
;		sta VDP_CURSOR_H
		lda #<GameOverText; < low byte of expression
		sta PTR_String_L
		lda #>GameOverText	; > high byte of expression
		sta PTR_String_H
		ldx #OS_Print_String_FN		
		jsr PrintStringRed
		ldx #PS2_GetKey_FN
		jsr PS2_Function_Call
		rts

GameOverText
!text "GAME OVER",CR,"PRESS X FOR EXIT",CR,"PRESS ANY KEY FOR NEW GAME",CR,00

;***************************
; Init zero page variable for the first time
;***************************

InitalInit
		stz HIGHSCORE
		stz HIGHSCORE+1
		rts

;***************************
; Init zero page variable for new game
;***************************

StartInit
		lda #MaximumFuel
		sta FUEL+1
		stz FUEL
		stz SCORE
		stz SCORE+1
		stz LANDINGPOINTS
		rts

;***************************
; Init zero page variable for new attempt
;***************************

GameInit
		lda #5
		sta LANDERCOLOR
		lda #100			; Init parameters
		sta XPOS+1
		stz XPOS
		lda #50
		sta YPOS+1
		stz YPOS
		stz SPEEDX
		stz SPEEDX+1
		stz SPEEDY
		stz SPEEDY+1
		stz LANDINGPLATFORM
		stz THRUSTSOUND_OLD
		lda #SpriteShip
		sta SPRITENAME
		rts
		
;***************************
; Main loop
;***************************

GameLoop
		bra GameLoop85		; V-Sync
GameLoop1
		lda #SpriteShip
		sta SPRITENAME
		jsr ShowLanderSprite
		jsr GetJoystick
		jsr CheckLandingZone
		bcs GameLoop_Success	
		jsr CheckCollision
		bcs GameLoop_Explode		
		jsr UpdatePos
		jsr ShowIndicators
GameLoop41
		jsr PS2_ChkKey
		bcs GameLoop_Explode

;GameLoop44		
;		ldx #PS2_GetKey_FN
;		jsr PS2_Function_Call
;		jsr ShowSurface
;		bra GameLoop
GameLoop80
		lda VDPSTATUS	
		ror 
		ror
		ror
		ror
		sta TMS9918REG	; Change frame color
		lda #$87
		sta TMS9918REG
GameLoop85		
		jsr Wait_V_Sync
		lda #$11		; Change frame color
		sta TMS9918REG
		lda #$87
		sta TMS9918REG	
		jmp GameLoop1
GameLoop_Explode
		jsr Explode
		jsr SoundOff
		sec
		rts
GameLoop_Success
GameLoop42		
		jsr SoundOff
		clc
		rts

;***************************
; Show lander
; Sprite attributes:
;	Offset	
;	0		Y sprite 0, special: Y=208 all following sprites are not displayed
;	1		X sprite 0
;	2		sprite pattern 0
;	3		colour sprite 0 + EC (EC=early clock, move sprite 32 pixels to the left)
;***************************

ShowLanderSprite
		lda #SpriteShipNo
		sta TMS9918REG		; set start address VRAM low byte
		nop	
		nop
		lda #$40 OR $1B		; set address maker and high byte 
		sta TMS9918REG		; set start address VRAM
		lda YPOS+1
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; Y Position
		lda XPOS+1
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; X Position
		lda SPRITENAME
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; Sprite Name 
		nop
		nop
		nop
		nop
		lda LANDERCOLOR		; Sprite color: 5 light blue
		sta TMS9918RAM		; Sprite color code
		rts

;***************************
; Diable all sprites
;
;***************************

DisableAllSprites
		lda #SpriteShipNo	; Sprite number 00
		sta TMS9918REG		; Set start address VRAM low byte
		nop	
		nop
		lda #$40 OR $1B		; Set address maker and high byte 
		sta TMS9918REG		; Set start address VRAM
		lda #208		; This an all following sprites are not dispayed
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; Y Position
		rts

;***************************
; Lander explode
;
;***************************

Explode
		lda #$FF
		sta JOYSTICK		; Disable thrust sprites and thrust sound
		jsr GetJoystick4
		jsr YMF262_Trig_Explode_ON
		lda #SpriteExplode1
		sta SPRITENAME
		lda #11 	; 11 = light yellow
		sta LANDERCOLOR
Explode5
		jsr ShowLanderSprite
		stz FRAMECTR
		jsr YMF262_Trig_Explode_OFF
Explode10
		lda FRAMECTR
		and #$08
		beq Explode10
		lda SPRITENAME
		clc
		adc #$4
		sta SPRITENAME
		cmp #$34
		beq Explode40
		bra Explode5		
Explode40
		lda #SpriteShip
		sta SPRITENAME
		jsr YMF262_Trig_Explode_OFF
		rts
		
;***************************
; Show vertical and horizontal speed 
; Sprite number 5 (offset 16)
;***************************

ShowIndicators
		lda #SpriteArrow
		sta SPRITENAME
		sta TMS9918REG		; set start address VRAM low byte
		nop	
		nop
		lda #$40 OR $1B		; set address maker and high byte 
		sta TMS9918REG		; set start address VRAM
		lda #0
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; Y Position
		lda SPEEDY
		sta INDICATOR
		lda SPEEDY+1
		sta INDICATOR+1		
		jsr IndicatorsShift		
		lda INDICATOR+1	
		clc
		adc #124
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; X Position
		lda SPRITENAME
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; Sprite Name 
		nop
		nop
		nop
		nop
		lda #13				; Sprite color: 13 magenta
		sta TMS9918RAM		; Sprite color code
		lda #16
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; Y Position
		lda SPEEDX
		sta INDICATOR
		lda SPEEDX+1
		sta INDICATOR+1
		jsr IndicatorsShift		
		lda INDICATOR+1	
		clc
		adc #124
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; X Position
		lda SPRITENAME
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; Sprite Name 
		nop
		nop
		nop
		nop
		lda #7				; Sprite color: 7 cyan
		sta TMS9918RAM		; Sprite color code
		lda #32
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; Y Position
		lda FUEL+1
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; X Position
		lda SPRITENAME
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; Sprite Name 
		nop
		nop
		nop
		nop
		lda #4				; Sprite color: 4 dark blue
		sta TMS9918RAM		; Sprite color code		
		nop
		nop
		nop
		nop
		lda #208			; Disable all other sprites
		sta TMS9918RAM		
		rts
		
;***************************
; Shift indicator 5 times right
;***************************	
	
IndicatorsShift
		clc
		rol INDICATOR
		rol INDICATOR+1
		clc
		rol INDICATOR
		rol INDICATOR+1	
		clc
		rol INDICATOR
		rol INDICATOR+1		
		clc
		rol INDICATOR
		rol INDICATOR+1	
		clc
		rol INDICATOR
		rol INDICATOR+1	
		rts		

;***************************
; Config YMF262
;
;***************************

YMF262Init
		lda #$05
		sta YMF262_ADDR2
		lda #$01			
		sta YMF262_DATA2	; OPL3 Mode
		ldy #$00
YMF262Init10
		lda YMF262_Instrument_Noise,y
		sta YMF262_ADDR1
		iny
		lda YMF262_Instrument_Noise,y
		sta YMF262_DATA1
		iny
		cpy #YMF262_Instrument_Noise_End-YMF262_Instrument_Noise
		bne YMF262Init10
		rts

YMF262_Instrument_Noise
; The groupings of twenty-two registers (20-35, 40-55, etc.) 
; have an odd order due to the use of two operators for each FM voice. 
; The following table shows the offsets within each group of registers for each operator.

;    Channel	1	2 	3	4	5 	6	7	8 	9
;    Operator 1	00	01 	02	08	09 	0A	10	11 	12
;    Operator 2	03	04 	05	0B	0C 	0D	13	14 	15
;Channel 1: thrust noise
		; Register, Data 
		!by $b0,$00 			; Turn the voice off; set the octave and freq MSB
		; Operator 1
		!by $20,$21			; Set the modulator's multiple 5x, Tremolo, Vibrator
		!by $40,$00			; Set the modulator's Key-scaling and Output-level: 0 = loudest
		!by $60,$F1			; Modulator attack: F is the fastest; decay :F is the fastest
		!by $80,$06			; Modulator sustain: 0 is the loudest; release: F is the fastest.
		!by $e0,$00			; Set Modulator wave form
		; Operator 2
		!by $23,$21+$40+$80		; Set the carrier's multiple 1x, vibrator, tremolo
		!by $43,$00			; Set the carrier Key Scaling and volume; 0 = loudest
		!by $63,$F1			; Carrier attack: F is the fastest ; decay: F is the fastest
		!by $83,$06			; Carrier sustain: 0 is the loudest; release: F is the fastest.
		!by $e3,$00			; Set Carrier wave form
		; Channel 1
		!by $a0,$10			; Set voice frequency's LSB
		!by $c0,$3E			; Set Feedback, Decay Alg

;Channel 2: music
		!by $b1,$00 			; Turn the voice off; set the octave and freq MSB
		; Operator 1
		!by $21,$00			; Set the modulator's multiple 5x, Tremolo, Vibrator
		!by $41,$3F			; Set the modulator's Key-scaling and Output-level: 0 = loudest
		!by $61,$00			; Modulator attack: F is the fastest; decay :F is the fastest
		!by $81,$00			; Modulator sustain: 0 is the loudest; release: F is the fastest.
		!by $e1,$00			; Set Modulator wave form
		; Operator 2
		!by $24,$21			; Set the carrier's multiple 1x, vibrator, tremolo
		!by $44,$0F			; Set the carrier Key Scaling and volume; 0 = loudest
		!by $64,$EF			; Carrier attack: F is the fastest ; decay: F is the fastest
		!by $84,$0E			; Carrier sustain: 0 is the loudest; release: F is the fastest.
		!by $e4,$06			; Set Carrier wave form
		; Channel 1
		!by $a1,$41			; Set voice frequency's LSB
		!by $c1,$31			; Set Feedback, Decay Alg

;Channel 3: explode
		!by $b2,$00 			; Turn the voice off; set the octave and freq MSB
		; Operator 1
		!by $22,$21			; Set the modulator's multiple 5x, Tremolo, Vibrator
		!by $42,$00			; Set the modulator's Key-scaling and Output-level: 0 = loudest
		!by $62,$F0			; Modulator attack: F is the fastest; decay :F is the fastest
		!by $82,$01			; Modulator sustain: 0 is the loudest; release: F is the fastest.
		!by $e2,$00			; Set Modulator wave form
		; Operator 2
		!by $25,$21+$40+$80		; Set the carrier's multiple 1x, vibrator, tremolo
		!by $45,$00			; Set the carrier Key Scaling and volume; 0 = loudest
		!by $65,$F0			; Carrier attack: F is the fastest ; decay: F is the fastest
		!by $85,$04			; Carrier sustain: 0 is the loudest; release: F is the fastest.
		!by $e5,$00			; Set Carrier wave form
		; Channel 1
		!by $a2,$01			; Set voice frequency's LSB
		!by $c2,$3E			; Set Feedback, Decay Alg
; General settings
		!by $BD,$B0			; Amplitude Modulation Depth / Vibrato Depth / Rhythm
		!by $03,$3C			; Timer 2 Data = 195 * 0.320msec (255-195=$3C)		
		
YMF262_Instrument_Noise_End

;***************************
; Thrust sound on or off
;
;***************************

YMF262_Trig_Noise_OFF
		lda #$b0
		sta YMF262_ADDR1
		lda #$03
		sta YMF262_DATA1
		stz THRUSTSOUND_OLD
		rts
YMF262_Trig_Noise_ON	
		lda THRUSTSOUND_OLD	; Check if already on
		bne YMF262_Trig_Noise_ON2
		lda #$b0
		sta YMF262_ADDR1
		lda #$33
		sta YMF262_DATA1	; Turn the voice on; set the octave and freq MSB
		sta THRUSTSOUND_OLD
YMF262_Trig_Noise_ON2
		rts

;***************************
; Trigger explode sound
;
;***************************

YMF262_Trig_Explode_OFF
		lda #$b2
		sta YMF262_ADDR1
		lda #$03
		sta YMF262_DATA1
		rts
YMF262_Trig_Explode_ON
		lda #$b2
		sta YMF262_ADDR1
		lda #$33
		sta YMF262_DATA1	; Turn the voice on; set the octave and freq MSB
		rts
		
;***************************
; Increment PTR_W_L/_H
;
;***************************

Inc_PTR1    	
		inc   PTR_String_L		; increments ptr1
		bne   Inc_PTR11
		inc   PTR_String_H 
Inc_PTR11      	
		rts 

;***************************
; Wait V-Sync
;
;***************************

Wait_V_Sync
		lda VSYNCFLAG
		beq Wait_V_Sync
		stz VSYNCFLAG
		rts
		
;***************************
; Update lander position
;
;***************************
	
UpdatePos
		clc
		lda XPOS
		adc SPEEDX
		sta XPOS
		lda XPOS+1
		adc SPEEDX+1
		sta XPOS+1
		clc
		lda SPEEDY
		adc #<Gravity
		sta SPEEDY
		lda SPEEDY+1
		adc #>Gravity
		sta SPEEDY+1		
		clc
		lda YPOS
		adc SPEEDY
		sta YPOS
		lda YPOS+1
		adc SPEEDY+1
		sta YPOS+1
		rts

;***************************
;	
; Check if tile below lander is landing zone
;***************************

CheckLandingZone
		jsr CalcCollisionOffset
		lda YPOS+1				; Calculate tile position
		and #$07
		beq	CheckLandingZone2	
		lda #65
		bra CheckLandingZone3
CheckLandingZone2
		lda #33
CheckLandingZone3
		clc
		adc TILEOFFSET
		sta TILEOFFSET
		lda #0
		adc TILEOFFSET+1
		sta TILEOFFSET+1
		jsr GetTile
		bcs CheckCollision10 	; If set then rocḱ
		sta LANDINGPLATFORM
		cmp #220
		rts
CheckCollision10
		clc
		rts

;***************************
; Check collision
;
; Check which tile is next to the lander
;***************************

CheckCollision
		jsr CalcCollisionOffset
		lda YPOS+1
		and #$07
		beq	CheckCollision1
		lda #32
		clc
		adc TILEOFFSET
		sta TILEOFFSET
		lda #0
		adc TILEOFFSET+1
		sta TILEOFFSET+1
CheckCollision1
		jsr GetTile			; Check positon top left
		bcs CheckCollision5	
		lda #1				
		clc
		adc TILEOFFSET
		sta TILEOFFSET
		lda #0
		adc TILEOFFSET+1
		sta TILEOFFSET+1
		jsr GetTile			; Check position next right
		bcs CheckCollision5	
;		lda YPOS+1
;		and #$07
;		beq	CheckCollision2	
;		lda #63
;		bra CheckCollision3
CheckCollision2
		lda #31				
CheckCollision3
		clc
		adc TILEOFFSET
		sta TILEOFFSET
		lda #0
		adc TILEOFFSET+1
		sta TILEOFFSET+1
		jsr GetTile			; Check position bottom left 
		bcs CheckCollision5	
		lda #1
		clc
		adc TILEOFFSET
		sta TILEOFFSET
		lda #0
		adc TILEOFFSET+1
		sta TILEOFFSET+1
		jsr GetTile			; Check position bottom right 
		bcs CheckCollision5	
		lda XPOS+1
		and #$07
		beq CheckCollision5	
		lda #1
		clc
		adc TILEOFFSET
		sta TILEOFFSET
		lda #0
		adc TILEOFFSET+1
		sta TILEOFFSET+1
		jsr GetTile			
CheckCollision5	
		rts

;***************************
; Check collision
;	
; Caclulate offset to tile behind the lander
;***************************

CalcCollisionOffset
		lda YPOS+1
		inc
		and #$F8
		sta TILEOFFSET
		stz TILEOFFSET+1
		clc
		rol TILEOFFSET		; Y*2 (Y*32/8 = Y*4)
		rol TILEOFFSET+1
		clc
		rol TILEOFFSET		; Y*4
		rol TILEOFFSET+1
		lda XPOS+1
		lsr			; X/2
		lsr 			; X/4
		lsr			; X/8
		clc
		adc TILEOFFSET
		sta TILEOFFSET
		lda #0
		adc TILEOFFSET+1
		sta TILEOFFSET+1
CalcCollisionOffset2
		rts

;***************************
; Get tile from VRAM and compare for > $BF
; 
;***************************

GetTile
		lda #<NameTableBase
		clc
		adc TILEOFFSET
		sta TMS9918REG		; Set start address VRAM low byte
		nop
		nop
		lda #>NameTableBase		; Set address maker and high byte 
		adc TILEOFFSET+1
		sta TMS9918REG		; Set start address VRAM high byte
		nop
		nop
		nop
		nop
		lda TMS9918RAM		
		cmp #$E0
;		php
;GetTile2
;		lda #<NameTableBase
;		clc
;		adc TILEOFFSET
;		sta TMS9918REG		; Set start address VRAM low byte
;		nop
;		nop
;		nop
;		nop
;		lda #$40 or >NameTableBase		; Set address maker and high byte 
;		adc TILEOFFSET+1
;		sta TMS9918REG		; Set start address VRAM high byte
;		nop
;		nop
;		nop
;		nop
;		nop
;		nop
;		nop
;		lda #207
;		sta TMS9918RAM	
;		plp
		rts
		
;***************************
; Get joystick 
;
;***************************		

GetJoystick		
		lda FUEL+1			; Check fuel
		bne GetJoystick1	
		lda FUEL
		bne GetJoystick1
		lda #$FF
		sta JOYSTICK			; if emtpy then no thrust
		bra GetJoystick4
GetJoystick1
		lda VIA2ORB			;Joystick port: bit 0 = up, bit 1 = down, bit 2 = left, bit 3 = right, bit 4 = fire		
		lsr
		lsr
GetJoystick4	
		lsr
		sta JOYSTICK
		bcs GetJoystick5	; Not left ?
		clc
		lda SPEEDX
		adc #<ThrustLeft
		sta SPEEDX
		lda SPEEDX+1
		adc #>ThrustLeft
		sta SPEEDX+1	
		lda #SpriteThrustRight
		jsr ShowTrustSprite
		bra GetJoystick6
GetJoystick5
		lda #SpriteThrustRight
		jsr HideSprite
GetJoystick6
		lsr JOYSTICK
		bcs GetJoystick7	; Not right ?
		clc
		lda SPEEDX
		adc #<ThrustRight
		sta SPEEDX
		lda SPEEDX+1
		adc #>ThrustRight
		sta SPEEDX+1
		lda #SpriteThrustLeft
		jsr ShowTrustSprite
		bra GetJoystick8
GetJoystick7
		lda #SpriteThrustLeft
		jsr HideSprite
GetJoystick8
		lsr JOYSTICK
		bcs GetJoystick9	; Not fire button ?
		clc
		lda SPEEDY
		adc #<ThrustUp
		sta SPEEDY
		lda SPEEDY+1
		adc #>ThrustUp
		sta SPEEDY+1
		lda #SpriteThrustBelow
		jsr ShowTrustSprite
		rts
GetJoystick9	
		lda #SpriteThrustBelow
		jsr HideSprite
		rts
		
;***************************
; Init Graphic Mode
;
;***************************

TMS9918InitMode
		lda #TMS9918_Set_Screen1
		ldx #VDP_Set_Video_Mode_FN
		jsr VDP_Function_Call
;		ldx #VDP_Init_Pattern_FN
;		jsr VDP_Function_Call	
		ldx #VDP_Clear_Text_Screen_FN
		jsr VDP_Function_Call
		lda #$d3		; Change frame color
		sta TMS9918REG
		lda #$87
		sta TMS9918REG	
		lda #$42+$80		; Set mode register
		sta TMS9918REG
		lda #$81
		sta TMS9918REG	
		rts
		
;***************************
; Check for key press
;***************************

PS2_ChkKey
		ldx #PS2_ChkKey_FN
		jsr PS2_Function_Call
		rts

;***************************	
; HideThrustSprite
; A= OffsetSprite
;***************************	

HideSprite	
		sta SPRITENAME		; Attribute table base address and sprite data
		sta TMS9918REG		; Set start address VRAM low byte
		nop	
		nop
		lda #$40 OR >SpriteAttrBase	; set address maker and high byte 
		sta TMS9918REG		; Set start address VRAM
HideSprite2
		lda #$FF
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; Y Position
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; X Position
		lda SPRITENAME
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; Sprite Name 
		nop
		nop
		nop
		nop
		lda #00			
		sta TMS9918RAM		; Sprite color code
		jsr YMF262_Trig_Noise_OFF
		rts

;***************************	
; ShowThrustSprite
; A=OffsetSprite
;***************************	

ShowTrustSprite
		sta SPRITENAME
		sta TMS9918REG		; set start address VRAM low byte
		nop	
		nop
		lda #$40 OR $1B		; set address maker and high byte 
		sta TMS9918REG		; set start address VRAM
		lda YPOS+1
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; Y Position
		lda XPOS+1
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; X Position
		lda SPRITENAME
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; Sprite Name 
		nop
		nop
		nop
		nop
		jsr YMF262_Trig_Noise_ON	
		clc
		lda FUEL
		adc #<FuelConsump
		sta FUEL
		lda FUEL+1
		beq ShowThrustSprite1
		adc #>FuelConsump
		sta FUEL+1
ShowThrustSprite1
		lda FRAMECTR
		and #$08
		bne ShowThrustSprite2
		and #$01
		bne ShowThrustSprite3
		lda #11			; Color for sprite: 11 = light yellow
		sta TMS9918RAM		; Sprite color code
		rts
ShowThrustSprite2
		lda #9			; Color for sprite: 9 = light red
		sta TMS9918RAM		; Sprite color code
		rts
ShowThrustSprite3
		lda #5			; Color for sprite: 5 =light blue
		sta TMS9918RAM		; Sprite color code
		rts
		
;***************************
; Interrupt service routine
; used for vertical sync
; (polling does not work frame accurately)
;***************************

Interrupt_Service
		lda TMS9918REG		; Clear interrupt flag
		sta VDPSTATUS
;		lda #$e9		; Change frame color
;		sta TMS9918REG
;		lda #$87
;		sta TMS9918REG
		lda #00
Interrupt_Service2
		inc VSYNCFLAG
		inc FRAMECTR
;		lda TMS9918REG		; Clear interrupt flag
;		lda #$11		; Change frame color
;		sta TMS9918REG
;		lda #$87
;		sta TMS9918REG
		lda ARegister
		ldx XRegister
		ldy YRegister
		rti
		

;***************************
; Show moon surface
; 
;***************************		

ShowSurface
		lda #$11		; Change frame color
		sta TMS9918REG
		lda #$87
		sta TMS9918REG	
		lda #<NameTableBase
		sta TMS9918REG		; set start address VRAM low Byte
		nop
		nop
		nop
		nop
		lda #$40 OR >NameTableBase		; set address maker and high Byte 
		sta TMS9918REG		; set start address VRAM
		lda #<SCREEN_0_0	; Low Byte
		sta PTR_String_L
		lda #>SCREEN_0_0	; High Byte
		sta PTR_String_H		
		ldy #32			; number of pattern 	
ShowSurface1
		ldx #24			; Byte per pattern		
ShowSurface2
		lda (PTR_String_L)
		jsr Inc_PTR1
		nop
		nop
		nop
		nop
		sta TMS9918RAM
		dex
		bne ShowSurface2
		dey
		bne ShowSurface1
		jsr PrintAllScore
		rts	
		
;***************************
; Disable Noise Channels
;***************************	

SoundOff
		lda #$b0			; Channel 1 off
		sta YMF262_ADDR1
		lda #$00
		sta YMF262_DATA1
		lda #$b1			; Channel 2 off
		sta YMF262_ADDR1
		lda #$00
		sta YMF262_DATA1	
		rts 
		
;***************************
; Play Intro Music
;***************************	

PlayIntroMusic
		lda #<Intro_MusicData
		sta PTR_R_L
		lda #>Intro_MusicData
		sta PTR_R_H
		jsr PlayMusic
		rts
		
;***************************
; Play Success Music
;***************************			

PlaySuccessMusic				
		lda #<Success_MusicData
		sta PTR_R_L
		lda #>Success_MusicData		
		sta PTR_R_H
		jsr PlayMusic
		rts
		
;***************************
; Play Fail Music
;***************************			

PlayFailMusic		
		lda #<Fail_MusicData
		sta PTR_R_L
		lda #>Fail_MusicData
		sta PTR_R_H
		jsr PlayMusic
		rts
				
;***************************
; Play Music
;***************************	

PlayMusic
		lda (PTR_R_L)
PlayMusic2	
		beq PlayMusic_exit
		cmp #$5E
		bne PlayMusic10
		jsr Read_Inc_PTR_R  
		sta YMF262_ADDR1
		jsr Read_Inc_PTR_R  
		sta YMF262_DATA1
		bra PlayMusic11
PlayMusic10
		cmp #$61
		bne PlayMusic_exit
		jsr Read_Inc_PTR_R
		jsr MusicDelay
PlayMusic11
		jsr Read_Inc_PTR_R  
		bra PlayMusic2
PlayMusic_exit
		rts
		
;***************************
; Increment PTR_R_L/_H and read from pointer
;
;***************************

Read_Inc_PTR_R    	
		inc PTR_R_L			; increments ptr2
		bne Read_Inc_PTR_R1
		inc PTR_R_H      
Read_Inc_PTR_R1      
		lda (PTR_R_L)
		rts		
		
;***************************
; Music delay ca. 65ms
; 
;***************************	

MusicDelay
		tax
MusicDelay4
		lda #$04		; Reset IRQ Flag, alle other bits are ignored
		sta YMF262_ADDR1
		lda #$80
		sta YMF262_DATA1 
		
		lda #$04		; Start timer2
		sta YMF262_ADDR1
		lda #$02
		sta YMF262_DATA1 		
MusicDelay10
		lda YMF262_ADDR1	; Read status timer 2
		and #$20
		beq MusicDelay10
		dex
		bne MusicDelay4	

		lda #$04		; Stop timer 2
		sta YMF262_ADDR1
		lda #$00
		sta YMF262_DATA1 
		rts
		
;***************************
; Init TMS9918 tiles
; 
;***************************		
		
TMS9918InitTiles		
		lda #<PatternBase
		sta TMS9918REG		; set start address VRAM low Byte
		nop
		nop
		nop
		nop
		lda #$40 OR >PatternBase		; set address maker and high Byte 
		sta TMS9918REG		; set start address VRAM
		lda #<BANK_PATTERN_0	; Low Byte
		sta PTR_String_L
		lda #>BANK_PATTERN_0	; High Byte
		sta PTR_String_H		
		ldy #00			; number of pattern 	
TMS9918InitTiles1
		ldx #8			; Byte per pattern		
TMS9918InitTiles2
		lda (PTR_String_L)
		jsr Inc_PTR1
		nop
		nop
		nop
		nop
		sta TMS9918RAM
		dex
		bne TMS9918InitTiles2
		dey
		bne TMS9918InitTiles1
		lda #<ColorTableBase
		sta TMS9918REG		; set start address VRAM low Byte
		nop
		nop
		nop
		nop
		lda #$40 OR >ColorTableBase		; set address maker and high Byte --> Pattern Generator Sub-Block: $3800 
		sta TMS9918REG		; set start address VRAM
		lda #<BANK_COLOR_0	; Low Byte
		sta PTR_String_L
		lda #>BANK_COLOR_0	; High Byte
		sta PTR_String_H		
		ldy #32			; number of pattern 	
TMS9918InitTiles10
		ldx #1			; Byte per pattern		
TMS9918InitTiles11
		lda (PTR_String_L)
		jsr Inc_PTR1
		nop
		nop
		nop
		nop
		sta TMS9918RAM
		dex
		bne TMS9918InitTiles11
		dey
		bne TMS9918InitTiles10
		rts
		
;***************************
; Init TMS9918 start screen
; 
;***************************

InitStartScreen
		lda #$45			; Change frame color
		sta TMS9918REG
		lda #$87
		sta TMS9918REG	
		ldx #VDP_Clear_Text_Screen_FN
		jsr VDP_Function_Call				
		lda #<InitStartScreenText	; < low byte of expression
		sta PTR_String_L
		lda #>InitStartScreenText	; > high byte of expression
		sta PTR_String_H
		ldx #OS_Print_String_FN		
		jsr OS_Function_Call
		rts

InitStartScreenText
;     "12345678901234567890123456789012"
!text LF
!text "    **** MOON LANDER V1 ****",LF
!text LF
!text "    (C) 2021 UWE GOTTSCHLING",LF
!text LF
!text "READY?",LF
!text LF
;     "12345678901234567890123456789012"
!text "Use a joystick for controlling",LF
!text "thrust. Land on one of three",LF
!text "positions. H-speed and v-speed",LF
!text "must come within the green area",LF
!text "of the top indicators when",LF
!text "landing. The score according to",LF
!text "the v-speed is multiplied by",LF
!text "the number below. The space",LF
!text "ship is refueled on a",LF
!text "successful landing.",LF,LF
!text "Press any key to start.",LF
!text 00
;***************************
; Check speed limits
; 
;***************************

CheckSpeedLimits
		lda #6
		sta VDP_CURSOR_V
		lda #0
		sta VDP_CURSOR_H
;		lda #$15		; Change frame color
;		sta TMS9918REG
;		lda #$87
;		sta TMS9918REG	
;		ldx #VDP_Clear_Text_Screen_FN
;		jsr VDP_Function_Call				
;		lda #<ResultsText; < low byte of expression
;		sta PTR_String_L
;		lda #>ResultsText	; > high byte of expression
;		sta PTR_String_H
;		ldx #OS_Print_String_FN		
;		jsr OS_Function_Call
		lda SPEEDX
		sta INDICATOR
		lda SPEEDX+1
		sta INDICATOR+1		
		jsr IndicatorsShift		
		lda INDICATOR+1			; Maximum: $FB to $05
		clc
		adc #5
		cmp #10
		bcc CheckSpeedLimits3
		jsr PrintSpeedToooHigh
		bra CheckSpeedLimitsErrorExit
CheckSpeedLimits3
;		ldx #OS_Print_Bin2Hex_FN		
;		jsr OS_Function_Call	
;		lda #LF
;		ldx #OS_Print_CHR_FN
;		jsr OS_Function_Call
		lda SPEEDY
		sta INDICATOR
		lda SPEEDY+1
		sta INDICATOR+1		
		jsr IndicatorsShift		
		lda INDICATOR+1			; Maximum: $07
		cmp #7
		bcc CheckSpeedLimits6		
		jsr PrintSpeedToooHigh
		bra CheckSpeedLimitsErrorExit
CheckSpeedLimits6	
		lda INDICATOR+1	
		eor #07					; Convert 0...7 --> 7 ...0
		inc 					; 1..8 points
		sta LANDINGPOINTS
;		ldx #OS_Print_Bin2Hex_FN		
;		jsr OS_Function_Call	
CheckSpeedLimitsExit
		rts	
CheckSpeedLimitsErrorExit		
		sec
		rts
		
;***************************
; Calculate score and print 
; 
;***************************		
					
CalcScore
		lda #6
		sta VDP_CURSOR_V
		lda #0
		sta VDP_CURSOR_H
		jsr YMF262_Trig_Noise_OFF
		stz SUMEOFPOINTS
		lda #<PointsText	; < low byte of expression
		sta PTR_String_L
		lda #>PointsText	; > high byte of expression
		sta PTR_String_H
		jsr PrintStringRed
		lda LANDINGPOINTS
		jsr PrintDigits		

		lda #<MultiplyText	; < low byte of expression
		sta PTR_String_L
		lda #>MultiplyText	; > high byte of expression
		sta PTR_String_H
		jsr PrintStringRed
	
		lda LANDINGPLATFORM
		cmp #221		; *2
		bne CalcScore10
		lda #2
		jsr PrintDigits				
		ldx #2
CalcScore8		
		lda SUMEOFPOINTS 
		clc
		adc LANDINGPOINTS
		sta SUMEOFPOINTS
		dex 
		bne CalcScore8
		bra CalcScore30		
CalcScore10
		cmp #222		; *5
		bne	CalcScore20
		lda #5
		jsr PrintDigits				
		ldx #5
CalcScore12		
		lda SUMEOFPOINTS 
		clc
		adc LANDINGPOINTS
		sta SUMEOFPOINTS
		dex 
		bne CalcScore12
		bra CalcScore30			
CalcScore20	
		cmp #223		;*10	
		lda #$10		
		jsr PrintDigits				
		ldx #10		
CalcScore22		
		lda SUMEOFPOINTS 
		clc
		adc LANDINGPOINTS
		sta SUMEOFPOINTS
		dex 
		bne CalcScore22
CalcScore30
		lda SUMEOFPOINTS
		clc
		adc SCORE
		sta SCORE
		lda #0
		adc SCORE+1
		sta SCORE+1
		lda FUEL+1
		clc
		adc SUMEOFPOINTS 
		sta FUEL+1
		lda FUEL+1
		cmp #MaximumFuel	; Maximum fuel 
		bcc CalcScore40
		lda #MaximumFuel
		sta FUEL+1		
CalcScore40
		lda SCORE+1		; Compare and set high score
		cmp HIGHSCORE+1
		bcc CalcScore50
		sta HIGHSCORE+1
CalcScore50		
		lda SCORE
		cmp HIGHSCORE
		bcc CalcScore52
		sta HIGHSCORE
CalcScore52		
		lda #<PressAnyKeyText	; < low byte of expression
		sta PTR_String_L
		lda #>PressAnyKeyText	; > high byte of expression
		sta PTR_String_H
		jsr PrintStringRed
		jsr PrintAllScore
		ldx #PS2_GetKey_FN
		jsr PS2_Function_Call
		rts

PointsText
!text "POINTS: ",00

MultiplyText
!text " X ",00

PressAnyKeyText
;     "12345678901234567890123456789012"
!text LF,"PRESS ANY KEY TO CONTINUE",LF,00

;***************************
; 16-Bit Binary to BCD
; input: Operand 1,2
; output: Operand 3,4,5
; used: A,X
;***************************

Bin2BCD16
		clc			
Bin2BCD16_3
		stz Operand3	; erase result (BCD Outbuffer)
		stz Operand4
		stz Operand5
		ldx #16			; number of bits
		sed				; set decimal mode
Bin2BCD16_4
		asl Operand1	; shift out one bit
		rol Operand2
		lda Operand5
		adc Operand5
		sta Operand5
		lda Operand4
		adc Operand4
		sta Operand4
		lda Operand3
		adc Operand3
		sta Operand3
		dex				; and repeat for next bit
		bne Bin2BCD16_4
		cld				; clear decimal mode
		rts

;***************************
; Print speed too high
;***************************		

PrintSpeedToooHigh		
		lda #<VSpeedTooHighText	; < low byte of expression
		sta PTR_String_L
		lda #>VSpeedTooHighText	; > high byte of expression
		sta PTR_String_H
		jsr PrintStringRed
		rts	
VSpeedTooHighText
!text "THE LANDING SPEED WAS TOO HIGH",LF,00			

;***************************
; Print score and hi-score
;***************************		

PrintAllScore
		lda #4
		sta VDP_CURSOR_V
		lda #26
		sta VDP_CURSOR_H
		lda SCORE
		sta Operand1
		lda SCORE+1
		sta Operand2
		jsr Bin2BCD16
		lda Operand3
		jsr PrintDigits
		lda Operand4
		jsr PrintDigits
		lda Operand5
		jsr PrintDigits
		lda #5
		sta VDP_CURSOR_V
		lda #26
		sta VDP_CURSOR_H
		lda HIGHSCORE
		sta Operand1
		lda HIGHSCORE+1
		sta Operand2
		jsr Bin2BCD16
		lda Operand3
		jsr PrintDigits
		lda Operand4
		jsr PrintDigits
		lda Operand5
		jsr PrintDigits		
		rts	
		
;***************************
; Print digits with red font
;***************************	

PrintDigits
		pha
		and #$f0
		lsr						
		lsr
		lsr
		lsr
		clc
		adc #144			
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		pla
		and #$0f
		clc
		adc #144
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		rts	
			
;***************************
; Print string with red font
;***************************	

PrintStringRed
		lda (PTR_String_L)
		beq PrintStringRedExit
		cmp #$1F
		bcc PrintStringRed5
		clc
		adc #$60
PrintStringRed5
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		jsr Inc_PTR1
		bra PrintStringRed
PrintStringRedExit
		rts

; TMS9918 colors: 
;0 transparent
;1 black
;2 medium green
;3 light green
;4 dark blue
;5 light blue
;6 dark red
;7 cyan
;8 medium red
;9 light red
;10 dark yellow
;11 light yellow
;12 dark green
;13 magenta
;14 gray
;15 white


Sprite_Data
; Sprite 00: 
; Lander
;		 12345678  top left
	!by	%00000001	;1
	!by	%00000111	;2
	!by	%00001111	;3
	!by	%00011100	;4
	!by	%00011100	;5
	!by	%00011111	;6
	!by	%00011110	;7
	!by	%00011111	;8
;		 12345678 bottom left
	!by	%00011110	;1
	!by	%00011111	;2
	!by	%00001110	;3
	!by	%00000111	;4
	!by	%00000100	;5
	!by	%00001000	;6
	!by	%00010000	;7
	!by	%11111000	;8
;		 12345678 top right
	!by	%10000000	;1
	!by	%11100000	;2
	!by	%11110000	;3
	!by	%00111000	;4
	!by	%00111000	;5
	!by	%11111000	;6
	!by	%01111000	;7
	!by	%11111000	;8
;		 12345678 bottom right
	!by	%01111000	;1
	!by	%11111000	;2
	!by	%01110000	;3
	!by	%11100000	;4
	!by	%00100000	;5
	!by	%00010000	;6
	!by	%00001000	;7
	!by	%00011111	;8

; Sprite 04: thrust below (Offset 4)
;		 12345678  oben links
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%00000000	;3
	!by	%00000000	;4
	!by	%00000000	;5
	!by	%00000000	;6
	!by	%00000000	;7
	!by	%00000000	;8
;		 12345678 unten links
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%00000000	;3
	!by	%00000000	;4
	!by	%00000001	;5
	!by	%00000011	;6
	!by	%00000101	;7
	!by	%00000010	;8
;		 12345678 oben rechts
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%00000000	;3
	!by	%00000000	;4
	!by	%00000000	;5
	!by	%00000000	;6
	!by	%00000000	;7
	!by	%00000000	;8
;		 12345678 unten rechts
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%00000000	;3
	!by	%00000000	;4
	!by	%10000000	;5
	!by	%11000000	;6
	!by	%10100000	;7
	!by	%01000000	;8

; Sprite 08: thrust left (Offset 8)
;		 12345678  oben links
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%00000000	;3
	!by	%00000000	;4
	!by	%01000000	;5
	!by	%10100000	;6
	!by	%01000000	;7
	!by	%00000000	;8
;		 12345678 unten links
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%00000000	;3
	!by	%00000000	;4
	!by	%00000000	;5
	!by	%00000000	;6
	!by	%00000000	;7
	!by	%00000000	;8
;		 12345678 oben rechts
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%00000000	;3
	!by	%00000000	;4
	!by	%00000000	;5
	!by	%00000000	;6
	!by	%00000000	;7
	!by	%00000000	;8
;		 12345678 unten rechts
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%00000000	;3
	!by	%00000000	;4
	!by	%00000000	;5
	!by	%00000000	;6
	!by	%00000000	;7
	!by	%00000000	;8

; Sprite 0C: thrust right(Offset 12)
;		 12345678  oben links
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%00000000	;3
	!by	%00000000	;4
	!by	%00000000	;5
	!by	%00000000	;6
	!by	%00000000	;7
	!by	%00000000	;8
;		 12345678 unten links
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%00000000	;3
	!by	%00000000	;4
	!by	%00000000	;5
	!by	%00000000	;6
	!by	%00000000	;7
	!by	%00000000	;8
;		 12345678 oben rechts
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%00000000	;3
	!by	%00000000	;4
	!by	%00000010	;5
	!by	%00000101	;6
	!by	%00000010	;7
	!by	%00000000	;8
;		 12345678 unten rechts
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%00000000	;3
	!by	%00000000	;4
	!by	%00000000	;5
	!by	%00000000	;6
	!by	%00000000	;7
	!by	%00000000	;8

; Sprite 10: Arrow
;		 12345678  oben links
	!by	%00000001	;1
	!by	%00000011	;2
	!by	%00000101	;3
	!by	%00001001	;4
	!by	%00000001	;5
	!by	%00000001	;6
	!by	%00000001	;7
	!by	%00000001	;8
;		 12345678 unten links
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%00000000	;3
	!by	%00000000	;4
	!by	%00000000	;5
	!by	%00000000	;6
	!by	%00000000	;7
	!by	%00000000	;8
;		 12345678 oben rechts
	!by	%10000000	;1
	!by	%11000000	;2
	!by	%10100000	;3
	!by	%10010000	;4
	!by	%10000000	;5
	!by	%10000000	;6
	!by	%10000000	;7
	!by	%10000000	;8
;		 12345678 unten rechts
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%00000000	;3
	!by	%00000000	;4
	!by	%00000000	;5
	!by	%00000000	;6
	!by	%00000000	;7
	!by	%00000000	;8

; Sprite 14: lander explode 1
;		 12345678  top left
	!by	%00000000	;1
	!by	%00000111	;2
	!by	%00001111	;3
	!by	%00011111	;4
	!by	%00011111	;5
	!by	%00011111	;6
	!by	%00011111	;7
	!by	%00011111	;8
;		 12345678 bottom left
	!by	%00011111	;1
	!by	%00011111	;2
	!by	%00001111	;3
	!by	%00000111	;4
	!by	%00000000	;5
	!by	%00000000	;6
	!by	%00000000	;7
	!by	%00000000	;8
;		 12345678 top right
	!by	%00000000	;1
	!by	%11100000	;2
	!by	%11110000	;3
	!by	%11111000	;4
	!by	%11111000	;5
	!by	%11111000	;6
	!by	%11111000	;7
	!by	%11111000	;8
;		 12345678 bottom right
	!by	%11111000	;1
	!by	%11111000	;2
	!by	%11110000	;3
	!by	%11100000	;4
	!by	%00000000	;5
	!by	%00000000	;6
	!by	%00000000	;7
	!by	%00000000	;8

; Sprite 18: lander explode 2
;		 12345678  top left
	!by	%00001000	;1
	!by	%00010111	;2
	!by	%00101111	;3
	!by	%01011111	;4
	!by	%01011110	;5
	!by	%01011110	;6
	!by	%01011110	;7
	!by	%01011110	;8
;		 12345678 bottom left
	!by	%01011110	;1
	!by	%01011110	;2
	!by	%00101111	;3
	!by	%00010111	;4
	!by	%00001000	;5
	!by	%00000111	;6
	!by	%00000000	;7
	!by	%00000000	;8
;		 12345678 top right
	!by	%00010000	;1
	!by	%11101000	;2
	!by	%11110100	;3
	!by	%11111010	;4
	!by	%01111010	;5
	!by	%01111010	;6
	!by	%01111010	;7
	!by	%01111010	;8
;		 12345678 bottom right
	!by	%01111010	;1
	!by	%01111010	;2
	!by	%11110100	;3
	!by	%11101000	;4
	!by	%00010000	;5
	!by	%11100000	;6
	!by	%00000000	;7
	!by	%00000000	;8
	
; Sprite 1C : lander explode 3
;		 12345678  top left
	!by	%00001001	;1
	!by	%00100111	;2
	!by	%01001111	;3
	!by	%10011110	;4
	!by	%10011100	;5
	!by	%10011100	;6
	!by	%10011100	;7
	!by	%10011100	;8
;		 12345678 bottom left
	!by	%10011100	;1
	!by	%10011100	;2
	!by	%01001110	;3
	!by	%00100111	;4
	!by	%00010000	;5
	!by	%00001000	;6
	!by	%00000111	;7
	!by	%00000000	;8
;		 12345678 top right
	!by	%10010000	;1
	!by	%11100100	;2
	!by	%11110010	;3
	!by	%01111001	;4
	!by	%00111001	;5
	!by	%00111001	;6
	!by	%00111001	;7
	!by	%00111001	;8
;		 12345678 bottom right
	!by	%00111001	;1
	!by	%00111001	;2
	!by	%01110010	;3
	!by	%11100100	;4
	!by	%00001000	;5
	!by	%00010000	;6
	!by	%11100000	;7
	!by	%00000000	;8	
	
; Sprite 20 : lander explode 4
;		 12345678  top left
	!by	%00010011	;1
	!by	%01001111	;2
	!by	%10011110	;3
	!by	%00111100	;4
	!by	%00111000	;5
	!by	%00111000	;6
	!by	%00111000	;7
	!by	%00111000	;8
;		 12345678 bottom left
	!by	%00011000	;1
	!by	%00011100	;2
	!by	%10001110	;3
	!by	%01000111	;4
	!by	%00100001	;5
	!by	%00010000	;6
	!by	%00001000	;7
	!by	%00000110	;8
;		 12345678 top right
	!by	%11001000	;1
	!by	%11110010	;2
	!by	%01111001	;3
	!by	%00111100	;4
	!by	%00011100	;5
	!by	%00011100	;6
	!by	%00011100	;7
	!by	%00011100	;8
;		 12345678 bottom right
	!by	%00011000	;1
	!by	%00111000	;2
	!by	%01110001	;3
	!by	%11100010	;4
	!by	%10000100	;5
	!by	%00001000	;6
	!by	%00010000	;7
	!by	%01100000	;8		

; Sprite 24 : lander explode 5
;		 12345678  top left
	!by	%00100110	;1
	!by	%10011110	;2
	!by	%00111100	;3
	!by	%01111000	;4
	!by	%01110000	;5
	!by	%01110000	;6
	!by	%01110000	;7
	!by	%01110000	;8
;		 12345678 bottom left
	!by	%00110000	;1
	!by	%00111000	;2
	!by	%00011100	;3
	!by	%10001110	;4
	!by	%01000001	;5
	!by	%00100000	;6
	!by	%00010000	;7
	!by	%00001100	;8
;		 12345678 top right
	!by	%01100100	;1
	!by	%01111001	;2
	!by	%00111100	;3
	!by	%00011110	;4
	!by	%00001110	;5
	!by	%00001110	;6
	!by	%00001110	;7
	!by	%00001110	;8
;		 12345678 bottom right
	!by	%00001100	;1
	!by	%00011100	;2
	!by	%00111000	;3
	!by	%01110001	;4
	!by	%10000010	;5
	!by	%00000100	;6
	!by	%00001000	;7
	!by	%00110000	;8		
	
; Sprite 28 : lander explode 6
;		 12345678  top left
	!by	%01001100	;1
	!by	%00111100	;2
	!by	%01111000	;3
	!by	%11110000	;4
	!by	%11100000	;5
	!by	%11100000	;6
	!by	%11100000	;7
	!by	%11100000	;8
;		 12345678 bottom left
	!by	%01100000	;1
	!by	%01110000	;2
	!by	%00111000	;3
	!by	%00011100	;4
	!by	%10000010	;5
	!by	%01000000	;6
	!by	%00100000	;7
	!by	%00011000	;8
;		 12345678 top right
	!by	%00110010	;1
	!by	%00111100	;2
	!by	%00011110	;3
	!by	%00001111	;4
	!by	%00000111	;5
	!by	%00000111	;6
	!by	%00000111	;7
	!by	%00000111	;8
;		 12345678 bottom right
	!by	%00000110	;1
	!by	%00001110	;2
	!by	%00011100	;3
	!by	%00111000	;4
	!by	%01000001	;5
	!by	%00000010	;6
	!by	%00000100	;7
	!by	%00011000	;8	

; Sprite 2c : lander explode 7
;		 12345678  top left
	!by	%10011000	;1
	!by	%01111000	;2
	!by	%11100000	;3
	!by	%11000000	;4
	!by	%11000000	;5
	!by	%11000000	;6
	!by	%11000000	;7
	!by	%11000000	;8
;		 12345678 bottom left
	!by	%11000000	;1
	!by	%11100000	;2
	!by	%01110000	;3
	!by	%00110000	;4
	!by	%00010000	;5
	!by	%10000100	;6
	!by	%01000000	;7
	!by	%00110000	;8
;		 12345678 top right
	!by	%00011001	;1
	!by	%00011110	;2
	!by	%00000111	;3
	!by	%00000011	;4
	!by	%00000011	;5
	!by	%00000011	;6
	!by	%00000011	;7
	!by	%00000011	;8
;		 12345678 bottom right
	!by	%00000011	;1
	!by	%00000111	;2
	!by	%00001110	;3
	!by	%00011100	;4
	!by	%00010000	;5
	!by	%00100001	;6
	!by	%00000010	;7
	!by	%00001100	;8	
	
; Sprite 30 : lander explode 8
;		 12345678  top left
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%00100001	;3
	!by	%00010001	;4
	!by	%00001001	;5
	!by	%00000000	;6
	!by	%00000000	;7
	!by	%00111001	;8
;		 12345678 bottom left
	!by	%00000001	;1
	!by	%00000000	;2
	!by	%00000000	;3
	!by	%00001001	;4
	!by	%00010001	;5
	!by	%00100001	;6
	!by	%00000000	;7
	!by	%00000000	;8
;		 12345678 top right
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%00000100	;3
	!by	%00001000	;4
	!by	%00010000	;5
	!by	%00000000	;6
	!by	%00000000	;7
	!by	%10011100	;8
;		 12345678 bottom right
	!by	%10000000	;1
	!by	%00000000	;2
	!by	%00000000	;3
	!by	%00100000	;4
	!by	%00010000	;5
	!by	%00001000	;6
	!by	%00000000	;7
	!by	%00000000	;8	
	

; Intro music 
; duration	note
; 60ms		D#4
; 60ms		A#4
; 60ms		pause
; 60ms		A#4
;900ms		D#5
; 60ms		F#5
; 60ms		D#5 
; 60ms		F#5
;970ms		A#5
;125ms		pause
; Play 2 times
;
; 127mS		pause
; 65ms		D#6
; 380mS		pause
; 65ms		D#6
; 320mS		pause
; 65ms		D#6
; 255mS		pause
; 65ms		D#6
; 191mS		pause
; 65ms		D#6
; 127mS		pause
; 65ms		D#6
; 66mS		pause
; 65ms		D#6
; 66mS		pause
; 65ms		D#6
; 66mS		pause
; 65ms		D#6
; 66mS		pause
; 65ms		D#6
; 66mS		pause
; 65ms		D#6
; 66mS		pause
; 65ms		D#6
;
; YMF262 implementation
; Timer 2 resolution: 0.32ms max. 255*0.32 = 81,6ms
; If timer 2 is enabled, the value in this 
; register will be incremented until it overflows.  Upon
; overflow, the sound card will signal a TIMER interrupt
; and set bits 7 and 5 in its status byte.  The
; value for this timer is incremented every three hundred
; twenty (320) microseconds.
; 
; 	In octave 4 (block), the F-number values for the chromatic scale and their corresponding frequencies would be:
; from YM3812 application manual
;F Number	Frequency
;(hex)	(dec)	(decimal)	Note
;16B	363		277.2		C#4
;181	385		293.7		D4
;198	408		311.1		D#4
;1B0	432		329.6		E4
;1CA	458		349.2		F4
;1E5	485		370.0		F#4
;202	514		392.0		G4
;220	544		415.3		G#4
;241	577		440.0		A4
;263	611		466.2		A#4
;287	647		493.9		B4
;2AE	686		523.3		C5	
;2D7	727		554.4		C#5
;302	770		587.3		D5
;330	816		622.2		D#5
;360	864		659.3		E5
;394	916		698.5		F5
;3CA	970 	740.0		F#5
; block number 5
;202	785		784.0		G5
;220	554		830.6		G#5
;241	577		880.0		A5
;263	611		932,3		A#5
;287	647		987,8		B5
;2AE	686		1046.5		C6
;2D7	727		1108.7		C#6
;302	770		1174.7		D6
;330	816		1244.5		D#6
;360 	864		1318.5		E6
;395 	916		1396.9		F6
;3CA	970 	1480		F#6
;block number 6
;202	785		1568		G6
;220	554		1661.2		G#6
;241	577		1760		A6
;263	611		1864,7		A#6
;287	647	 	1975,5		B6 
;1975.53 	B6
;2093.00	C7	



;
; Music data: 
; 00 = end 
; $5E			; YMF262 port 0, write value dd to register aa 
; $61			; Wait 62ms

Intro_MusicData					; Music data from VIC-20 jupiter lander
!by  $5E,$b1,$20+$10+$01		; Note D#4 (hex 198)
!by  $5E,$a1,$98				;
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$20+$10+$02 		; Note A#4 (hex 263)
!by  $5E,$a1,$63				; 
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$00				; Channel 1 off
!by  $61,$01					; 62ms pause
!by  $5E,$b1,$20+$10+$02 		; Note A#4 (hex 263)
!by  $5E,$a1,$63				; 
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$20+$10+$03 		; Note D#5 (hex 330)
!by  $5E,$a1,$30
!by  $61,$10					; 620 ms (10x62ms)
!by  $5E,$b1,$20+$10+$03 		; Note F#5 (hex 3CA)
!by  $5E,$a1,$CA
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$20+$10+$03 		; Note D#5 (hex 330)
!by  $5E,$a1,$30
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$20+$10+$03 		; Note F#5 (hex 3CA)
!by  $5E,$a1,$CA
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$20+$14+$02 		; Note A#5 (hex 263) block 5
!by  $5E,$a1,$63
!by  $61,$12					; 744 ms (12x62ms)
!by  $5E,$b1,$00				; Channel 1 off
!by  $61,$02					; Pause 2x62ms
!by  $5E,$b1,$20+$10+$01		; Note D#4 (hex 198)
!by  $5E,$a1,$98				;
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$20+$10+$02 		; Note A#4 (hex 263)
!by  $5E,$a1,$63				; 
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$00				; Channel 1 off
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$20+$10+$02 		; Note A#4 (hex 263)
!by  $5E,$a1,$63				; 
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$20+$10+$03 		; Note D#5 (hex 330)
!by  $5E,$a1,$30
!by  $61,$10					; 620 ms (10x62ms)
!by  $5E,$b1,$20+$10+$03 		; Note F#5 (hex 3CA)
!by  $5E,$a1,$CA
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$20+$10+$03 		; Note D#5 (hex 330)
!by  $5E,$a1,$30
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$20+$10+$03 		; Note F#5 (hex 3CA)
!by  $5E,$a1,$CA
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$20+$14+$02 		; Note A#5 (hex 263) block 5
!by  $5E,$a1,$63
!by  $61,$12					; 744 ms (12x62ms)
!by  $5E,$b1,$00				; Channel 1 off
!by  $61,$02					; Pause 2x62ms

!by  $5E,$b1,$20+$14+$02		; Note D#6 (hex 2D7) block 5
!by  $5E,$a1,$D7
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$00				; Channel 1 off
!by  $61,$06					; 380ms pause (6x62ms)
!by  $5E,$b1,$20+$14+$02		; Note D#6 (hex 2D7) block 5
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$00				; Channel 1 off
!by  $61,$05					; 320ms pause (5x62ms)
!by  $5E,$b1,$20+$14+$02		; Note D#6 (hex 2D7) block 5
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$00				; Channel 1 off
!by  $61,$04					; 260ms pause (4x62ms)
!by  $5E,$b1,$20+$14+$02		; Note D#6 (hex 2D7) block 5
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$00				; Channel 1 off
!by  $61,$03					; 195ms pause (3x62ms)
!by  $5E,$b1,$20+$14+$02		; Note D#6 (hex 2D7) block 5
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$00				; Channel 1 off
!by  $61,$02					; 130ms pause (2x62ms)

!by  $5E,$b1,$20+$14+$02		; Note D#6 (hex 2D7) block 5
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$00				; Channel 1 off
!by  $61,$01					; 62ms pause (1x62ms)

!by  $5E,$b1,$20+$14+$02		; Note D#6 (hex 2D7) block 5
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$00				; Channel 1 off
!by  $61,$01					; 62ms pause (1x62ms)

!by  $5E,$b1,$20+$14+$02		; Note D#6 (hex 2D7) block 5
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$00				; Channel 1 off
!by  $61,$01					; 62ms pause (1x62ms)

!by  $5E,$b1,$20+$14+$02		; Note D#6 (hex 2D7) block 5
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$00				; Channel 1 off
!by  $61,$01					; 62ms pause (1x62ms)

!by  $5E,$b1,$20+$14+$02		; Note D#6 (hex 2D7) block 5
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$00				; Channel 1 off
!by  $61,$01					; 62ms pause (1x62ms)

!by  $5E,$b1,$20+$14+$02		; Note D#6 (hex 2D7) block 5
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$00				; Channel 1 off
!by  $61,$01					; 62ms pause (1x62ms)
!by  $5E,$b1,$20+$14+$02		; Note D#6 (hex 2D7) block 5
!by  $61,$01					; Wait 62ms
!by  $5E,$b1,$00				; Channel 1 off
!by  $00						; End
; Music succes
; Duration Frequency
; 126ms		D#5	ok
; 125ms		pause ok 
; 216ms		D#5 ok
; 125ms		F#5 ok 
; 125ms		A#5 OK
; 125ms		F#5 OK
; 125ms		A#5 OK
; 125ms		F#5 OK
; 125ms		F5 OK
; 125ms		pause OK (pos. 1.278 sec)
; 125ms		F5 OK 
; 125ms		G#5 ok
; 500ms		C6 OK
; 125ms		D6 OK
; 125ms		A#5 OK
; 125ms		F#5 OK
; 125ms		D6 OK
; 125ms		A#5 OK
; 125ms		F#5	(pos.: 2.86sec) OK
; 125ms		D6 OK 
; 125ms		A#5 OK 
; 125ms		D#6 OK
; 125ms		pause OK 
; 125ms		D6 OK
; 560ms		D#6 
Success_MusicData
!by  $5E,$b1,$20+$10+$03 		; Note D#5 (hex 330)
!by  $5E,$a1,$30
!by  $61,$02				; Wait 124ms
!by  $5E,$b1,$00			; Channel 1 off
!by  $61,$02				; Pause 2x62ms
!by  $5E,$b1,$20+$10+$03 		; Note D#5 (hex 330)
!by  $5E,$a1,$30
!by  $61,$02				; Wait 124ms
!by  $5E,$b1,$20+$10+$03 		; Note F#5 (hex 3CA)
!by  $5E,$a1,$CA
!by  $61,$02				; Wait 124ms
!by  $5E,$b1,$20+$14+$02 		; A#5 (hex 263)	block 5	
!by  $5E,$a1,$63
!by  $61,$02				; Wait 124ms
!by  $5E,$b1,$20+$10+$03 		; Note F#5 (hex 3CA)
!by  $5E,$a1,$CA
!by  $61,$02				; Wait 124ms
!by  $5E,$b1,$20+$14+$02 		; A#5 (hex 263)	block 5	
!by  $5E,$a1,$63
!by  $61,$02				; Wait 124ms
!by  $5E,$b1,$20+$10+$03 		; Note F#5 (hex 3CA)
!by  $5E,$a1,$CA
!by  $61,$02				; Wait 124ms
!by  $5E,$b1,$20+$10+$03 		; Note F5 (hex 394)
!by  $5E,$a1,$94
!by  $61,$02				; Wait 124ms
!by  $5E,$b1,$00			; Channel 1 off
!by  $61,$02				; Pause 2x62ms
!by  $5E,$b1,$20+$10+$03 		; Note F5 (hex 394)
!by  $5E,$a1,$94
!by  $61,$02				; Wait 124ms
!by  $5E,$b1,$20+$14+$02 		; Note G#5 (hex 220) block 5
!by  $5E,$a1,$20
!by  $61,$02				; Wait 124ms
!by  $5E,$b1,$20+$14+$02 		; Note C6 (hex 287) block 5
!by  $5E,$a1,$AE
!by  $61,$08				; Wait 500ms
!by  $5E,$b1,$20+$14+$03 		; Note D6 (hex 302) block 5
!by  $5E,$a1,$02
!by  $61,$02				; Wait 124ms
!by  $5E,$b1,$20+$14+$02 		; A#5 (hex 263)	block 5	
!by  $5E,$a1,$63
!by  $61,$02				; Wait 124ms
!by  $5E,$b1,$20+$10+$03 		; Note F#5 (hex 3CA)
!by  $5E,$a1,$CA
!by  $61,$02				; Wait 124ms
!by  $5E,$b1,$20+$14+$03 		; Note D6 (hex 302) block 5
!by  $5E,$a1,$02
!by  $61,$02				; Wait 124ms
!by  $5E,$b1,$20+$14+$02 		; Note A#5 (hex 263) block 5	
!by  $5E,$a1,$63
!by  $61,$02				; Wait 124ms
!by  $5E,$b1,$20+$10+$03 		; Note F#5 (hex 3CA)
!by  $5E,$a1,$CA
!by  $61,$02				; Wait 124ms
!by  $5E,$b1,$20+$14+$03 		; Note D6 (hex 302) block 5
!by  $5E,$a1,$02
!by  $61,$02				; Wait 124ms
!by  $5E,$b1,$20+$14+$02 		; A#5 (hex 263)	block 5	
!by  $5E,$a1,$63
!by  $61,$02				; Wait 124ms
!by  $5E,$b1,$20+$14+$03 		; Note D#6 (hex 330) block 5	
!by  $5E,$a1,$30
!by  $61,$02				; Wait 124ms
!by  $5E,$b1,$00			; Channel 1 off
!by  $61,$02				; Pause 2x62ms
!by  $5E,$b1,$20+$14+$03 		; Note D6 (hex 302) block 5
!by  $5E,$a1,$02
!by  $61,$02				; Wait 124ms
!by  $5E,$b1,$20+$14+$03 		; Note D#6 (hex 330) block 5	
!by  $5E,$a1,$30
!by  $61,$09				; Wait 585ms
!by  $5E,$b1,$00			; Channel 1 off
!by  $00


; Music fail
; Duration Frequency
; 62ms		A#5 OK
; 62ms		1. pause (pos. 0.146ms) OK
; 62ms		D#6 OK
; 62ms		A#5	OK
; 62ms 		F#5 OK
; 62ms		2. pause (pos. 0,438)	OK
; 62ms		A#5	OK
; 62ms		F#5	OK
; 62ms		D#5 OK
; 62ms		3. pause (pos. 0.730) OK
; 62ms		F#5 OK
; 62ms		D#5 OK
; 62ms		A#4	OK
; 62ms		4. pause (pos. 1.094)
; 62ms		A4	OK
; 62ms		A#4	OK
; 62ms		B4	OK
; 62ms		C5  OK
; 62ms		B4	OK
; 62ms		C5  OK
; 62ms		C#5 OK		
; 62ms		D#5 OK		
; 145ms		5. pause (pos. 1,971) OK
; 162ms 	A4  OK
; 162ms		B4	OK
; 162ms		A#4 OK
; 219ms		6. pause (pos. 2,555) OK
; 62ms		D5 OK
; 220ms		7. pause (pos. 2.848)
; 62ms		D#5

Fail_MusicData
!by  $5E,$b1,$20+$14+$02 		; Note A#5 (hex 263) block 5	
!by  $5E,$a1,$63
!by  $61,$02				; Wait 2x62ms
!by  $5E,$b1,$00			; Channel 1 off
!by  $61,$02				; 62ms 1. pause ********
!by  $5E,$b1,$20+$14+$03		; Note D#6 (hex 330) block 5
!by  $5E,$a1,$30
!by  $61,$02				; Wait 2x62ms
!by  $5E,$b1,$20+$14+$02		; Note A#5 (hex 263) block 5
!by  $5E,$a1,$63
!by  $61,$02				; Wait 2x62ms
!by  $5E,$b1,$20+$10+$03		; Note F#5 (hex 3CA) 
!by  $5E,$a1,$CA
!by  $61,$02				; Wait 2x62ms
!by  $5E,$b1,$00			; Channel 1 off
!by  $61,$02				; 62ms 2. pause ********
!by  $5E,$b1,$20+$14+$02 		; Note A#5 (hex 263) block 5	
!by  $5E,$a1,$63
!by  $61,$02				; Wait 2x62ms
!by  $5E,$b1,$20+$10+$03 		; Note F#5 (hex 3CA)
!by  $5E,$a1,$CA
!by  $61,$02				; Wait 2x62ms
!by  $5E,$b1,$20+$10+$03 		; Note D#5 (hex 330)
!by  $5E,$a1,$30
!by  $61,$02				; Wait 2x62ms
!by  $5E,$b1,$00			; Channel 1 off
!by  $61,$02				; 62ms 3. pause ********
!by  $5E,$b1,$20+$10+$03 		; Note F#5 (hex 3CA)
!by  $5E,$a1,$CA
!by  $61,$02				; Wait 2x62ms
!by  $5E,$b1,$20+$10+$03 		; Note D#5 (hex 330)
!by  $5E,$a1,$30
!by  $61,$02				; Wait 2x62ms
!by  $5E,$b1,$20+$10+$02 		; Note A#4 (hex 263)
!by  $5E,$a1,$63			; 
!by  $61,$02				; Wait 2x62ms
!by  $5E,$b1,$00			; Channel 1 off
!by  $61,$04				; 62ms 4. pause ********
!by  $5E,$b1,$20+$10+$02 		; Note A4 (hex 241)
!by  $5E,$a1,$41			; 
!by  $61,$02				; Wait 2x62ms
!by  $5E,$b1,$20+$10+$02 		; Note A#4 (hex 263)
!by  $5E,$a1,$63			; 
!by  $61,$02				; Wait 2x62ms
!by  $5E,$b1,$20+$10+$02 		; Note B4 (hex 287)
!by  $5E,$a1,$87			;
!by  $61,$02				; Wait 2x62ms
!by  $5E,$b1,$20+$10+$02 		; Note C5 (hex 2AE)
!by  $5E,$a1,$AE			;
!by  $61,$02				; Wait 2x62ms
!by  $5E,$b1,$20+$10+$02 		; Note B4 (hex 287)
!by  $5E,$a1,$87			;
!by  $61,$02				; Wait 2x62ms
!by  $5E,$b1,$20+$10+$02 		; Note C5 (hex 2AE)
!by  $5E,$a1,$AE			;
!by  $61,$02				; Wait 2x62ms
!by  $5E,$b1,$20+$10+$02 		; Note C#5 (hex 2D7)
!by  $5E,$a1,$D7			;
!by  $61,$02				; Wait 2x62ms
!by  $5E,$b1,$20+$10+$03 		; Note D#5 (hex 330)
!by  $5E,$a1,$30
!by  $61,$02				; Wait 2x62ms
!by  $5E,$b1,$00			; Channel 1 off
!by  $61,$02				; 145ms 5. pause ********

!by  $5E,$b1,$20+$10+$02 		; Note A4 (hex 241)
!by  $5E,$a1,$41			; 
!by  $61,$04				; Wait 162ms
!by  $5E,$b1,$20+$10+$02 		; Note B4 (hex 287)
!by  $5E,$a1,$87			;
!by  $61,$04				; Wait 162ms
!by  $5E,$b1,$20+$10+$02 		; Note A#4 (hex 263)
!by  $5E,$a1,$63			; 
!by  $61,$04				; Wait 162ms
!by  $5E,$b1,$00			; Channel 1 off
!by  $61,$04				; 219ms 6. pause ********

!by  $5E,$b1,$20+$10+$03 		; Note D5 (hex 302)
!by  $5E,$a1,$02			; 
!by  $61,$02				; Wait 2x62ms
!by  $5E,$b1,$00			; Channel 1 off
!by  $61,$02				; 219ms 7. pause ********
!by  $5E,$b1,$20+$10+$03 		; Note D#5 (hex 330)
!by  $5E,$a1,$30
!by  $61,$02				; Wait 2x62ms
!by  $5E,$b1,$00			; Channel 1 off

!by	 $00

!src "Moon_Lander_Screen.asm"
!src "Moon_Lander_Tiles.asm"


