; (C) 2021 Matt Heffernan 
; https://github.com/SlithyMatt/multi-mandlebrot
; Modification Oak65C02 Uwe Gottschling 2021

FP_A = $50	; 32bit
FP_B = $54	; Do not change order !
FP_C = $58
FP_R = $5c	


	!al
	!rl

!macro FP_LDA_WORD word_int{
	stz FP_A
	lda word_int
	sta FP_A+2	
}

!macro FP_LDB_WORD word_int{
	stz FP_B
	lda word_int
	sta FP_B+2
}

!macro FP_LDA addr{
	lda addr
	sta FP_A
	lda addr+2
	sta FP_A+2
}

!macro FP_LDB addr{
	lda addr
	sta FP_B
	lda addr+2
	sta FP_B+2
}   

!macro FP_LDA_IMM val{
	lda #val and $0000ffff
	sta FP_A
	lda #val >> 16
	sta FP_A+2
}

!macro FP_LDB_IMM val{
	lda #val and $0000ffff
	sta FP_B
	lda #val >> 16
	sta FP_B+2
}

!macro FP_LDA_IMM_INT val{
	stz FP_A
	lda #val
	sta FP_A+2
}

!macro FP_LDB_IMM_INT val{
	stz FP_B
	lda #val
	sta FP_B+2
}
	
!macro FP_STC addr{
	lda FP_C
	sta addr
	lda FP_C+2
	sta addr+2
}

!macro FP_TCA{
	lda FP_C
	sta FP_A
	lda FP_C+2
	sta FP_A+2
}

!macro FP_TCB{
	lda FP_C
	sta FP_B
	lda FP_C+2
	sta FP_B+2
}

!macro fp_subtract{ ; FP_C = FP_A - FP_B
	lda FP_A
	sec
	sbc FP_B
	sta FP_C
	lda FP_A+2
	sbc FP_B+2
	sta FP_C+2
}

!macro fp_add{ ; FP_C = FP_A + FP_B
	lda FP_A
	clc
	adc FP_B
	sta FP_C
	lda FP_A+2
	adc FP_B+2
	sta FP_C+2
}

fp_divide ; FP_C = FP_A / FP_B; FP_R = FP_A % FP_B
	lda FP_A
	bne fp_divide2
	lda FP_A+2
	bne fp_divide2
	jmp FP_RETURN0
fp_divide2
	phx
	phy
	lda FP_B+2
	pha		; preserve original B on stack
	bit FP_A+2
	bmi @abs_a
	lda FP_A
	sta FP_C
	lda FP_A+2
	sta FP_C+2
	bra @check_sign_b
@abs_a
	lda #0
	sec
	sbc FP_A
	sta FP_C
	lda #0
	sbc FP_A+2
	sta FP_C+2	; C = |A|
@check_sign_b
	bit FP_B+2
	bpl @shift_b
	lda #0
	sec
	sbc FP_B
	sta FP_B
	lda #0
	sbc FP_B+2
	sta FP_B+2
@shift_b
	lda FP_B+2
	sta FP_B
	stz FP_B+2
	stz FP_R
	stz FP_R+2
	ldx #24		;Reduced from 32 bit to 24 bit for more speed
@loop1
	asl FP_C	;Shift hi bit of C into REM
	rol FP_C+2	;(vacating the lo bit, which will be used for the quotient)
	rol FP_R
	rol FP_R+2
	lda FP_R
	sec		;Trial subtraction
	sbc FP_B
	tay
	lda FP_R+2
	sbc FP_B+2
	bcc @loop2	;Did subtraction succeed?
	sta FP_R+2	;If yes, save it
	sty FP_R
	inc FP_C	;and record a 1 in the quotient
@loop2
	dex
	bne @loop1
	
	lda FP_C+2	; shift for 32/24 bit correction
	sta FP_C+3
	lda FP_C
	sta FP_C+1
	lda FP_C
	and #$FF00
	sta FP_C
		
	pla
	sta FP_B+2
	bit FP_B+2
	bmi @check_cancel
	bit FP_A+2
	bmi @negative
	jmp @return
@check_cancel
	bit FP_A+2
	bmi @return
@negative
	lda #0
	sec
	sbc FP_C
	sta FP_C
	lda #0
	sbc FP_C+2
	sta FP_C+2
@return
	ply
	plx
	rts

FP_RETURN0
	stz FP_C
	stz FP_C+2
	rts	

fp_multiply	; FP_C = FP_A * FP_B; FP_R overflow
	lda FP_A+2
	bne fp_multiply2
	lda FP_A
	bne fp_multiply2	
	bra FP_RETURN0		; skip if factor is zero
fp_multiply2	
	lda FP_B+2
	bne fp_multiply3
	lda FP_B
	bne fp_multiply3	
	bra FP_RETURN0		; skip if factor is zero
fp_multiply3
	phx
	phy
	; push original A and B to stack
	lda FP_A+2
	pha
	lda FP_B+2
	pha
	bit FP_A+2
	bpl @check_sign_b
	lda #0
	sec
	sbc FP_A
	sta FP_A
	lda #0
	sbc FP_A+2
	sta FP_A+2	; A = |A|
@check_sign_b
	bit FP_B+2
	bpl @init_c
	lda #0
	sec
	sbc FP_B
	sta FP_B
	lda #0
	sbc FP_B+2
	sta FP_B+2	; B = |B|
@init_c
	stz FP_R
	stz FP_R+2
	stz FP_C
	stz FP_C+2

	lda FP_B+1	; reduce from 32 bit to 24 bit accuracy
	sta FP_B	; shift to one byte left
	lda FP_B+3
;	and #$00FF
	sta FP_B+2
	
	lda FP_A+1
	sta FP_A
	lda FP_A+3
	and #$00FF
	sta FP_A+2	
	
	ldx #24		; reduced from 32 to 24 bit for more speed
@loop1
	lsr FP_B+2
	ror FP_B
	bcc @loop2
	clc
	lda FP_R		
	adc FP_A		
	sta FP_R
	lda FP_R+2
	adc FP_A+2
	sta FP_R+2
@loop2
	ror FP_R+2
	ror FP_R
	ror FP_C+2
;	ror FP_C
	dex
	bne @loop1
@loop3
	lda FP_C+1	; shift for fixed point correction	
	sta FP_C		
	lda FP_C+3
	sta FP_C+2
	lda FP_R+1
	sta FP_R
	; restore A and B for sign check 
	pla
	sta FP_B+2
	pla
	sta FP_A+2
	bit FP_B+2
	bmi @check_cancel
	bit FP_A+2
	bmi @negative
	jmp @return
@check_cancel
	bit FP_A+2
	bmi @return
@negative
	lda #0
	sec
	sbc FP_C
	sta FP_C
	lda #0
	sbc FP_C+2
	sta FP_C+2
@return
	ply
	plx
	rts


