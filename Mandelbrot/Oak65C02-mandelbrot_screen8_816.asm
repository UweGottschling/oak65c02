; (C) 2021 Matt Heffernan 
; https://github.com/SlithyMatt/multi-mandlebrot
; Modification Oak65C02 Uwe Gottschling 2021
; TMS9918 screen mode 3 

NameTableBase		= $800 ; 32*24=768 bytes --> $800 to $AFF
PatternBase		= $000 ; 32*48=1536 bytes --> $000 to $600
SpriteAttrBase		= $1B00	; max. 128 bytes
Vaddress		= $58	; word
Vread			= $5A	; byte
Vwrite			= $5B	; byte
MAND_MAX_IT 		= 255
!cpu 65816
*=$0FFE
!word $1000 	;--> Start address of this program

   jmp start

MAND_WIDTH 	= 256
MAND_HEIGHT 	= 212

!src "mandelbrot32_816.asm"  
!src "../../Kernel/include/label_definition.asm"
!src "../../Kernel/include/Jumptable.asm"



loopx	=$90	;!word 0
loopy	=$92	;!word 0
i_result=$94 	;!byte 0 

start
	jsr StartMessage
	jsr SetsGraphicMode
	clc
!cpu 65816
	xce			; set native mode
	rep #$30		; set accu and index to 16-bit
	!al
	!rl
	ldx #0
	ldy #0
@loop
	stz mand_x
	stx mand_x+2
	stz mand_y
	sty mand_y+2
	jsr mand_get
	jsr PLOT
	inx
	cpx #MAND_WIDTH
	bne @loop
	ldx #0
	iny
	cpy #MAND_HEIGHT
	bne @loop
	sec
	xce			; set native mode
	sep #$30		; set accu and index to 8-bit
	!as
	!rs
	jsr WaitKey
	jsr Restore_Video
	rts
   
PLOT
	sep #$30		; accu and index 8 bit	
	!as
	!rs	
	pha			
;	jsr V9938_Wait_Command

	lda #0
	sta TMS9918REG
	lda #128+45		; set to register R#45: Destination
	sta TMS9918REG	

	txa
	sta TMS9918REG
	lda #128+36		; set to register R#36: X Register
	sta TMS9918REG	

	stz TMS9918REG
	lda #128+37		; set to register R#37: X Register
	sta TMS9918REG	

	tya
	sta TMS9918REG
	lda #128+38		; set to register R#38: Y Register
	sta TMS9918REG	

	stz TMS9918REG
	lda #128+39		; set to register R#39: Y Register
	sta TMS9918REG	

	pla
	sta TMS9918REG	
	lda #128+44		; set to register R#44: color registser
	sta TMS9918REG	

	lda #$50
	sta TMS9918REG
	lda #128+46		; set to register R#46: PSET command
	sta TMS9918REG	
	rep #$30		; set accu and index to 16-bit
	rts

CLEAR_SCREEN
	ldx #VDP_Clear_Text_Screen_FN
	jsr VDP_Function_Call
	rts	
	
SetsGraphicMode
	lda #V9938_Set_Screen8	;256 x 212 pixels 16 colors out of 512 
	ldx #VDP_Set_Video_Mode_FN	
	jsr VDP_Function_Call
	rts	

StartMessage
	lda #<StartText; < low byte of expression
	sta PTR_String_L
	lda #>StartText	; > high byte of expression
	sta PTR_String_H
StartMessage2	
	ldx #OS_Print_String_FN		
	jsr OS_Function_Call		
WaitKey	
	ldx #PS2_GetKey_FN
	jsr PS2_Function_Call
	rts	
EndMessage
	lda #<PressKey; < low byte of expression
	sta PTR_String_L
	lda #>PressKey	; > high byte of expression
	sta PTR_String_H
	jmp StartMessage2

Restore_Video
 	lda #TMS9918_Set_Screen0
	ldx #VDP_Set_Video_Mode_FN
	jsr VDP_Function_Call	
	ldx #VDP_Init_Pattern_FN
	jsr VDP_Function_Call	
	rts	

StartText
!text LF,"Multi-mandelbrot from Matt Heffernan",LF,"65C816 screen 8",LF,"32-bit fixed-point arithmetic",LF
PressKey
!text "Press any key",LF,00

	sep #$30		; set accu and index to 8-bit
	!as
	!rs
DebugOutput	
		php
		phx
		phy
		pha
		sep #$30		; set accu and index to 8-bit
		lda FP_C+3
		jsr Bin2Hex	;convert the packed BCD number in A to two ASCII characters in A and X
		jsr ACIA_CHAR_Transmit
		txa
		jsr ACIA_CHAR_Transmit		
		lda FP_C+2
		jsr Bin2Hex	;convert the packed BCD number in A to two ASCII characters in A and X
		jsr ACIA_CHAR_Transmit
		txa
		jsr ACIA_CHAR_Transmit
		lda #"."
		jsr ACIA_CHAR_Transmit	
		lda FP_C+1
		jsr Bin2Hex	;convert the packed BCD number in A to two ASCII characters in A and X
		jsr ACIA_CHAR_Transmit
		txa
		jsr ACIA_CHAR_Transmit
		lda FP_C
		jsr Bin2Hex	;convert the packed BCD number in A to two ASCII characters in A and X
		jsr ACIA_CHAR_Transmit
		txa
		jsr ACIA_CHAR_Transmit	
		lda #CR
		jsr ACIA_CHAR_Transmit	
		lda #LF
		jsr ACIA_CHAR_Transmit		
		rep #$30		; set accu and index to 16-bit		
		pla
		ply	
		plx
		plp
		rts
		
Bin2Hex
		pha                     ; save A; , output a,x
		and #$0F
		jsr BintoHex2
		tax			; X = high nibble
		pla			; from here on, the bytes for the lower byte half
		lsr                    	; push 4 times to the right
		lsr
		lsr
		lsr
		jsr BintoHex2		; convert to ASCIIn
		rts			; A = low nibble
BintoHex2
		cmp #10			; Check whether in the number range
		bpl BintoHex3
		ora #$30		; Convert to ASCII space
		rts
BintoHex3
		adc #'A'-10-1		; Convert to ASCII letter range
		rts
		
ACIA_CHAR_Transmit
		pha
ACIA_CHAR_Wait_Transmit
		lda UARTSTATUS		; wait for send buffer empty
		and #$10
		beq ACIA_CHAR_Wait_Transmit
		pla
		sta UARTDATA
		rts				

