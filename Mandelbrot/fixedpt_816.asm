; (C) 2021 Matt Heffernan 
; https://github.com/SlithyMatt/multi-mandlebrot
; Modification Oak65C02 Uwe Gottschling


FP_A = $50		; words 
FP_B = $52
FP_C = $54
FP_R = $56

	!al			; set accu to 16 bit for compiler

!macro fp_lda_byte{		; FP_A = A
	xba			; exchange A to high byte
	sta FP_A
}

!macro fp_ldb_byte{		; FP_B = A
	xba			; exchange A to high byte
	sta FP_B
}

!macro FP_LDA .addr{
	lda .addr
	sta FP_A
}

!macro FP_LDB .addr{
	lda .addr
	sta FP_B
}

!macro FP_LDA_IMM .val{
	lda #.val
	sta FP_A
}

!macro FP_LDB_IMM .val{
	lda #.val
	sta FP_B
}

!macro FP_LDA_IMM_INT .val{
	lda #.val
	xba
	sta FP_A
}

!macro FP_LDB_IMM_INT .val{
	lda #.val
	xba
	sta FP_B
}

!macro FP_STC .addr{
	lda FP_C
	sta .addr
}

!macro FP_TCA{
	lda FP_C
	sta FP_A
}

!macro FP_TCB{
	lda FP_C
	sta FP_B
}

!macro fp_subtract{		; FP_C = FP_A - FP_B
	lda FP_A
	sec
	sbc FP_B
	sta FP_C
}

!macro fp_add{			; FP_C = FP_A + FP_B
	lda FP_A
	clc
	adc FP_B
	sta FP_C
}


fp_divide		; FP_C = FP_A / FP_B; FP_R = FP_A % FP_B
	lda FP_A
	beq FP_RETURN0
	phx
	lda FP_B
	pha		; preserve original B to stack
	bit FP_A
	bmi @abs_a
	lda FP_A
	sta FP_C
	bra @check_sign_b
@abs_a
	lda #0
	sec
	sbc FP_A
	sta FP_C	; C = |A|
@check_sign_b
	bit FP_B
	bpl @shift_b
	lda #0
	sec
	sbc FP_B
	sta FP_B
@shift_b
	ldx FP_B+1
	stx FP_B
	ldx #0
	stx FP_B+1	
	stz FP_R
	ldx #16		;There are 16 bits in C
@loop1
	asl FP_C	;Shift hi bit of C into REM
	rol FP_R	;(vacating the lo bit, which will be used for the quotient)
	lda FP_R
	sec		;Trial subtraction
	sbc FP_B
	bcc @loop2	;Did subtraction succeed?
	sta FP_R	;If yes, save it
	inc FP_C	;and record a 1 in the quotient
@loop2
	dex
	bne @loop1
	pla
	sta FP_B
	bit FP_B
	bmi @check_cancel
	bit FP_A
	bmi @negative
	bra @return
@check_cancel
	bit FP_A
	bmi @return
@negative
	lda #0
	sec
	sbc FP_C
	sta FP_C
@return
	plx
	rts


FP_RETURN0
	stz FP_C
	rts
fp_multiply		; FP_C = FP_A * FP_B; FP_R overflow
	lda FP_A
	beq FP_RETURN0	; check for zero in factor and skip routine
	lda FP_B
	beq FP_RETURN0
	phx
	phy
	lda FP_A
	pha
	lda FP_B
	pha
	bit FP_A
	bpl @check_sign_b
	lda #0
	sec
	sbc FP_A
	sta FP_A	; A = |A|
@check_sign_b
	bit FP_B
	bpl @init_c
	lda #0
	sec
	sbc FP_B
	sta FP_B	; B = |B|
@init_c
	lda #0
	sta FP_R
	sta FP_C
	ldx #16
@loop1
	lsr FP_B
	bcc @loop2
	clc
	adc FP_A
@loop2
	ror 
	ror FP_C
	dex
	bne @loop1
	sta FP_R
	ldx FP_C+1	; "shift" FP_R and FP_C 8 bit (1 byte) right
	stx FP_C
	ldx FP_R
	stx FP_C+1
	pla		; restore FP_A and FP_B
	sta FP_B
	pla
	sta FP_A
	bit FP_B
	bmi @check_cancel
	bit FP_A
	bmi @negative
	bra @return
@check_cancel
	bit FP_A
	bmi @return
@negative
	lda #0
	sec
	sbc FP_C
	sta FP_C
@return
	ply
	plx
	rts

		


