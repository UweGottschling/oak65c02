; (C) 2021 Matt Heffernan 
; https://github.com/SlithyMatt/multi-mandlebrot
; Modification Oak65C02 Uwe Gottschling

;.include "fixedpt.asm"
!src "fixedpt_816.asm"

!ifndef MAND_XMIN{
MAND_XMIN = $FD80 ; -2.5
}

!ifndef MAND_XMAX{
MAND_XMAX = $0380 ; 3.5
}

!ifndef MAND_YMIN{
MAND_YMIN = $FF00 ; -1
}

!ifndef MAND_YMAX{
MAND_YMAX = $0200 ; 2
}

!ifndef MAND_WIDTH{
MAND_WIDTH = 32
}

!ifndef MAND_HEIGHT{
MAND_HEIGHT = 22
}

!ifndef MAND_MAX_IT{
MAND_MAX_IT = 15
}

mand_x0		=$60;	!word 0
mand_y0		=$62;	!word 0
mand_x		=$64;	!word 0
mand_y		=$66;	!word 0
mand_x2		=$68;	!word 0
mand_y2		=$6A;	!word 0
mand_xtemp	=$6C;	!word 0

mand_get	; Input:
		; X,Y - bitmap coordinates
		; Output: A - # iterations executed (0 to MAND_MAX_IT-1)
	phx
	rep #$20			; set accu to 16-bit
	!al
	txa	
	+fp_lda_byte			; A = X coordinate
	+FP_LDB_IMM MAND_XMAX		; B = max scaled X
	jsr fp_multiply			; C = A*B
	+FP_TCA				; A = C (X*Xmax)
	+FP_LDB_IMM_INT MAND_WIDTH	; B = width
	jsr fp_divide			; C = A/B
	+FP_TCA				; A = C (scaled X with zero min)
	+FP_LDB_IMM MAND_XMIN		; B = min scaled X
	+fp_add				; C = A+B (scaled X)
	+FP_STC mand_x0			; x0 = C
	tya				; retrieve Y coordinate from Y register
	+fp_lda_byte			; A = Y coordinate
	+FP_LDB_IMM MAND_YMAX		; B = max scaled Y
	jsr fp_multiply			; C = A*B
	+FP_TCA				; A = C (Y*Ymax)
	+FP_LDB_IMM_INT  MAND_HEIGHT	; B = height
	jsr fp_divide			; C = A/B
	+FP_TCA				; A = C (scaled Y with zero min)
	+FP_LDB_IMM MAND_YMIN		; B = min scaled Y
	+fp_add				; C = A+B (scaled Y)
	+FP_STC mand_y0			; y0 = C
	stz mand_x
	stz mand_y
	ldx #0				; X = I (init to 0)
@loop
	+FP_LDA mand_x		; A = X
	+FP_LDB mand_x		; B = X
	jsr fp_multiply		; C = X^2
	+FP_STC mand_x2
	+FP_LDA mand_y		; A = Y
	+FP_LDB mand_y		; B = Y
	jsr fp_multiply		; C = Y^2
	+FP_STC mand_y2
	+FP_LDA mand_x2		; A = X^2
	+FP_TCB			; B = Y^2
	+fp_add			; C = X^2+Y^2
	lda FP_C
	sec
	sbc #$400
	beq @check_fraction
	bmi @do_it
	bra @dec_i
@check_fraction
	lda FP_C
	bne @dec_i
@do_it
	+fp_subtract		; C = X^2 - Y^2
	+FP_TCA			; A = C (X^2 - Y^2)
	+FP_LDB mand_x0		; B = X0
	+fp_add			; C = X^2 - Y^2 + X0
	+FP_STC mand_xtemp	; Xtemp = C
	+FP_LDA mand_x		; A = X
	asl FP_A		; A = 2*X
	+FP_LDB mand_y		; B = Y
	jsr fp_multiply		; C = 2*X*Y
	+FP_TCA			; A = C (2*X*Y)
	+FP_LDB mand_y0		; B = Y0
	+fp_add			; C = 2*X*Y + Y0
	+FP_STC mand_y		; Y = C (2*X*Y + Y0)
	lda mand_xtemp
	sta mand_x		; X = Xtemp
	inx
	cpx #MAND_MAX_IT
	beq @dec_i
	jmp @loop
@dec_i
	sep #$20		; set accu to 8-bit	
	!as
	dex
	txa
	plx
	rts

	
;C64 Basic:	
;10 print chr$(147)
;100 for py=0 to 21
;110 for px=0 to 31
;120 xz = px*3.5/32-2.5
;130 yz = py*2/22-1
;140 x = 0
;150 y = 0
;160 for i=0 to 14
;170 if x*x+y*y > 4 then goto 215
;180 xt = x*x - y*y + xz
;190 y = 2*x*y + yz
;200 x = xt
;210 next i
;215 i = i-1
;220 poke 1024+py*40+px,160
;230 poke 55296+py*40+px,i
;240 next px
;260 next py
;270 for i=0 to 9
;280 print chr$(17)
;290 next i	

   
