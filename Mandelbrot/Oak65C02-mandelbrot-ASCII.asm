; (C) 2021 Matt Heffernan 
; https://github.com/SlithyMatt/multi-mandlebrot
; Modification Oak65C02 Uwe Gottschling 2021
; ASCII art mandelbrot 

;NameTableBase		= $0800 ; 32*24=768 bytes --> $800 to $AFF
;PatternBase		= $0000 ; 32*48=1536 bytes --> $000 to $600
;SpriteAttrBase		= $1B00	; max. 128 bytes
REVERSE_ON		= $00 ; TBD
SPACE			= $00 ; TBD
RETURN			= $00 ; TBD

!cpu 65c02
*=$0FFE
!word $1000 	;--> Start address of this program

   jmp start

!src "mandelbrot.asm"  
!src "../../Kernel/include/label_definition.asm"
!src "../../Kernel/include/Jumptable.asm"

i_result = $60 ; byte

color_codes			; Ascii-art from light to dark
	!byte $0B, $82, $81, $80;  (block elements)
	!byte "%", "$", "#", "*"
	!byte "|", "!", ";", ":"
	!byte ",", ".", " " 

start
	jsr StartMessage
	jsr CLEAR_SCREEN
	ldx #0
	ldy #0
@loop
	jsr mand_get
	sta i_result
	txa
	pha ; preserve X
	ldx i_result
	lda color_codes,x
	jsr CHROUT
	pla
	tax ; restore X
	inx
	cpx #MAND_WIDTH
	bne @loop
	lda #LF
	jsr CHROUT
	ldx #0
	iny
	cpy #MAND_HEIGHT
	bne @loop
	jsr EndMessage
	rts
	
CHROUT	
	phx
	phy
	ldx #VDP_print_CHR_FN
	jsr VDP_Function_Call	
	ply
	plx
	rts   	

CLEAR_SCREEN
	ldx #VDP_Clear_Text_Screen_FN
	jsr VDP_Function_Call
	rts	

StartMessage
	lda #<StartText	; < low byte of expression
	sta PTR_String_L
	lda #>StartText	; > high byte of expression
	sta PTR_String_H
StartMessage2	
	ldx #OS_Print_String_FN		
	jsr OS_Function_Call		
	ldx #PS2_GetKey_FN
	jsr PS2_Function_Call
	rts
		
EndMessage
	lda #<PressKey	; < low byte of expression
	sta PTR_String_L
	lda #>PressKey	; > high byte of expression
	sta PTR_String_H
	jmp StartMessage2
	rts	

StartText
!text LF,"Multi-mandelbrot from Matt Heffernan",LF
PressKey
!text "Press any key",LF,00



