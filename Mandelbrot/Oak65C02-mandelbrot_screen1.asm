; (C) 2021 Matt Heffernan 
; https://github.com/SlithyMatt/multi-mandlebrot
; Modification Oak65C02 Uwe Gottschling 2021
; ASCII art mandelbrot 

NameTableBase		= $1800 ; $1800 ... $1AFF
PatternBase		= $0000	; $0000 ... $17FF
SpriteAttrBase		= $1B00	; max. 128 bytes
ColorTableBase		= $2000	; $2000 ... $201F
Vaddress		= $58	; word
Vread			= $5A	; byte
Vwrite			= $5B	; byte
MAND_MAX_IT 		= 15
!cpu 65c02
*=$0FFE
!word $1000 	;--> Start address of this program

   jmp start

;MAND_WIDTH 	= 32
;MAND_HEIGHT 	= 22

!src "mandelbrot.asm"  
!src "../../Kernel/include/label_definition.asm"
!src "../../Kernel/include/Jumptable.asm"


;i_result 	!byte 0 
loopx	=$58		;!word 0
loopy	=$5A		;!word 0

start
	jsr SetScreen_Mode2
	jsr CLEAR_SCREEN
	jsr StartMessage
	jsr SetColorPattern
	jsr SetNameTableBase
	ldx #0
	ldy #0
@loop
	jsr mand_get
	asl	; *2
	asl	; *4
	asl	; *8
	clc
	adc #128+8
	sta TMS9918RAM
	inx
	cpx #MAND_WIDTH
	bne @loop
	ldx #0
	iny
	cpy #MAND_HEIGHT
	bne @loop
	jsr EndMessage
	rts

SetNameTableBase
	lda #<NameTableBase 
	sta TMS9918REG			; set start address VRAM low Byte
	lda #$40 OR >NameTableBase	; set address maker and high Byte 
	sta TMS9918REG			; set start address VRAM
	rts

CLEAR_SCREEN
	ldx #VDP_Clear_Text_Screen_FN
	jsr VDP_Function_Call
	ldx #VDP_Init_Pattern_FN
	jsr VDP_Function_Call
	rts	
	
SetScreen_Mode2
	lda #TMS9918_Set_Screen1	; 32 x 24 characters/tiles 256x192 pixel
	ldx #VDP_Set_Video_Mode_FN	; Name table --> $800 ... $AFF
	jsr VDP_Function_Call
	rts
	
SetColorPattern	
	lda #<ColorTableBase+16
	sta TMS9918REG			; set start address VRAM low Byte
	lda #$40 OR >ColorTableBase	; set address maker and high Byte 
	sta TMS9918REG			; set start address VRAM
	lda #0
CLEAR_SCREEN1
	ldx #0				; byte per pattern		
CLEAR_SCREEN2
	txa
	asl		
	asl
	asl
	asl
	sta Vwrite		
	txa
	ora Vwrite		
	sta TMS9918RAM
	inx
	cpx #16
	bne CLEAR_SCREEN2
	nop
	nop
	nop
	nop
	lda #<SpriteAttrBase
	sta TMS9918REG		; set start address VRAM low Byte
	lda #$40 OR >SpriteAttrBase		; set address maker and high Byte 
	sta TMS9918REG		; set start address VRAM
	lda #208
	ldy #31			; number of pattern 	
CLEAR_SCREEN5
	ldx #4			; Byte per pattern		
CLEAR_SCREEN6
	nop
	nop
	nop
	nop
	sta TMS9918RAM
	dex
	bne CLEAR_SCREEN6
	dey
	bne CLEAR_SCREEN5	
	rts	

StartMessage
	ldx #VDP_Clear_Text_Screen_FN
	jsr VDP_Function_Call
	lda #<StartText		; < low byte of expression
	sta PTR_String_L
	lda #>StartText		; > high byte of expression
	sta PTR_String_H
StartMessage2	
	ldx #OS_Print_String_FN		
	jsr OS_Function_Call		
WaitKey	
	ldx #PS2_GetKey_FN
	jsr PS2_Function_Call
	rts	
	
EndMessage
	stz VDP_CURSOR_H
	lda #22 
	sta VDP_CURSOR_V
	lda #<PressKey		; < low byte of expression
	sta PTR_String_L
	lda #>PressKey		; > high byte of expression
	sta PTR_String_H
	jmp StartMessage2
Restore_Video
 	lda #TMS9918_Set_Screen0
	ldx #VDP_Set_Video_Mode_FN
	jsr VDP_Function_Call	
	ldx #VDP_Init_Pattern_FN
	jsr VDP_Function_Call	
	rts	

StartText
!text LF,"Multi-mandelbrot",LF,"from Matt Heffernan",LF,"65C02",LF
PressKey
!text "Press any key",LF,00



