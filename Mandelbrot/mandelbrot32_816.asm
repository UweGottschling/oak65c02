; (C) 2021 Matt Heffernan 
; https://github.com/SlithyMatt/multi-mandlebrot
; Modification Oak65C02 Uwe Gottschling 2021

!src "fixedpt32_816.asm"

!ifndef MAND_XMIN{
MAND_XMIN = $FFFD8000 ; -2.5
}

!ifndef MAND_XMAX{
MAND_XMAX = $00038000 ; 3.5
}

!ifndef MAND_YMIN{
MAND_YMIN = $FFFEB000 ; -1.3125
}

!ifndef MAND_YMAX{
MAND_YMAX = $0002A000 ; 2.625
}

!ifndef MAND_WIDTH{
MAND_WIDTH = 256
}

!ifndef MAND_HEIGHT{
MAND_HEIGHT = 212
}

!ifndef MAND_MAX_IT{
MAND_MAX_IT = 15
}

;32 bit 
mand_x0		= $70		
mand_y0		= $74
mand_x		= $78
mand_y		= $7C
mand_x2		= $80
mand_y2		= $84
mand_xtemp	= $88




mand_get			; Input:
	!al
	!rl
	phx
	phy
				; mand_x,mand_y - bitmap coordinates
				; Output: A - # iterations executed (0 to mand_max_it-1)
	+FP_LDA mand_x		; A = X coordinate
	+FP_LDB_IMM MAND_XMAX	; B = max scaled X - min scaled X
	jsr fp_multiply		; C = A*B
	+FP_TCA			; A = C (X*Xmax)
	+FP_LDB_IMM_INT MAND_WIDTH	; B = width
	jsr fp_divide		; C = A/B
	+FP_TCA			; A = C (scaled X with zero min)
	+FP_LDB_IMM MAND_XMIN	; B = min scaled X
	+fp_add			; C = A+B (scaled X)
	+FP_STC mand_x0		; x0 = C
	+FP_LDA mand_y		; A = Y coordinate
	+FP_LDB_IMM MAND_YMAX	; B = max scaled Y - min scaled Y
	jsr fp_multiply		; C = A*B
	+FP_TCA			; A = C (Y*Ymax)
	+FP_LDB_IMM_INT MAND_HEIGHT	; B = height
	jsr fp_divide		; C = A/B
	+FP_TCA			; A = C (scaled Y with zero min)
	+FP_LDB_IMM MAND_YMIN	; B = min scaled Y
	+fp_add			; C = A+B (scaled Y)
	+FP_STC mand_y0		; y0 = C
	stz mand_x
	stz mand_x+2
	stz mand_y
	stz mand_y+2
	ldx #0			; X = I (init to 0)
@loop
	+FP_LDA mand_x		; A = X
	+FP_LDB mand_x		; B = X
	jsr fp_multiply		; C = X^2
	+FP_STC mand_x2
	+FP_LDA mand_y		; A = Y
	+FP_LDB mand_y		; B = Y
	jsr fp_multiply		; C = Y^2
	+FP_STC mand_y2
	+FP_LDA mand_x2		; A = X^2
	+FP_TCB			; B = Y^2
	+fp_add			; C = X^2+Y^2
	lda FP_C+2
	sec
	sbc #4
	beq @check_fraction
	bmi @do_it
	jmp @dec_i
@check_fraction
	lda FP_C
	beq @do_it
	jmp @dec_i
@do_it
	+fp_subtract		; C = X^2 - Y^2
	+FP_TCA			; A = C (X^2 - Y^2)
	+FP_LDB mand_x0		; B = X0
	+fp_add			; C = X^2 - Y^2 + X0
	+FP_STC mand_xtemp	; Xtemp = C
	+FP_LDA mand_x		; A = X
	asl FP_A
	rol FP_A+2		; A = 2*X
	+FP_LDB mand_y		; B = Y
	jsr fp_multiply		; C = 2*X*Y
	+FP_TCA			; A = C (2*X*Y)
	+FP_LDB mand_y0 	; B = Y0
	+fp_add			; C = 2*X*Y + Y0
	+FP_STC mand_y		; Y = C (2*X*Y + Y0)
	lda mand_xtemp
	sta mand_x
	lda mand_xtemp+2	; X = Xtemp
	sta mand_x+2
	inx
	cpx #MAND_MAX_IT
	beq @dec_i
	jmp @loop
@dec_i
  	dex
   	txa
   	ply
   	plx
	rts
   
   
;C64 BASIC:
;
;10 print chr$(147)
;100 for py=0 to 21
;110 for px=0 to 31
;120 xz = px*3.5/32-2.5
;130 yz = py*2/22-1
;140 x = 0
;150 y = 0
;160 for i=0 to 14
;170 if x*x+y*y > 4 then goto 215
;180 xt = x*x - y*y + xz
;190 y = 2*x*y + yz
;200 x = xt
;210 next i
;215 i = i-1
;220 poke 1024+py*40+px,160
;230 poke 55296+py*40+px,i
;240 next px
;260 next py
;270 for i=0 to 9
;280 print chr$(17)
;290 next i   
   
