; (C) 2021 Matt Heffernan 
; https://github.com/SlithyMatt/multi-mandlebrot
; Modification Oak65C02 Uwe Gottschling 2021
; TMS9918 screen mode 3 

NameTableBase		= $800 ; 32*24=768 bytes --> $800 to $AFF
PatternBase		= $000 ; 32*48=1536 bytes --> $000 to $600
SpriteAttrBase		= $1B00	; max. 128 bytes
Vaddress		= $58	; word
Vread			= $5A	; byte
Vwrite			= $5B	; byte
MAND_MAX_IT 		= 15
!cpu 65c02
*=$0FFE
!word $1000 	;--> Start address of this program

   jmp start

MAND_WIDTH 	= 64
MAND_HEIGHT 	= 48

!src "mandelbrot24.asm"  
!src "../../Kernel/include/label_definition.asm"
!src "../../Kernel/include/Jumptable.asm"



loopx	=$60	;!word 0
loopy	=$63	;!word 0
i_result=$65 	;!byte 0 

start
	jsr StartMessage
	jsr CLEAR_SCREEN_Multicolor
	stz loopx
	stz loopx+1
	stz loopy
	stz loopy+1
@loop
	stz mand_x
	lda loopx
	sta mand_x+1
	lda loopx+1
	sta mand_x+2
	stz mand_y
	lda loopy
	sta mand_y+1
	lda loopy+1
	sta mand_y+2
	jsr mand_get
	adc #80
	cmp #128
	bne @write_pixel
	lda #0
@write_pixel
	jsr PLOT
	lda loopx
	clc 
	adc #1
	sta loopx
	lda loopx+1
	adc #0
	sta loopx+1
	cmp #>MAND_WIDTH
	bne @loop
	lda loopx
	cmp #<MAND_WIDTH
	bne @loop
	stz loopx
	stz loopx+1
	lda loopy
	clc
	adc #1
	sta loopy
	lda loopy+1
	adc #0
	sta loopy+1
	cmp #>MAND_HEIGHT
	bne @loop
	lda loopy
	cmp #<MAND_HEIGHT
	bne @loop
	cli
	jsr WaitKey
	jsr Restore_Video
	rts
   
PLOT
	ldx loopx
	ldy loopy
	and #$0f
	sta i_result
	stz Vaddress+1
	tya
	and #$f8
	sta Vaddress
	asl Vaddress	;*2
	rol Vaddress+1
	asl Vaddress	;*4
	rol Vaddress+1
	asl Vaddress	;*8
	rol Vaddress+1
	asl Vaddress	;*16
	rol Vaddress+1	
	asl Vaddress	;*32
	rol Vaddress+1	

	txa
	lsr		;X/2
	and #$1F 	; 
	asl		;*2
	asl		;*4
	asl		;*8
	clc
	adc Vaddress
	sta Vaddress
	lda #0
	adc Vaddress+1
	sta Vaddress+1
	tya
	and #$07
	clc
	adc Vaddress
	sta Vaddress
	lda #0
	adc Vaddress+1
	sta Vaddress+1

	lda Vaddress		; get start address VRAM low byte
	sta TMS9918REG
	lda Vaddress+1		; get start address VRAM high byte
	and #$3f
	sta TMS9918REG
	nop			; Delay
	nop
	nop
	nop
	nop		 
	nop
	lda TMS9918RAM		; read source
	sta Vread
	txa
	and #$01
	beq PLOT2
	lda Vread
	and #$F0
	ora i_result
	sta Vwrite	
	bra PLOT3
PLOT2
	lda i_result
	asl 
	asl
	asl
	asl
	sta i_result
	lda Vread
	and #$0F
	ora i_result
	sta Vwrite	
PLOT3
	lda Vaddress		; lda zp = 3 cyl. ; SUM = 15 cycl.
	sta TMS9918REG		; set start address VRAM low Byte
	lda Vaddress+1
	ora #$40		; set address maker and high Byte 
	sta TMS9918REG		; set start address VRAM
	lda Vwrite
	sta TMS9918RAM	
	rts

CLEAR_SCREEN
	ldx #VDP_Clear_Text_Screen_FN
	jsr VDP_Function_Call
	rts	
	
CLEAR_SCREEN_Multicolor
	lda #TMS9918_Set_Screen3	; 64 x 48 pixel 16 color 
	ldx #VDP_Set_Video_Mode_FN	; Name table --> $800 ... $AFF
	jsr VDP_Function_Call
	lda #<NameTableBase
	sta TMS9918REG			; set start address VRAM low Byte
	lda #$40 OR >NameTableBase	; set address maker and high Byte 
	sta TMS9918REG			; set start address VRAM
	lda #0
	ldy #32				; number of pattern 	
CLEAR_SCREEN1
	ldx #0				; byte per pattern
CLEAR_SCREEN2
	stx Vwrite
	tya
	and #$FC
	asl
	asl
	asl
	ora Vwrite		
	sta TMS9918RAM
	inx
	cpx #32
	bne CLEAR_SCREEN2
	iny
	cpy #24
	bne CLEAR_SCREEN1
	nop
	nop
	nop
	nop
	lda #<PatternBase
	sta TMS9918REG			; set start address VRAM low Byte
	lda #$40 OR >PatternBase 	; set address maker and high Byte 
	sta TMS9918REG			; set start address VRAM
	lda #$12
	ldy #32				; number of pattern 	
CLEAR_SCREEN3
	ldx #48				; Byte per pattern		
CLEAR_SCREEN4
	nop
	nop
	nop
	nop
	nop
	nop
	sta TMS9918RAM
	dex
	bne CLEAR_SCREEN4
	dey
	bne CLEAR_SCREEN3
	nop
	nop
	nop
	nop
	lda #<SpriteAttrBase
	sta TMS9918REG		; set start address VRAM low Byte
	lda #$40 OR >SpriteAttrBase		; set address maker and high Byte 
	sta TMS9918REG		; set start address VRAM
	lda #208
	ldy #31			; number of pattern 	
CLEAR_SCREEN5
	ldx #4			; Byte per pattern		
CLEAR_SCREEN6
	nop
	nop
	nop
	nop
	sta TMS9918RAM
	dex
	bne CLEAR_SCREEN6
	dey
	bne CLEAR_SCREEN5	
	rts	

StartMessage
	lda #<StartText; < low byte of expression
	sta PTR_String_L
	lda #>StartText	; > high byte of expression
	sta PTR_String_H
StartMessage2	
	ldx #OS_Print_String_FN		
	jsr OS_Function_Call		
WaitKey	
	ldx #PS2_GetKey_FN
	jsr PS2_Function_Call
	rts	
EndMessage
	lda #<PressKey; < low byte of expression
	sta PTR_String_L
	lda #>PressKey	; > high byte of expression
	sta PTR_String_H
	jmp StartMessage2

Restore_Video
 	lda #TMS9918_Set_Screen0
	ldx #VDP_Set_Video_Mode_FN
	jsr VDP_Function_Call	
	ldx #VDP_Init_Pattern_FN
	jsr VDP_Function_Call	
	rts	

StartText
!text LF,"Multi-mandelbrot from Matt Heffernan",LF
PressKey
!text "Press any key",LF,00




