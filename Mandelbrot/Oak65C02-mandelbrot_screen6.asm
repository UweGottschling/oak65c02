; (C) 2021 Matt Heffernan 
; https://github.com/SlithyMatt/multi-mandlebrot
; Modification Oak65C02 Uwe Gottschling 2021
; Screen mode 6 = 512*212 pixel, 4 color

!cpu 65c02
*=$0FFE
!word $1000 	;--> Start address of this program

   jmp start

MAND_MAX_IT = 4
MAND_WIDTH = 512

!src "mandelbrot24.asm"  
!src "../../Kernel/include/label_definition.asm"
!src "../../Kernel/include/Jumptable.asm"

i_result	=$60	; byte
loopx		=$61	; word
loopy		=$63	; word

start
	jsr StartMessage
	jsr SetsGraphicMode
	stz loopx
	stz loopx+1
	stz loopy
	stz loopy+1
@loop
	stz mand_x
	lda loopx
	sta mand_x+1
	lda loopx+1
	sta mand_x+2
	stz mand_y
	lda loopy
	sta mand_y+1
	lda loopy+1
	sta mand_y+2
	jsr mand_get
	adc #80
	cmp #128
	bne @write_pixel
	lda #0
@write_pixel
	jsr Plot
	lda loopx
	clc 
	adc #1
	sta loopx
	lda loopx+1
	adc #0
	sta loopx+1
	cmp #>MAND_WIDTH
	bne @loop
	lda loopx
	cmp #<MAND_WIDTH
	bne @loop
	stz loopx
	stz loopx+1
	lda loopy
	clc
	adc #1
	sta loopy
	lda loopy+1
	adc #0
	sta loopy+1
	cmp #>MAND_HEIGHT
	bne @loop
	lda loopy
	cmp #<MAND_HEIGHT
	bne @loop
	cli
	jsr WaitAnyKey
	jsr EndMessage
	rts

Plot	
	pha			
	jsr V9938_Wait_Command

	lda #0
	sta TMS9918REG
	lda #128+45		; set to register R#45: Destination
	sta TMS9918REG	

	lda loopx
	sta TMS9918REG
	lda #128+36		; set to register R#36: X Register
	sta TMS9918REG	

	lda loopx+1
	sta TMS9918REG
	lda #128+37		; set to register R#37: X Register
	sta TMS9918REG	

	lda loopy
	sta TMS9918REG
	lda #128+38		; set to register R#38: Y Register
	sta TMS9918REG	

	stz TMS9918REG
	lda #128+39		; set to register R#39: Y Register
	sta TMS9918REG	

	pla
	sta TMS9918REG	
	lda #128+44		; set to register R#44: color registser
	sta TMS9918REG	

	lda #$50
	sta TMS9918REG
	lda #128+46		; set to register R#46: PSET command
	sta TMS9918REG	

	rts   	

SetsGraphicMode
	lda #V9938_Set_Screen6	; 256 x 212 pixels 16 colors out of 512 
	ldx #VDP_Set_Video_Mode_FN	
	jsr VDP_Function_Call
	rts	

StartMessage
	lda #<StartText		; < low byte of expression
	sta PTR_String_L
	lda #>StartText		; > high byte of expression
	sta PTR_String_H
StartMessage2	
	ldx #OS_Print_String_FN		
	jsr OS_Function_Call		
WaitAnyKey
	ldx #PS2_GetKey_FN
	jsr PS2_Function_Call
	rts	
EndMessage
	lda TMS9918_Set_Screen0		; 40 x 25 patterns
	ldx #VDP_Set_Video_Mode_FN	
	jsr VDP_Function_Call
	ldx #VDP_Init_Pattern_FN
	jsr VDP_Function_Call	
	rts

V9938_Wait_Command
	lda #$02			; set status-register #2
	sta TMS9918REG
	lda #128+15
	sta TMS9918REG
V9938_Wait_Command1
	lda TMS9918REG
	and #$01
	bne V9938_Wait_Command1
	lda #$00			; set status-register #0
	sta TMS9918REG
	lda #128+15
	sta TMS9918REG
	rts	

StartText
!text LF,"Multi-mandelbrot from Matt Heffernan",LF
PressKey
!text "Press any key",LF,00



