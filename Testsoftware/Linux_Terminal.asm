;Compiler : ACME
;UTF-8 Unicode Transformation Format

;***************************
; Linux Terminal
; sudo agetty -L 19200 ttyS0 
;***************************
!src "../Kernel/include/label_definition.asm"
!src "../Kernel/include/Jumptable.asm"
!cpu 65816

*=$0ffe
!word $1000	; start vector

		jsr ACIA_Init

Loop
		jsr ACIA_Check_Receive_Buffer
		beq Loop2
		ldx #UART_CHAR_Receive_FN
		jsr ACIA_Function_Call
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
Loop2
		ldx #PS2_ChkKey_FN
		jsr PS2_Function_Call
		bcc Loop
		ldx #PS2_GetKey_FN
		jsr PS2_Function_Call
		cmp #CR
		bne Loop3
		ldx #UART_CHAR_Transmit_FN
		jsr ACIA_Function_Call
		lda #LF
Loop3
		ldx #UART_CHAR_Transmit_FN
		jsr ACIA_Function_Call
		bra Loop

ACIA_Check_Receive_Buffer
		lda UARTSTATUS		; wait for new character in latch and store it in accumulator
		and #$08
		rts
ACIA_Init
		lda #$16	; 19200 baud, baud generator intern, 1 stop, 8 data bits
		sta UARTCTRL
		lda #$0b	; no parity, no echo, IRQ disable, High -RTS, Interrupt disable, -DTR to high disable, Trans+Rel
		sta UARTCMD
		rts

