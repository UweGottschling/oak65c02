

!src "../Kernel/include/label_definition.asm"
!src "../Kernel/include/Jumptable.asm"


*=$3ffe
!word $4000	; start vector
Main

		lda #STDIO_DISP
		sta STDOUT
		lda #STDIO_UART
		sta STDIN
		rts

HelloWorld_Text
!text LF
!text "Hello World!",LF
!text 00
