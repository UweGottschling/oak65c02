; (C) 2021 Uwe Gottschling
; Little Mario jumps between asteroids
;


!src "../Kernel/include/label_definition.asm"
!src "../Kernel/include/Jumptable.asm"
!src "../Kernel/DOS/DOS_FAT_constant.asm"

!cpu 65c02
XPOS = $50
YPOS = $51
XDELTA=$52
YDELTA=$53
SPRITENAME=$54
SaveIRQVector_L=$55
SaveIRQVector_H=$56
VSYNCFLAG=$57

*=$0FFE			; Header
!word $1000

Main
		stz VSYNCFLAG

		jsr TMS9918Init
		
		lda #<Starttext	; < low byte of expression
		sta PTR_String_L
		lda #>Starttext	; > high byte of expression
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call	
		jsr TMS9918Init_Sprite_Pattern

		sei
		lda IRQVector_L
		sta SaveIRQVector_L
		lda IRQVector_H
		sta SaveIRQVector_H
		
		lda #<Interrupt_Service
		sta IRQVector_L	
		lda #>Interrupt_Service
		sta IRQVector_H
		cli
	
		jsr Main_Loop
Main2
		sei
		lda SaveIRQVector_L
		sta IRQVector_L
		lda SaveIRQVector_H
		sta IRQVector_H

		lda #$42			; Disable interrupt
		sta TMS9918REG
		lda #$81
		sta TMS9918REG
		cli
		
		ldx #PS2_GetKey_FN
		jsr PS2_Function_Call
		lda #CR
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		rts


Starttext
!text "Asteroids demo",CR
!text "Press any key to exit",CR,00


;***************************
; TMS9918 Sprite Pattern
;
;***************************

TMS9918Init_Sprite_Pattern
		lda #$00
		sta TMS9918REG		; set start address VRAM low Byte
		nop
		nop
		nop
		nop
		lda #$40 OR $38		; set address maker and high Byte --> Pattern Generator Sub-Block: $3800 
		sta TMS9918REG		; set start address VRAM
		lda #<Sprite_Data	; Low Byte
		sta PTR_String_L
		lda #>Sprite_Data	; High Byte
		sta PTR_String_H		
		ldy #32			; number of pattern 	
TMS9918Init_Sprite_Pattern1
		ldx #32			; Byte per pattern		
TMS9918Init_Sprite_Pattern2
		lda (PTR_String_L)
		jsr Inc_PTR1
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		sta TMS9918RAM
		dex
		bne TMS9918Init_Sprite_Pattern2
		dey
		bne TMS9918Init_Sprite_Pattern1
; Set attribute

		lda #$00
		sta Operand1
		ldx #$00
		ldy #32				; number of sprites

		sta TMS9918REG		; set start address VRAM low Byte --> 1B00 
		nop	
		nop
		lda #$40 OR $1B		; set address maker and high Byte 
		sta TMS9918REG		; set start address VRAM
TMS9918Init_Sprite_Pattern3
		nop	
		nop
		nop	
		nop
		nop
		sta TMS9918RAM		; Y position
		nop
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; X position
		ldx Operand1
		nop
		nop
		nop
		nop
		nop
		stx TMS9918RAM		; sprite name 
		ldx #02
		nop
		nop
		nop
		nop
		nop
		stx TMS9918RAM		; Sprite Color Code
		nop
		nop
		nop
		nop
		nop
		clc
		adc #8
		inc Operand1
		inc Operand1
		inc Operand1
		inc Operand1
		dey 
		bne TMS9918Init_Sprite_Pattern3
		rts

;***************************
; TMS9918 Sprite Attribute
;
;***************************

Main_Loop
		stz XPOS
		stz YPOS
		lda #1
		sta XDELTA
		sta YDELTA
		sei
		lda #$42+$20		; Enable interrupt
		sta TMS9918REG
		lda #$81
		sta TMS9918REG
Main_Loop1
		stz SPRITENAME
		lda #$00
		ldx #$00
		sta TMS9918REG		; set start address VRAM low Byte
		nop	
		nop
		lda #$40 OR $1B		; set address maker and high Byte 
		sta TMS9918REG		; set start address VRAM
		ldy #4			; number of attribute

Main_Loop2
		lda YPOS
		nop
		nop
		nop
		sta TMS9918RAM		; Y Position
		lda XPOS
		nop
		nop
		nop
		sta TMS9918RAM		; X Position
		lda SPRITENAME
		nop
		nop
		nop
		sta TMS9918RAM		; Sprite Name 
		nop
		nop
		nop
		lda Sprite_Colors,x
		sta TMS9918RAM		; Sprite Color Code
		lda #4
		clc
		adc SPRITENAME
		sta SPRITENAME
		inx
		dey
		bne Main_Loop2

		lda XDELTA
		beq Main_Loop10
		inc XPOS
		bra Main_Loop11
Main_Loop10
		dec XPOS
Main_Loop11
		
		lda YDELTA
		beq Main_Loop20
		inc YPOS
		bra Main_Loop21
Main_Loop20
		dec YPOS
Main_Loop21	

		lda XPOS
		bne Main_Loop30
		lda #1
		sta XDELTA
		bra Main_Loop31
Main_Loop30
		cmp #240		; Right border
		bne Main_Loop31
		stz XDELTA
Main_Loop31
		lda YPOS
		bne Main_Loop40
		lda #1
		sta YDELTA
		bra Main_Loop41
Main_Loop40
		cmp #176		; Bottom border
		bne Main_Loop41
		stz YDELTA
Main_Loop41
		jsr PS2_ChkKey
		bcc Main_Loop80
		rts		
Main_Loop80
		lda #$e5		; Change frame color
		sta TMS9918REG
		lda #$87
		sta TMS9918REG
		cli
		jsr Wait_V_Sync
		jmp Main_Loop1



;***************************
; Increment PTR_W_L/_H
;
;***************************
Inc_PTR1    	INC   PTR_String_L		; increments ptr1
               	BNE   Inc_PTR11
               	INC   PTR_String_H 
Inc_PTR11      	RTS 

;***************************
; Increment PTR_J_L/_H
;
;***************************
Inc_PTR2    	INC   PTR_J_L			; increments ptr2
               	BNE   Inc_PTR21         
               	INC   PTR_J_H      
Inc_PTR21      	RTS

;***************************
; Wait V-Sync
;
;***************************

Wait_V_Sync
		lda VSYNCFLAG
		beq Wait_V_Sync
		stz VSYNCFLAG
		rts
		
;***************************
; Init Graphic Mode
;
;***************************
TMS9918Init
		lda #TMS9918_Set_Screen1
		ldx #VDP_Set_Video_Mode_FN
		jsr VDP_Function_Call
		ldx #VDP_Init_Pattern_FN
		jsr VDP_Function_Call	
		ldx #VDP_Clear_Text_Screen_FN
		jsr VDP_Function_Call

		ldx #$00		; Index auf 0
TMS9918Init_loop1
		lda TMS9918_Reginit,x
		sta TMS9918REG
		inx
		cpx #End_TMS9918_Reginit-TMS9918_Reginit
		bne TMS9918Init_loop1

		rts

TMS9918_Reginit
; Register 2 Name Table Base --> $1800
; Register 3 Color table base --> Value/$400 --> $2000
; Register 4 Pattern generator base --> Value/$800 -->$000
; Register 5 Sprite Attribute Table Base Address --> $1B00	
; Register 6 Sprite Pattern Generator Base Address --> $3800
!by $d3,$87 ; Register 7 Farbe Schrift und Hintergrund
End_TMS9918_Reginit

PS2_ChkKey
		ldx #PS2_ChkKey_FN
		jsr PS2_Function_Call
		rts
;***************************
; Interrupt service routine
; used for vertical sync
; (polling does not work frame accurately)
;***************************

Interrupt_Service
		lda TMS9918REG	; Clear interrupt flag
		lda #$e9		; Change frame color
		sta TMS9918REG
		lda #$87
		sta TMS9918REG

		lda #00
Interrupt_Service2
		inc VSYNCFLAG

		lda ARegister
		ldx XRegister
		ldy YRegister
		rti

Sprite_Colors
!by $01	; Mario Schwarz
!by $06	; Mario Rot
!by $0A	; Mario Braun
!by $05	; Mario Blau
!by $02	; #5
!by $02	; #6
!by $03	; #7
!by $04	; #8
!by $05	; #9
!by $06	; #10
!by $07	; #11
!by $08	; #12
!by $09	; #13
!by $0A	; #14
!by $0B	; #15
!by $0C	; #16
!by $0D	; #17
!by $0E	; #18
!by $0F	; #19
!by $02	; #20
!by $02	; #21
!by $02	; #22
!by $02	; #23
!by $02	; #24
!by $02	; #25
!by $02	; #26
!by $02	; #27
!by $02	; #28
!by $02	; #29
!by $02	; #30
!by $02	; #31
!by $02	; #32

Sprite_Data
; tile (0,0)-(15,15) Nummer1 Mario Schwarz
;		 12345678  oben links
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%00001110	;3
	!by	%00010100	;4
	!by	%00010110	;5
	!by	%00011000	;6
	!by	%00000000	;7
	!by	%00000000	;8
;		 12345678 unten links
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%00000000	;3
	!by	%00000000	;4
	!by	%00000000	;5
	!by	%00000000	;6
	!by	%00011100	;7
	!by	%00111100	;8
;		 12345678 oben rechts
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%01000000	;3
	!by	%01000000	;4
	!by	%00100000	;5
	!by	%01111000	;6
	!by	%00000000	;7
	!by	%00000000	;8
;		 12345678 unten rechts
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%00000000	;3
	!by	%00000000	;4
	!by	%00000000	;5
	!by	%00000000	;6
	!by	%00111000	;7
	!by	%00111100	;8

; tile (0,0)-(15,15) Nummer 2 Mario Rot
;		 12345678  oben links
	!by	%00000111	;1
	!by	%00001111	;2
	!by	%00000000	;3
	!by	%00000000	;4
	!by	%00000000	;5
	!by	%00000000	;6
	!by	%00000000	;7
	!by	%00001101	;8
;		 12345678 unten links
	!by	%00011101	;1
	!by	%00111100	;2
	!by	%00001000	;3
	!by	%00000000	;4
	!by	%00000000	;5
	!by	%00000000	;6
	!by	%00000000	;7
	!by	%00000000	;8
;		 12345678 oben rechts
	!by	%11100000	;1
	!by	%11111000	;2
	!by	%00000000	;3
	!by	%00000000	;4
	!by	%00000000	;5
	!by	%00000000	;6
	!by	%00000000	;7
	!by	%11000000	;8
;		 12345678 unten rechts
	!by	%10111000	;1
	!by	%00111100	;2
	!by	%00010000	;3
	!by	%00000000	;4
	!by	%00000000	;5
	!by	%00000000	;6
	!by	%00000000	;7
	!by	%00000000	;8

; tile (32,0)-(47,15) Nummer3
; tile (0,0)-(15,15)  Mario Braun
;		 12345678  oben links
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%00000001	;3
	!by	%00001011	;4
	!by	%00001001	;5
	!by	%00000111	;6
	!by	%00001111	;7
	!by	%00000000	;8
;		 12345678 unten links
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%00110010	;3
	!by	%00111000	;4
	!by	%00110000	;5
	!by	%00000000	;6
	!by	%00000000	;7
	!by	%00000000	;8
;		 12345678 oben rechts
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%10100000	;3
	!by	%10111000	;4
	!by	%11011100	;5
	!by	%10000000	;6
	!by	%11110000	;7
	!by	%00000000	;8
;		 12345678 unten rechts
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%01001100	;3
	!by	%00011100	;4
	!by	%00001100	;5
	!by	%00000000	;6
	!by	%00000000	;7
	!by	%00000000	;8

; tile (32,0)-(47,15) Nummer4
; tile (0,0)-(15,15)  Mario Blau
;		 12345678  oben links
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%00000000	;3
	!by	%00000000	;4
	!by	%00000000	;5
	!by	%00000000	;6
	!by	%00000000	;7
	!by	%00000010	;8
;		 12345678 unten links
	!by	%00000010	;1
	!by	%00000011	;2
	!by	%00000101	;3
	!by	%00000111	;4
	!by	%00001111	;5
	!by	%00001110	;6
	!by	%00000000	;7
	!by	%00000000	;8
;		 12345678 oben rechts
	!by	%00000000	;1
	!by	%00000000	;2
	!by	%00000000	;3
	!by	%00000000	;4
	!by	%00000000	;5
	!by	%00000000	;6
	!by	%00000000	;7
	!by	%00000000	;8
;		 12345678 unten rechts
	!by	%01000000	;1
	!by	%11000000	;2
	!by	%10100000	;3
	!by	%11100000	;4
	!by	%11110000	;5
	!by	%01110000	;6
	!by	%00000000	;7
	!by	%00000000	;8

; tile (64,0)-(79,15)  Nummer5
	!by	%00011111
	!by	%00100000
	!by	%01000000
	!by	%10000000
	!by	%01100000
	!by	%00010000
	!by	%00100000
	!by	%01000000
	!by	%10000010
	!by	%01000110
	!by	%00101010
	!by	%00010011
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%10000000
	!by	%01000000
	!by	%00100000
	!by	%00010000
	!by	%00010000
	!by	%00010000
	!by	%00010000
	!by	%00010000
	!by	%00010000
	!by	%00100000
	!by	%01000000
	!by	%10000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (80,0)-(95,15)  Nummer6
	!by	%00010000
	!by	%00101001
	!by	%01000110
	!by	%10000000
	!by	%10000000
	!by	%10000000
	!by	%10000000
	!by	%10000000
	!by	%10000000
	!by	%01000000
	!by	%00100000
	!by	%00011111
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%11000000
	!by	%00100000
	!by	%00010000
	!by	%00100000
	!by	%01000000
	!by	%01000000
	!by	%00100000
	!by	%00100000
	!by	%00010000
	!by	%00100000
	!by	%01000000
	!by	%10000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (96,0)-(111,15)  Nummer7
	!by	%00011000
	!by	%01100101
	!by	%10000010
	!by	%01000000
	!by	%00100000
	!by	%00100000
	!by	%01000000
	!by	%10000000
	!by	%10000000
	!by	%01000110
	!by	%00101001
	!by	%00010000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%11000000
	!by	%00100000
	!by	%00010000
	!by	%00100000
	!by	%01000000
	!by	%01000000
	!by	%00100000
	!by	%00100000
	!by	%00010000
	!by	%00100000
	!by	%01000000
	!by	%10000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (112,0)-(127,15)  Nummer8
	!by	%01111111
	!by	%00100000
	!by	%00010000
	!by	%11111000
	!by	%10000000
	!by	%10000000
	!by	%10000000
	!by	%10000000
	!by	%10000000
	!by	%01000110
	!by	%00101001
	!by	%00010000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%10000000
	!by	%01000000
	!by	%00100000
	!by	%00010000
	!by	%01100000
	!by	%10000000
	!by	%01000000
	!by	%00100000
	!by	%00010000
	!by	%00100000
	!by	%01000000
	!by	%10000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (128,0)-(143,15)  Nummer9
	!by	%00111100
	!by	%01000010
	!by	%10000001
	!by	%01100001
	!by	%01000001
	!by	%10001001
	!by	%01011010
	!by	%00101100
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (144,0)-(159,15) Nummer10
	!by	%00100110
	!by	%01011001
	!by	%10000010
	!by	%10000100
	!by	%10000010
	!by	%10000001
	!by	%01000010
	!by	%00111100
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (160,0)-(175,15)  Nummer11
	!by	%01100110
	!by	%10011001
	!by	%01000010
	!by	%01000100
	!by	%10000010
	!by	%10000001
	!by	%01011010
	!by	%00100100
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (176,0)-(191,15)  Nummer12
	!by	%01111100
	!by	%00100010
	!by	%11110001
	!by	%10000010
	!by	%10000010
	!by	%10000001
	!by	%01011001
	!by	%00100110
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (192,0)-(207,15)  Nummer13
	!by	%00000011
	!by	%00000100
	!by	%00000100
	!by	%00001111
	!by	%00110000
	!by	%01000000
	!by	%11111111
	!by	%01000000
	!by	%00110000
	!by	%00001111
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%10000000
	!by	%10000000
	!by	%11000000
	!by	%00110000
	!by	%00001000
	!by	%11111100
	!by	%00001000
	!by	%00110000
	!by	%11000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (208,0)-(223,15)  Nummer14
	!by	%00001100
	!by	%00010010
	!by	%00111111
	!by	%01000000
	!by	%11111111
	!by	%01000000
	!by	%00111111
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%10000000
	!by	%11000000
	!by	%10000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (224,0)-(239,15) Nummer15
	!by	%01000000
	!by	%11100000
	!by	%01000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (240,0)-(255,15) Nummer16
	!by	%10000000
	!by	%01000000
	!by	%00100000
	!by	%00010000
	!by	%00001000
	!by	%00000100
	!by	%00000010
	!by	%00000001
	!by	%00000001
	!by	%00000010
	!by	%00000100
	!by	%00001000
	!by	%00010000
	!by	%00100000
	!by	%01000000
	!by	%10000000
	!by	%11000000
	!by	%00110000
	!by	%00001100
	!by	%00000011
	!by	%00001100
	!by	%00110000
	!by	%11000000
	!by	%10000000
	!by	%11000000
	!by	%11100000
	!by	%11110000
	!by	%11111000
	!by	%11111100
	!by	%11111110
	!by	%11111111
	!by	%11000001
; tile (0,16)-(15,31) Nummer17
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00011000
	!by	%00010111
	!by	%00001000
	!by	%00001000
	!by	%00001000
	!by	%00001000
	!by	%00001000
	!by	%00010111
	!by	%00011000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%11000000
	!by	%00111000
	!by	%00000110
	!by	%00111000
	!by	%11000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (16,16)-(31,31) Nummer18
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00111110
	!by	%00010001
	!by	%00001000
	!by	%00001000
	!by	%00001000
	!by	%00001000
	!by	%00001011
	!by	%00001100
	!by	%00001000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%11111000
	!by	%00000110
	!by	%00011000
	!by	%00100000
	!by	%11000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (32,16)-(47,31) Nummer19
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00111111
	!by	%00010000
	!by	%00001000
	!by	%00001000
	!by	%00000100
	!by	%00000101
	!by	%00000110
	!by	%00000100
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00111100
	!by	%11000100
	!by	%00001000
	!by	%00110000
	!by	%01000000
	!by	%10000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (48,16)-(63,31) Nummer20
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00001111
	!by	%01110000
	!by	%00010000
	!by	%00001000
	!by	%00000100
	!by	%00000100
	!by	%00000101
	!by	%00000010
	!by	%00000010
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00001100
	!by	%11111000
	!by	%00010000
	!by	%00010000
	!by	%00100000
	!by	%01000000
	!by	%10000000
	!by	%10000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (64,16)-(79,31) Nummer21
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000001
	!by	%00000110
	!by	%00011000
	!by	%01100000
	!by	%00011000
	!by	%00000100
	!by	%00000010
	!by	%00000010
	!by	%00000001
	!by	%00000001
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00011000
	!by	%01101000
	!by	%10010000
	!by	%00010000
	!by	%00100000
	!by	%00100000
	!by	%01000000
	!by	%01000000
	!by	%10000000
	!by	%10000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (80,16)-(95,31) Nummer22
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000001
	!by	%00000010
	!by	%00001100
	!by	%00010000
	!by	%01100000
	!by	%00011100
	!by	%00000010
	!by	%00000001
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00010000
	!by	%00110000
	!by	%11100000
	!by	%00100000
	!by	%00100000
	!by	%00100000
	!by	%01000000
	!by	%01000000
	!by	%01000000
	!by	%01000000
	!by	%10000000
	!by	%10000000
	!by	%10000000
	!by	%00000000
	!by	%00000000
; tile (96,16)-(111,31) Nummer23
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000001
	!by	%00000001
	!by	%00000010
	!by	%00000100
	!by	%00001000
	!by	%00010000
	!by	%00111100
	!by	%00000011
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%01100000
	!by	%10100000
	!by	%00100000
	!by	%00100000
	!by	%01000000
	!by	%01000000
	!by	%01000000
	!by	%01000000
	!by	%01000000
	!by	%01000000
	!by	%11000000
	!by	%01000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (112,16)-(127,31) Nummer24
	!by	%00000000
	!by	%00000000
	!by	%00000001
	!by	%00000001
	!by	%00000010
	!by	%00000100
	!by	%00000100
	!by	%00001000
	!by	%00001000
	!by	%00010000
	!by	%00111111
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%10000000
	!by	%10000000
	!by	%01000000
	!by	%01000000
	!by	%01000000
	!by	%01000000
	!by	%01000000
	!by	%01000000
	!by	%00100000
	!by	%00100000
	!by	%10100000
	!by	%01100000
	!by	%00100000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (128,16)-(143,31) Nummer25
	!by	%00000001
	!by	%00000001
	!by	%00000010
	!by	%00000010
	!by	%00000010
	!by	%00000100
	!by	%00000100
	!by	%00001000
	!by	%00001000
	!by	%00001000
	!by	%00010111
	!by	%00011000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%10000000
	!by	%10000000
	!by	%10000000
	!by	%01000000
	!by	%01000000
	!by	%00100000
	!by	%00100000
	!by	%00100000
	!by	%11010000
	!by	%00110000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (144,16)-(159,31) Nummer26
	!by	%00000010
	!by	%00000010
	!by	%00000101
	!by	%00000101
	!by	%00000100
	!by	%00000100
	!by	%00000100
	!by	%00000100
	!by	%00001000
	!by	%00001000
	!by	%00001011
	!by	%00001100
	!by	%00001000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%10000000
	!by	%01000000
	!by	%01000000
	!by	%00100000
	!by	%00100000
	!by	%00010000
	!by	%11111000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (160,16)-(175,31) Nummer27
	!by	%00000000
	!by	%00001100
	!by	%00001010
	!by	%00001001
	!by	%00001001
	!by	%00000100
	!by	%00000100
	!by	%00000100
	!by	%00000100
	!by	%00000100
	!by	%00000101
	!by	%00000110
	!by	%00000100
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%10000000
	!by	%01000000
	!by	%00100000
	!by	%00010000
	!by	%01111000
	!by	%10000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (176,16)-(191,31) Nummer28
	!by	%00000000
	!by	%00010000
	!by	%00011000
	!by	%00001110
	!by	%00001001
	!by	%00001000
	!by	%00001000
	!by	%00000100
	!by	%00000100
	!by	%00000100
	!by	%00000100
	!by	%00000011
	!by	%00000010
	!by	%00000010
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%10000000
	!by	%01100000
	!by	%00010000
	!by	%00001100
	!by	%01110000
	!by	%10000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (192,16)-(207,31) Nummer29
	!by	%00000000
	!by	%00000000
	!by	%00110000
	!by	%00101100
	!by	%00010011
	!by	%00010000
	!by	%00001000
	!by	%00001000
	!by	%00000100
	!by	%00000100
	!by	%00000010
	!by	%00000010
	!by	%00000001
	!by	%00000001
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%11000000
	!by	%00110000
	!by	%00001100
	!by	%00110000
	!by	%01000000
	!by	%10000000
	!by	%10000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (208,16)-(223,31) Nummer30
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%01100000
	!by	%00111110
	!by	%00010001
	!by	%00010000
	!by	%00001000
	!by	%00000100
	!by	%00000010
	!by	%00000010
	!by	%00000001
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%11100000
	!by	%00011100
	!by	%00010000
	!by	%00100000
	!by	%01000000
	!by	%01000000
	!by	%01000000
	!by	%10000000
	!by	%10000000
	!by	%00000000
	!by	%00000000
; tile (224,16)-(239,31) Nummer31
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%01111000
	!by	%01000111
	!by	%00100000
	!by	%00011000
	!by	%00000100
	!by	%00000010
	!by	%00000001
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%11111000
	!by	%00010000
	!by	%00100000
	!by	%00100000
	!by	%01000000
	!by	%01000000
	!by	%11000000
	!by	%01000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
; tile (240,16)-(255,31) Nummer32
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00111111
	!by	%11000000
	!by	%00110000
	!by	%00001000
	!by	%00000110
	!by	%00000001
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%00000000
	!by	%11111000
	!by	%00010000
	!by	%00100000
	!by	%00100000
	!by	%00100000
	!by	%00100000
	!by	%10100000
	!by	%01100000
	!by	%00100000
	!by	%00000000
	!by	%00000000
	!by	%00000000




