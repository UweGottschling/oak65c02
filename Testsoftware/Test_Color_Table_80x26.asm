;Compiler : ACME
;UTF-8 Unicode Transformation Format

;***************************
; Test softeware
;
;***************************
!src "../Kernel/include/label_definition.asm"
!src "../Kernel/include/Jumptable.asm"
!cpu 65816

*=$0ffe
!word $1000	; start vector
Testit

VDP_Clear_Line_26_5
		clc
		lda VDP_Layout_L
		adc #$20		
		sei
		jmp VDP_Clear_Color_Tab_1

		sta TMS9918REG
		lda VDP_Layout_H
		adc #$08		; 26*80 = $820
		ora #$40
		sta TMS9918REG
		lda #SP
		ldy VDP_Text_MaxH
VDP_Clear_Line_26_5_2
		nop
		nop
		nop
		nop
		sta TMS9918RAM
		dey
		bne VDP_Clear_Line_26_5_2
		nop
		nop
		nop
		nop
VDP_Clear_Color_Tab_1		; clear color table $0800h ... $090D
		lda #$F0
		sta TMS9918REG
		lda #$07
		ora #$40
		sta TMS9918REG
		lda #$00
		ldy #10
VDP_Clear_Color_Tab_2
		ldx #27
VDP_Clear_Color_Tab_3
		nop
		nop
		nop
		nop
		sta TMS9918RAM
		dex
		bne VDP_Clear_Color_Tab_3
		dey
		bne VDP_Clear_Color_Tab_2
		cli
		rts
