;
; "MSX SC2/GRP" files begin with fe 00 00 ff 37 00 00.
;    Byte: #FE (type of file)
;    Word: Begin address of file
;    Word: End address of file
;    Word: Start address of file (only important when ",R" parameter is defined with BLOAD-command)


!src "../Kernel/include/label_definition.asm"
!src "../Kernel/include/Jumptable.asm"
!src "../Kernel/DOS/DOS_FAT_constant.asm"
!cpu 65c02

Data_RAM = $4000

*=$0FFE
!word $1000

Graphic2_Main
		jsr Main
		jsr Print_Text_SC2
		jsr Graphic2_Open_File
		bcs Graphic2_Main_Error
;		jsr SetSTDOtoUART
;		lda #CR
;		jsr Print_CHR
		jsr TMS9918_Init_Graphic2Mode
		jsr Graphic2_Init_Loop
 		jsr Graphic2_Loop
;		jsr Wait_Key
;		lda PTR_R_H
;		jsr DOS_Bin2Hex
;		lda PTR_R_L
;		jsr DOS_Bin2Hex

;		jsr Fill_VRAM_NameTable
;		jsr Wait_Key

;		jsr Read_VRAM_NameTable
;		jsr Wait_Key

;		jsr Fill_VRAM_NameTable2
		jsr Wait_Key
Graphic2_Main2
		jsr TMS9918_Init_Textmode1
		jsr TMS9918_Init_Pattern
		lda #CR
		jsr Print_CHR
		rts

Print_Text_SC2
		lda #<Text_SC2
		sta PTR_String_L
		lda #>Text_SC2
		sta PTR_String_H
		jsr Print_String
		rts

Graphic2_Main_Error
		lda #<TypeNotFound_Text
		sta PTR_String_L
		lda #>TypeNotFound_Text
		sta PTR_String_H
		jsr Print_String
		rts

Text_SC2
!text "Load .SC2 file",CR,"Name (8.3):",00

;***************************
; Wait PS2 Key Press
;
;***************************

Wait_Key
		lda VIA2IFR
		and #$02
		beq Wait_Key
		lda VIA2ORA
		rts

;***************************
; Main Loop
;
;***************************

Graphic2_Init_Loop
		lda #$00
		sta TMS9918REG		; set start address VRAM low Byte
		lda #$40		; set address maker and high byte --> $0000
		sta TMS9918REG		; set start address VRAM
Graphic2_Init_Loop2
		lda #<Data_RAM
		sta PTR_R_L
		lda #>Data_RAM
		sta PTR_R_H
		rts
Graphic2_Loop
		clc
		lda #7			; skip file header
		adc PTR_R_L
		sta PTR_R_L
		lda #0
		adc PTR_R_H
		sta PTR_R_H
Graphic2_Loop3
		lda (PTR_R_L)
		sta TMS9918RAM
		lda PTR_W_L
		cmp PTR_R_L
		bne Graphic2_Loop4
		lda PTR_W_H
		cmp PTR_R_H
		beq Graphic2_Loop_Exit
Graphic2_Loop4
		inc PTR_R_L
		bne Graphic2_Loop3
		inc PTR_R_H
		bra Graphic2_Loop3

Graphic2_Loop_Exit
		rts

;***************************
; Open File
;
;***************************

Graphic2_Open_File
		jsr LEditor			; get file name
		jsr DOS_Open_File
		bcs Graphic2_Open_File_Exit

		lda #<TypeFound_Text
		sta PTR_String_L
		lda #>TypeFound_Text
		sta PTR_String_H
		jsr Print_String

		lda #<Data_RAM
		sta PTR_W_L
		lda #>Data_RAM
		sta PTR_W_H
		jsr Print_Start_Address
		jsr DOS_Load_Data
		rts
Graphic2_Open_File_Exit
		sec
		rts

;***************************
; Wait V Sync
;
;***************************

Wait_V_Sync
		lda TMS9918REG
		nop
		nop
Wait_V_Sync1
		lda TMS9918REG
		and #$80
		beq Wait_V_Sync1
		rts
;***************************
; Init Graphic Mode
;
;***************************

TMS9918_Init_Graphic2Mode
		lda #32
		sta Text_MaxH
		lda #24
		sta Text_MaxV
		lda #2
		sta Video_Mode
		ldx #0		; Index
		ldy #$80	; start tms9918 register
TMS9918_Init_Graphic2Mode_1
		lda TMS9918_Reginit,x
		sta TMS9918REG
		sty TMS9918REG
		iny
		inx
		cpx #End_TMS9918_Reginit-TMS9918_Reginit
		bne TMS9918_Init_Graphic2Mode_1
		rts

TMS9918_Reginit
; Erst Registerinhalt und dann die Registernummer
!by $02 ; Register 0 ($80) M3=1 (Graphic Mode 2)
!by $C2 ; Register 1 ($81) 16K-Ram - Blank off - M1 = M2 = 0, Sprite 16x16
!by $06 ; Register 2 ($82) Pattern Name Table Base  --> Text Reg1 --> $1800
!by $ff ; Register 3 ($83) Color Table Base  -->  Value/$80 --> $2000
!by $03 ; Register 4 ($84) Pattern Generator Base --> Value/$800 -->$0000
!by $36 ; Register 5 ($85) Sprite Attribute Table Base Address --> Value/$80 --> $1B00
!by $07 ; Register 6 ($86) Sprite Pattern Generator Base Address -->Value/$800 --> $3800
!by $f1 ; Register 7 ($87) text color 1 and backdrop color 0
End_TMS9918_Reginit


; MSX Screen Mode2:
; SCREEN 2 - 256*192 Graphics mode
;  0000-17FF   BG Tiles
;  1800-1AFF   BG Map
;  1B00-1B7F   OBJ Attributes
;  2000-37FF   BG Colors
;  3800-3FFF   OBJ Tiles

;***************************
;
;
;***************************

SetSTDOtoUART
		lda #2
		sta STDOUT
		rts
SetSTDOtoDis
		lda #3
		sta STDOUT
		rts
Print_CHR_SP
		phx
		phy
		pha
		lda #" "
		jsr Print_CHR
		pla
		ply
		plx
		rts

;!src "../Kernel/DOS/DOS_65C02.asm"

