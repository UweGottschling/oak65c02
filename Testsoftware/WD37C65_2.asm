; WD37C65 driver
;
;


!src "label_definition.asm"
!src "Jumptable.asm"
!src "DOS_FAT_Constant.asm"
!cpu 65c02
Sectorbuffer = $2000
;#include "monitor9.asm"

FD765_STAT=$DF70	; Master Status Register
FD765_DAT=$DF71		; Data Register
FD765_CR=$DF72		; LDCR --> Control Register
FD765_OR=$DF74		; PC:Digital Output Register DOR or Operation Register

*=$0FFE
!word $1000


Main
		jsr Main_Reset
Main_Text
		jsr Print_StartDostext
		jsr Get_CHR
		pha
		jsr Print_CHR	; Echo Command
		pla
		ldx #End_Commands-Commands
Main1
		dex
		bmi Main2
		cmp Commands,x
		bne Main1

		txa
		asl
		tax
		lda Command_Loc,x	; TODO: Command_LOC --> String
		sta PTR_J_L
		lda Command_Loc+1,x
		sta PTR_J_H
		lda #CR
		jsr Print_CHR

		jsr Main3
		jmp Main_Text
Main2
		cmp #"x"
		beq Main_Exit
		jmp Main
Main3
		jmp (PTR_J_L)
		jmp Main_Text
Main_Exit
		rts

Commands
!text "0"
!text "8"
!text "r"
!text "o"
!text "f"
!text CR
!text LF
End_Commands

Command_Loc
!word Main_Tack0			;0
!word Main_SeekTrack80			;8
!word Main_PrintStatus			;r
!word Main_Floppy_On			;o
!word Main_Floppy_Off			;f
!word Main				;CR
!word Main				;LF



Print_StartDostext
		lda #<StartDostext	; < low byte of expression
		sta PTR_String_L
		lda #>StartDostext	; > high byte of expression
		sta PTR_String_H
		jsr Print_String
		rts

StartDostext
!text CR
!text "WD37C65 Testsoftware",CR
!text "commands:",CR
!text "0 = track 0",CR
!text "8 = track 80",CR
!text "r = read status register",CR
!text "o = floppy motor on",CR
!text "f = floppy motor off",CR
!text "x = exit",CR
!text 00




Main_Reset

		lda #$00
		sta FD765_OR		; Soft Reset --> OR2, Mode select AT/EISA Mode
		jsr Delay
		lda #CR
		jsr Print_CHR

		lda #$04		; Motor One Enable, Not Reset , AT/EISA Mode
		sta FD765_OR

		rts
Main_Floppy_On
		lda #$14
		sta FD765_OR		
		rts

Main_Floppy_Off
		lda #$04
		sta FD765_OR		
		rts


;		jsr FD765_WaitBusy_Write	; Warte bis Software-Reset beendet
Main_Tack0
		jsr FD765_Tack0		; Kommando Spur 0
		jsr Delay
;		lda FD765_STAT
;		jsr Bin2Hex		; Ergebnis ausgeben
;		lda #CR
;		jsr Print_CHR
		rts


		jsr FD765_Sense
		jsr Bin2Hex 		; Ergebnis ausgeben
		lda #CR
		jsr Print_CHR

Main_SeekTrack80
		lda #80
		jsr FD765_Seek		; Kommand Seek Spur 80
		jsr Bin2Hex 		; Ergebnis ausgeben
		lda #CR
		jsr Print_CHR
		rts
; 		jmp Main_Loop_FD4

Main_PrintStatus
		jsr Bin2Hex 		; Ergebnis ausgeben
		lda #CR
		rts

Main_Loop_FD3
		jsr FD765_Sense
		and #$20
		beq Main_Loop_FD3

		lda #40
		jsr FD765_Seek
		jsr Bin2Hex 		; Ergebnis ausgeben
		lda #CR
		jsr Print_CHR
Main_Loop_FD4
		jsr FD765_WaitBusy_Write
		lda #$04		; Drive A + Motor aus
		sta FD765_OR
		rts

;***************************
; FD765 Track 0
;
;***************************
FD765_Tack0
		jsr FD765_WaitBusy_Write
		lda #$07		; Command Calibrate Drive (x7h) 
		sta FD765_DAT
		jsr FD765_WaitBusy_Write
		lda #$00
		sta FD765_DAT
		rts

;***************************
; FD765 Track X
;
;***************************
FD765_Seek
		pha
		jsr FD765_WaitBusy_Write
		lda #$0F		; Command Seek/Park Head
		sta FD765_DAT
		jsr FD765_WaitBusy_Write
		lda #$00
		sta FD765_DAT
		jsr FD765_WaitBusy_Write
		pla
		sta FD765_DAT		; Track Accu
		rts

;***************************
; FD765 Sense Interrupt Status
;
;***************************
FD765_Sense

		jsr FD765_WaitBusy_Write
		lda #$08		; Command
		sta FD765_DAT
		jsr FD765_WaitBusy_Read
		lda FD765_DAT		; ST0
		jsr FD765_WaitBusy_Read
		ldx FD765_DAT		; Present Cylinder
		rts

;***************************
; FD765 wait busy write
;
;***************************
FD765_WaitBusy_Write
		lda FD765_STAT
		and #$80		; Bit7 = RQM Bit6=Data Input
		beq FD765_WaitBusy_Write ; Warte auf nicht mehr Busy
		rts

;***************************
; FD765 wait busy read
;
;***************************
FD765_WaitBusy_Read
		lda FD765_STAT
		and #$80		; Bit8 = RQM Bit7=Data Input
		beq FD765_WaitBusy_Read ; Warte auf nicht mehr Busy
		rts


;***************************
; Delay
;
;***************************
Delay
		phy
		ldy #255
Delay1
		iny
		bne Delay1
		ply
		rts

;***************************
; Delay long
;
;***************************
Delay_long
		phy
		phx

		ldx #255
		ldy #255
Delay1_long
		iny
		bne Delay1_long
		inx
		bne Delay1_long

		plx
		ply
		rts


;***************************
; Texte fuer Ausgabe
;
;***************************

!text "Track(Hex):",00


!text "Sector(Hex):",00


!text "Side:",00


