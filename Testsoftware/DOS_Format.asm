; Format floppy disk 720k for WD1772
; (c) 2021 Uwe Gottschling
; TODO: detect write protected disk

!src "../Kernel/include/label_definition.asm"
!src "../Kernel/include/Jumptable.asm"
!cpu 65c02


Max_Sector = 10
Max_Track = 80

WD177XIRQID = $D4 ;Force interrupt on index pulse
WD177XIRQOFF= $D0

*=$0FFE
!word $1000 	;--> Start address of this program

Format_Main
		stz Floppy_DriveNo
		jsr Print_Text_Format
		jsr OS_Function_Call
		jsr Wait_YN
		beq Format_Main2
		rts
Format_Main2	
		jsr Format_Tracks
		bcs Format_Error_Exit
;		jsr Format_Debug_Read_Track
		jsr Verify_Format
		bcs Format_Error_Exit
		jsr Write_Boot_Sector
		bcs Format_Error_Exit	
		jsr Write_FAT_Sector
		bcs Format_Error_Exit
Format_Exit		
		rts
Format_Error_Exit
		jsr Print_Format_Error	
		rts

;***************************
; Print start message
;
;***************************

Print_Text_Format
		lda #<Text_Start_Format
		sta PTR_String_L
		lda #>Text_Start_Format
		sta PTR_String_H
		ldx #OS_Print_String_FN
		rts

Text_Start_Format
!text LF,"Format floppy disk 720K",LF
!text "Only for WD1772 FDC",LF
!text "Insert disk in drive A:",LF
!text "Are you sure (y/n)?",LF,00

;***************************
; Wait PS2 Key Press
;
;***************************

Wait_Key
		ldx #PS2_ChkKey_FN
		jsr PS2_Function_Call
		bcc Wait_Key
		ldx #PS2_GetKey_FN
		jsr PS2_Function_Call
		rts

;***************************
; Wait key press Y
; if y set zero
;***************************

Wait_YN
		ldx #OS_Get_CHR_FN
		jsr OS_Function_Call
		cmp #"y"
		beq Wait_YN2	
		cmp #"Y"
Wait_YN2
		rts

;***************************
; Error messages
;
;***************************

Print_Format_Error
		lda #<TypeFormatError_Text
		sta PTR_String_L
		lda #>TypeFormatError_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		lda WD177XCMD
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call		
		lda #LF
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call	
		sec
		rts
TypeFormatError_Text
!text "FDC error code :",00

;***************************
; Fomat tracks
; Low level format routine for WD1772
;***************************

Format_Tracks
		stz Track
		stz Side
		lda #01
		sta Sector
		ldx #FDC_Reset_FN
		jsr FDC_Function_Call
		ldx #FDC_Restore_FN
		jsr FDC_Function_Call
		bcc Format_Tracks00
		lda #WD177XIRQID	;Force interrupt on index pulse
		sta WD177XCMD
		rts
Format_Tracks00
		lda Track
		jsr FDC_SeekTrack
		bcc Format_Tracks01
		jmp Format_Tracks_Error_Exit	
Format_Tracks01		
		jsr FDC_SetSide
		lda #WD177XWRITET	; Write track command
		sta WD177XCMD
Format_Tracks1
		ldx #60			; GAP 1 - post index gap
		lda #$4E		; 60 x $4E
Format_Tracks2
		bit FLOPPYLATCH
		bpl Format_Tracks2	; Wait for DRQ
		sta WD177XDAT
		dex
		bne Format_Tracks2
		ldx #0
Format_Tracks3		
		bit FLOPPYLATCH
		bpl Format_Tracks3	; Wait for DRQ
		lda IDRecordData,x	; GAP 2 and ID Record
		sta WD177XDAT
		cmp #$FB		; Last byte ?
		beq Format_Tracks6
		inx		
		bra Format_Tracks3
Format_Tracks6	
		bit FLOPPYLATCH
		bpl Format_Tracks6	; Wait for DRQ
Format_Tracks7			
		lda Sector		; Set sector number
		sta WD177XSEC
Format_Tracks10				; Write sector data
		lda #$00
		ldx #$00
		ldy #$02		; Write 2 * 256 bytes $00
Format_Tracks11		
		bit FLOPPYLATCH
		bpl Format_Tracks11	; Wait for DRQ		
		sta WD177XDAT
		dex
		bne	Format_Tracks11
		dey
		bne Format_Tracks11	
Format_Tracks12		
		bit FLOPPYLATCH
		bpl Format_Tracks12	; Wait for DRQ		
		lda #$F7		; DATA Record 2 CRC bytes	
		sta WD177XDAT
		ldx #$40		; Write GAP 4 Post Data Gap
		lda #$4E
Format_Tracks13		
		bit FLOPPYLATCH
		bpl Format_Tracks13	; Wait for DRQ										
		sta WD177XDAT
		dex
		beq Format_Tracks14
		bra Format_Tracks13		
Format_Tracks14	
		inc Sector
		lda Sector
		cmp #Max_Sector
		beq Format_Tracks15		
		jmp Format_Tracks3	; Write next sector
Format_Tracks15	
		ldy #$4e
Format_Tracks16
		bit FLOPPYLATCH
		bvs Format_Tracks17
		bpl Format_Tracks16
		sty WD177XDAT
		bra Format_Tracks16
Format_Tracks17
		lda #1
		sta Sector
		jsr Print_Track
		inc Side
		lda Side
		cmp #$02
		beq Format_Tracks20
		jmp Format_Tracks00	; Write next side
Format_Tracks20
		stz Side
		inc Track
		lda Track
		cmp #Max_Track
		beq Format_Tracks_Exit
		jmp Format_Tracks00	; Write next track
Format_Tracks_Exit
		lda #LF
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call		
		clc
		rts
Format_Tracks_Error_Exit
		lda #LF
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call		
		sec
		rts

;***************************
; Print track number
; 
;***************************
Print_Track
		lda #<Print_Track_Number_Text
		sta PTR_String_L
		lda #>Print_Track_Number_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		lda Track
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call

		lda #<Print_Head_Number_Text
		sta PTR_String_L
		lda #>Print_Head_Number_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		lda Side
		clc
		adc #"0"
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call

		lda #CR
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call				
		rts
		
Print_Track_Number_Text
!text "Track: ",00		
Print_Head_Number_Text	
!text " Head: ",00	

;***************************
; FDC Seek Track
; input: A = track
; output: A = status, C=1=error
; used: A,X
;***************************	

FDC_SeekTrack
		sta WD177XDAT		; Track number to track register
		lda #WD177XSEEK  	; Seek command
		sta WD177XCMD
FDC_WaitBusy
		bit FLOPPYLATCH
		bvc FDC_WaitBusy
		lda WD177XCMD
		tax
		clc
		and #$59		; $A6 is no error
		beq FDC_SeekTrack1
		sec
FDC_SeekTrack1
		txa
		rts

;***************************
; WD177X set side
;
;***************************

FDC_SetSide
		lda Floppy_DriveNo
		clc
		rol		
		ora Side		; BIT0 = side select. Side = 0 for boot sector
		sta FLOPPYLATCH
		rts

;***************************
; Print verify ok
; 
;***************************
Print_Verify
		lda #<Verify_OK_Text
		sta PTR_String_L
		lda #>Verify_OK_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		rts
Verify_OK_Text	
!text "Verify ",00
	
;***************************
; WD177X Verify Disk
;
;***************************

Verify_Format
		stz Floppy_Head
		stz Floppy_Track
		lda #01
		sta Floppy_Sector
		lda #WD177XIRQOFF	;Force interrupt on index pulse
		sta WD177XCMD
		ldx #FDC_Restore_FN
		jsr FDC_Function_Call
		bcc Verify_Format10
		jsr Print_Format_Error
		rts 
Verify_Format10
		lda #<DOS_DataSectorBuff
		sta PTR_FLOPPY_L
		lda #>DOS_DataSectorBuff	; Set start address
		sta PTR_FLOPPY_H
		ldx #FDC_RSect_FN
		jsr FDC_Function_Call		; Read sector to PTR_FLOPPY
		bcc Verify_Format20
		bra Verify_Format_Error_Exit		
Verify_Format20		
		inc Floppy_Sector
		lda Floppy_Sector
		cmp #Max_Sector
		beq Verify_Format23		
		jmp Verify_Format10		; Write next sector
Verify_Format23
		lda #1
		sta Floppy_Sector
		jsr Print_Verify
		lda Floppy_Track
		sta Track
		lda Floppy_Head
		sta Side
		jsr Print_Track
		inc Floppy_Head
		lda Floppy_Head
		cmp #$02
		beq Verify_Format24
		jmp Verify_Format10		; Write next side
Verify_Format24
		stz Floppy_Head
		inc Floppy_Track
		lda Floppy_Track
		cmp #Max_Track
		beq Verify_Format_Exit
		jmp Verify_Format10		; Write next track
Verify_Format_Exit
		lda #LF
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		clc
		rts
Verify_Format_Error_Exit
		jsr Print_TrackSideSector
		sec
		rts


;***************************
; Print Track, Head, Sector
;
;***************************
Print_TrackSideSector
		lda #<Print_Track_Number_Text
		sta PTR_String_L
		lda #>Print_Track_Number_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		lda Floppy_Track
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call

		lda #<Print_Head_Number_Text
		sta PTR_String_L
		lda #>Print_Head_Number_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		lda Floppy_Head	
		clc
		adc #"0"
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call

		lda #<Write_Sector_Text
		sta PTR_String_L
		lda #>Write_Sector_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		lda Floppy_Sector
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call

		lda #LF
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call				
		rts

Write_Sector_Text
!text " Sector: ",00			

;***************************
; WD177X Read Track at $2000
;
;***************************
	
;Format_Debug_Read_Track	
;		ldx #FDC_Restore_FN
;		jsr FDC_Function_Call
;		lda #<$2000
;		sta PTR_FLOPPY_L
;		lda #>$2000		; Set start address
;		sta PTR_FLOPPY_H
;		ldx #FDC_RTrack_FN
;		jsr FDC_Function_Call	; Read sector to PTR_FLOPPY
;		rts 
;		
;***************************
; Write boot sector
;
;***************************		
Write_Boot_Sector
		lda #<Write_Boot_text
		sta PTR_String_L
		lda #>Write_Boot_text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call	
		ldx #FDC_Restore_FN
		jsr FDC_Function_Call
		lda #<DOS_DataSectorBuff
		sta PTR_W_L
		lda #>DOS_DataSectorBuff
		sta PTR_W_H
		lda #0
		ldx #0
		ldy #2			; Fill with 2*256 Bytes value $00
Write_Boot_Sector2
		sta (PTR_W_L)
		jsr Inc_PTR_W
		dex
		bne Write_Boot_Sector2
		dey
		bne Write_Boot_Sector2
Write_Boot_Sector4
		lda #<DOS_DataSectorBuff
		sta PTR_W_L
		lda #>DOS_DataSectorBuff
		sta PTR_W_H	
		ldx #0
Write_Boot_Sector5
		lda Boot_Sector_Data,x
		sta (PTR_W_L)
		jsr Inc_PTR_W
		inx
		cpx #Boot_Sector_Data_End-Boot_Sector_Data
		beq Write_Boot_Sector7
		bra Write_Boot_Sector5
Write_Boot_Sector7		
		lda #<DOS_DataSectorBuff
		sta PTR_W_L
		lda #>DOS_DataSectorBuff
		sta PTR_W_H
		lda PTR_W_L
		clc
		adc #$FE			; Add offset $1fe
		sta PTR_W_L
		lda PTR_W_H
		adc #01
		sta PTR_W_H
		lda #$55			; Write magic bytes to boot sector
		sta (PTR_W_L)
		jsr Inc_PTR_W
		lda #$AA
		sta (PTR_W_L)
		lda #<DOS_DataSectorBuff
		sta PTR_FLOPPY_L
		lda #>DOS_DataSectorBuff
		sta PTR_FLOPPY_H	
		lda #00 
		sta Floppy_Track
		sta Floppy_Head
		lda #$01
		sta Floppy_Sector
		jsr FDC_WSect
		rts 	
Write_Boot_text
!text "Write boot sector",LF,00		
;***************************
; Increase PTR_W
;
;***************************	
Inc_PTR_W
		inc PTR_W_L
		bne Inc_PTR_W_Exit
		inc PTR_W_H
Inc_PTR_W_Exit       	
		rts
       	
       	
;***************************
; Write FAT
;
;***************************		
Write_FAT_Sector
		lda #<Write_FAT_text
		sta PTR_String_L
		lda #>Write_FAT_text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call	
		ldx #FDC_Restore_FN
		jsr FDC_Function_Call
		lda #<DOS_DataSectorBuff
		sta PTR_W_L
		lda #>DOS_DataSectorBuff
		sta PTR_W_H
		lda #0
		ldx #0
		ldy #2				; Fill with 2*256 Bytes value $00
Write_FAT_Sector2
		sta (PTR_W_L)
		jsr Inc_PTR_W
		dex
		bne Write_FAT_Sector2
		dey
		bne Write_FAT_Sector2
Write_FAT_Sector4
		lda #<DOS_DataSectorBuff
		sta PTR_W_L
		lda #>DOS_DataSectorBuff
		sta PTR_W_H	  
Write_FAT_Sector6
		lda #$f9
		sta (PTR_W_L)
		jsr Inc_PTR_W
		lda #$ff		
 		sta (PTR_W_L)
		jsr Inc_PTR_W
		sta (PTR_W_L)
		jsr Inc_PTR_W		
		lda #<DOS_DataSectorBuff
		sta PTR_FLOPPY_L
		lda #>DOS_DataSectorBuff
		sta PTR_FLOPPY_H	
		lda #00 
		sta Floppy_Track
		sta Floppy_Head
		lda #$02
		sta Floppy_Sector
		jsr FDC_WSect	
		bcs	Write_FAT_Sector_Exit
		lda #<DOS_DataSectorBuff
		sta PTR_FLOPPY_L
		lda #>DOS_DataSectorBuff
		sta PTR_FLOPPY_H	
		lda #00 
		sta Floppy_Track
		sta Floppy_Head
		lda #$05
		sta Floppy_Sector
		jsr FDC_WSect			
Write_FAT_Sector_Exit	
		rts
Write_FAT_text
!text "Write FAT",LF,00				      	
;***************************
; FDC write sector
;
;***************************

FDC_WSect
		jsr FDC_Set_CHS		; lda Floppy_Sector
		lda Floppy_Sector
		sta WD177XSEC		; Set sector register
		lda #WD177XWRITES	; Write sector
		sta WD177XCMD
		jmp FDC_WData

;***************************
; WD177X Set Head, drive and Track number
;
;***************************
FDC_Set_CHS
		lda Floppy_DriveNo
		clc
		rol		
		ora Floppy_Head		; BIT0 = side select. Side = 0 for boot sector
		sta FLOPPYLATCH
		lda Floppy_Track
		jsr FDC_SeekTrack
		rts
		
;***************************
; Write data from PTR_FLOPPY to disk
; Output A: Status; C=1=Error
;***************************
FDC_WData
		ldy #$0
FDC_WData2
		bit FLOPPYLATCH			; Bit 7 = DRQ, Bit 6 = INTRQ
		bvs FDC_WData4
		bpl FDC_WData2
FDC_WData3
		lda (PTR_FLOPPY_L),y
		sta WD177XDAT
		iny
		bne FDC_WData2
		inc PTR_FLOPPY_H
		bra FDC_WData2
FDC_WData4
		tya
		clc
		adc PTR_FLOPPY_L		; Set pointer correctly using Y registers
		sta PTR_FLOPPY_L
		lda #0
		adc PTR_FLOPPY_H
		sta PTR_FLOPPY_H
;		ply
		lda WD177XCMD
		tax
		clc
		and #$7F			; $80 is no error
		beq FDC_WData5
		sec
FDC_WData5
		txa
		rts		
		

IDRecordData
!byte 0,0,0,0,0,0,0,0,0,0,0,0,$F5,$F5,$F5,$FE	; GAP 2 
Track
!byte 0	; 0 ... 79
Side
!byte 0 ; 0 ... 1
Sector
!byte 0 ; 1 ... 10
Length
!byte 02 ; 512 Byes
!byte $F7 ; 2 CRC Bytes
!byte $4E,$4E,$4E,$4E,$4E,$4E,$4E,$4E,$4E,$4E,$4E ; GAP 3a
!byte $4E,$4E,$4E,$4E,$4E,$4E,$4E,$4E,$4E,$4E,$4E
!byte 0,0,0,0,0,0,0,0,0,0,0,0	; Gap 3b
!byte $F5,$F5,$F5
!byte $FB	; Data address mark
			
			
Boot_Sector_Data 	; Boot sector data from 0000 to 006F
!byte  $eb,$3c,$90,$4d,$54,$4f,$4f,$34,$30,$32,$36,$00,$02,$02,$01,$00 ;$0000  
!byte  $02,$70,$00,$a0,$05,$f9,$03,$00,$09,$00,$02,$00,$00,$00,$00,$00 ;$0010  
!byte  $00,$00,$00,$00,$00,$00,$29,$d3,$04,$51,$07,$4e,$4f,$20,$4e,$41 ;$0020 
!byte  $4d,$45,$20,$20,$20,$20,$46,$41,$54,$31,$32,$20,$20,$20,$fa,$31 ;$0030 
!byte  $c0,$8e,$d8,$8e,$c0,$fc,$b9,$00,$01,$be,$00,$7c,$bf,$00,$80,$f3 ;$0040 
!byte  $a5,$ea,$56,$00,$00,$08,$b8,$01,$02,$bb,$00,$7c,$ba,$80,$00,$b9 ;$0050 
!byte  $01,$00,$cd,$13,$72,$05,$ea,$00,$7c,$00,$00,$cd,$19,$00,$00,$00 ;$0060 
Boot_Sector_Data_End
			
			
Start_Data_Space



;https://www-user.tu-chemnitz.de/~heha/basteln/PC/usbfloppy/floppy.chm/
;The tables below indicates the standard values of the different gaps in a "standard" Atari diskette with 9 sectors of 512 user data bytes. 
;It also indicates the minimum acceptable values of these gaps, as specified in the WD1772 datasheet, when formatting non standard diskettes.
;Name					Standard Values (9 sectors) 	Minimum Values (Datasheet)
;Gap 1 Post Index		60 x $4E						32 x $4E
;Gap 2 Pre ID			12 x $00 + 3 x $A1				8 x 00 + 3 x $A1
;Gap 3a Post ID			22 x $4E						22 x $4E
;Gap 3b Pre Data		12 x $00 + 3 x $A1				12 x $00 + 3 x $A1
;Gap 4 Post Data		40 x $4E						24 x $4E
;Gap 5 Pre Index		~ 664 x $4E						16 x $4E

; Empty 720k floppy disk image with superformat /dev/fd0 dd:
;
;00000000  eb 3c 90 4d 54 4f 4f 34  30 32 36 00 02 02 01 00  |.<.MTOO4026.....| --> Boot sector
;00000010  02 70 00 a0 05 f9 03 00  09 00 02 00 00 00 00 00  |.p..............|
;00000020  00 00 00 00 00 00 29 d3  04 51 07 4e 4f 20 4e 41  |......)..Q.NO NA|
;00000030  4d 45 20 20 20 20 46 41  54 31 32 20 20 20 fa 31  |ME    FAT12   .1|
;00000040  c0 8e d8 8e c0 fc b9 00  01 be 00 7c bf 00 80 f3  |...........|....|
;00000050  a5 ea 56 00 00 08 b8 01  02 bb 00 7c ba 80 00 b9  |..V........|....|
;00000060  01 00 cd 13 72 05 ea 00  7c 00 00 cd 19 00 00 00  |....r...|.......|
;00000070  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
;*
;000001b0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 80 00  |................|
;000001c0  01 00 01 01 09 4f 00 00  00 00 a0 05 00 00 00 00  |.....O..........|
;000001d0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
;*
;000001f0  00 00 00 00 00 00 00 00  00 00 00 00 00 00 55 aa  |..............U.|
;00000200  f9 ff ff 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
;00000210  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
;*
;00000800  f9 ff ff 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
;00000810  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
;*
;000b4000




