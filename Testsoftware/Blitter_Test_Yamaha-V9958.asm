; Blitter test for YAMAHA V9958
; (C) 2021 Uwe Gottschling


!src "../Kernel/include/label_definition.asm"
!src "../Kernel/include/Jumptable.asm"
!src "../Kernel/DOS/DOS_FAT_constant.asm"
!cpu 65c02

Data_RAM = $4000

*=$0FFE
!word $1000

BlitterTest_Main

		jsr V9938_Init_Screen7
		jsr Graphic6_Init_Loop
		jsr Wait_Key
BlitterTest_Main2

		lda #TMS9918_Set_Screen0
		ldx #VDP_Set_Video_Mode_FN
		jsr VDP_Function_Call
		ldx #VDP_Init_Pattern_FN
		jsr VDP_Function_Call
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		rts

;***************************
; Wait PS2 Key Press
;
;***************************

Wait_Key
		ldx #PS2_GetKey_FN
		jsr PS2_Function_Call
		rts

;***************************
; Main Loop
;
;***************************

Graphic6_Init_Loop
		lda #$00
		sta TMS9918REG		; set start address VRAM high Byte
		lda #$8e		; set address maker and high byte --> $0000
		sta TMS9918REG		; set start address VRAM
		lda #$00
		sta TMS9918REG		; set start address VRAM low Byte
		lda #$40		; set address maker and high byte --> $0000
		sta TMS9918REG		; set start address VRAM
		ldx #$00
		ldy #$d4
Graphic6_Init_Loop2
		lda #$22
		sta TMS9918RAM		; Clear Screen
		dex
		bne Graphic6_Init_Loop2
		dey
		bne Graphic6_Init_Loop2
		ldx #$ff
		ldy #$2C		
Graphic6_Init_Loop4
		nop
		stz TMS9918RAM
		dex
		bne Graphic6_Init_Loop4
		dey
		bne Graphic6_Init_Loop4
		ldy #0
Graphic6_Init_Loop18
		jsr V9938_Wait_Command
		lda #36
		sta TMS9918REG
		lda #17+128
		sta TMS9918REG
		ldx #0
Graphic6_Init_Loop19
		lda V9938_CMD1,x
		sta V9938INDREG
		inx
		cpx #END_V9938_CMD1-V9938_CMD1
		bne Graphic6_Init_Loop19
Graphic6_Init_Loop20
		sty TMS9918REG
		lda #128+38	; DY
		sta TMS9918REG
		sty TMS9918REG	; Color
		lda #128+44
		sta TMS9918REG
		lda #$70	; LINE - CMD
		sta TMS9918REG
		lda #128+46
		sta TMS9918REG
		iny
		cpy #212		; draw 212 lines with different colors
		bne Graphic6_Init_Loop18
		jmp Graphic6_Init_Loop28

V9938_CMD1
!by $00;,128+36 ; Register 36 ($24) DX
!by $00;,128+37 ; Register 37 ($25) DX
!by $00;,128+38 ; Register 38 ($26) DY
!by $00;,128+39 ; Register 39 ($27) DY
!by $FF;,128+40 ; Register 40 ($28) NX
!by $01;,128+41 ; Register 41 ($29) NX
!by $00;,128+42 ; Register 42 ($2a) NY
!by $00;,128+43 ; Register 43 ($2b) NY
!by $0F;,128+44 ; Register 44 ($2c) color
!by $00;,128+45 ; Register 45 ($2c) destination memory
END_V9938_CMD1

Graphic6_Init_Loop28	
		ldy #0
Graphic6_Init_Loop29
		jsr V9938_Wait_Command
		lda #32
		sta TMS9918REG
		lda #17+128
		sta TMS9918REG
		ldx #0
Graphic6_Init_Loop30
		lda V9938_CMD2,x
		sta V9938INDREG
		inx
		cpx #END_V9938_CMD2-V9938_CMD2
		bne Graphic6_Init_Loop30

Graphic6_Init_Loop31
		sty TMS9918REG
		lda #128+34	; SY
		sta TMS9918REG
		sty TMS9918REG	; Color
		lda #128+44
		sta TMS9918REG
		lda #$D0	; CMD HMMM (High speed move VRAM to VRAM)
		sta TMS9918REG
		lda #128+46
		sta TMS9918REG
		iny
		cpy #128	
		bne Graphic6_Init_Loop29
		jmp Graphic6_Init_Loop40
		
V9938_CMD2
!by $00;,128+32 ; Register 32 ($20) SX Starting Point low
!by $00;,128+33 ; Register 33 ($21) SX Starting Point high
!by $00;,128+34 ; Register 34 ($22) SY Starting Point low
!by $00;,128+35 ; Register 35 ($23) SY Starting Point high
!by $64;,128+36 ; Register 36 ($24) DX
!by $00;,128+37 ; Register 37 ($25) DX
!by $32;,128+38 ; Register 38 ($26) DY
!by $00;,128+39 ; Register 39 ($27) DY
!by $ff;,128+40 ; Register 40 ($28) NX
!by $00;,128+41 ; Register 41 ($29) NX
!by $64;,128+42 ; Register 42 ($2a) NY
!by $00;,128+43 ; Register 43 ($2b) NY
!by $0F;,128+44 ; Register 44 ($2c) color
!by $00;,128+45 ; Register 45 ($2c) destination memory
END_V9938_CMD2

Graphic6_Init_Loop40
		ldy #0
Graphic6_Init_Loop41
		jsr V9938_Wait_Command
		lda #36
		sta TMS9918REG
		lda #17+128
		sta TMS9918REG
		ldx #0
Graphic6_Init_Loop42
		lda V9938_CMD3,x
		sta V9938INDREG
		inx
		cpx #END_V9938_CMD3-V9938_CMD3
		bne Graphic6_Init_Loop42
Graphic6_Init_Loop43
		sty TMS9918REG
		lda #128+36	; DX
		sta TMS9918REG
		sty TMS9918REG
		lda #128+38	; DY
		sta TMS9918REG
		sty TMS9918REG	; Color
		lda #128+44
		sta TMS9918REG
		lda #$C0	; CMD HMMV (High speed move VDP to VRAM)
		sta TMS9918REG
		lda #128+46
		sta TMS9918REG
		iny
		cpy #128	
		bne Graphic6_Init_Loop41
		jmp Graphic6_Init_Loop50

V9938_CMD3
!by $00;,128+36 ; Register 36 ($24) DX
!by $00;,128+37 ; Register 37 ($25) DX
!by $00;,128+38 ; Register 38 ($26) DY
!by $00;,128+39 ; Register 39 ($27) DY
!by $50;,128+40 ; Register 40 ($28) NX
!by $00;,128+41 ; Register 41 ($29) NX
!by $50;,128+42 ; Register 42 ($2a) NY
!by $00;,128+43 ; Register 43 ($2b) NY
!by $0f;,128+44 ; Register 44 ($2c) color
!by $00;,128+45 ; Register 45 ($2c) destination memory
END_V9938_CMD3

Graphic6_Init_Loop50
		ldy #0
Graphic6_Init_Loop51
		jsr V9938_Wait_Command
		lda #32
		sta TMS9918REG
		lda #17+128
		sta TMS9918REG
		ldx #0
Graphic6_Init_Loop52
		lda V9938_CMD4,x
		sta V9938INDREG
		inx
		cpx #END_V9938_CMD4-V9938_CMD4
		bne Graphic6_Init_Loop52
Graphic6_Init_Loop53
		tya
		clc
		adc #128  
		sta TMS9918REG	; Destination transfer point X
		lda #128+36	; DX
		sta TMS9918REG

		tya
		asl
		asl	
		asl
		sta TMS9918REG	; Destination transfer point Y
		lda #128+38	; DY
		sta TMS9918REG
		sty TMS9918REG	; Color
		lda #128+44
		sta TMS9918REG
		tya
		and #$0F	; Logical operation	
		ora #$90	; CMD LMMM (Logical move VRAM to VRAM)
		sta TMS9918REG
		lda #128+46
		sta TMS9918REG
		iny
		cpy #$0F	
		bne Graphic6_Init_Loop51
		jmp Graphic6_Init_Loop60

V9938_CMD4
!by $05;,128+32 ; Register 32 ($20) SX Source transfer point X low
!by $00;,128+33 ; Register 33 ($21) SX Source transfer point X high
!by $00;,128+34 ; Register 34 ($22) SY Source transfer point Y low
!by $00;,128+35 ; Register 35 ($23) SY Source transfer point Y high
!by $ff;,128+36 ; Register 36 ($24) DX Destination transfer point X
!by $00;,128+37 ; Register 37 ($25) DX
!by $00;,128+38 ; Register 38 ($26) DY Destination transfer point Y
!by $00;,128+39 ; Register 39 ($27) DY
!by $ff;,128+40 ; Register 40 ($28) NX Number of dots in x-axi
!by $00;,128+41 ; Register 41 ($29) NX
!by $10;,128+42 ; Register 42 ($2a) NY Number of dots in Y-axis
!by $00;,128+43 ; Register 43 ($2b) NY
!by $0f;,128+44 ; Register 44 ($2c) color
!by $00;,128+45 ; Register 45 ($2c) destination memory
END_V9938_CMD4

Graphic6_Init_Loop60			; sprites attributes $FA00
		lda #$03
		sta TMS9918REG		; set start address VRAM high Byte
		lda #$8e		; set address maker and high byte --> $FA00
		sta TMS9918REG		; set start address VRAM
		lda #$00
		sta TMS9918REG		; set start address VRAM low Byte
		lda #$7A		; set address maker and high byte --> $FA00, write command
		sta TMS9918REG		; set start address VRAM
		ldx #0
Graphic6_Init_Loop61
		txa
		clc
		asl	
		sta TMS9918RAM		; y-pos.
		asl
		asl
		nop
		nop
		sta TMS9918RAM		; x-pos
		lda #$00
		nop
		nop
		sta TMS9918RAM		; sprite pattern number
		lda #$0E
		nop
		nop
		sta TMS9918RAM		; sprite color
		inx
		cpx #32
		bne Graphic6_Init_Loop61
		lda #$03
		sta TMS9918REG		; set start address VRAM high Byte
		lda #$8e		; set address maker and high byte --> $F000
		sta TMS9918REG		; set start address VRAM
		lda #$00
		sta TMS9918REG		; set start address VRAM low Byte
		lda #$70		; set address maker and high byte --> $F000, write command
		sta TMS9918REG		; set start address VRAM
		ldx #0
Graphic6_Init_Loop62
		lda Sprites_0_Pattern,x
		sta TMS9918RAM
		inx
		cpx #End_Sprites_0_Pattern-Sprites_0_Pattern
		bne Graphic6_Init_Loop62
		lda #$03
		sta TMS9918REG		; set start address VRAM high Byte
		lda #$8e		; set address maker and high byte --> $F800
		sta TMS9918REG		; set start address VRAM
		lda #$00
		sta TMS9918REG		; set start address VRAM low Byte
		lda #$78		; set address maker and high byte --> $F800, write command
		sta TMS9918REG		; set start address VRAM
		ldy #32			; number of sprites
Graphic6_Init_Loop63
		ldx #0
Graphic6_Init_Loop64
		tya
		ora Sprites_0_color,x
Graphic6_Init_Loop65
		sta TMS9918RAM
		inx
		cpx #End_Sprites_0_color-Sprites_0_color
		bne Graphic6_Init_Loop64		
		dey
		bne Graphic6_Init_Loop63
		jmp Graphic6_Init_Loop70
Sprites_0_Attr	; Sprite attribute
!by $20	; y-coordinate	
!by $20	; x-coordinate	
!by $00	; pattern number
!by $8F	; EC, color code
End_Sprites_0_Attr

Sprites_0_Pattern	; 16x16 pixel = 32 Byte
!by $1F,$3F,$7F,$FF,$F3,$E3,$E3,$E3
!by $F3,$FF,$FF,$FF,$FF,$FF,$FB,$20
!by $F8,$FC,$FE,$FF,$9F,$8F,$8F,$8F
!by $9F,$FF,$FF,$FF,$FF,$FF,$EE,$84
End_Sprites_0_Pattern

Sprites_0_color	; 16x16 pixel = 16 Byte
!by $01,$02,$03,$04,$04,$04,$04,$05
!by $08,$07,$06,$05,$04,$03,$02,$01
End_Sprites_0_color

Graphic6_Init_Loop70
		ldx #100
Graphic6_Init_Loop71
		lda #$03		; move sprite
		sta TMS9918REG		; set start address VRAM high Byte
		lda #$8e		; set address maker and high byte --> $FA00
		sta TMS9918REG		; set start address VRAM
		lda #$00
		sta TMS9918REG		; set start address VRAM low Byte
		lda #$7A		; set address maker and high byte --> $FA00, write command
		sta TMS9918REG		; set start address VRAM
		jsr Wait_V_Sync
		stx TMS9918RAM
		nop
		nop
		nop
		nop
		stx TMS9918RAM
		dex
		bne Graphic6_Init_Loop71
Graphic6_Init_Loop72
		jsr V9938_Init_Screen8
		lda #$03
		sta TMS9918REG		; set start address VRAM high Byte
		lda #$8e		; set address maker and high byte --> $FA00
		sta TMS9918REG		; set start address VRAM
		ldy #0			; counter y-pos
Graphic6_Init_Loop80
		ldx #0			; Sprite number
Graphic6_Init_Loop81
		txa
		asl			; x2
		asl			; x2
		sta TMS9918REG		; set start address VRAM low Byte
		lda #$3A		; set address maker and high byte --> $FA00, read command
		sta TMS9918REG		; set start address VRAM		
		nop			; Delay for V9938
		nop
		nop
		nop
		lda TMS9918RAM
		inc
		pha			
		txa
		asl			; x2
		asl			; x2
		sta TMS9918REG		; set start address VRAM low Byte
		lda #$7A		; set address maker and high byte --> $FA00, write command
		sta TMS9918REG		; set start address VRAM	
		pla
		sta TMS9918RAM
		inx
		cpx #32
		bne Graphic6_Init_Loop81
		jsr Wait_V_Sync
		iny
		cpy #100
		bne Graphic6_Init_Loop80
	
; Draw lines from center to the border

Graphic6_Init_Loop90
		ldx #0
Graphic6_Init_Loop91
		ldy #0
Graphic6_Init_Loop92
		jsr V9938_Wait_Command
		lda #36
		sta TMS9918REG
		lda #17+128
		sta TMS9918REG	
Graphic6_Init_Loop93
		jsr V9938_Wait_Command
		lda #$80	
		sta V9938INDREG	; DX low
		lda #$00
		sta V9938INDREG ; DX high
		lda #$6A
		sta V9938INDREG	; DY low
		lda #$00
		sta V9938INDREG	; DY high
Graphic6_Init_Loop94
		lda #$80
		sta V9938INDREG ; NX low
		lda #0	
		sta V9938INDREG	; NX high
		stx V9938INDREG	; NY low
		lda #$0
		sta V9938INDREG ; NY high
Graphic6_Init_Loop95
		stx V9938INDREG ; color
		tya
		asl
		asl
		sta V9938INDREG	; destination memory, x / y transfer direction 
		lda #$70
		sta V9938INDREG	; command
		iny
		cpy #4
		bne Graphic6_Init_Loop92
		inx
		cpx #106	; lines to draw
		bne Graphic6_Init_Loop91
		ldx #0
Graphic6_Init_Loop102
		ldy #0
Graphic6_Init_Loop103
		jsr V9938_Wait_Command
		lda #36
		sta TMS9918REG
		lda #17+128
		sta TMS9918REG	
Graphic6_Init_Loop104
		jsr V9938_Wait_Command
		lda #$80	
		sta V9938INDREG	; DX low
		lda #$00
		sta V9938INDREG ; DX high
		lda #$6A
		sta V9938INDREG	; DY low
		lda #$00
		sta V9938INDREG	; DY high
		cpx #106
		bcc Graphic6_Init_Loop105
		stx V9938INDREG ; NX low
		lda #0	
		sta V9938INDREG	; NX high
		lda #$6A
		sta V9938INDREG	; NY low
		lda #$0
		sta V9938INDREG ; NY high
		bra Graphic6_Init_Loop106	
Graphic6_Init_Loop105
		lda #$6A
		sta V9938INDREG ; NX low
		lda #0	
		sta V9938INDREG	; NX high
		stx V9938INDREG	; NY low
		lda #$0
		sta V9938INDREG ; NY high
Graphic6_Init_Loop106	
		stx V9938INDREG ; color
		tya
		asl
		asl
		cpx #106
		bcs Graphic6_Init_Loop109
		ora #1
Graphic6_Init_Loop109
		sta V9938INDREG	; destination memory, x / y transfer direction 
		lda #$70
		sta V9938INDREG	; command
		iny
		cpy #4
		bne Graphic6_Init_Loop103
		inx
		cpx #129	; lines to draw
		bne Graphic6_Init_Loop102
		rts

;***************************
; Wait_Command_Execution
;
;***************************

V9938_Wait_Command
		lda #$02	; Set Status-Register #2
		sta TMS9918REG
		lda #128+15
		sta TMS9918REG
V9938_Wait_Command1
		lda TMS9918REG
		and #$01
		bne V9938_Wait_Command1
		lda #$00	; Set Status-Register #0
		sta TMS9918REG
		lda #128+15
		sta TMS9918REG
		rts

;***************************
; Wait V Sync
;
;***************************

Wait_V_Sync
		phx
		phy
Wait_V_Sync1
		lda TMS9918REG
		and #$80
		beq Wait_V_Sync1
		ply
		plx	
		rts

;		ldx #VDP_Wait_V_SYNC_FN
;		jsr VDP_Function_Call
;		rts


;***************************
; Init Screen 7 Mode
; 512 pixel x 212 pixel x 16 color
; Input: Output:
; It also work with screen 5,6,8,12
;***************************

V9938_Init_Screen7
		lda #V9938_Set_Screen7	; 512 x 212 pixels 16 colors 
		ldx #VDP_Set_Video_Mode_FN
		jsr VDP_Function_Call
		lda #$08
		sta TMS9918REG	
		lda #$88		; enable sprites
		sta TMS9918REG	
		rts

;***************************
; Init Screen Mode 8 Mode
; 256 pixel x 212 pixel x 256 color
; Input: Output:
;***************************
V9938_Init_Screen8
		lda #V9938_Set_Screen8
		ldx #VDP_Set_Video_Mode_FN
		jsr VDP_Function_Call
		lda #$08
		sta TMS9918REG	
		lda #$88		; enable sprites
		sta TMS9918REG	
		rts

;***************************
; Text Mode1 with v9938 compatibility
;
;***************************

TMS9918_Init_Textmode1_new
		lda #V9938_Set_Screen0_W80
		ldx #VDP_Set_Video_Mode_FN
		jsr VDP_Function_Call
		rts

