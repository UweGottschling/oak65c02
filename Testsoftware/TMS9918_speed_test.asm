; (C) 2021 Uwe Gottschling
; TMS9918A speed test
; Tested with 2MHz 65C816 CPU, emulation mode

!src "../Kernel/include/label_definition.asm"
!src "../Kernel/include/Jumptable.asm"
!src "../Kernel/DOS/DOS_FAT_constant.asm"

!cpu 65c02



;Zero page
		TilesPattern 	= $60
		SpriteXPos	= $61
		SpriteYPos	= $62
		SpriteName	= $63
		SpriteNumber	= $64
		SpriteColor 	= $65
		Repeats		= $66
		Buffer		= $67
		
; Constants
; Register 2 Name Table Base --> $1800
; Register 3 Color table base --> Value/$400 --> $2000
; Register 4 Pattern generator base --> Value/$800 -->$000
; Register 5 Sprite Attribute Table Base Address --> $1B00	
; Register 6 Sprite Pattern Generator Base Address --> $3800
NAMETABLEBASE		= $1800 ; 32*24=768 bytes
COLORTABLEBASE		= $2000 ; 256/8=32 bytes; each byte set the color of 8 characters
PATTERNBASE		= $0000 ; 256*8=2048 nytes
SPRITEATTRBASE		= $1B00	; max. 128 bytes
SPRITEPATTERNBASE 	= $3800	; max. 2048 byes
SPRITENAME		= $00

*=$0FFE			; programme start address header
!word $1000		

Main
		jsr InitZeroPage
		jsr ClearVDPMemory
		jsr TMS9918InitMode
		jsr InitStartScreen
		jsr TMS9918InitSpritePattern
		jsr SetSprites		; all sprites on
		jsr WaitKeyboard
		jsr FillNameTableVDP
;		jsr WaitKeyboard
;		jsr FillNameTableCPU
;		jsr WaitKeyboard
;		jsr CopyNameTableCPU
;		jsr WaitKeyboard
		jsr CopySpriteAttrCPU
		jsr WaitKeyboard
		rts

;***************************
; Init zero page
;
;***************************

InitZeroPage
		stz TilesPattern
		stz SpriteXPos	
		stz SpriteYPos	
		stz SpriteName	
		stz SpriteNumber
		stz SpriteColor
		stz Repeats
		rts
		
;***************************
; Position 32 sprites on screen
;
;***************************

SetSprites
		ldx #0		; Counter
		lda #1		; Start sprite color
		sta SpriteColor
SetSprites10
		jsr PosSprite

		lda SpriteYPos
		clc
		adc #4	
		sta SpriteYPos

		lda SpriteNumber
		clc 
		adc #4
		sta SpriteNumber		

		lda SpriteXPos
		clc
		adc #16
		sta SpriteXPos

		lda SpriteColor
		inc
		and #$0F
		bne SetSprites15
		inc
SetSprites15		
		sta SpriteColor
		
		inx
		cpx #32		; Number of sprites
		bne SetSprites10

		rts

;***************************
; Positioning the sprites
; Sprite attributes:
;	Offset	
;	0		Y sprite 0, special: Y=208 all following sprites are not displayed
;	1		X sprite 0
;	2		sprite pattern 0
;	3		colour sprite 0 + EC (EC=early clock, move sprite 32 pixels to the left)
;***************************

PosSprite
		lda SpriteNumber
		sta TMS9918REG		; set start address VRAM low byte
		lda #$40 OR $1B		; set address maker and high byte 
		sta TMS9918REG		; set start address VRAM
		lda SpriteYPos
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; Y Position
		lda SpriteXPos
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; X Position
		lda SPRITENAME
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; Sprite Name 
		nop
		nop
		nop
		nop
		lda SpriteColor		; Sprite color: 5 light blue
		sta TMS9918RAM		; Sprite color code
		rts

;***************************
; FillNameTable auto increment by VDP
;
;***************************

FillNameTableVDP
		lda #3
		sta Repeats
FillNameTableVDP0
		lda #<NAMETABLEBASE
		sta TMS9918REG			; set start address VRAM low Byte
		lda #$40 OR >NAMETABLEBASE	; set address maker and high Byte 
		sta TMS9918REG			; set start address VRAM
		lda Repeats			; ASCII code for filling
		ldy #32				; number of pattern 	
FillNameTableVDP1
		ldx #24				; Byte per pattern		
FillNameTableVDP2
		nop				; Minimum 3*NOP, before or after write to VDP-RAM needed
		nop
		nop	
		sta TMS9918RAM
		dex
		bne FillNameTableVDP2
		dey
		bne FillNameTableVDP1
		dec Repeats
		bne FillNameTableVDP0
		rts	
		
;***************************
; FillNameTable increment by CPU
;
;***************************

FillNameTableCPU
		lda #200
		sta Repeats
FillNameTableCPU0		
		lda #<NAMETABLEBASE
		sta PTR_String_L		; set start address VRAM low Byte
		lda #$40 OR >NAMETABLEBASE	; set address maker and high Byte 
		sta PTR_String_H		; set start address VRAM
FillNameTableCPU4
		ldy #32				; number of pattern
FillNameTableCPU5
		ldx #24				; number of pattern	
FillNameTableCPU10
		lda PTR_String_L
		sta TMS9918REG			; set start address VRAM low Byte
		lda PTR_String_H		; set address maker and high Byte 
		sta TMS9918REG			; set start address VRAM
FillNameTableCPU20
		lda Repeats			; ASCII code for filling		
		sta TMS9918RAM			; No Delay need before writing to memoy because
		jsr Inc_PTR1 			; there is a lot of delay after.
		dex
		bne FillNameTableCPU10
		dey
		bne FillNameTableCPU5
		dec Repeats
		bne FillNameTableCPU0
		rts	

;***************************
; Read and write to name table
;***************************

CopyNameTableCPU
		lda #12
		sta Repeats
CopyNameTableCPU0		
		lda #<NAMETABLEBASE
		sta PTR_String_L	; set start address VRAM low Byte
		lda #>NAMETABLEBASE	; set address maker and high Byte 
		sta PTR_String_H	; set start address VRAM
CopyNameTableCPU4
		ldy #32			; number of pattern
CopyNameTableCPU10
		lda PTR_String_L
		sta TMS9918REG		; set start address VRAM low Byte
		lda PTR_String_H	; set address maker and high Byte 
		and #$3f
		sta TMS9918REG		; set start address VRAM
		nop			; minimum 6 * NOP = 12 cycl.
		nop
		nop
		nop
		nop		 
		nop
		lda TMS9918RAM		; ASCII code for filling		
		sta Buffer		; sta zp = 3 cycl.
		inc Buffer		; inc zp = 5 cycl.
		nop			; nop = 2 cycl.					
		nop			; nop = 2 cycl.					 
		lda PTR_String_L	; lda zp = 3 cyl. ; SUM = 15 cycl.
		sta TMS9918REG		; set start address VRAM low Byte
		lda PTR_String_H
		ora #$40		; set address maker and high Byte 
		sta TMS9918REG		; set start address VRAM
CopyNameTableCPU20
		lda Buffer		; ASCII code for filling		
		sta TMS9918RAM		; No Delay need before writing to memory 
		jsr Inc_PTR1 		; Increment pointer 4 times
		jsr Inc_PTR1 	
		jsr Inc_PTR1 	
		jsr Inc_PTR1 	
		dey
		bne CopyNameTableCPU10
		dec Repeats
		bne CopyNameTableCPU0
		rts	

;***************************
; Read and write sprite attribute
;	0	Y sprite no.
;	1	X sprite no-
;	2	sprite pattern no.
;	3	colour sprite no. + EC
;***************************

CopySpriteAttrCPU
		lda #$FF
		sta Repeats
CopySpriteAttrCPU0		
		lda #<SPRITEATTRBASE
		sta PTR_String_L	; set start address VRAM low Byte
		lda #>SPRITEATTRBASE	; set address maker and high Byte 
		sta PTR_String_H	; set start address VRAM
		jsr VDP_Wait_V_Sync
CopySpriteAttrCPU4
		ldy #32			; number of pattern
CopySpriteAttrCPU10
;Increase y-positon
		lda PTR_String_L
		sta TMS9918REG		; set start address VRAM low Byte
		lda PTR_String_H	; set address maker and high Byte 
		and #$3f
		sta TMS9918REG		; set start address VRAM
		nop			; Minimum 6 * NOP
		nop
		nop
		nop
		nop		 
		nop
		lda TMS9918RAM
		sta Buffer
		inc Buffer
		cmp #192
		bne CopySpriteAttrCPU15
		stz Buffer
CopySpriteAttrCPU15
		lda PTR_String_L
		sta TMS9918REG		; set start address VRAM low Byte
		lda PTR_String_H
		ora #$40		; set address maker and high Byte 
		sta TMS9918REG		; set start address VRAM
CopySpriteAttrCPU20
		lda Buffer		; ASCII code for filling		
		sta TMS9918RAM		; No Delay need before writing to memory
		jsr Inc_PTR1		; Increase X-Positon
		lda PTR_String_L
		sta TMS9918REG		; set start address VRAM low Byte
		lda PTR_String_H	; set address maker and high Byte 
		and #$3f
		sta TMS9918REG		; set start address VRAM
		nop			; minimum 6 * NOP
		nop
		nop
		nop
		nop		 
		nop
		lda TMS9918RAM		
		sta Buffer
		inc Buffer
CopySpriteAttrCPU25
		lda PTR_String_L
		sta TMS9918REG		; set start address VRAM low Byte
		lda PTR_String_H
		ora #$40		; set address maker and high Byte 
		sta TMS9918REG		; set start address VRAM
CopySpriteAttrCPU30
		lda Buffer		; ASCII code for filling		
		sta TMS9918RAM		; no Delay need before writing to memoy
		jsr Inc_PTR1 	
		jsr Inc_PTR1 	
		jsr Inc_PTR1 	
		dey
		bne CopySpriteAttrCPU10
		dec Repeats
		bne CopySpriteAttrCPU0
		rts			

;***************************
; Clear VDP Memory 
; 16384 Bytes ($4000)
;***************************

ClearVDPMemory
		lda #00
		sta TMS9918REG		; set start address VRAM low Byte
		lda #$40 OR 00		; set address maker and high Byte 
		sta TMS9918REG		; set start address VRAM
		lda #$00		; ASCII code for filling
		ldy #$40		; number of pattern 	
ClearVDPMemory1
		ldx #$00		; Byte per pattern		
ClearVDPMemory2
		nop
		nop
		nop
		sta TMS9918RAM
		dex
		bne ClearVDPMemory2
		dey
		bne ClearVDPMemory1
		rts	

;***************************
; Increment PTR_W_L/_H
;
;***************************

Inc_PTR1    	
		inc   PTR_String_L	; increments ptr1
		bne   Inc_PTR11
		inc   PTR_String_H 
Inc_PTR11      	
		rts 

;***************************
; Wait for any key from keyboard
;
;***************************

WaitKeyboard
		lda #<WaitKeyText	; < low byte of expression
		sta PTR_String_L
		lda #>WaitKeyText	; > high byte of expression
		sta PTR_String_H
		ldx #OS_Print_String_FN		
		jsr OS_Function_Call
		ldx #PS2_GetKey_FN
		jsr PS2_Function_Call
		rts
WaitKeyText
!text "Press any key",LF,00

;***************************
; Init Graphic Mode
;
;***************************

TMS9918InitMode
		lda #TMS9918_Set_Screen1
		ldx #VDP_Set_Video_Mode_FN
		jsr VDP_Function_Call
		lda #$42+$80		; Set mode register
		sta TMS9918REG
		lda #$81
		sta TMS9918REG	
		ldx #VDP_Init_Pattern_FN
		jsr VDP_Function_Call
		ldx #VDP_Clear_Text_Screen_FN
		jsr VDP_Function_Call
		lda #$1E		; Change frame color
		sta TMS9918REG
		lda #$87
		sta TMS9918REG	
		rts

;***************************
; Init TMS9918 start screen
; 
;***************************
InitStartScreen
		ldx #VDP_Clear_Text_Screen_FN
		jsr VDP_Function_Call				
		lda #<InitStartScreenText; < low byte of expression
		sta PTR_String_L
		lda #>InitStartScreenText	; > high byte of expression
		sta PTR_String_H
		ldx #OS_Print_String_FN		
		jsr OS_Function_Call
		rts

InitStartScreenText
;     "12345678901234567890123456789012"

!text "TMS9918A timing",LF
!text "test programm",LF
!text LF
!text "(C) 2021 UWE GOTTSCHLING",LF
!text LF
!text 00


;***************************
; TMS9918 Sprite Pattern
;
;***************************

TMS9918InitSpritePattern
		lda #$00
		sta TMS9918REG		; set start address VRAM low Byte
		nop
		nop
		lda #$40 OR $38		; set address maker and high Byte --> Pattern Generator Sub-Block: $3800 
		sta TMS9918REG		; set start address VRAM
		lda #<Sprite_Data	; Low Byte
		sta PTR_String_L
		lda #>Sprite_Data	; High Byte
		sta PTR_String_H		
		ldy #1			; number of pattern 	
TMS9918InitSpritePattern1
		ldx #32			; Byte per pattern		
TMS9918InitSpritePattern2
		lda (PTR_String_L)
		jsr Inc_PTR1
		nop
		nop
		nop
		nop
		sta TMS9918RAM
		dex
		bne TMS9918InitSpritePattern2
		dey
		bne TMS9918InitSpritePattern1
		rts

;***************************
; Wait Vertical Sync Impulse
; Not accurate with V9958 ?
;
;***************************
				
VDP_Wait_V_Sync				
		lda TMS9918REG
		and #$80
		beq VDP_Wait_V_Sync
		rts		

; TMS9918 colors: 
;0 transparent
;1 black
;2 medium green
;3 light green
;4 dark blue
;5 light blue
;6 dark red
;7 cyan
;8 medium red
;9 light red
;10 dark yellow
;11 light yellow
;12 dark green
;13 magenta
;14 gray
;15 white

Sprite_Data
; Sprite 00: 
; Lander
;		 12345678  top left
	!by	%00000001	;1
	!by	%00000111	;2
	!by	%00001111	;3
	!by	%00011100	;4
	!by	%00011100	;5
	!by	%00011111	;6
	!by	%00011110	;7
	!by	%00011111	;8
;		 12345678 bottom left
	!by	%00011110	;1
	!by	%00011111	;2
	!by	%00001110	;3
	!by	%00000111	;4
	!by	%00000100	;5
	!by	%00001000	;6
	!by	%00010000	;7
	!by	%11111000	;8
;		 12345678 top right
	!by	%10000000	;1
	!by	%11100000	;2
	!by	%11110000	;3
	!by	%00111000	;4
	!by	%00111000	;5
	!by	%11111000	;6
	!by	%01111000	;7
	!by	%11111000	;8
;		 12345678 bottom right
	!by	%01111000	;1
	!by	%11111000	;2
	!by	%01110000	;3
	!by	%11100000	;4
	!by	%00100000	;5
	!by	%00010000	;6
	!by	%00001000	;7
	!by	%00011111	;8
	
