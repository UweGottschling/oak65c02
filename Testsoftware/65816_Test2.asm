;Compiler : ACME
;UTF-8 Unicode Transformation Format

;***************************
; Test BRK 65C816 Service Routine
;
;***************************
!src "../Kernel/include/label_definition.asm"
!src "../Kernel/include/Jumptable.asm"
!cpu 65816

*=$0ffe
!word $1000	; start vector
Testit

	clc	
	xce		; native mode
	rep #$30	; accu and index 16 bit	
	!al
	!rl
	lda #$1234 
	sta data
	ldx #$2345
	stx data+2	; write 45 to data and 23 to data+1
	stz data	; write 16 bit zero to data
	sep #$10	; set index to 8 bit
	txa
	sta data	; write 45 to data and 00 to data+1	
	jsr Subroutine1
Testit2	
	sep #$30	; accu and index 8 bit	
	!as
	!rs
	lda #<StartText		; < low byte of expression
	sta PTR_String_L
	lda #>StartText		; > high byte of expression
	sta PTR_String_H
	ldx #OS_Print_String_FN		
	jsr OS_Function_Call	
		
	sec
	xce	; emulation mode
	rts
	
data !word 0,0,0


Subroutine1
	rts

JumpPoint1
	jmp Testit2
	
	
	
StartText
!text LF,"65C816 native mode",LF,00	
