; Screen VRAM Loader.
; Support .SC1, SC2, SC3, SC5, SC6, SC7, SC8, S12
; "MSX SC8/GRP" files begin with fe 00 00 ff d3 00 00.
;    Byte: #FE (Type of file)
;    Word: Begin address of file
;    Word: End address of file
;    Word: Start address of file (Only important when ",R" parameter is defined with BLOAD-command)


!src "../Kernel/include/label_definition.asm"
!src "../Kernel/include/Jumptable.asm"
!cpu 65c02

;Data_RAM = $4000

*=$0FFE
!word $1000 	;--> Start address of this program

Graphic_Main
		jsr Print_Text_SCxx
		jsr Graphic_Open_File
		bcs Graphic_Main_Error1
		jsr Get_Video_Mode
		jsr Test_Print_Result
		bcs Graphic_Main_Error1	
		jsr VDP_Set_Video_Mode
		jsr Graphic_Init_Loop
		bcs Graphic_Main_Error2
		jsr Wait_Key
Graphic_Main2
		lda #TMS9918_Set_Screen0	; Set Text Mode
		jsr VDP_Set_Video_Mode
		ldx #VDP_Init_Pattern_FN
		jsr VDP_Function_Call
		lda #LF
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		rts

Print_Text_SCxx
		lda #<Text_SCxx
		sta PTR_String_L
		lda #>Text_SCxx
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		rts

Graphic_Main_Error1
		jsr DOS_Print_File_Not_Found
		rts
Graphic_Main_Error2
		lda #0	; Set Text Mode
		jsr VDP_Set_Video_Mode
		ldx #VDP_Init_Pattern_FN
		jsr VDP_Function_Call		
		jsr DOS_Print_Read_Error
		lda #LF
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		rts 

Text_SCxx
!text "MSX screen dump loader V0.1",LF
!text "Support .SC1 .SC2 .SC3 .SC5 .SC6 .SC7 .SC8 .S12",LF,"Load .SXX file",LF
!text "Name (8.3):",00

;***************************
; Wait PS2 Key Press
;
;***************************

Wait_Key
		ldx #PS2_ChkKey_FN
		jsr PS2_Function_Call
		bcc Wait_Key
		ldx #PS2_GetKey_FN
		jsr PS2_Function_Call
		rts

;***************************
; VDP_Set_Video_Mode
; A = VDP Screen Mode
;***************************

VDP_Set_Video_Mode
		ldx #VDP_Set_Video_Mode_FN
		jsr VDP_Function_Call
		rts
	
;***************************
; Print result for screen mode
;
;***************************

Test_Print_Result
		pha
		bcs Test_Print_Result2
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call
		lda #<Text_Test_Result_OK
		sta PTR_String_L
		lda #>Text_Test_Result_OK
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call	
		pla
		clc
		rts

Test_Print_Result2
		lda #<Text_Test_Result_NOK
		sta PTR_String_L
		lda #>Text_Test_Result_NOK
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		pla
		sec
		rts

Text_Test_Result_OK
!text LF,"Screen mode OK",LF
!text 00	

Text_Test_Result_NOK
!text LF,"Screen mode not OK",LF
!text 00	

;***************************
; Main loop
; Copy data to screen memory
;***************************

Graphic_Init_Loop
		ldx #7	
Graphic_Init_Loop1
		phx
		ldx #DOS_Load_Byte_FN
		jsr DOS_Function_Call
		bcs Graphic_Loop_Exit
		plx
		dex
		bne Graphic_Init_Loop1
		lda #$00
		sta TMS9918REG		; Set start address VRAM low Byte
		lda #$40			; Set address maker and high byte --> $0000
		sta TMS9918REG		; Set start address VRAM
Graphic_Loop2
		ldx #DOS_Load_Byte_FN
		jsr DOS_Function_Call
		sta TMS9918RAM
		bcs Graphic_Loop_Exit
Graphic_Loop3
		bra Graphic_Loop2
Graphic_Loop_Exit
		cmp #00				; Is it end of file ?
		bne Graphic_Loop_Error_Exit
		ldx #DOS_Close_File_FN
		jsr DOS_Function_Call
		clc
		rts
Graphic_Loop_Error_Exit		
		ldx #DOS_Close_File_FN
		jsr DOS_Function_Call
		sec
		rts

;***************************
; Open file
;
;***************************

Graphic_Open_File
		ldx #DOS_Init_FN
		jsr DOS_Function_Call
		ldx #DOS_LEditor_FN
		jsr DOS_Function_Call
		ldx #DOS_Open_File_FN
		jsr DOS_Function_Call
		bcs Graphic_Open_File_Exit
		jsr DOS_Print_File_Found
		clc
		rts
Graphic_Open_File_Exit
		rts

;***************************
; Detect video mode from filename extension
;
;***************************

Get_Video_Mode
		lda #<Text_Select_Screenmode
		sta PTR_String_L
		lda #>Text_Select_Screenmode
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		ldx #0		; counter for Text_Supported_Modes
Get_Video_Mode_1
		ldy #0 		; counter for filename extension
Get_Video_Mode_2
		lda Text_Supported_Modes,x
		beq Get_Video_Mode_Exit_Error	; if "00" end of text
		cmp #LF				; Skip LF
		beq Get_Video_Mode_3
		lda FCB_1+FCB_File_Name+8,y
		cmp Text_Supported_Modes,x
		beq Get_Video_Mode_4	  
Get_Video_Mode_3		
		inx
		ldy #0
		bra Get_Video_Mode_2
Get_Video_Mode_4		
		inx
		iny
		cpy #3
		beq Get_Video_Mode_5
		bra Get_Video_Mode_2
Get_Video_Mode_5	; All 3 chars are equal
		txa
		pha
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call
		lda #LF
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		pla
		lsr
		lsr
		tax	
		lda Select_Video_Modes,x
		pha
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call
		lda #LF
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		pla
		clc
		rts
Get_Video_Mode_Exit_Success
		clc
		rts
Get_Video_Mode_Exit_Error
		sec
		rts
Text_Select_Screenmode

!text "Supported file extensions:",LF
Text_Supported_Modes
!text "SC1",LF	; screen mode 2, 256x192, 2 of 16 color, 32 * 24 patterns, graphic 1 mode
!text "SC2",LF	; screen mode 2, 256x192, 2 of 16 color, 32 * 24 patterns, graphic 2 mode
!text "SC3",LF	; screen mode 3, 64x48, 16 color, multi color mode
!text "SC5",LF	; screen mode 5, 256x212, 16 color, graphic 4 (G4) mode 
!text "SC6",LF	; screen mode 6, 512x212, 4 color, graphic 5 (G5) mode
!text "SC7",LF	; screen mode 7, 512x212, 16 color, graphic 6 (G6) mode
!text "SC8",LF	; screen mode 8, 256x212, 256 color, graphic 7 (G7) mode
!text "S12",LF	; screen mode 12, 256x192, 19268 color
!text 00

Select_Video_Modes
!by 01	; screen mode 1
!by 02	; screen mode 2
!by 03	; screen mode 3
!by 05	; screen mode 5
!by 06	; screen mode 6
!by 07	; screen mode 7
!by 08	; screen mode 8
!by 12	; screen mode 12

;***************************
; Error messages
;
;***************************

DOS_Print_File_Not_Found
		lda #<TypeNotFound_Text
		sta PTR_String_L
		lda #>TypeNotFound_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		sec
		rts
TypeNotFound_Text
!text "File not found",LF,00

DOS_Print_File_Found
		lda #<TypeFound_Text
		sta PTR_String_L
		lda #>TypeFound_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		rts
TypeFound_Text
!text "File found",LF,00

DOS_Print_Read_Error
		lda #<TypeReadError_Text
		sta PTR_String_L
		lda #>TypeReadError_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		sec
		rts
TypeReadError_Text
!text "Read error",LF,00


;Content of disk 
;ANDROGYN SC5     32775 2018-12-09  22:18  androgyn.SC5
;BC_QUEST SC1     16391 2018-12-15  18:11  BC_quest.SC1
;DIGDUG   SC1     16391 2018-12-15  12:04 
;HERO     SC2     16391 2018-11-29  21:05 
;JAPAN    SC3     16391 2018-12-16  11:03  japan.SC3
;MONTY    SC2     16391 2018-11-29  20:52  Monty.SC2
;ZAXXON   SC2     16391 2018-12-15  17:33  zaxxon.SC2
;XPAWN2   S12     54279 2018-04-24  21:55 
;MONKEY   SC8     54279 2018-05-14  19:22  monkey.SC8
;MALAIKA  SC2     16391 2018-04-02  19:17  malaika.sc2
;81JRJTGR SC6     27143 2018-12-09  23:03 
;SOFA     SC7     54279 2018-12-09  23:12  Sofa.SC7
;ZAXXON   SC1     16391 2018-12-15  17:33  zaxxon.SC1
;DIGDUG2  SC1     16391 2018-12-15  17:36 
;NMSXTI~1 SC2     14343 2018-12-17  20:17  nMSXtiles.sc2
;NMSXTI~2 SC2     14343 2018-12-17  20:21  nMSXtiles2.sc2
;PACMAN   SC2     14343 2018-12-17  20:33  pacman.sc2
;LABELDEF ASM      9965 2018-12-30  21:09  labeldef.asm
;XMAS0    TXT      8233 2019-01-03  18:24  xmas0.txt
;XMAS1    TXT      8861 2019-01-03  18:24  xmas1.txt
;HELLO_~1 O65        30 2019-12-01  17:38  Hello_World.o65
;DOS      ASM     48178 2019-03-03  11:14  DOS.asm
;blitt    o65       970 2020-12-29  23:44 
;BELL     TXT        22 2019-05-21  20:24  bell.txt
;LABEL_~1 ASM     13696 2019-05-21  22:19  label_definition.asm
;SXX_LO~1 O65       604 2021-01-06  22:37  Sxx_Loader.o65


