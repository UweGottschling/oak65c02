

!src "../Kernel/include/label_definition.asm"
!src "../Kernel/include/Jumptable.asm"


*=$0ffe
!word $1000	; start vector
Main

		lda #<HelloWorld_Text	; < low byte of expression
		sta PTR_String_L
		lda #>HelloWorld_Text	; > high byte of expression
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		rts

HelloWorld_Text
!text LF
!text "Hello World!",LF
!text 00
