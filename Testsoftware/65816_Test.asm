;Compiler : ACME
;UTF-8 Unicode Transformation Format

;***************************
; Test BRK 65C816 Service Routine
;
;***************************
!src "../Kernel/include/label_definition.asm"
!src "../Kernel/include/Jumptable.asm"
!cpu 65816

*=$1ffe
!word $2000	; start vector
Testit
;		lda #<BRKService816_1
;		sta BRKService816Vector_L
;		lda #>BRKService816_1
;		sta BRKService816Vector_H
		clc
		xce
		!al		; Accumulator long (ACME compiler)
		!rl		; Register long (ACME compiler)
		rep #$30	; Set all CPU registers to 16-bit
		lda #$1234	; Load some number to check the brk routine

		ldx #$5678
		ldy #$9ABC
		sei		; Set I=1
		sec		; Set carry
		lda #$4321
		tcd
		sep #$20	; 8 bit accumulator
		!as
		lda #$aa
		pha
		plb		; set data bank register to $aa
		rep #$30	; Set all CPU registers to 16-bit
		brk
		brk

BRKService816_1				; PB Register is set to zero after BRK
		!CPU 65816	; Push direct register to stack and set direct register to 0
		rep #$30	; 16 bit all registers
		!al
		!rl
		phb		; push data bank register on stack (8-Bit)
		phd		; push direct register on stack (16-Bit)
		pea $0000
		pld
;		jmp (BRKService816Vector_L)
BRKService816_2
		sta $000000+ARegister	; $000000+ --> for telling ACME that this is a 24 bit address
		txa
		sta $000000+XRegister
		tya
		sta $000000+YRegister
		pla
		sta $000000+DRegister
		sep #$20	; 8 bit accumulator
		!as
		lda #$00
		pha
		plb		; set data bank register to $00
		pla
		sta DBRegister
		pla
		sta PRegister
		plx
		stx PCLRegister
		pla
		sta PBRegister
		tsx
		stx SPRegister
		sec ; SEt Carry bit
		xce ; eXchange Carry and Emulation bit
		!CPU 65c02
		lda #<BRKTXT65816	; < low byte of expression
		sta PTR_String_L
		lda #>BRKTXT65816	; > high byte of expression
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		ldy #0
BRKService816_3
		lda ARegister,y
		phy
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call
	;	jsr Print_Bin2Hex	; Print Hex Byte to stdout
		lda #SP
	;	jsr Print_CHR
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		ply
		iny
		cpy #DRegister_H-ARegister+1
		bne BRKService816_3
		lda #<SRF65816	; < low byte of expression
		sta PTR_String_L
		lda #>SRF65816	; > high byte of expression
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		lda PRegister
;		jsr Print_BIN
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call
BRKService816_Exit
;		ldx #$ff
;		txs		; init stackpointer
		cli
		;jmp Reset1 	; back to system
		brk

BRKTXT65816
!text CR,"Native mode break routine, enter emulation mode",CR
!text "Register content",CR
!text "A  B  XL XH YL YH SL SH PL PH DB PB DL DH",CR,00
SRF65816
!text CR,"NVMXDIZC",CR,00


