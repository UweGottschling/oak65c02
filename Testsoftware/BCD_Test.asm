; Test program 
; 32 - bit count up, display decimal  

!src "../Kernel/include/label_definition.asm"
!src "../Kernel/include/Jumptable.asm"
!cpu 65c02

;Data_RAM = $4000

Test = $50
PTR_Test = $60
*=$0FFE
!word $1000 	;--> Start address of this program

Main
		lda #<Start_Text
		sta PTR_String_L
		lda #>Start_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		lda #$FE
		sta Test
		sta Test+1
		sta Test+2
		sta Test+3
		lda #<Test
		sta <PTR_Test
		lda #>Test
		sta >PTR_Test
Main2	
		inc Test
		bne Main3
		inc Test+1
		bne Main3
		inc Test+2
		bne Main3
		inc Test+3
Main3
		jsr PrintSize
		jmp Main2
		
PrintSize
		ldy #0	; Offset to file size
		ldx #0
PrintSize20
		lda (PTR_Test),y	; Convert file size to BCD
		sta Operand1,x
		iny
		inx
		cpx #4			; 4 Byte file size
		bne PrintSize20
		ldx #OS_Bin2BCD32_FN
		jsr OS_Function_Call
PrintSize21
		jsr Print_File_Size_SP	; Print file size
		rts

;******************
; Print BCD (32-bit), 0 fill with space except the last, special for file size
; input: BCD format Operant5 .. Operand 9
;******************

Print_File_Size_SP
		ldx #0
Print_File_Size_SP1
		lda Operand5,x
		and #$F0
		bne Print_File_Size_SP7
		jsr Print_File_Size_Space
Print_File_Size_SP3
		lda Operand5,x
		and #$0F
		bne Print_File_Size_SP9
		cpx #4
		beq Print_File_Size_SP9
		jsr Print_File_Size_Space
Print_File_Size_SP4
		inx
		cpx #5
		bne Print_File_Size_SP1
		rts
Print_File_Size_SP6		;Normal output
		lda Operand5,x
		and #$F0
Print_File_Size_SP7
		lsr
		lsr
		lsr
		lsr
		ora #$30
		jsr Print_File_Size_SP11
Print_File_Size_SP8
		lda Operand5,x
		and #$0F
Print_File_Size_SP9
		ora #$30
		jsr Print_File_Size_SP11
		inx
		cpx #5
		bne Print_File_Size_SP6
		rts
Print_File_Size_Space
		phx
		lda #SP
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		plx
		rts
Print_File_Size_SP11
		phx
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		plx
		rts

Start_Text
!text "BCD Test program",LF,00


