; (C) 2021 Matt Heffernan 
; https://github.com/SlithyMatt/multi-mandlebrot
; Modification Oak65C02 Uwe Gottschling 2021
; TMS9918 screen mode 3 

NameTableBase		= $800 ; 32*24=768 bytes --> $800 to $AFF
PatternBase		= $000 ; 32*48=1536 bytes --> $000 to $600
SpriteAttrBase		= $1B00	; max. 128 bytes
Vaddress		= $58	; word
Vread			= $5A	; byte
Vwrite			= $5B	; byte
MAND_MAX_IT 		= 15
!cpu 65c02
*=$0FFE
!word $1000 	;--> Start address of this program

   jmp start


!src "../Kernel/include/label_definition.asm"
!src "../Kernel/include/Jumptable.asm"



RANDSEED=$60	;64-bit 
RANDBYTE=$64


start
	jsr StartMessage
	jsr SETRANDOMSEED
	ldx #255
@loop
	phx
	jsr GETRANDBYTE
	lda RANDBYTE
	jsr PrintHex
	plx	
	dex
	bra @loop
	cli
	jsr WaitKey
	rts

StartMessage
	lda #<StartText; < low byte of expression
	sta PTR_String_L
	lda #>StartText	; > high byte of expression
	sta PTR_String_H
StartMessage2	
	ldx #OS_Print_String_FN		
	jsr OS_Function_Call		
WaitKey	
	ldx #PS2_GetKey_FN
	jsr PS2_Function_Call
	rts	
EndMessage
	lda #<PressKey; < low byte of expression
	sta PTR_String_L
	lda #>PressKey	; > high byte of expression
	sta PTR_String_H
	jmp StartMessage2
	rts	

SETRANDOMSEED
	lda #$12
	sta RANDSEED
	lda #$34
	sta RANDSEED+1
	lda #$56
	sta RANDSEED+2
	lda #$78
	sta RANDSEED+3
	rts

RANDBIT
	lsr	RANDSEED	;shift right one bit
	ror	RANDSEED+1
	ror	RANDSEED+2
	ror	RANDSEED+3
	php			;save carry (output) bit	
	bcc	randbitdone	;don't xor if lsb is 0
	lda 	RANDSEED	;xor with 0xD0000001
	eor	#$D0		;(x^32 + x^31 + x^29 + x + 1)
	sta 	RANDSEED
	lda 	RANDSEED+3
	eor	#$01		
	sta 	RANDSEED+3
randbitdone	
	plp
	rts
	
GETRANDBYTE
	ldx #8
GETRANDBYTE2
	jsr RANDBIT
	ror RANDBYTE
	dex
	bne GETRANDBYTE2
RANDBYTEExit		
	rts
		
PrintHex		
	ldx #OS_Print_Bin2Hex_FN
	jsr OS_Function_Call
	rts

	

StartText
!text LF,"Linear-feedback shift register",LF
PressKey
!text "Press any key",LF,00



