; (C) 2021 Uwe Gottschling
; YMF262 percussion test


!src "../Kernel/include/label_definition.asm"
!src "../Kernel/include/Jumptable.asm"
!src "../Kernel/DOS/DOS_FAT_constant.asm"

;Zero page
PatternStartL	=$60
PatternStartH	=$61


!cpu 65c02

*=$0FFE			; Programme start address header
!word $1000		

Main
		jsr StartMessage
		jsr YMF262_Reset
		jsr YMF262Init
		jsr MainLoop
		jsr YMF262_Reset
		rts
		
;***************************
; Loop press key
;***************************

MainLoop		
		ldx #$ff
MainLoop2
		dex
		bne MainLoop2		; littel delay for trigger off
		jsr YMF262_Trig_OFF	
		ldx #PS2_GetKey_FN
		jsr PS2_Function_Call
;		pha

;		pla
		cmp #"5"
		bne MainLoop5
		jsr LoadPattern1
		bra MainLoop		
MainLoop5		
		cmp #"4"
		bne MainLoop10
		jsr YMF262_BassDrum_ON	
		bra MainLoop		
MainLoop10
		cmp #"3"
		bne MainLoop15
		jsr YMF262_SnareDrum_ON
		bra MainLoop	
MainLoop15
		cmp #"2"
		bne MainLoop20
		jsr YMF262_TomTom_ON
		bra MainLoop
MainLoop20
		cmp #"1"
		bne MainLoop25
		jsr YMF262_Cymbal_ON
		bra MainLoop
MainLoop25
		cmp #"0"
		bne MainLoop30
		jsr YMF262_HiHat_ON
		bra MainLoop
MainLoop30		
		cmp #"6"
		bne MainLoop40
		jsr LoadPattern2
		bra MainLoop		
MainLoop40		
		cmp #"7"
		bne MainLoop45
		jsr LoadPattern3
		bra MainLoop			
MainLoop45		
		cmp #"8"
		bne MainLoop50
		jsr LoadPattern4
		bra MainLoop			
MainLoop50
		cmp #"x"
		bne MainLoop
		rts
		
; Register $BD 		
; D4 = Bass Drum = $10
; D3 = Snare Drum = $08
; D2 = Tom Tom = $04
; D1 = Cymbal = $02
; D0 = Hi-Hat = $01
		
;***************************
;  Bass Drum channel
;***************************		

YMF262_BassDrum_ON	
		lda #$BD
		sta YMF262_ADDR1
		lda #$20+$10
		sta YMF262_DATA1	; Turn the voice on; set the octave and freq MSB
		rts
		
;***************************
; Snare Drum channel
;***************************

YMF262_SnareDrum_ON
		lda #$BD
		sta YMF262_ADDR1
		lda #$20+$08
		sta YMF262_DATA1	; Turn the voice on; set the octave and freq MSB
		rts

;***************************
;  Tom-Tom channel
;***************************

YMF262_TomTom_ON
		lda #$BD
		sta YMF262_ADDR1
		lda #$20+$04
		sta YMF262_DATA1	; Turn the voice on; set the octave and freq MSB
		rts		
		
;***************************
;  Cymbal channel
;***************************

YMF262_Cymbal_ON
		lda #$BD
		sta YMF262_ADDR1
		lda #$20+$02
		sta YMF262_DATA1	; Turn the voice on; set the octave and freq MSB
		rts		

;***************************
;  Hi-Hat channel
;***************************

YMF262_HiHat_ON
		lda #$BD
		sta YMF262_ADDR1
		lda #$20+$01
		sta YMF262_DATA1	; Turn the voice on; set the octave and freq MSB
		rts				

;***************************
; Sound Off
;***************************

YMF262_Trig_OFF
		lda #$BD
		sta YMF262_ADDR1
		lda #$20
		sta YMF262_DATA1
		rts

;***************************
; Config YMF262
;
;***************************

YMF262Init
;		lda #$05
;		sta YMF262_ADDR2
;		lda #$01			
;		sta YMF262_DATA2	; OPL3 Mode
;		ldy #$00
YMF262Init10
		lda YMF262_Instrument_Noise,y
		sta YMF262_ADDR1
		iny
		lda YMF262_Instrument_Noise,y
		sta YMF262_DATA1
		iny
		cpy #YMF262_Instrument_Noise_End-YMF262_Instrument_Noise
		bne YMF262Init10
		rts		

YMF262_Instrument_Noise
; The groupings of twenty-two registers (20-35, 40-55, etc.) 
; have an odd order due to the use of two operators for each FM voice. 
; The following table shows the offsets within each group of registers for each operator.

;    Channel	1	2 	3	4	5 	6	7	8 	9
;    Operator 1	00	01 	02	08	09 	0A	10	11 	12
;    Operator 2	03	04 	05	0B	0C 	0D	13	14 	15

; General settings
		!by $BD,$20			; Bit 5: 1= Percussion Mode 
		!by $03,$3C			; Timer 2 Data = 195 * 0.320msec (255-195=$3C)		

;Channel 6: bass drum ($2A,$2D)
		; Register, Data 
		!by $B0+$06,$02			; Turn the voice off; set the octave and freq MSB
		; Operator 1 (modulator): bass drum
		!by $20+$10,$00			; Set the modulator's multiple, Tremolo, Vibrator
		!by $40+$10,$8C			; Set the modulator's Key-scaling and Output-level: 0 = loudest
		!by $60+$10,$F6			; Modulator attack: F is the fastest; decay :F is the fastest
		!by $80+$10,$F8			; Modulator sustain: 0 is the loudest; release: F is the fastest
		!by $E0+$10,$00			; Set Modulator wave form: 
		; Operator 2 (carrier): bass drum
		!by $20+$13,$01			; Set the carrier's multiple, vibrator, tremolo
		!by $40+$13,$00			; Set the carrier Key Scaling and volume; 0 = loudest
		!by $60+$13,$F7			; Carrier attack: F is the fastest ; decay: F is the fastest
		!by $80+$13,$F8			; Carrier sustain: 0 is the loudest; release: F is the fastest
		!by $E0+$13,$00			; Set Carrier wave form: 0 to 3 work, only 0 usefull
		; Channel 6
		!by $A0+$06,$31			; Set voice frequency's LSB
		!by $C0+$06,$04+$00		; Set Feedback, Decay Alg: Only FM mode usefull

;Channel 7: hi-hat, snare
		!by $B0+$07,$03 		; Turn the voice off; set the octave and freq MSB
		; Operator 1: hi-hat
		!by $20+$11,$03			; Set the modulator's multiple, Tremolo, Vibrator
		!by $40+$11,$00			; Set the modulator's Key-scaling and Output-level: 0 = loudest
		!by $60+$11,$F4			; Modulator attack: F is the fastest; decay :F is the fastest
		!by $80+$11,$08			; Modulator sustain: 0 is the loudest; release: F is the fastest
		!by $E0+$11,$01			; Set Modulator wave form: 0 to 3 work, 0 to 1 sound realistic 
		; Operator 2: snare
		!by $20+$14,$03			; Set the carrier's multiple 1x, vibrator, tremolo
		!by $40+$14,$00			; Set the carrier Key Scaling and volume; 0 = loudest
		!by $60+$14,$F5			; Carrier attack: F is the fastest ; decay: F is the fastest
		!by $80+$14,$08			; Carrier sustain: 0 is the loudest; release: F is the fastest
		!by $E0+$14,$01			; Set Carrier wave form: 0 to 2 work here.
		; Channel 7
		!by $A0+$07,$61			; Set voice frequency's LSB
		!by $C0+$07,$00			; Set Feedback, Decay Alg

;Channel 8: tom-tom, symbal
		!by $B0+$08,$03 		; Turn the voice off; set the octave and freq MSB
		; Operator 1: tomtom
		!by $20+$12,$05			; Set the modulator's multiple, Tremolo, Vibrator
		!by $40+$12,$00			; Set the modulator's Key-scaling and Output-level: 0 = loudest
		!by $60+$12,$F7			; Modulator attack: F is the fastest; decay :F is the fastest
		!by $80+$12,$07			; Modulator sustain: 0 is the loudest; release: F is the fastest
		!by $E0+$12,$00			; Set Modulator wave form: 0 to 3 work here, only 0 usefull
		; Operator 2: cymbal
		!by $20+$15,$01+$40+$80	; Set the carrier's multiple 1x, vibrator, tremolo
		!by $40+$15,$00			; Set the carrier Key Scaling and volume; 0 = loudest
		!by $60+$15,$F4			; Carrier attack: F is the fastest ; decay: F is the fastest
		!by $80+$15,$06			; Carrier sustain: 0 is the loudest; release: F is the fastest
		!by $E0+$15,$00			; Set Carrier wave form: only 0 and 1 work here, 0 most sound realistic 
		; Channel 8
		!by $A0+$08,$F0			; Set voice frequency's LSB
		!by $C0+$08,$00			; Set Feedback, Decay Alg
		
YMF262_Instrument_Noise_End

;Percussion Mode
;In this mode 6 operators are used to produce five different percussion instruments:
;    Bass Drum (2 operators) Slot 13,16
;    Snare Drum (1 operator) Slot 17
;    Tom-Tom (1 operator) Slot 15
;    Cymbal (1 operator) Slot 18
;    Hi-Hat (1 operator) Slot 14	
; Based on notes from MAME's OPL emulator, the following points hold true:
;    Setting the block/fnum on each percussive channel may affect multiple instruments.
;        The pitch on channel 7 affects just the bass drum
;        The pitch on channel 8 affects the hihat, cymbal and snare
;        The pitch on channel 9 affects the hihat, cymbal and tomtom
;    The modulator/carrier output level is used to set the volume of each instrument.
;        channel 7 carrier controls the volume of the bass drum
;        channel 8 modulator controls the volume of the hi-hat
;        channel 8 carrier controls the volume of the snare
;        channel 9 modulator controls the volume of the tomtom
;        channel 9 carrier controls the volume of the cymbal
;    Panning L/R bits set via register 0xC0 affect instruments as follows:
;        channel 7 affects the bass drum
;        channel 8 affects hi-hat and snare
;        channel 9 affects cymbal and tomtom
;
; The AdLib Programmer's Manual contains playback code that uses the Tom-Tom frequency for all percussion instruments. 
; It is possible this is the source of the confusion. Here, when a note is played on the Tom-Tom channel, 
; that note will of course cause the Top Cymbal to change as well because of the shared channel. 
; But the code increases this pitch by seven semitones and sets that frequency for channel 7 
; at the same time, ensuring the Hi-Hat and Snare Drum are always set to seven semitones above the 
; last Tom-Tom note. (See AdLib MIDI Format for details.) 
;	┌────────────┐         ┌────────────┐
;	│            │         │            │
;	│ Operator 1 ├────────>│ Operator 2 ├────────> Output
;	│(Modulator) │         │ (Carrier)  │
;	└────────────┘         └────────────┘

;***************************
; Reset YMF262
; input: -
; output: -
; used: A,Y 
;***************************

YMF262_Reset
		lda #$05
		sta YMF262_ADDR2
		lda #$01			
		sta YMF262_DATA2	; OPL3 Mode
		ldy #$B0			; Key off 	
YMF262_Reset1		
		sty YMF262_ADDR1
		stz YMF262_DATA1
		sty YMF262_ADDR2
		stz YMF262_DATA2			
		iny
		cpy #$B9
		bne YMF262_Reset1
		ldy #$01
		
YMF262_Reset6
;		jsr debug
		sty YMF262_ADDR1
		stz YMF262_DATA1
	
		sty YMF262_ADDR2
		stz YMF262_DATA2	
		iny
		cpy #$F6
		bne YMF262_Reset6

		lda #$05
		sta YMF262_ADDR2
		stz YMF262_DATA2	; Set to OPL2 Mode
;		jsr VGM_Wait_62
		rts		

;***************************
; Print start text
;
;***************************
		
StartMessage		
		lda #<StartText; < low byte of expression
		sta PTR_String_L
		lda #>StartText	; > high byte of expression
		sta PTR_String_H
		ldx #OS_Print_String_FN		
		jsr OS_Function_Call		
		rts	
		
StartText
!text LF,"YMF262 percussion test program",LF
!text "press keys:",LF
!text "8 = Play Pattern4",LF
!text "7 = Play Pattern3",LF
!text "6 = Play Pattern2",LF
!text "5 = Play Pattern1",LF
!text "4 = Bass Drum",LF
!text "3 = Snare Drum",LF
!text "2 = Tom Tom",LF
!text "1 = Cymbal",LF
!text "0 = Hi-Hat",LF
!text "x = exit",LF,00

;bit 4: BD On. KEY-ON of the Bass Drum channel.
;bit 3: SD On. KEY-ON of the Snare Drum channel.
;bit 2: TT On. KEY-ON of the Tom-Tom channel.
;bit 1: CY On. KEY-ON of the Cymbal channel.
;bit 0: HH On. KEY-ON of the Hi-Hat channel.

;***************************
;
;***************************
LoadPattern1
		lda #<DrumPattern1
		sta PatternStartL
		lda #>DrumPattern1
		sta PatternStartH
		jmp PlayPattern

LoadPattern2	
		lda #<DrumPattern2
		sta PatternStartL
		lda #>DrumPattern2
		sta PatternStartH
		jmp PlayPattern
LoadPattern3		
		lda #<DrumPattern3
		sta PatternStartL
		lda #>DrumPattern3
		sta PatternStartH
		jmp PlayPattern
LoadPattern4		
		lda #<DrumPattern4
		sta PatternStartL
		lda #>DrumPattern4
		sta PatternStartH
		jmp PlayPattern		
		
;***************************
; Print start text
;
;***************************

PlayPattern
		lda PatternStartL
		sta PTR_R_L
		lda PatternStartH
		sta PTR_R_H
PlayPattern1
		jsr PS2_ChkKey
		bcs PlayPattern_exit
;		lda #$1
;		jsr MusicDelay
PlayPattern2	
		lda #$BD 
		sta YMF262_ADDR1
		lda (PTR_R_L)
		cmp #$FF
		beq PlayPattern
		and #%00011111
		ora #%00100000
		sta YMF262_DATA1
PlayPattern11
		lda #$3
		jsr MusicDelay
		bcs PlayPattern_exit
		lda #$BD 
		sta YMF262_ADDR1
		lda #$20
		sta YMF262_DATA1
		jsr Read_Inc_PTR_R  
		bra PlayPattern1
PlayPattern_exit
		rts


;***************************
; DrumPattern
; $FF = end of pattern
;   +-7-+-6-+-5-+-4-+-3-+-2-+-1-+-0-+
;   |Tre|Vib|Per|BD |SD |TT |CY |HH |
;   +---+---+---+---+---+---+---+---+
;***************************

DrumPattern1

!by  %00010001
!by  %00000001
!by  %00001001
!by  %00000011
!by  %00010001
!by  %00000001
!by  %00001001
!by  %00000011
!by  $FF

DrumPattern2
!by  %00010001
!by  %00000010
!by  %00011001
!by  %00000110
!by  %00010001
!by  %00000010
!by  %00011001
!by  %00000110
!by  $FF

DrumPattern3
;        BSTCH
!by  %00010000
!by  %00000001
!by  %00001000
!by  %00000001
!by  %00010000
!by  %00000001
!by  %00001000
!by  %00000001
!by  %00010000
!by  %00000001
!by  %00001000
!by  %00000001
!by  %00010000
!by  %00001001
!by  %00001000
!by  %00000001
!by  $FF

DrumPattern4
;        BSTCH
!by  %00010001
!by  %00000000
!by  %00001001
!by  %00000000
!by  %00010011
!by  %00000000
!by  %00001001
!by  %00000000
!by  %00010001
!by  %00000000
!by  %00001011
!by  %00000000
!by  %00010001
!by  %00001000
!by  %00001001
!by  %00000000
!by  $FF

;***************************
; Music delay ca. 65ms
; A = number of repeats
;***************************	

MusicDelay
		tax
MusicDelay4
		lda #$04			; Reset IRQ Flag, alle other bits are ignored
		sta YMF262_ADDR1
		lda #$80
		sta YMF262_DATA1 
		
		lda #$04			; Start timer2
		sta YMF262_ADDR1
		lda #$02
		sta YMF262_DATA1 		
MusicDelay10
		lda YMF262_ADDR1	; Read status timer 2
		and #$20
		beq MusicDelay10
		dex
		bne MusicDelay4	
		lda #$04			; Stop timer 2
		sta YMF262_ADDR1
		lda #$00
		sta YMF262_DATA1 
		rts

;***************************
; Check for key press
;***************************

PS2_ChkKey
		ldx #PS2_ChkKey_FN
		jsr PS2_Function_Call
		rts

;***************************
; Increment PTR_R_L/_H and read from pointer
;
;***************************

Read_Inc_PTR_R    	
		inc PTR_R_L			; increments ptr2
		bne Read_Inc_PTR_R1
		inc PTR_R_H      
Read_Inc_PTR_R1      
		lda (PTR_R_L)
		rts		
		
; ¯\_(ツ)_/¯

