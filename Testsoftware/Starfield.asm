;********************************
;     STARFIELD-SIMULATION      *
;                               *
;      BY MARC GOLOMBECK        *
;                               *
;   VERSION 1.00 / 02.04.2018   *
;                               *
; Converted to my homebrew      *
; computer 2021 Uwe Gottschling *
;********************************
;
; 	DSK 	starfield3D
; 	MX   	%11

!src "../Kernel/include/label_definition.asm"
!src "../Kernel/include/Jumptable.asm"
!src "../Kernel/DOS/DOS_FAT_constant.asm"

; Mode 2 TMS 9918A --> 256 x 192 pixels

PATTERNBASE	= $0000 ; $0000 ... $69FF
SPRITEATTRBASE	= $1B00	; max. 128 bytes
SPRITEPATTBASE	= $3800	; max. 2048 byes
	
XPOS		=$50
YPOS		=$51


!cpu 65c02
*=$0FFE
!word $1000 	;--> Start address of this program
;
Temp	= $FD
adrl	= $FE	; + $FF
adrh	= $FF
PRNG	= $60	; pseudo random number generator EOR-val
MATOR	= $61
ANDMSK	= $62
REPEATS	= $63
PSLO	= $D8	; USING FAC-ADRESS RANGE
PSHI	= $DA	; FOR POINTER IN MULT-TAB
PDLO	= $DC	; USING ARG-ADRESS RANGE
PDHI	= $DE	; FOR POINTER IN MULT-TAB
posx	= $DF
posy	= $F0

;HCLR	= $F3F2	; CLEAR HIRES SCREEN TO BLACK1
;WAIT	= $FCA8	; wait a bit

INIT	
;	STA $C010	; delete keystrobe
;	LDA $C050	; text
;	LDA $C054	; page 1
;	LDA $C052	; mixed off
;	LDA $C057	; hires
;	LDA #32
;	STA $E6		; DRAW ON 1
	JSR HCLR	; clear screen
	STZ PRNG
	LDA #SSQLO/256	; SETUP multiplication tables
	STA PSLO+1
	LDA #SSQHI/256
	STA PSHI+1
       	LDA #DSQLO/256
	STA PDLO+1
	LDA #DSQHI/256
	STA PDHI+1

;MAIN
_BP1	
	LDX #60		; number of stars
_BP	
	DEC STAR_Z,X	; decrease star z-distance
	BMI _reset	; reset Z-distance 
	TXA
	AND #%00000011	; every fourth star has double the z-speed
	BNE _noDEC1
	DEC STAR_Z,X
	BMI _reset
	TXA
_noDEC1		
	AND #%00000111	; every eigth star has triple the z-speed
	BNE _noDEC2
	DEC STAR_Z,X
	BMI _reset
	DEC STAR_Z,X
	BMI _reset
_noDEC2		
	LDA #1		; slow down value for WAIT-routine
	JSR WAIT	; slow down the animation, approx 660 cycles for A=10
	BRA _action	; move a star
_cont		
	DEX	
	BPL _BP	
	BRA _BP1
_reset		
	LDA #30
	STA STAR_Z,X
	LDA Temp	; calculate new star base speed
	ADC STAR_X,X
	ASL
	BEQ noEOR1
	BCC noEOR1
	INC PRNG
	EOR PRNG	; pseudo random number generation
noEOR1		
	;STA Temp	
	BNE noFIX1	; avoid zero value
	ADC PRNG
noFIX1		
	STA STAR_X,X	; save generated pseudo random star base speed
	ADC STAR_Y,X
	ASL
	BEQ noEOR2
	BCC noEOR2
	EOR PRNG	; pseudo random number generation
noEOR2		
	STA Temp
	BNE noFIX2
	ADC PRNG
noFIX2		
	STA STAR_Y,X	; save generated pseudo random star base speed
	JMP _cont		
;			
_action 	
	PHX			; save X index
	LDA STAR_PLOT_Y,X	; move Star y pos to A-reg
	sta YPOS
;	LDA YLOOKLO,Y		; 
;	STA ADRHIR		; calc HIRES line bas address
;	LDA YLOOKHI,Y		; 
;	ORA #$20		; draw on page 1
;	STA ADRHIR+1		; 
	LDA STAR_PLOT_X,X
	sta XPOS 
;	TAX
;;LOTABLE2  	
;	LDY DIV7LO,X
;   	LDA MOD7LO,X
;;GOTTAB2   	
;	TAX
;   	LDA CLRMASK,X		; CLRMASK: pixel masks for clearing a pixel 
;   	STA ANDMSK
;   	LDA (ADRHIR),Y
;   	AND ANDMSK
;   	STA (ADRHIR),Y
	jsr ClearPlot
	PLX
	PHX			; calc XPLOT = STAR_X/STAR_Z
	LDA STAR_X,X		; can be a signed value here
	TAY
	LDA STAR_Z,X
	TAX
	LDA PROJTAB,X		; PROJTAB: table for 1/Z-calculus
	STA MATOR
	PLX 
	STA PSHI
	EOR #$FF
	STA PDHI
	SEC
	LDA (PSHI),Y
	SBC (PDHI),Y
	LDY STAR_X,X
	BPL starx_done
	SEC
	SBC MATOR
starx_done	
	CLC
	ADC #140		; add xoffset 140 pixel
;	BCC addHIGH		; X-value > 255 -> plotting at right screen edge
	STA STAR_PLOT_X,X
	STZ STAR_PLOT_XH,X
	BRA doY
addHIGH		
	STA STAR_PLOT_X,X
	LDA #1
	STA STAR_PLOT_XH,X
doY		
	PHX			; calc XPLOT = STAR_X/STAR_Z
	LDA STAR_Y,X		; can be a signed value here
	TAY
	LDA STAR_Z,X
	TAX
	LDA PROJTAB,X		; PROJTAB: table for 1/Z-calculus
	STA MATOR
	PLX
	STA PSHI
	EOR #$FF
	STA PDHI
	SEC
	LDA (PSHI),Y
	SBC (PDHI),Y
	LDY STAR_Y,X
	BPL stary_done
	SEC
	SBC MATOR
stary_done	
	CLC
	ADC #96			; add yoffset 96 pixel
	STA STAR_PLOT_Y,X
	sta YPOS
	CMP #192		; check for illegal line numbers!
	BCS _doCONT
	PHX
	TAY			; move Star y-pos to Y-reg
;	LDA YLOOKLO,Y		; Y-lookup low address
;	STA ADRHIR		; 
;	LDA YLOOKHI,Y		; Y-lookup high address
;	ORA #$20		; draw on page 1
;	STA ADRHIR+1		; 
	LDA STAR_PLOT_XH,X	; x-coordinate > 255?
	BEQ doLOTABLE
	LDA STAR_PLOT_X,X
	sta XPOS
	CMP #25
	BCS _doCONT1		; if x > 279 then do not plot!
	TAX
;	LDY DIV7HI,X
;	LDA MOD7HI,X
	BRA GOTTAB
doLOTABLE	
	LDA STAR_PLOT_X,X
	sta XPOS
;	TAX				
;LOTABLE   	
;	LDY DIV7LO,X
;      	LDA MOD7LO,X
GOTTAB    	
;	TAX
;   	LDA ANDMASK,X		; ANDMASK: pixel masks for setting a pixel 
;   	STA ANDMSK
;   	LDA (ADRHIR),Y
;   	ORA ANDMSK
;   	STA (ADRHIR),Y
	jsr Plot
_doCONT1	
	PLX			; pull X-register back from stack
;	
_doCONT		
	JMP _cont			
		
;***************************
; Init Graphic Mode TMS9918A
;
;***************************		

HCLR 
SetsGraphicMode
	lda #V9938_Set_Screen5		;256 x 212 pixels 16 colors out of 512 
	ldx #VDP_Set_Video_Mode_FN	
	jsr VDP_Function_Call
	
	lda #<PATTERNBASE
	sta TMS9918REG		; set start address VRAM low Byte
	lda #$40 OR >PATTERNBASE ; set address maker and high Byte 
	sta TMS9918REG		; set start address VRAM
	lda #$0
	ldy #212	; number of pattern 	
CLEAR_SCREEN3
	ldx #0		; Byte per pattern		
CLEAR_SCREEN4
	nop
	nop
	nop
	nop
	nop
	nop
	sta TMS9918RAM
	dex
	bne CLEAR_SCREEN4
	dey
	bne CLEAR_SCREEN3	
	rts	

;***************************	
; Wait routine
;
;***************************			
		
WAIT
	phx
	ldx #0
WAIT2
	inx 
	bne WAIT2
	dec
	bne WAIT2	
	plx		
	rts		
		

V9938_Wait_Command
	lda #$02	; Set Status-Register #2
	sta TMS9918REG
	lda #128+15
	sta TMS9918REG
V9938_Wait_Command1
	lda TMS9918REG
	and #$01
	bne V9938_Wait_Command1
	lda #$00	; Set Status-Register #0
	sta TMS9918REG
	lda #128+15
	sta TMS9918REG
	rts

ClearPlot	
	phx
	phy
	pha			
	jsr V9938_Wait_Command

	lda #0
	sta TMS9918REG
	lda #128+45		; set to register R#45: Destination
	sta TMS9918REG	

	lda XPOS
	sta TMS9918REG
	lda #128+36		; set to register R#36: X Register
	sta TMS9918REG	

	stz TMS9918REG
	lda #128+37		; set to register R#37: X Register
	sta TMS9918REG	

	lda YPOS
	sta TMS9918REG
	lda #128+38		; set to register R#38: Y Register
	sta TMS9918REG	

	stz TMS9918REG
	lda #128+39		; set to register R#39: Y Register
	sta TMS9918REG	

	lda #$00
	sta TMS9918REG	
	lda #128+44		; set to register R#44: color registser
	sta TMS9918REG	

	lda #$50
	sta TMS9918REG
	lda #128+46		; set to register R#46: PSET command
	sta TMS9918REG	
	
	pla
	ply
	plx
	rts   	
	
Plot	
	phx
	phy
	pha			
	jsr V9938_Wait_Command

	lda #0
	sta TMS9918REG
	lda #128+45		; set to register R#45: Destination
	sta TMS9918REG	

	lda XPOS
	sta TMS9918REG
	lda #128+36		; set to register R#36: X Register
	sta TMS9918REG	

	stz TMS9918REG
	lda #128+37		; set to register R#37: X Register
	sta TMS9918REG	

	lda YPOS
	sta TMS9918REG
	lda #128+38		; set to register R#38: Y Register
	sta TMS9918REG	

	stz TMS9918REG
	lda #128+39		; set to register R#39: Y Register
	sta TMS9918REG	

	lda #$0F
	sta TMS9918REG	
	lda #128+44		; set to register R#44: color registser
	sta TMS9918REG	

	lda #$50
	sta TMS9918REG
	lda #128+46		; set to register R#46: PSET command
	sta TMS9918REG	

	pla
	ply
	plx
	rts   	

;
; intermediate star X,Y,Z-data storage with initial values
;
STAR_Y 	
	!byte 120,-20,40,-60,80,-100,120,-70,4,-45,60,-5,90,-75,110,-95,80,-17
	!byte 12,-7,8,-24,31,115,120,125,130,135,140,145,150,155
	!byte 160,165,170,175,180,185,190
	!byte 10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10
	!byte 10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10
	!byte 10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10
STAR_X 	
	!byte 120,100,80,60,-40,-60,-80,-100,70,10,43,122,-23,-70,-92,-5,15,12
	!byte 39,05,-34,-21,-14,-35,08,13,19,25,11,03,20,30,37,18,04,16
	!byte 17,09,38
	!byte 10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10
	!byte 10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10
	!byte 10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10
STAR_Z	
	!byte 25,30,35,60,35,50,32,17,05,47,23,59,17,31,5,52,16,38,20,41,13,39
	!byte 2,36
	!byte 10,15,20,25,30,35,40,45,40,35,30,25,20,15,10,05,10,15,20,25,30,35
	!byte 40,34,29,24,19,14,09,04,08,13,18,23,28,33,38,43,39,34,29,24,19,14,9
	!byte 4,7,12,17,22,27,32,37,42,47,46,41,36,31,26,21,16,11,6,1
STAR_PLOT_X	
	!byte 00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00
	!byte 00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00
	!byte 00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00
	!byte 00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00
	!byte 00,00,00
STAR_PLOT_XH	
	!byte 00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00
	!byte 00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00
	!byte 00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00
	!byte 00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00
	!byte 00,00,00
STAR_PLOT_Y	
	!byte 00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00
	!byte 00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00
	!byte 00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00
	!byte 00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00
	!byte 00,00,00
;
; pixel masks for setting and clearing a pixel in a HIRES-byte
; 
;ANDMASK   	
;	!byte $81,$82,$84,$88,$90,$a0,$c0
;CLRMASK		
;	!byte $7E,$7D,$7B,$77,$6F,$5F,$3F		
; 
; page alignment --> fill with $0 to next page boundary
;!align 255, 0
;YLOOKLO   
;	!hex 0000000000000000
;	!hex 8080808080808080
;	!hex 0000000000000000
;	!hex 8080808080808080
;	!hex 0000000000000000
;	!hex 8080808080808080
;	!hex 0000000000000000
;	!hex 8080808080808080
;	!hex 2828282828282828
;	!hex a8a8a8a8a8a8a8a8
;	!hex 2828282828282828
;	!hex a8a8a8a8a8a8a8a8
;	!hex 2828282828282828
;	!hex a8a8a8a8a8a8a8a8
;	!hex 2828282828282828
;	!hex a8a8a8a8a8a8a8a8
;	!hex 5050505050505050
;	!hex d0d0d0d0d0d0d0d0
;	!hex 5050505050505050
;	!hex d0d0d0d0d0d0d0d0
;	!hex 5050505050505050
;	!hex d0d0d0d0d0d0d0d0
;	!hex 5050505050505050
;	!hex d0d0d0d0d0d0d0d0
;--> fill with $0 to next page boundary
;!align 255, 0
;;                  
;YLOOKHI   	
;	!hex 0004080c1014181c
;	!hex 0004080c1014181c
;	!hex 0105090d1115191d
;	!hex 0105090d1115191d
;	!hex 02060a0e12161a1e
;	!hex 02060a0e12161a1e
;	!hex 03070b0f13171b1f
;	!hex 03070b0f13171b1f
;	!hex 0004080c1014181c
;	!hex 0004080c1014181c
;	!hex 0105090d1115191d
;	!hex 0105090d1115191d
;	!hex 02060a0e12161a1e
;	!hex 02060a0e12161a1e
;	!hex 03070b0f13171b1f
;	!hex 03070b0f13171b1f
;	!hex 0004080c1014181c
;	!hex 0004080c1014181c
;	!hex 0105090d1115191d
;	!hex 0105090d1115191d
;	!hex 02060a0e12161a1e
;	!hex 02060a0e12161a1e
;	!hex 03070b0f13171b1f
;	!hex 03070b0f13171b1f
;--> fill with $0 to next page boundary
!align 255, 0
; 
; Table for 1/Z-calculus
; 
PROJTAB   	
	!hex FFF0
	!hex E8D5C4B6AA9F96
	!hex 8E8680797A6F6A
	!hex 66625E5B585552
	!hex 504D4B49474543
	!hex 41403E3D3B3A39
	!hex 37363534333231
	!hex 302F2E2C2927
	!hex 24211E1B19171614
	!hex 1312110F0E0D0C0B
	!hex 0A0A090908080707
	!hex 0707060606060505
	!hex 0505050505040404
;--> fill with $0 to next page boundary
!align 255, 0
; 
; division by 7 tables for pixel positioning
; 
;DIV7HI    	
;	!hex 2424242525252525
;	!hex 2525262626262626
;	!hex 2627272727272727
;MOD7HI    	
;	!hex 0405060001020304
;	!hex 0506000102030405
;	!hex 0600010203040506
;--> fill with $0 to next page boundary
;!align 255, 0
; 
;DIV7LO          
;	!hex 0000000000000001
;	!hex 0101010101010202
;	!hex 0202020202030303
;	!hex 0303030304040404
;	!hex 0404040505050505
;	!hex 0505060606060606
;	!hex 0607070707070707
;	!hex 0808080808080809
;	!hex 0909090909090a0a
;	!hex 0a0a0a0a0a0b0b0b
;	!hex 0b0b0b0b0c0c0c0c
;	!hex 0c0c0c0d0d0d0d0d
;	!hex 0d0d0e0e0e0e0e0e
;	!hex 0e0f0f0f0f0f0f0f
;	!hex 1010101010101011
;	!hex 1111111111111212
;	!hex 1212121212131313
;	!hex 1313131314141414
;	!hex 1414141515151515
;	!hex 1515161616161616
;	!hex 1617171717171717
;	!hex 1818181818181819
;	!hex 1919191919191a1a
;	!hex 1a1a1a1a1a1b1b1b
;	!hex 1b1b1b1b1c1c1c1c
;	!hex 1c1c1c1d1d1d1d1d
;	!hex 1d1d1e1e1e1e1e1e
;	!hex 1e1f1f1f1f1f1f1f
;	!hex 2020202020202021
;	!hex 2121212121212222
;	!hex 2222222222232323
;	!hex 2323232324242424 
;MOD7LO          
;	!hex 0001020304050600
;	!hex 0102030405060001
;	!hex 0203040506000102
;	!hex 0304050600010203
;	!hex 0405060001020304
;	!hex 0506000102030405
;	!hex 0600010203040506
;	!hex 0001020304050600
;	!hex 0102030405060001
;	!hex 0203040506000102
;	!hex 0304050600010203
;	!hex 0405060001020304
;	!hex 0506000102030405
;	!hex 0600010203040506
;	!hex 0001020304050600
;	!hex 0102030405060001
;	!hex 0203040506000102
;	!hex 0304050600010203
;	!hex 0405060001020304
;	!hex 0506000102030405
;	!hex 0600010203040506
;	!hex 0001020304050600
;	!hex 0102030405060001
;	!hex 0203040506000102
;	!hex 0304050600010203
;	!hex 0405060001020304
;	!hex 0506000102030405
;	!hex 0600010203040506
;	!hex 0001020304050600
;	!hex 0102030405060001
;	!hex 0203040506000102
;	!hex 0304050600010203
; 
; multiplication tables
; 
SSQLO            
	!byte $00,$00,$01,$02,$04,$06,$09,$0C
	!byte $10,$14,$19,$1E,$24,$2A,$31,$38
	!byte $40,$48,$51,$5A,$64,$6E,$79,$84
	!byte $90,$9C,$A9,$B6,$C4,$D2,$E1,$F0
	!byte $00,$10,$21,$32,$44,$56,$69,$7C
	!byte $90,$A4,$B9,$CE,$E4,$FA,$11,$28
	!byte $40,$58,$71,$8A,$A4,$BE,$D9,$F4
	!byte $10,$2C,$49,$66,$84,$A2,$C1,$E0
	!byte $00,$20,$41,$62,$84,$A6,$C9,$EC
	!byte $10,$34,$59,$7E,$A4,$CA,$F1,$18
	!byte $40,$68,$91,$BA,$E4,$0E,$39,$64
	!byte $90,$BC,$E9,$16,$44,$72,$A1,$D0
	!byte $00,$30,$61,$92,$C4,$F6,$29,$5C
	!byte $90,$C4,$F9,$2E,$64,$9A,$D1,$08
	!byte $40,$78,$B1,$EA,$24,$5E,$99,$D4
	!byte $10,$4C,$89,$C6,$04,$42,$81,$C0
	!byte $00,$40,$81,$C2,$04,$46,$89,$CC
	!byte $10,$54,$99,$DE,$24,$6A,$B1,$F8
	!byte $40,$88,$D1,$1A,$64,$AE,$F9,$44
	!byte $90,$DC,$29,$76,$C4,$12,$61,$B0
	!byte $00,$50,$A1,$F2,$44,$96,$E9,$3C
	!byte $90,$E4,$39,$8E,$E4,$3A,$91,$E8
	!byte $40,$98,$F1,$4A,$A4,$FE,$59,$B4
	!byte $10,$6C,$C9,$26,$84,$E2,$41,$A0
	!byte $00,$60,$C1,$22,$84,$E6,$49,$AC
	!byte $10,$74,$D9,$3E,$A4,$0A,$71,$D8
	!byte $40,$A8,$11,$7A,$E4,$4E,$B9,$24
	!byte $90,$FC,$69,$D6,$44,$B2,$21,$90
	!byte $00,$70,$E1,$52,$C4,$36,$A9,$1C
	!byte $90,$04,$79,$EE,$64,$DA,$51,$C8
	!byte $40,$B8,$31,$AA,$24,$9E,$19,$94
	!byte $10,$8C,$09,$86,$04,$82,$01,$80
	!byte $00,$80,$01,$82,$04,$86,$09,$8C
	!byte $10,$94,$19,$9E,$24,$AA,$31,$B8
	!byte $40,$C8,$51,$DA,$64,$EE,$79,$04
	!byte $90,$1C,$A9,$36,$C4,$52,$E1,$70
	!byte $00,$90,$21,$B2,$44,$D6,$69,$FC
	!byte $90,$24,$B9,$4E,$E4,$7A,$11,$A8
	!byte $40,$D8,$71,$0A,$A4,$3E,$D9,$74
	!byte $10,$AC,$49,$E6,$84,$22,$C1,$60
	!byte $00,$A0,$41,$E2,$84,$26,$C9,$6C
	!byte $10,$B4,$59,$FE,$A4,$4A,$F1,$98
	!byte $40,$E8,$91,$3A,$E4,$8E,$39,$E4
	!byte $90,$3C,$E9,$96,$44,$F2,$A1,$50
	!byte $00,$B0,$61,$12,$C4,$76,$29,$DC
	!byte $90,$44,$F9,$AE,$64,$1A,$D1,$88
	!byte $40,$F8,$B1,$6A,$24,$DE,$99,$54
	!byte $10,$CC,$89,$46,$04,$C2,$81,$40
	!byte $00,$C0,$81,$42,$04,$C6,$89,$4C
	!byte $10,$D4,$99,$5E,$24,$EA,$B1,$78
	!byte $40,$08,$D1,$9A,$64,$2E,$F9,$C4
	!byte $90,$5C,$29,$F6,$C4,$92,$61,$30
	!byte $00,$D0,$A1,$72,$44,$16,$E9,$BC
	!byte $90,$64,$39,$0E,$E4,$BA,$91,$68
	!byte $40,$18,$F1,$CA,$A4,$7E,$59,$34
	!byte $10,$EC,$C9,$A6,$84,$62,$41,$20
	!byte $00,$E0,$C1,$A2,$84,$66,$49,$2C
	!byte $10,$F4,$D9,$BE,$A4,$8A,$71,$58
	!byte $40,$28,$11,$FA,$E4,$CE,$B9,$A4
	!byte $90,$7C,$69,$56,$44,$32,$21,$10
	!byte $00,$F0,$E1,$D2,$C4,$B6,$A9,$9C
	!byte $90,$84,$79,$6E,$64,$5A,$51,$48
	!byte $40,$38,$31,$2A,$24,$1E,$19,$14
	!byte $10,$0C,$09,$06,$04,$02,$01,$00
; multiplication tables
SSQHI            
	!byte $00,$00,$00,$00,$00,$00,$00,$00
	!byte $00,$00,$00,$00,$00,$00,$00,$00
	!byte $00,$00,$00,$00,$00,$00,$00,$00
	!byte $00,$00,$00,$00,$00,$00,$00,$00
	!byte $01,$01,$01,$01,$01,$01,$01,$01
	!byte $01,$01,$01,$01,$01,$01,$02,$02
	!byte $02,$02,$02,$02,$02,$02,$02,$02
	!byte $03,$03,$03,$03,$03,$03,$03,$03
	!byte $04,$04,$04,$04,$04,$04,$04,$04
	!byte $05,$05,$05,$05,$05,$05,$05,$06
	!byte $06,$06,$06,$06,$06,$07,$07,$07
	!byte $07,$07,$07,$08,$08,$08,$08,$08
	!byte $09,$09,$09,$09,$09,$09,$0A,$0A
	!byte $0A,$0A,$0A,$0B,$0B,$0B,$0B,$0C
	!byte $0C,$0C,$0C,$0C,$0D,$0D,$0D,$0D
	!byte $0E,$0E,$0E,$0E,$0F,$0F,$0F,$0F
	!byte $10,$10,$10,$10,$11,$11,$11,$11
	!byte $12,$12,$12,$12,$13,$13,$13,$13
	!byte $14,$14,$14,$15,$15,$15,$15,$16
	!byte $16,$16,$17,$17,$17,$18,$18,$18
	!byte $19,$19,$19,$19,$1A,$1A,$1A,$1B
	!byte $1B,$1B,$1C,$1C,$1C,$1D,$1D,$1D
	!byte $1E,$1E,$1E,$1F,$1F,$1F,$20,$20
	!byte $21,$21,$21,$22,$22,$22,$23,$23
	!byte $24,$24,$24,$25,$25,$25,$26,$26
	!byte $27,$27,$27,$28,$28,$29,$29,$29
	!byte $2A,$2A,$2B,$2B,$2B,$2C,$2C,$2D
	!byte $2D,$2D,$2E,$2E,$2F,$2F,$30,$30
	!byte $31,$31,$31,$32,$32,$33,$33,$34
	!byte $34,$35,$35,$35,$36,$36,$37,$37
	!byte $38,$38,$39,$39,$3A,$3A,$3B,$3B
	!byte $3C,$3C,$3D,$3D,$3E,$3E,$3F,$3F
	!byte $40,$40,$41,$41,$42,$42,$43,$43
	!byte $44,$44,$45,$45,$46,$46,$47,$47
	!byte $48,$48,$49,$49,$4A,$4A,$4B,$4C
	!byte $4C,$4D,$4D,$4E,$4E,$4F,$4F,$50
	!byte $51,$51,$52,$52,$53,$53,$54,$54
	!byte $55,$56,$56,$57,$57,$58,$59,$59
	!byte $5A,$5A,$5B,$5C,$5C,$5D,$5D,$5E
	!byte $5F,$5F,$60,$60,$61,$62,$62,$63
	!byte $64,$64,$65,$65,$66,$67,$67,$68
	!byte $69,$69,$6A,$6A,$6B,$6C,$6C,$6D
	!byte $6E,$6E,$6F,$70,$70,$71,$72,$72
	!byte $73,$74,$74,$75,$76,$76,$77,$78
	!byte $79,$79,$7A,$7B,$7B,$7C,$7D,$7D
	!byte $7E,$7F,$7F,$80,$81,$82,$82,$83
	!byte $84,$84,$85,$86,$87,$87,$88,$89
	!byte $8A,$8A,$8B,$8C,$8D,$8D,$8E,$8F
	!byte $90,$90,$91,$92,$93,$93,$94,$95
	!byte $96,$96,$97,$98,$99,$99,$9A,$9B
	!byte $9C,$9D,$9D,$9E,$9F,$A0,$A0,$A1
	!byte $A2,$A3,$A4,$A4,$A5,$A6,$A7,$A8
	!byte $A9,$A9,$AA,$AB,$AC,$AD,$AD,$AE
	!byte $AF,$B0,$B1,$B2,$B2,$B3,$B4,$B5
	!byte $B6,$B7,$B7,$B8,$B9,$BA,$BB,$BC
	!byte $BD,$BD,$BE,$BF,$C0,$C1,$C2,$C3
	!byte $C4,$C4,$C5,$C6,$C7,$C8,$C9,$CA
	!byte $CB,$CB,$CC,$CD,$CE,$CF,$D0,$D1
	!byte $D2,$D3,$D4,$D4,$D5,$D6,$D7,$D8
	!byte $D9,$DA,$DB,$DC,$DD,$DE,$DF,$E0
	!byte $E1,$E1,$E2,$E3,$E4,$E5,$E6,$E7
	!byte $E8,$E9,$EA,$EB,$EC,$ED,$EE,$EF
	!byte $F0,$F1,$F2,$F3,$F4,$F5,$F6,$F7
	!byte $F8,$F9,$FA,$FB,$FC,$FD,$FE,$00
; 
DSQLO            
	!byte $80,$01,$82,$04,$86,$09,$8C,$10
	!byte $94,$19,$9E,$24,$AA,$31,$B8,$40
	!byte $C8,$51,$DA,$64,$EE,$79,$04,$90
	!byte $1C,$A9,$36,$C4,$52,$E1,$70,$00
	!byte $90,$21,$B2,$44,$D6,$69,$FC,$90
	!byte $24,$B9,$4E,$E4,$7A,$11,$A8,$40
	!byte $D8,$71,$0A,$A4,$3E,$D9,$74,$10
	!byte $AC,$49,$E6,$84,$22,$C1,$60,$00
	!byte $A0,$41,$E2,$84,$26,$C9,$6C,$10
	!byte $B4,$59,$FE,$A4,$4A,$F1,$98,$40
	!byte $E8,$91,$3A,$E4,$8E,$39,$E4,$90
	!byte $3C,$E9,$96,$44,$F2,$A1,$50,$00
	!byte $B0,$61,$12,$C4,$76,$29,$DC,$90
	!byte $44,$F9,$AE,$64,$1A,$D1,$88,$40
	!byte $F8,$B1,$6A,$24,$DE,$99,$54,$10
	!byte $CC,$89,$46,$04,$C2,$81,$40,$00
	!byte $C0,$81,$42,$04,$C6,$89,$4C,$10
	!byte $D4,$99,$5E,$24,$EA,$B1,$78,$40
	!byte $08,$D1,$9A,$64,$2E,$F9,$C4,$90
	!byte $5C,$29,$F6,$C4,$92,$61,$30,$00
	!byte $D0,$A1,$72,$44,$16,$E9,$BC,$90
	!byte $64,$39,$0E,$E4,$BA,$91,$68,$40
	!byte $18,$F1,$CA,$A4,$7E,$59,$34,$10
	!byte $EC,$C9,$A6,$84,$62,$41,$20,$00
	!byte $E0,$C1,$A2,$84,$66,$49,$2C,$10
	!byte $F4,$D9,$BE,$A4,$8A,$71,$58,$40
	!byte $28,$11,$FA,$E4,$CE,$B9,$A4,$90
	!byte $7C,$69,$56,$44,$32,$21,$10,$00
	!byte $F0,$E1,$D2,$C4,$B6,$A9,$9C,$90
	!byte $84,$79,$6E,$64,$5A,$51,$48,$40
	!byte $38,$31,$2A,$24,$1E,$19,$14,$10
	!byte $0C,$09,$06,$04,$02,$01,$00,$00
	!byte $00,$01,$02,$04,$06,$09,$0C,$10
	!byte $14,$19,$1E,$24,$2A,$31,$38,$40
	!byte $48,$51,$5A,$64,$6E,$79,$84,$90
	!byte $9C,$A9,$B6,$C4,$D2,$E1,$F0,$00
	!byte $10,$21,$32,$44,$56,$69,$7C,$90
	!byte $A4,$B9,$CE,$E4,$FA,$11,$28,$40
	!byte $58,$71,$8A,$A4,$BE,$D9,$F4,$10
	!byte $2C,$49,$66,$84,$A2,$C1,$E0,$00
	!byte $20,$41,$62,$84,$A6,$C9,$EC,$10
	!byte $34,$59,$7E,$A4,$CA,$F1,$18,$40
	!byte $68,$91,$BA,$E4,$0E,$39,$64,$90
	!byte $BC,$E9,$16,$44,$72,$A1,$D0,$00
	!byte $30,$61,$92,$C4,$F6,$29,$5C,$90
	!byte $C4,$F9,$2E,$64,$9A,$D1,$08,$40
	!byte $78,$B1,$EA,$24,$5E,$99,$D4,$10
	!byte $4C,$89,$C6,$04,$42,$81,$C0,$00
	!byte $40,$81,$C2,$04,$46,$89,$CC,$10
	!byte $54,$99,$DE,$24,$6A,$B1,$F8,$40
	!byte $88,$D1,$1A,$64,$AE,$F9,$44,$90
	!byte $DC,$29,$76,$C4,$12,$61,$B0,$00
	!byte $50,$A1,$F2,$44,$96,$E9,$3C,$90
	!byte $E4,$39,$8E,$E4,$3A,$91,$E8,$40
	!byte $98,$F1,$4A,$A4,$FE,$59,$B4,$10
	!byte $6C,$C9,$26,$84,$E2,$41,$A0,$00
	!byte $60,$C1,$22,$84,$E6,$49,$AC,$10
	!byte $74,$D9,$3E,$A4,$0A,$71,$D8,$40
	!byte $A8,$11,$7A,$E4,$4E,$B9,$24,$90
	!byte $FC,$69,$D6,$44,$B2,$21,$90,$00
	!byte $70,$E1,$52,$C4,$36,$A9,$1C,$90
	!byte $04,$79,$EE,$64,$DA,$51,$C8,$40
	!byte $B8,$31,$AA,$24,$9E,$19,$94,$10
	!byte $8C,$09,$86,$04,$82,$01,$80,$00
		; 
DSQHI            
	!byte $3F,$3F,$3E,$3E,$3D,$3D,$3C,$3C
	!byte $3B,$3B,$3A,$3A,$39,$39,$38,$38
	!byte $37,$37,$36,$36,$35,$35,$35,$34
	!byte $34,$33,$33,$32,$32,$31,$31,$31
	!byte $30,$30,$2F,$2F,$2E,$2E,$2D,$2D
	!byte $2D,$2C,$2C,$2B,$2B,$2B,$2A,$2A
	!byte $29,$29,$29,$28,$28,$27,$27,$27
	!byte $26,$26,$25,$25,$25,$24,$24,$24
	!byte $23,$23,$22,$22,$22,$21,$21,$21
	!byte $20,$20,$1F,$1F,$1F,$1E,$1E,$1E
	!byte $1D,$1D,$1D,$1C,$1C,$1C,$1B,$1B
	!byte $1B,$1A,$1A,$1A,$19,$19,$19,$19
	!byte $18,$18,$18,$17,$17,$17,$16,$16
	!byte $16,$15,$15,$15,$15,$14,$14,$14
	!byte $13,$13,$13,$13,$12,$12,$12,$12
	!byte $11,$11,$11,$11,$10,$10,$10,$10
	!byte $0F,$0F,$0F,$0F,$0E,$0E,$0E,$0E
	!byte $0D,$0D,$0D,$0D,$0C,$0C,$0C,$0C
	!byte $0C,$0B,$0B,$0B,$0B,$0A,$0A,$0A
	!byte $0A,$0A,$09,$09,$09,$09,$09,$09
	!byte $08,$08,$08,$08,$08,$07,$07,$07
	!byte $07,$07,$07,$06,$06,$06,$06,$06
	!byte $06,$05,$05,$05,$05,$05,$05,$05
	!byte $04,$04,$04,$04,$04,$04,$04,$04
	!byte $03,$03,$03,$03,$03,$03,$03,$03
	!byte $02,$02,$02,$02,$02,$02,$02,$02
	!byte $02,$02,$01,$01,$01,$01,$01,$01
	!byte $01,$01,$01,$01,$01,$01,$01,$01
	!byte $00,$00,$00,$00,$00,$00,$00,$00
	!byte $00,$00,$00,$00,$00,$00,$00,$00
	!byte $00,$00,$00,$00,$00,$00,$00,$00
	!byte $00,$00,$00,$00,$00,$00,$00,$00
	!byte $00,$00,$00,$00,$00,$00,$00,$00
	!byte $00,$00,$00,$00,$00,$00,$00,$00
	!byte $00,$00,$00,$00,$00,$00,$00,$00
	!byte $00,$00,$00,$00,$00,$00,$00,$01
	!byte $01,$01,$01,$01,$01,$01,$01,$01
	!byte $01,$01,$01,$01,$01,$02,$02,$02
	!byte $02,$02,$02,$02,$02,$02,$02,$03
	!byte $03,$03,$03,$03,$03,$03,$03,$04
	!byte $04,$04,$04,$04,$04,$04,$04,$05
	!byte $05,$05,$05,$05,$05,$05,$06,$06
	!byte $06,$06,$06,$06,$07,$07,$07,$07
	!byte $07,$07,$08,$08,$08,$08,$08,$09
	!byte $09,$09,$09,$09,$09,$0A,$0A,$0A
	!byte $0A,$0A,$0B,$0B,$0B,$0B,$0C,$0C
	!byte $0C,$0C,$0C,$0D,$0D,$0D,$0D,$0E
	!byte $0E,$0E,$0E,$0F,$0F,$0F,$0F,$10
	!byte $10,$10,$10,$11,$11,$11,$11,$12
	!byte $12,$12,$12,$13,$13,$13,$13,$14
	!byte $14,$14,$15,$15,$15,$15,$16,$16
	!byte $16,$17,$17,$17,$18,$18,$18,$19
	!byte $19,$19,$19,$1A,$1A,$1A,$1B,$1B
	!byte $1B,$1C,$1C,$1C,$1D,$1D,$1D,$1E
	!byte $1E,$1E,$1F,$1F,$1F,$20,$20,$21
	!byte $21,$21,$22,$22,$22,$23,$23,$24
	!byte $24,$24,$25,$25,$25,$26,$26,$27
	!byte $27,$27,$28,$28,$29,$29,$29,$2A
	!byte $2A,$2B,$2B,$2B,$2C,$2C,$2D,$2D
	!byte $2D,$2E,$2E,$2F,$2F,$30,$30,$31
	!byte $31,$31,$32,$32,$33,$33,$34,$34
	!byte $35,$35,$35,$36,$36,$37,$37,$38
	!byte $38,$39,$39,$3A,$3A,$3B,$3B,$3C
	!byte $3C,$3D,$3D,$3E,$3E,$3F,$3F,$00
; 
; end of program
; 
