; VGM Player.
; Support uncompressed .VGM files 
; VGM files header:
;    3 ASCII Byte: "Vgm"
 

!src "../Kernel/include/label_definition.asm"
!src "../Kernel/include/Jumptable.asm"
!cpu 65c02

;Data_RAM = $4000

*=$0EFE
!word $F00 	;--> Start address of this program


; Zero page addresses
PTR_Read_VGM_L		= $50
PTR_Read_VGM_H		= $51
PTR_Write_VGM_L		= $52	; PTR_Write_VGM_L is also data load end address
PTR_Write_VGM_H		= $53
PTR_StartData_VGM_L	= $54
PTR_StartData_VGM_H	= $55
PTR_LoopOffset_VGM_L	= $56
PTR_LoopOffset_VGM_H	= $57



VGM_Main
		lda #$DE
		sta MEMTOP_H
		lda #$ff
		sta MEMTOP_L
		lda #$ff
		sta VIA1ORB
		sta VIA1DDRB
		jsr YMF262_Reset
		jsr Print_Data_Start_End_Address
		jsr Print_Text_VGM
		jsr VGM_Open_File
		bcs VGM_Main_Error1
		jsr VGM_Load_Data
		bcs VGM_Main_Error2
VGM_Main1
		jsr Test_Print_Result
		jsr VGM_Compare
		bcs VGM_Main_Exit
		jsr VGM_Init_Loop
		jsr VGM_Play_Loop
		jsr VGM_Close_File
		jsr Print_End
VGM_Main2
		lda #LF
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
VGM_Main_Exit
;		jsr YMF262_Reset
		rts

Print_Text_VGM
		lda #<Text_VGM
		sta PTR_String_L
		lda #>Text_VGM
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		rts

VGM_Main_Error1
		jsr DOS_Print_File_Not_Found
		rts

VGM_Main_Error2
		cmp #00
		bne VGM_Main_Error3
		jsr DOS_Print_Memory_Error
		jmp VGM_Main1			; Play anyway
VGM_Main_Error3
		jsr DOS_Print_Read_Error
		lda #LF
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		jsr YMF262_Reset
		rts 

Text_VGM
!text "VGM Player V0.1",LF
!text "Press CTRL-C to exit, p for pause",LF
!text "Support YMF262 (OPL3), YM3812 (OPL2)",LF,"Load .vgm file",LF
!text "Name (8.3):",00

;***************************
; Wait PS2 Key Press
;
;***************************

Print_End
		lda #<End_Text
		sta PTR_String_L
		lda #>End_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		rts	
End_Text
!text LF,"End",LF,00

;***************************
; Load VGM File
; 
;***************************

VGM_Load_Data
		lda #<Data_Start_Address
		sta PTR_Write_VGM_L
		lda #>Data_Start_Address
		sta PTR_Write_VGM_H
VGM_Load_Data1
		ldx #DOS_Load_Byte_FN
		jsr DOS_Function_Call
		bcc VGM_Load_Data2
		cmp #00					; End of file ?
		beq VGM_Load_Exit
VGM_Load_Data2
		sta (PTR_Write_VGM_L)
		lda PTR_Write_VGM_L		; Check for end of memory
		cmp MEMTOP_L
		bne VGM_Load_Data3
		lda PTR_Write_VGM_H
		cmp MEMTOP_H
		beq VGM_Load_Outofmem_Exit 
VGM_Load_Data3
	   	inc PTR_Write_VGM_L
	       	bne VGM_Load_Data4
       		inc PTR_Write_VGM_H
VGM_Load_Data4
		bra VGM_Load_Data1
VGM_Load_Exit
		clc
VGM_Load_Error_Exit	
		rts
VGM_Load_Outofmem_Exit		
		lda #00
		sec
		rts

;***************************
; VGM initialisation
; 
;***************************

VGM_Init_Loop
;		lda #<Data_Start_Address
;		sta PTR_Read_VGM_L
;		lda #>Data_Start_Address
;		sta PTR_Read_VGM_H
		jsr VGM_Set_Start_Address
		clc
		lda PTR_Read_VGM_L
		adc #$1c			; Loop offset 
		sta PTR_Read_VGM_L
		lda PTR_Read_VGM_H
		adc #$0
		sta PTR_Read_VGM_H

		lda (PTR_Read_VGM_L)
		sta PTR_LoopOffset_VGM_L
		jsr VGM_Inc_PTR_Read
		lda (PTR_Read_VGM_L)
		sta PTR_LoopOffset_VGM_H			

;		lda #<Data_Start_Address
;		sta PTR_Read_VGM_L
;		lda #>Data_Start_Address
;		sta PTR_Read_VGM_H
		jsr VGM_Set_Start_Address
		clc
		lda #$34			; Data offset 
		adc PTR_Read_VGM_L
		sta PTR_Read_VGM_L
		lda #$0
		adc PTR_Read_VGM_H
		sta PTR_Read_VGM_H
		lda (PTR_Read_VGM_L)
		sta PTR_StartData_VGM_L
		jsr VGM_Inc_PTR_Read
		lda (PTR_Read_VGM_L)
		sta PTR_StartData_VGM_H			

		lda PTR_StartData_VGM_L
		bne VGM_Init_Loop2
		lda PTR_StartData_VGM_H
		bne VGM_Init_Loop2		
		
;		lda #<Data_Start_Address
;		sta PTR_Read_VGM_L
;		lda #>Data_Start_Address
;		sta PTR_Read_VGM_H	
		jsr VGM_Set_Start_Address
		lda PTR_Read_VGM_L
		clc
		adc #$80			; Add fix offset to data
		sta PTR_Read_VGM_L
		lda PTR_Read_VGM_H
		adc #$0
		sta PTR_Read_VGM_H
;		jsr VGM_PrintStartAdress
		rts
VGM_Init_Loop2	
;		lda #<Data_Start_Address
;		sta PTR_Read_VGM_L
;		lda #>Data_Start_Address
;		sta PTR_Read_VGM_H	
		jsr VGM_Set_Start_Address
		clc
		lda PTR_Read_VGM_L
		adc #$34			; Data offset 
		sta PTR_Read_VGM_L
		lda PTR_Read_VGM_H
		adc #$0
		sta PTR_Read_VGM_H

		clc
		lda PTR_Read_VGM_L
		adc PTR_StartData_VGM_L	; Add data offet
		sta PTR_Read_VGM_L
		lda PTR_Read_VGM_H
		adc PTR_StartData_VGM_H
		sta PTR_Read_VGM_H		
;		jsr VGM_PrintStartAdress	
		rts
		
;***************************
; VGM print start adress
; 
;***************************
;VGM_PrintStartAdress

;		lda #<Text_PrintStartAdress
;		sta PTR_String_L
;		lda #>Text_PrintStartAdress
;		sta PTR_String_H
;		ldx #OS_Print_String_FN
;		jsr OS_Function_Call		

;		lda PTR_Read_VGM_H
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call
;		lda PTR_Read_VGM_L
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		lda #LF
;		ldx #OS_Print_CHR_FN
;		jsr OS_Function_Call
;		rts

;Text_PrintStartAdress
;!text LF,"PTR_Read_VGM_H:",00

;***************************
; VGM set start address for reading
; 
;***************************

VGM_Set_Start_Address
		lda #<Data_Start_Address
		sta PTR_Read_VGM_L
		lda #>Data_Start_Address
		sta PTR_Read_VGM_H
		rts
;***************************
; VGM Play Loop
; 
;***************************		
		
VGM_Play_Loop
;		lda PTR_Read_VGM_L
;		clc
;		adc #$80			; Offset to data
;		sta PTR_Read_VGM_L
;		lda PTR_Read_VGM_H
;		adc #$0
;		sta PTR_Read_VGM_H
VGM_Play_Loop2
		jsr VGM_CheckBreakPauseKey
		bcc	VGM_Play_Loop2_1
		rts
VGM_Play_Loop2_1
		stz VIA1ORB			; Enable RAM
		lda (PTR_Read_VGM_L)
;		jsr debug
		cmp #$5E			; YMF262 port 0, write value dd to register aa
		bne VGM_Play_Loop3
		jsr VGM_Inc_PTR_Read
		bcc VGM_Play_Loop2_2
		jmp VGM_Play_Loop_End
VGM_Play_Loop2_2		
		lda (PTR_Read_VGM_L)
;		jsr debug2
		sta YMF262_ADDR1
		jsr VGM_Inc_PTR_Read
		bcc VGM_Play_Loop2_3
		jmp VGM_Play_Loop_End
VGM_Play_Loop2_3		
		lda (PTR_Read_VGM_L)
;		jsr debug2
		sta YMF262_DATA1
		jmp VGM_Play_Loop_100
VGM_Play_Loop3
		cmp #$5F			; YMF262 port 1, write value dd to register aa
		bne VGM_Play_Loop4
		jsr VGM_Inc_PTR_Read
		bcs VGM_Play_Loop_End
		lda (PTR_Read_VGM_L)
;		jsr debug2
		sta YMF262_ADDR2
		jsr VGM_Inc_PTR_Read
		bcs VGM_Play_Loop_End
		lda (PTR_Read_VGM_L)
;		jsr debug2
		sta YMF262_DATA2
		jmp VGM_Play_Loop_100
VGM_Play_Loop4
		cmp #$5A			;  YM3526, write value dd to register aa
		bne VGM_Play_Loop5
		jsr VGM_Inc_PTR_Read
		bcs VGM_Play_Loop_End
		lda (PTR_Read_VGM_L)
;		jsr debug2
		sta YMF262_ADDR1
		jsr VGM_Inc_PTR_Read
		bcs VGM_Play_Loop_End
		lda (PTR_Read_VGM_L)
;		jsr debug2
		sta YMF262_DATA1
		jmp VGM_Play_Loop_100
VGM_Play_Loop5
		cmp #$61			; Wait
		bne VGM_Play_Loop6
		jsr VGM_Wait_61
		bcs VGM_Play_Loop_End
		jmp VGM_Play_Loop_100
VGM_Play_Loop6
		cmp #$62			; Wait
		bne VGM_Play_Loop7
		jsr VGM_Wait_62
		bcs VGM_Play_Loop_End
		jmp VGM_Play_Loop_100
VGM_Play_Loop7
		cmp #$63			; Wait
		bne VGM_Play_Loop8
		jsr VGM_Wait_63
		bcs VGM_Play_Loop_End		
		jmp VGM_Play_Loop_100
VGM_Play_Loop8
		cmp #$66			; End of data
		beq VGM_Play_Loop_End
		ldx #$FF
		stx VIA1ORB			; Enable ROM
		jsr VGM_Unknown_Command
VGM_Play_Loop_100
		ldx #$FF
		stx VIA1ORB			; Enable ROM
		jsr VGM_Inc_PTR_Read
		bcs VGM_Play_Loop_End
VGM_Play_Loop_101		
		jmp VGM_Play_Loop2

VGM_Play_Loop_End
		ldx #$FF
		stx VIA1ORB			; Enable ROM
		jsr VGM_Check_Loop
		bcs VGM_Play_Loop_101
		rts


;***************************
; Check if loop
; C=1 if loop
;***************************
VGM_Check_Loop
		lda PTR_LoopOffset_VGM_L
		bne VGM_Check_Loop10
VGM_Check_Loop2		
		lda PTR_LoopOffset_VGM_H
		bne VGM_Check_Loop10
VGM_Check_Loop_no_loop

		clc
		rts 
VGM_Check_Loop10
;		lda #<Data_Start_Address
;		sta PTR_Read_VGM_L
;		lda #>Data_Start_Address
;		sta PTR_Read_VGM_H
		jsr VGM_Set_Start_Address
		lda PTR_Read_VGM_L		
		clc
		adc PTR_LoopOffset_VGM_L
		sta PTR_Read_VGM_L	
		lda PTR_Read_VGM_H
		adc PTR_LoopOffset_VGM_H
		sta PTR_Read_VGM_H
		
		lda PTR_Read_VGM_L
		clc
		adc #$1c 		; Offset loop data
		sta PTR_Read_VGM_L
		lda PTR_Read_VGM_H
		adc #0
		sta PTR_Read_VGM_H
;		jsr VGM_PrintStartAdress		
		sec
		rts
		

;***************************
; For debugging
;***************************
VGM_Unknown_Command
; 		jsr VGM_PrintStartAdress	
		lda #CR
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		lda (PTR_Read_VGM_L)		; For debugging: print unknown VGM commands
		ldx #OS_Print_Bin2Hex_FN				
		jsr OS_Function_Call	
		
		lda #CR
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		rts

;***************************
; VGM wait 0x61
; 0x61,nn,nn
; Wait n samples, n can range from 0 to 65535 (approx 1.49 seconds)
; VGMs run with a rate of 44100 samples per second. All sample values use this unit. 
; 1 sample is approx 22,68µs.
; Longer pauses than this are represented by multiple wait commands
; Opmtimized for 2MHz CPU clock
;***************************

VGM_Wait_61
		jsr VGM_Inc_PTR_Read
		bcs VGM_Wait_61_EOF
		lda (PTR_Read_VGM_L)
		tax
		jsr VGM_Inc_PTR_Read
		bcs VGM_Wait_61_EOF
		lda (PTR_Read_VGM_L)
		tay
VGM_Wait_61_1 
		lda #7
VGM_Wait_61_2
		dec
		bne VGM_Wait_61_2
		cpx #00
		beq VGM_Wait_61_3
		dex
		bra VGM_Wait_61_1 
VGM_Wait_61_3
		cpy #00
		beq VGM_Wait_61_end
		dey
		dex
		bra VGM_Wait_61_1
VGM_Wait_61_end 
		clc
VGM_Wait_61_EOF
   		rts
   		
   		
;***************************
; VGM wait 0x62
; wait 735 samples (60th of a second), a shortcut for 0x61 0xdf 0x02
; 1/60s = 16,7ms
;***************************

VGM_Wait_62
		ldx #$df		; Low byte
		ldy #$02		; high byte
		jmp VGM_Wait_61_1
   		
 ;***************************
; VGM wait 0x63
; wait 882 samples (50th of a second), a shortcut for 0x61 0x72 0x03
; 1/50s = 20ms
;***************************
VGM_Wait_63
		ldx #$72		; Low byte
		ldy #$03		; high byte
		jmp VGM_Wait_61_1
   
;***************************
; Test_Print_Result
;
;***************************

Test_Print_Result
		lda #<Text_Result_End_Adress
		sta PTR_String_L
		lda #>Text_Result_End_Adress
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call		

		lda PTR_Write_VGM_H
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call
		lda PTR_Write_VGM_L
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call

		lda #LF
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		rts

Text_Result_End_Adress
!text LF,"Data load end address hex:",00

;***************************
; Open file
;
;***************************

VGM_Open_File
		ldx #DOS_Init_FN
		jsr DOS_Function_Call
		ldx #DOS_LEditor_FN
		jsr DOS_Function_Call
		ldx #DOS_Open_File_FN
		jsr DOS_Function_Call
		bcs VGM_Open_File_Exit
		jsr DOS_Print_File_Found
		clc
		rts
VGM_Open_File_Exit
		rts
		
;***************************
; Close file
;
;***************************	
	
VGM_Close_File		
		ldx #DOS_Close_File_FN
		jsr DOS_Function_Call
		rts

;***************************
; Compare "Vgm" string
;
;***************************

VGM_Compare
;		lda #<Data_Start_Address
;		sta PTR_Read_VGM_L
;		lda #>Data_Start_Address
;		sta PTR_Read_VGM_H
		jsr VGM_Set_Start_Address		
		lda (PTR_Read_VGM_L)
		cmp #"V"
		jsr VGM_Inc_PTR_Read
		bcs VGM_Compare_Error
		lda (PTR_Read_VGM_L)
		cmp #"g"
		jsr VGM_Inc_PTR_Read
		bcs VGM_Compare_Error
		lda (PTR_Read_VGM_L)
		cmp #"m"
		jsr VGM_Inc_PTR_Read
		bcs VGM_Compare_Error
		lda (PTR_Read_VGM_L)
		cmp #" "				; Address offset +3
		clc
VGM_Compare_Error
		rts

;***************************
; Increase PTR_Rear and compare with PTR_Write
;
;***************************

VGM_Inc_PTR_Read
	   	inc PTR_Read_VGM_L
		bne VGM_Inc_PTR_Read_2
		inc PTR_Read_VGM_H
VGM_Inc_PTR_Read_2
		lda PTR_Read_VGM_L
		cmp PTR_Write_VGM_L
		bne VGM_Inc_PTR_Read_exit
VGM_Inc_PTR_Read_3      	
		lda PTR_Read_VGM_H
		cmp PTR_Write_VGM_H
		beq VGM_Inc_PTR_Read_End_Exit
VGM_Inc_PTR_Read_exit 
		clc 
		rts
VGM_Inc_PTR_Read_End_Exit
		sec
		rts       	

;***************************
; Print data addresses
;
;***************************

Print_Data_Start_End_Address
		lda #<Text_Data_Start_Address
		sta PTR_String_L
		lda #>Text_Data_Start_Address
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call		
		
		lda #>Data_Start_Address
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call
		lda #<Data_Start_Address
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call
		
		lda #<Text_Data_End_Address
		sta PTR_String_L
		lda #>Text_Data_End_Address
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call

		lda MEMTOP_H
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call
		lda MEMTOP_L
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call		
		
		lda #<Text_Data_Length
		sta PTR_String_L
		lda #>Text_Data_Length
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call		

		lda MEMTOP_L
		sec
		sbc #<Data_Start_Address
		sta Operand1
		lda MEMTOP_H
		sbc #>Data_Start_Address		
		sta Operand2

		stz Operand3
		stz Operand4
		stz Operand5
		ldx #OS_Bin2BCD32_FN
		jsr OS_Function_Call
		ldx #OS_Print_BCD_FN
		jsr OS_Function_Call	
		
		lda #LF
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call			
		
		rts

Text_Data_Start_Address
!text LF,"Data start address hex:",00
Text_Data_End_Address
!text LF,"Data end address hex:  ",00
Text_Data_Length
!text LF,"Data length dez: ",00

;***************************
; Reset YMF262
; input: -
; output: -
; used: A,Y 
;
;***************************

YMF262_Reset
		lda #$05
		sta YMF262_ADDR2
		lda #$01			
		sta YMF262_DATA2	; OPL3 Mode
		ldy #$B0		; Key off 	
YMF262_Reset1		
		sty YMF262_ADDR1
		stz YMF262_DATA1
		sty YMF262_ADDR2
		stz YMF262_DATA2			
		iny
		cpy #$B9
		bne YMF262_Reset1
		ldy #$01
		
YMF262_Reset6
;		jsr debug
		sty YMF262_ADDR1
		stz YMF262_DATA1
	
		sty YMF262_ADDR2
		stz YMF262_DATA2	
		iny
		cpy #$F6
		bne YMF262_Reset6



		lda #$05
		sta YMF262_ADDR2
		stz YMF262_DATA2	; Set to OPL2 Mode
;		jsr VGM_Wait_62
		rts

;***************************
; Debug
;***************************

;debug
;		pha
;		phx
;		phy
;		php
;		pha
;		lda #LF
;		ldx #OS_Print_CHR_FN
;		jsr OS_Function_Call	
;		pla
;;		tya
;		ldx #OS_Print_Bin2Hex_FN	; For debugging: print A
;		jsr OS_Function_Call	
;		lda #SP
;		ldx #OS_Print_CHR_FN
;		jsr OS_Function_Call			
;		plp
;		ply
;		plx
;		pla
;		rts
;		
;		
;debug2
;		pha
;		phx
;		phy
;		php
;;		tya
;		ldx #OS_Print_Bin2Hex_FN	; For debugging: print A
;		jsr OS_Function_Call			

;		lda #SP
;		ldx #OS_Print_CHR_FN
;		jsr OS_Function_Call		

;		plp
;		ply
;		plx
;		pla
;		rts		
		

;***************************
; Check for break key press (CTRL+C)
; "p" for pause
; C=1 if break key 
;***************************

VGM_CheckBreakPauseKey		; TODO: move function to kernel
		ldx #PS2_ChkKey_FN
		jsr PS2_Function_Call
		bcc VGM_CheckBreakPauseKey3
		ldx #PS2_GetKey_FN
		jsr PS2_Function_Call
		cmp #ETX	; ETX or CTRL-C on keyboard
		beq VGM_CheckBreakPauseKey2
		cmp #"p" 	; p = pause
		beq VGM_CheckBreakPauseKey4
		clc
		bra VGM_CheckBreakPauseKey3
VGM_CheckBreakPauseKey2
		lda #<Break_Text
		sta PTR_String_L
		lda #>Break_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		sec
VGM_CheckBreakPauseKey3
		rts
VGM_CheckBreakPauseKey4
		ldx #PS2_ChkKey_FN
		jsr PS2_Function_Call
		bcc VGM_CheckBreakPauseKey4
		clc
		rts

Break_Text
!text LF,"Break",LF,00


;***************************
; Error messages
;
;***************************

DOS_Print_File_Not_Found
		lda #<TypeNotFound_Text
		sta PTR_String_L
		lda #>TypeNotFound_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		sec
		rts
TypeNotFound_Text
!text "File not found",LF,00

DOS_Print_File_Found
		lda #<TypeFound_Text
		sta PTR_String_L
		lda #>TypeFound_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		rts
TypeFound_Text
!text "File found",LF,00

DOS_Print_Read_Error
		lda #<TypeReadError_Text
		sta PTR_String_L
		lda #>TypeReadError_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		rts
TypeReadError_Text
!text "Read error",LF,00

DOS_Print_Memory_Error
		lda #<TypeMemoryError
		sta PTR_String_L
		lda #>TypeMemoryError
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		rts
TypeMemoryError
!text "Out of memory! Play anyway.",LF,00

Data_Start_Address



; Volume in drive A has no label
; Volume Serial Number is 2D85-7003
;Directory for A:/

;01TITL~1 VGM     10881 2021-01-16  23:38  01 Title.vgm (Cybersphere)
;02OPTI~1 VGM     27824 2021-01-16  23:38  02 Options, Level Complete.vgm (Cybersphere)
;02THEL~1 VGM     30368 2021-01-16  23:38  02 The Lookout.vgm (Monkey Island)
;04LEVE~1 VGM     37406 2021-01-16  23:38  04 Level BGM 1.vgm (Cybersphere)
;07BONU~1 VGM     34961 2021-01-16  23:38  07 Bonus Level BGM 1.vgm (Cybersphere)
;09DEEP~1 VGM     39959 2021-01-16  23:38  09 Deep Blue.vgm (Miwaku no Chousho)
;12MORN~1 VGM     56126 2021-01-16  23:38  12 Morning Glow of Despair.vgm (Miwaku no Chousho)
;44_END~1 VGM     35904 2021-01-16  23:38  44_Ending.vgm (Doukyusei)
;ADR1FT   vgm     36861 2021-01-16  23:38  (Diode Miliampere)
;A_HA_T~1 VGM     32606 2021-01-16  23:38  A_ha_Take_On_Me.vgm
;JAN_HA~1 VGM     37064 2021-01-16  23:38  Jan_Hammer_-_Forever_Tonight.vgm
;OPL3_O~2 VGM     23178 2021-01-17   0:43  OPL3_Our_Statue_of_Liberty.vgm (Miwaku no Chousho)
;OPL2_E~1 VGM     13679 2021-01-16  23:38  OPL2_Ending.vgm (Ultima VI: The False Prophet)
;OPL2_H~1 VGM     12651 2021-01-16  23:38  OPL2_Highways_Adlib_Juke_Box.vgm (Adlib)
;OPL2_P~1 VGM      7959 2021-01-16  23:38  OPL2_Part_Scroll.vgm (Monkey Island)
;OPL2_S~1 VGM      1914 2021-01-16  23:38  OPL2_Softstar_Logo.vgm (Empire of the Angel II)
;OPL2_S~2 VGM     15377 2021-01-16  23:38  OPL2_Stalking_the_Shopkeeper.vgm (Monkey Island)
;OPL2_T~1 VGM     25502 2021-01-16  23:38  OPL2_The_Ghost_Ship.vgm (Monkey Island)
;OPL2_W~1 VGM     12523 2021-01-16  23:38  OPL2_Wacky_Wheels_Ashes.vgm (Wacky Wheels)
;OPL3_0~1 VGM     20264 2021-01-16  23:38  OPL3_02 Sweet on You.vgm (Doukyusei 2)
;OPL3_0~2 VGM     22064 2021-01-16  23:38  OPL3_02 Takurou's Room 1.vgm (Doukyusei)
;OPL3_0~3 VGM      9222 2021-01-16  23:38  OPL3_03_Sector_Selection.vgm (Cybersphere)
;OPL3_4~1 VGM     29056 2021-01-16  23:38  OPL3_43 Girls Room.vgm (Doukyusei 2)
;OPL3_I~1 VGM     23179 2021-01-16  23:38  OPL3_Its_the_Select_Screen.vgm (Puyo Puyo Tsu)
;OPL3_L~1 VGM     26643 2021-01-16  23:38  OPL3_Level_BGM_2.vgm (Cybersphere)
;OPL3_M~1 VGM     20789 2021-01-16  23:38  OPL3_Main_Theme_EGA_Demo.vgm (Monkey Island)
;OPL3_M~2 VGM     29869 2021-01-16  23:38  OPL3_Mode_Select.vgm (Puyo Puyo Tsu)
;OPL3_O~1 VGM     28438 2021-01-17   0:38  OPl3_Opening.vgm (Sorcerian Forever)
;       28 files             702 267 bytes
;                             12 288 bytes free

; Volume in drive A has no label
; Volume Serial Number is 0DB4-98A6
;Directory for A:/

;02SWEE~1 VGM     20264 2021-01-17  12:13  02 Sweet on You.vgm (Doukyusei 2)
;03TIME~1 VGM     57294 2021-01-17  12:13  03 Timeless Area.vgm (Miwaku no Chousho)
;FINAL_~1 VGM     38681 2021-01-17  12:13  Final_Fantasy_V_-_Clash_on_the_Big_Bridge.vgm
;MIG_29~1 VGM     57697 2021-01-17  12:21  MIG_29M_Super_Fulcrum_-_Title_(Hybrid_Cracktro_Version).vgm
;RADIOH~1 VGM     53120 2021-01-17  12:13  Radiohead_-_Creep.vgm
;02DREA~1 VGM     39473 2021-01-17  13:16  02 Dream.vgm (Wacky Wheels)
;02OPEN~1 VGM     40053 2021-01-17  13:16  02 Opening 1.vgm (Princess Maker 2)
;04FALL~1 VGM     41258 2021-01-17  13:16  04 Fall Leaves (Introduction).vgm (Ultima VI: The False Prophet)
;09BASS~1 VGM     50034 2021-01-17  13:16  09 Bass Attack.vgm (Wacky Wheels)
;04LET_~1 VGM     38986 2021-01-17  22:15  04 Let's Go Big.vgm (Doukyusei 2)
;06NEWY~1 VGM     32487 2021-01-17  22:15  06 New Year.vgm (Doukyusei 2)
;07CHIN~1 VGM     27756 2021-01-17  22:15  07 China Event.vgm (Princess Maker: Yumemiru Yousei)
;44ENDI~1 VGM     35904 2021-01-17  22:15  44 Ending.vgm (Doukyusei)
;OPL3_O~1 VGM     23178 2021-01-18  18:55  OPL3_Our_Statue_of_Liberty.vgm (Miwaku no Chousho)
;       14 files             556 185 bytes
;                             86 016 bytes free


