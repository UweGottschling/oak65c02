; (C) 2021 Uwe Gottschling
; 6809 to 65C02 instruction comparison



Registers

+------------------------------------------------+
|15 14 13 12 11 10 09 08|07 06 05 04 03 02 01 00 | bit possition
+------------------------------------------------+
|                       D                        |
|           A           |           B            | accumulators
+-----------------------+------------------------+
|                       X index register         |
+------------------------------------------------+
|                       Y index register         |
+------------------------------------------------+
|                       U user stack pointer     |
+------------------------------------------------+
|                       S system stack pointer   |
+------------------------------------------------+
|                      PC programm counter       |
+------------------------------------------------+
|                       | condition code register|
|DP (DIRECT PAGE)       |E  F  H  I  N  Z  V  C  |
+------------------------------------------------+

MC6809 is big-endian: STD store A in adress + 0, B in adress + 1
	


6809 										65C02 comparable
Mnemonic	Description 
ABX 	  X = B+X (Unsigned) 	  	  	  	  	 
ADC 	
	ADCA 	A = A+M+C					65C02: ADC 
	ADCB 	B = B+M+C
ADD 	
	ADDA 	A = A+M						65C02: CLC, ADC 
	ADDB 	B = B+M
	ADDD 	D = D+M:M+1 (16-bit)
AND 	
	ANDA 	A = A && M 					65C02: AND 
	ANDB 	B = B && M 
	ANDCC 	CC = CC && IMM
ASL 	 Arithmetic shift left			
	ASLA	A: C<--B7...B0<--0				65C02: ASL
	ASLB 	B: C<--B7...B0<--0	
	ASL 	M: C<--B7...B0<--0
ASR 	Arithmetic shift right
	ASRA 	A:B7-->B7...B0-->C				65C02: LDA addr , ASL , ROR addr
	ASRB 	B:B7-->B7...B0-->C
	ASR		M:B7-->B7...B0-->C
BCC	Branch if Carry Clear					65C02: BCC
BCS	Branch if Carry Set					65C02: BCS
BEQ	Branch if Equal 					65C02: BEQ
BGE	Branch if Great/Equal
BGT	Branch if Greater Than
BHI	Branch if Higher
BHS	Branch if Higher/Same					65C02: BCS
	
BIT 
	BITA 	Bit Test A (M&&A)				65C02: LDA,AND
	BITB 	Bit Test B (M&&B) 

BLE	Branch if Less/Equal
BLO	Branch if Lower 
BLS	Branch if Lower/Same					65C02: BCC
BLT	Branch if Less Than	
BMI	Branch if Minus 					65C02: BMI
BNE	Branch if Not Equal					65C02: BNE
BPL	Branch if Plus 						65C02: BPL
BRA	Branch Always 						65C02: BRA
BRN	Branch Never 
BSR	Branch to Subroutine 					65C02: JSR
BVC	Branch if Overflow Clr
BVS	Branch if Overflow Set
	
CLR 	
	CLRA 	0-->A						65C02: LDA#0					
	CLRB 	0-->B
	CLR  	0-->M						65C02: STZ 
CMP 	
	CMPA 	Compare M from A				65C02: CMP
	CMPB 	Compare M from B 
	CMPD 	Compare M:M+1 from D (16-bit)
	CMPS 	Compare M:M+1 from S (16-bit)
	CMPU 	Compare M:M+1 from U (16-bit)
	CMPX 	Compare M:M+1 from X (16-bit)
	CMPY 	Compare M:M+1 from Y (16-bit)
COM 	
	COMA 	A = complement(A)				65C02: LDA#FF,EOR
	COMB 	B = complement(B) 	
	COM 	M = complement(M)
CWAI 	CC = CC ^ IMM; Wait for Interrupt 			65C02: WAI
DAA 	Decimal Adjust A 					
DEC 	
	DECA 	A = A-1 	 				65C02: DEC
	DECB 	B = B-1
	DEC 	M = M-1 	
EOR 	
	EORA 	A = A XOR M					65C02: EOR
	EORB 	B = M XOR B
EXG		Exchange R1,R2 	  	  	  	  	65C02: push / pull registers from / to stack
INC 	
	INCA 	A = A + 1					65C02: INC
	INCB 	B = B + 1
	INC 	M = M + 1 
JMP 	pc = EA 	  	  	  	  	 	65C02: JMP
JSR 	jump to subroutine 	  	  	  	  	65C02: JSR		 
LD 	
	LDA 	A = M 						65C02: LDA
	LDB 	B = M
	LDD 	D = M:M+1 (16-bit) 				65C02: use zero page as 16 bit register
	LDS 	S = M:M+1 (16-bit)				
	LDU 	U = M:M+1 (16-bit)
	LDX 	X = M:M+1 (16-bit)
	LDY 	Y = M:M+1 (16-bit)
LEA		EA is the effective address			65C02: LDA absolut indexd
	LEAS 	S = EA	
	LEAU 	U = EA
	LEAX 	X = EA						65C02: LDA a,x
	LEAY 	Y = EA						65C02: LDA a,y
LSL 	Logical shift left					
	LSLA	A: C<--B7...B0<--0				65C02: ASL
	LSLB	B: C<--B7...B0<--0
	LSL		M: C<--B7...B0<--0
LSR		Logical shift right
	LSRA 	A:0-->B7...B0-->C				65C02: LSR
	LSRB	B:0-->B7...B0-->C
	LSR		M:0-->B7...B0-->C
MUL		D = A*B (Unsigned)				65C02: Must be done in software	
NEG 										
	NEGA	A = !A + 1					65C02: EOR $FF , ADC 1
	NEGB	B = !B + 1
	NEG		M = !M + 1
NOP 	No Operation 	  	  	  	  	 	65C02: NOP
OR 	
	ORA 	A = A || M					65C02: ORA
	ORB 	B = B || M
	ORCC 	C = CC || IMM
PSH 	
	PSHS 	Push Registers on S Stack			65C02: PHX,PHY,PHA	 	  	  	  	  	 
	PSHU 	Push Registers on U Stack 	  	  	  	  	 
PUL 	
	PULS 	Pull Registers from S Stack 	  		65C02: PLA,PLY,PLX	  	  	  	 
	PULU 	Pull Registers from U Stack 	  	  	  	  	 
ROL		Rotate left thru carry				65C02: ROL
	ROLA 	A: C<--B7...B0<--C 	
	ROLB	B: C<--B7...B0<--C
	ROL		M: C<--B7...B0<--C
ROR 	Rotate Right thru carry 				65C02: ROR
	RORA	A: C-->B7...B0-->C   
	RORB 	B: C-->B7...B0-->C
	ROR 	M: C-->B7...B0-->C
RTI		Return from Interrupt  				65C02: RTI
RTS		Return from subroutine 	  	  	  	  	 
SBC 			
	SBCA A = A - M - C 					65C02: SBC
	SBCB B = B - M - C  
SEX Sign extend B into A 
ST 	
	STA 	M = A						65C02: STA
	STB 	M = B
	STD 	M:M+1 = D (16-bit)
	STS 	M:M+1 = S (16-bit)
	STU 	M:M+1 = U (16-bit)
	STX 	M:M+1 = X (16-bit)				65C02: STX
	STY 	M:M+1 = Y (16-bit)				65C02: STY
SUB 	
	SUBA 	A = A - M					65C02: CLC,SBC
	SUBB 	B = B - M
	SUBD 	D = D - M:M+1 (16-bit)
SWI 	
	SWI Software interrupt 1 	  	  	  	  	 
	SWI2 Software interrupt 2 	  	  	  	  	 
	SWI3 Software interrupt 3 	  	  	  	  	 
SYNC Synchronize to Interrupt 	  	  	  	  	 
TFR R1,R2 	transfer register R1 to R2			65C02: TAX,TAY,TSX,TXA,TXS,TYA  	  	  	  	 
TST 	
	TSTA 	Test A 						65C02: 
	TSTB 	Test B 
	TST 	Test M 

