;Asteroids converted to 65C02 ACME assember (C) 2021 Uwe Gottschling
;source code translated dully and not optimised!
;Based on https://github.com/74hc595/Ultim809/tree/master/code/user/asteroids  
;Matt Sarnoff
;www.msarnoff.org
;github.com/74hc595
;October 23, 2010
; Matt Sarnoff (msarnoff.org/6809) - May 5, 2010
;
; SPACE ROCKS IN SPACE!
; An asteroids clone.
;

!CPU W65C02

!src "../Kernel/include/label_definition.asm"
!src "../Kernel/include/Jumptable.asm"
!src "../Kernel/DOS/DOS_FAT_constant.asm"
	;.define	ONE_BULLET
	
; VRAM addresses (for Graphics I)
SPRPATTABLE		= $3800	; $3800 ... $3FFF
PATTABLE		= $0000	; $0000 ... $17FF
SPRATTABLE		= $1B00	; $1B00 ... $1B7F
NAMETABLE		= $1800	; $1800 ... $1AFF
COLORTABLE		= $2000	; $2000 ... $201F
VRAM			= $40	; TMS9918 video ram register bit

; Parameters
MAX_LG_ASTS		= 6	;max. 6 big --> 12 medium --> 24 small asteroids = 6 + 12 + 24 = 42
NUM_ASTEROIDS		= MAX_LG_ASTS*7	; 6 * 7 = 42 asteroids
NUM_SCRBULLETS		= 2	;saucer bullets

!ifdef ONE_BULLET {
NUM_SHIPBULLETS		= 1
} else {
NUM_SHIPBULLETS		= 4
}

AST_LG_SIZE		= 16
AST_MED_SIZE		= 12
AST_SM_SIZE		= 8
SHIP_SIZE		= 16
EXPLOSION_SIZE		= 16
SAUCER_LG_SIZE		= 14
SAUCER_SM_SIZE		= 10
EXTRA_LIFE_AMT		= $10	;BCD thousands (10000)	

; Object structure offsets
life			= 0	;00 = dead, 01 ... $7F = ageing, $80 ... $FF = life, $30 = kill
type			= 1
xpos			= 2	;x and y are 8.8 fixed-point
xpos_frac		= 2
xpos_int		= 3
ypos			= 4
ypos_frac		= 4
ypos_int		= 5
xvel			= 6	;x and y velocities are 8.8 fixed point
yvel			= 8	;low byte = frac, hight byte = int
pattern			= 10
siz			= 11
OBJ_SIZE		= 12

; Object types
; Lower 2 bits of bullet type indicate owner
saucer			= 1
player1			= 2
player2			= 3
player_mask		= 2
owner_mask		= 3
bullet			= 8
asteroid		= 16

;Joystick port: bit 0 = up, bit 1 = down, bit 2 = left, bit 3 = right, bit 4 = fire	
JOY_UP		= %00000001
JOY_DOWN	= %00000010
JOY_LEFT	= %00000100
JOY_RIGHT	= %00001000
JOY_FIRE	= %00010000

; sprite pattern numbers
SPR_AST1_LG		=	0
SPR_AST2_LG		=	4
SPR_AST3_LG		=	8
SPR_AST4_LG		=	12	;$0C
SPR_AST1_MED		=	16	;$10
SPR_AST2_MED		=	20	;$14
SPR_AST3_MED		=	24	;$18
SPR_AST4_MED		=	28	;$1C
SPR_AST1_SM		=	32	;$20
SPR_AST2_SM		=	36	;$24
SPR_AST3_SM		=	40	;$28
SPR_AST4_SM		=	44	;$2C
SPR_SAUCER_LG		=	48	;$30
SPR_SAUCER_SM		=	52	;$34
SPR_BULLET		=	56	;$38
SPR_SHIP		=	64	;$40
SPR_EXPL1_START		=	192	;asteroid/saucer explosion
SPR_EXPL2_START		=	208	;ship explosion

;------------------------------------------------------------------------------
; variables
;------------------------------------------------------------------------------

; Put all game variables in the direct page
VARSTART	=	$80

; Temporary storage
TEMP1		=	VARSTART
TEMP2		=	VARSTART+1
TEMP3		=	VARSTART+2
TEMP4		=	VARSTART+3

ATTRACTMODE	=	VARSTART+4
SCORE		=	VARSTART+5	;4 BCD digits (0-99990)
SCORE_MSB	=	VARSTART+5
SCORE_LSB	=	VARSTART+6
LIVES		=	VARSTART+7	;player lives
NEXT_LIFE_AT	=	VARSTART+8
RESPAWN_TIMER	=	VARSTART+9
NEW_LEVEL_TIMER	=	VARSTART+10
THUMP_TIMER	=	VARSTART+11
THUMP_PITCH	=	VARSTART+12
LIFESND_TIMER	=	VARSTART+13
START_ASTEROIDS	=	VARSTART+14	;number of asteroids at level start
ASTEROIDS_LEFT	=	VARSTART+15	;number of asteroids left
FRAMECOUNTER	=	VARSTART+16
PAD1STATE	=	VARSTART+17
PAD2STATE	=	VARSTART+18
PREVPAD1STATE	=	VARSTART+19
PREVPAD2STATE	=	VARSTART+20
NEXT_AST_PTR	=	VARSTART+21	;2 bytes
SHIPANGLE	=	VARSTART+23
HYPERSPACE	=	VARSTART+24	;ship is in hyperspace
SAUCER_TIMER	=	VARSTART+25	;time until next saucer
NEXT_SAUCER_INT	=	VARSTART+26	;next interval between saucers
LARGE_SAUCERS	=	VARSTART+27	;large saucers left
NEXT_LG_SAUCERS	=	VARSTART+28	;next number of large saucers

; Display actions
DISPLAY_ACTION	=	VARSTART+29

printerror	=	1
printstart	=	2
printgameover	=	3
clearstrings	=	4

; Sound
; Channel 1:	thrust noise
; Channel 2:	saucer sound
; Channel 3:	explosions
; Channel 4:	bullet sound
; Channel 5: 	thump sound 
; Channel 6:	life sound

SNDPTR		=	VARSTART+30	;2 bytes
SAUCERSNDPTR	=	VARSTART+32	;2 bytes
BGSOUND_BITS	=	VARSTART+34
lifesnd		=	$80	;extra life sound 
saucersnd	=	$40	;then saucer sound
thrustsnd	=	$20	;then thrust sound
thumpsnd	=	$10	;then thump sound

ERROR_MSG_PTR	=	VARSTART+35	;2 bytes
SAVEIRQVECT_L	=	VARSTART+37
SAVEIRQVECT_H	=	VARSTART+38

REGISTER_D_L	=	VARSTART+39	;register D replacement 
REGISTER_D_H	=	VARSTART+40

REGISTER_B	=	VARSTART+39	;register B replacement shared with register D
REGISTER_A	=	VARSTART+40	;register A replacement shared with register D 

REGISTER_X_L	=	VARSTART+41	;register X replacement 
REGISTER_X_H	=	VARSTART+42

REGISTER_Y_L	=	VARSTART+43	;register Y replacement 
REGISTER_Y_H	=	VARSTART+44

REGISTER_U_L	=	VARSTART+45	;register U replacement 
REGISTER_U_H	=	VARSTART+46

REGISTER_RND	=	VARSTART+47

SCORETEMP1	= 	VARSTART+48
SCORETEMP2	=	VARSTART+49
SCORETEMP3	=	VARSTART+50
SCORETEMP4	=	VARSTART+51
RANDSEED	= 	VARSTART+52
RANDSEED1	= 	VARSTART+53
RANDSEED2	= 	VARSTART+54
RANDSEED3	= 	VARSTART+55
SNDPTR_SHIP	=	VARSTART+56	; 2 Byte

*=$0FFE			;programm start address header
!word $1000	
	
;------------------------------------------------------------------------------
; setup
;------------------------------------------------------------------------------

		stz VIA2DDRB	; input for joystick interface
		stz VIA2ACR	; input latch disable
		lda #$FF
		sta VIA2ORB 
		sei
		lda IRQVector_L		;save old interrupt vector
		sta SAVEIRQVECT_L
		lda IRQVector_H
		sta SAVEIRQVECT_H
		lda #<VBLANK		;set up interrupt vector
		sta IRQVector_L	
		lda #>VBLANK
		sta IRQVector_H			
		cli
		ldx #VDP_Set_Video_Mode_FN
		lda #TMS9918_Set_Screen1	;32 x 24 patterns, 256 types, 256x192 pixel
		jsr VDP_Function_Call
		ldx #VDP_Init_Pattern_FN	;init standard pattern
		jsr VDP_Function_Call
		ldx #VDP_Clear_Text_Screen_FN	;clear VRAM
		jsr VDP_Function_Call
		jsr SETRANDOMSEED
; write the sprite patterns to VRAM
		lda #<SPRITEPATS
		sta REGISTER_X_L
		lda #>SPRITEPATS
		sta REGISTER_X_H
		lda #<SPRPATTABLE
		sta TMS9918REG
		lda #VRAM OR >SPRPATTABLE
		sta TMS9918REG		
		ldy #>SPRITEPATS_END-SPRITEPATS	;256 patterns (2048 bytes)
@loop1			
		ldx #<SPRITEPATS_END-SPRITEPATS
@loop2		
		nop
		nop
		nop
		nop
		lda (REGISTER_X_L)
		sta TMS9918RAM
		jsr IncPTR_X
		dex
		bne @loop2
		dey
		bne @loop1
; write the text patterns to VRAM
		lda #<TEXTPATS
		sta REGISTER_X_L
		lda #>TEXTPATS
		sta REGISTER_X_H
		lda #<PATTABLE+(32*8)
		sta TMS9918REG
		lda #VRAM OR >PATTABLE+(32*8)
		sta TMS9918REG		
		ldy #>TEXTPATS_END-TEXTPATS
@loop3			
		ldx #<TEXTPATS_END-TEXTPATS
@loop4		
		nop
		nop
		nop
		lda (REGISTER_X_L)
		sta TMS9918RAM
		jsr IncPTR_X
		dex
		bne @loop4
		dey
		bne @loop3
; clear the color table
		sta TMS9918REG	
		lda #VRAM OR>COLORTABLE	;set address marker and high Byte
		sta TMS9918REG	
		ldy #$20
		lda #$F0		;clear text color / background color
VDP_Clear_Screen1
		sta TMS9918RAM
		nop			;3 x 2 cycl.
		nop			
		nop			
		dey
		bne VDP_Clear_Screen1
; write to the name table
		lda #<authorstr		
		sta REGISTER_X_L
		lda #>authorstr		
		sta REGISTER_X_H	
		jsr VDP_PRINTPSTR
		lda #<authorstr2	
		sta REGISTER_X_L
		lda #>authorstr2	
		sta REGISTER_X_H	
		jsr VDP_PRINTPSTR
; initialize sound effects
		lda #<MUTE
		sta SNDPTR
		stz SNDPTR_SHIP		
		lda #>MUTE
		sta SNDPTR+1
		stz SNDPTR_SHIP+1
		stz SAUCERSNDPTR
		stz SAUCERSNDPTR+1
		stz SNDPTR_SHIP+1
		jsr YMF262Init
; set up attract mode
		jsr CLR_OBJECTS		;clear everything first
		stz HYPERSPACE
		lda #4			;put some asteroids up
		sta REGISTER_B
		jsr AST_NEW_SET		;register B = number of asteroids
		stz FRAMECOUNTER
		lda #0			
		sta SCORE		;no score, no lives
		sta SCORE+1
		sta LIVES
		sta RESPAWN_TIMER	;turn off timers	
		sta NEW_LEVEL_TIMER
		sta SAUCER_TIMER
		sta BGSOUND_BITS
		inc
		sta ATTRACTMODE
		lda #printstart
		sta DISPLAY_ACTION
; enable interrupts
; turn on the display, enable vertical blanking interrupt
		lda #$42+$20+$80	;enable VDC interrupt
		sta TMS9918REG
		lda #$81
		sta TMS9918REG
		jmp loop 

;------------------------------------------------------------------------------
; controller input routines
;------------------------------------------------------------------------------
rotateleft
		
		lda SHIPANGLE
		inc

storeshipangle
		and #%00011111
		sta SHIPANGLE
		jmp checkthrust

rotateright
		lda SHIPANGLE
		dec
		bra storeshipangle

thrust
		lda #<TRIGTABLE		;get thrust vector
		sta REGISTER_U_L
		lda #>TRIGTABLE
		sta REGISTER_U_H	
		lda SHIPANGLE
		asl
		asl
		tay			;get entry in trig table
		lda (REGISTER_U_L),y	;load frac
		sta REGISTER_B
		iny
		lda (REGISTER_U_L),y	;load int
		sta REGISTER_A
		lda REGISTER_A
		asl 			;divide by 4
		ror REGISTER_A
		ror REGISTER_B
		lda REGISTER_A
		asl 
		ror REGISTER_A
		ror REGISTER_B
		lda REGISTER_B
		clc
		adc PLAYERSHIP+xvel	;add to x velocity component
		sta PLAYERSHIP+xvel
		lda REGISTER_A
		adc PLAYERSHIP+xvel+1
		sta PLAYERSHIP+xvel+1
		iny 
		lda (REGISTER_U_L),y	;load frac
		sta REGISTER_B		;get sine value
		iny
		lda (REGISTER_U_L),y	;load int
		sta REGISTER_A	
		lda REGISTER_A
		asl 			;divide by 4
		ror REGISTER_A
		ror REGISTER_B
		lda REGISTER_A
		asl 
		ror REGISTER_A
		ror REGISTER_B
		lda REGISTER_B		
		clc
		adc PLAYERSHIP+yvel	;add to y velocity component
		sta PLAYERSHIP+yvel
		lda REGISTER_A
		adc PLAYERSHIP+yvel+1
		sta PLAYERSHIP+yvel+1	
		jsr THRUSTSND_ON
		bra gameinpdone		
	
checkfire
		lda PAD1STATE
		and #JOY_FIRE
		bne fire
		rts
		
fire 
		lda PREVPAD1STATE
		and #JOY_FIRE
		beq fire2
		rts

fire2
		jsr GET_BULLET		;find an available bullet ; returns a pointer to it in X
		bcc gameinpdone		;no place free for bullet
		lda #<PLAYERSHIP	;pointer to object firing the bullet
		sta REGISTER_Y_L
		lda #>PLAYERSHIP
		sta REGISTER_Y_H
		lda SHIPANGLE		;bullet angle
		jsr FIRE_BULLET		;fire bullet
		lda #<SHIPFIRE_SOUND
		sta SNDPTR_SHIP
		lda #>SHIPFIRE_SOUND
		sta SNDPTR_SHIP+1		
		rts

set_hyperspace
		stz PLAYERSHIP+life	;disappear ship
		jsr RANDBYTE		;will hyperspace kill?
		ora #%11111000		;1 in 8 chance
		sta HYPERSPACE
		lda #$30		;enable random position spawn
		sta RESPAWN_TIMER
		bra gameinpdone
GAME_INPUT	
		jsr checkfire
		lda FRAMECOUNTER 	;read game input every other frame
		and #%00000001
		bne gameinpdone
		jsr checkhyper
		lda PAD1STATE
		and #JOY_LEFT
		beq GAME_INPUT2
		jmp rotateleft
GAME_INPUT2
		lda PAD1STATE
		and #JOY_RIGHT
		beq checkthrust
		jmp rotateright
checkthrust
		lda FRAMECOUNTER
		and #%00000011		;read thrust button every 4th frame
		bne gameinpdone
		lda PAD1STATE
		and #JOY_UP
		beq checkthrust2	
		jmp thrust
checkthrust2
		jsr THRUSTSND_OFF	;turn off thrust sound
		rts

checkhyper
		lda PAD1STATE
		and #JOY_DOWN
		bne set_hyperspace
		rts
gameinpdone
		rts
		
;------------------------------------------------------------------------------
; timer response routines
;------------------------------------------------------------------------------
; new level timer
CHECK_TIMERS
		lda NEW_LEVEL_TIMER	;new level?
		beq chkrespawn		;timer is 0, so no
		dec NEW_LEVEL_TIMER	;timer is 1? yes
		beq startnewlevel
; respawn player timer
chkrespawn
		lda RESPAWN_TIMER	;need to respawn the player?
		beq chksaucer		;timer is 0, so no
		cmp #1
		beq respawn		;timer is 1, try to respawn
		dec RESPAWN_TIMER	;timer is > 1, decrement timer
		bra chksaucer
startnewlevel
		lda START_ASTEROIDS	;next level has 1 more asteroid
		inc
		cmp #MAX_LG_ASTS
		bmi newastset
		lda #MAX_LG_ASTS
newastset
		sta START_ASTEROIDS
		sta REGISTER_B
		jsr AST_NEW_SET		;register B = number of asteroids
		lda #240		;first saucer after 8 seconds
		sta NEXT_SAUCER_INT
		sta SAUCER_TIMER
		lda NEXT_LG_SAUCERS	;set number of large saucers
		sta LARGE_SAUCERS
		stz NEW_LEVEL_TIMER
		jsr THUMPSND_ON		;play thump sound
		bra chkrespawn
respawn
		lda LIVES		;can't respawn with zero lives
		beq chksaucer
		jsr SHIP_CAN_SPAWN	;is it safe to respawn? ; can't respawn C=clear
		bcc chksaucer		;if not, try again next frame
		jsr THUMPSND_ON		;play thump sound
		stz RESPAWN_TIMER
		lda NEXT_SAUCER_INT	;reset saucer timer
		sta SAUCER_TIMER
		jsr SHIP_INIT		;the ship might die here if in hypersp.
		stz HYPERSPACE
chksaucer
		lda FRAMECOUNTER	;update countdown every other frame
		and #%00000001
		bne timersdone
		lda SAUCER_TIMER	;is timer zero?
		beq timersdone		;if so, don't update
		dec SAUCER_TIMER	;decrement saucer counter
		bne timersdone		;greater than 0? do nothing
		jsr SAUCER_INIT		;0 now? spawn a saucer
timersdone	
		rts
;------------------------------------------------------------------------------
; logic update routine
;------------------------------------------------------------------------------

; update timers
loop
		jsr CHECK_TIMERS	

; update thump sound effect
		jsr THUMP_UPDATE
; read controllers
chkinput
		lda ATTRACTMODE		;attract mode? ; greater than 0 is attract-mode
		bne attractinput	;yes, wait for start button
chkinput2
		lda PLAYERSHIP+life
		bpl updatesaucer	;don't read game controls if dead
		jsr GAME_INPUT		;no, read game controls
		bra updateship		;player is alive, update ship
; attract mode input, wait for button
attractinput
		lda RESPAWN_TIMER	;game over timer
		beq attractinput2
		dec RESPAWN_TIMER
		bra updatesaucer
attractinput2
		lda PAD1STATE 
		and #JOY_FIRE		;start button pressed?
		beq updatesaucer	;no, skip directly to object update
		jsr NEW_GAME		;otherwise, start game
		jmp updatesaucer
; update ship state (skipped if player is dead)
updateship
		lda SHIPANGLE		;update ship sprite
		asl			;multiply by 4 to get sprite number
		asl
		clc
		adc #SPR_SHIP		;add sprite offset
		sta PLAYERSHIP+pattern
		lda #<PLAYERSHIP+xvel
		sta REGISTER_X_L
		lda #>PLAYERSHIP+xvel
		sta REGISTER_X_H
		jsr DECELERATE
		jsr IncPTR_X
		jsr IncPTR_X
		jsr DECELERATE
; update saucer
updatesaucer
		lda SAUCER+life		;saucer alive?
		bpl astcoll		;no, skip
		lda SAUCER+xvel+1	;moving left or right?
		bmi movingleft
movingright
		lda SAUCER+xpos_int
		cmp #240		;off right edge? ; xboundscheck is 248 dez - siz/2 = 241 for big saucer
		bcc keeponscreen
		bra removesaucer
movingleft
		lda SAUCER+xpos_int
		cmp #2			;off left edge?
		bcc removesaucer	;branch if lower/same

keeponscreen
		jsr SAUCER_ACTION	;move and/or fire
		bra astcoll
removesaucer
		stz SAUCER+life		;remove saucer
		jsr SAUCERSND_OFF	;stop sound
		jsr SAUCER_NEXT		;set up the next one
astcoll
		lda #<SAUCER
		sta REGISTER_X_L
		lda #>SAUCER
		sta REGISTER_X_H
astcolloop
		ldy #life		;skip dead/dying objects
		lda (REGISTER_X_L),y
		bpl nextast	
astshipcoll
		lda PLAYERSHIP+life	;player alive?
		bpl astsaucercoll	;no, skip
		lda #<PLAYERSHIP
		sta REGISTER_Y_L
		lda #>PLAYERSHIP
		sta REGISTER_Y_H
		jsr OBJ_OBJ_COLL
astsaucercoll
		lda REGISTER_X_L	;don't compare saucer to saucer
		cmp #<SAUCER
		bne astsaucercoll2		
		lda REGISTER_X_H
		cmp #>SAUCER		
		beq nextast
astsaucercoll2
		lda SAUCER+life		;saucer alive?
		bpl nextast		;no, skip
		lda #<SAUCER
		sta REGISTER_Y_L
		lda #>SAUCER
		sta REGISTER_Y_H
		jsr OBJ_OBJ_COLL		
			
nextast		
		lda REGISTER_X_L	; object structure size
		clc
		adc #OBJ_SIZE
		sta REGISTER_X_L
		lda REGISTER_X_H
		adc #0
		sta REGISTER_X_H
		lda REGISTER_X_L
		cmp #<ASTEROIDS_END		
		bne astcolloop
		lda REGISTER_X_H
		cmp #>ASTEROIDS_END		
		bne astcolloop
; bullet -> ship/saucer/asteroid collision detection
		lda #<BULLETS	
		sta REGISTER_Y_L
		lda #>BULLETS	
		sta REGISTER_Y_H
bulletloop
		ldy #life		;skip dead bullets
		lda (REGISTER_Y_L),y		
		beq nextbullet
		ldy #type		;get bullet owner
		lda (REGISTER_Y_L),y
		and #owner_mask		;owner_mask = %00000011
		tax			;save bullet owner ;save owner for compare
;---- inner loop
		lda #<PLAYERSHIP	
		sta REGISTER_X_L
		lda #>PLAYERSHIP	
		sta REGISTER_X_H
objcolloop
		ldy #life
		lda (REGISTER_X_L),y	;skip dead/dying objects		
		bpl nextcollobj
		ldy #type
		txa			;get old owner 
		cmp (REGISTER_X_L),y	;bullets can't kill their owners
		beq nextcollobj		;skip if this object owns the bullet
		jsr BULLET_COLL
		ldy #life		
		lda (REGISTER_Y_L),y	;might have killed the bullet
		beq nextbullet
nextcollobj
		lda REGISTER_X_L
		clc
		adc #OBJ_SIZE
		sta REGISTER_X_L
		lda REGISTER_X_H
		adc #0
		sta REGISTER_X_H		
		lda REGISTER_X_L
		cmp #<OBJECTS_END		
		bne objcolloop
		lda REGISTER_X_H
		cmp #>OBJECTS_END
		bne objcolloop
;---- end inner loop
nextbullet
		lda REGISTER_Y_L
		clc
		adc #OBJ_SIZE
		sta REGISTER_Y_L
		lda REGISTER_Y_H
		adc #0
		sta REGISTER_Y_H
		lda REGISTER_Y_L
		cmp #<BULLETS_END		
		bne bulletloop
		lda REGISTER_Y_H
		cmp #>BULLETS_END	
		bne bulletloop
; move objects
updateobjects
		lda #<OBJECTS
		sta REGISTER_X_L
		lda #>OBJECTS
		sta REGISTER_X_H	
objloop
		ldy #life
		lda (REGISTER_X_L),y	;skip dead objects
		beq nextobj
		jsr OBJ_MOVE		;add velocity to position
		jsr OBJ_BOUNDSCHK	;handle wraparound
		jsr OBJ_UPDATELIFE	;decrement life counter if necessary
nextobj
		lda REGISTER_X_L
		clc
		adc #<OBJ_SIZE
		sta REGISTER_X_L
		lda REGISTER_X_H
		adc #0
		sta REGISTER_X_H
		lda REGISTER_X_H
		cmp #>OBJECTS_END		
		bne objloop			
		lda REGISTER_X_L
		cmp #<OBJECTS_END		
		bne objloop
; advance frame counter and save controller state
		inc FRAMECOUNTER
		lda PAD1STATE
		sta PREVPAD1STATE
		lda PAD1STATE+1
		sta PREVPAD1STATE+1
; update sound effects
		jsr SND_UPDATE

CheckExit		
		ldx #PS2_ChkKey_FN
		jsr PS2_Function_Call	
		bcc notexit
		ldx #PS2_GetKey_FN
		jsr PS2_Function_Call
		cmp #"x"		;press x for exit
		beq CheckExit2
		bra CheckExit3
CheckExit2
		bra CheckExit4
CheckExit3
		cmp #"p"		;pause key
		bne notexit		
		ldx #PS2_GetKey_FN
		jsr PS2_Function_Call 
		bra notexit
CheckExit4
		sei	
		lda SAVEIRQVECT_L
		sta IRQVector_L
		lda SAVEIRQVECT_H
		sta IRQVector_H
		lda #$42+$80		;disable VDC interrupt
		sta TMS9918REG
		lda #$81
		sta TMS9918REG
		lda #$11		;change frame color
		sta TMS9918REG
		lda #$87
		sta TMS9918REG	
		jsr OPL3_RESET	
		rts	
notexit
		wai			;wait for interrupt, only with W65C02 / W65C816 !
		jmp loop
		
;------------------------------------------------------------------------------
; end of logic update routine
;------------------------------------------------------------------------------		
THUMP_UPDATE
		dec THUMP_TIMER
		bne nothumpreset
		lda ASTEROIDS_LEFT
		asl
		clc
		adc #10
		cmp #60
		bmi setthtimer	 	;branch if lower
		lda #60
setthtimer
		sta THUMP_TIMER
		lda THUMP_PITCH
		eor #$ff
		sta THUMP_PITCH		;complement(A)
nothumpreset
		rts		

;update position of object pointed to by X
OBJ_MOVE
		ldy #xvel		;add velocity to position
		lda (REGISTER_X_L),y
		ldy #xpos
		clc
		adc (REGISTER_X_L),y
		sta (REGISTER_X_L),y
		ldy #xvel+1
		lda (REGISTER_X_L),y
		ldy #xpos+1
		adc (REGISTER_X_L),y
		sta (REGISTER_X_L),y		
		ldy #yvel
		lda (REGISTER_X_L),y
		ldy #ypos
		clc
		adc (REGISTER_X_L),y
		sta (REGISTER_X_L),y
		ldy #yvel+1
		lda (REGISTER_X_L),y
		ldy #ypos+1
		adc (REGISTER_X_L),y
		sta (REGISTER_X_L),y	
		rts
		
; handle bounds checking and wraparound of object pointed to by X
OBJ_BOUNDSCHK
		lda #$FF		;max coordinates
		ldy #siz
		sec
		sbc (REGISTER_X_L),y
		sta TEMP1		;TEMP1 = $FF - siz = max. X coordinate
		lda #$BF		;$BF = 191 dez
		sec
		sbc (REGISTER_X_L),y				
		sta TEMP2		;TEMP2 = $BF - siz = max. Y coordinate
		lda (REGISTER_X_L),y
		beq yboundscheck	;ignore the x check if size is 0
				
xboundscheck
		ldy #xpos_int
		lda (REGISTER_X_L),y
		cmp #$F8		;$F8 = 248 dez ;object has gone past left edge?
		bcs warptoright		;branch if higher/same
		beq warptoright
		cmp TEMP1		;object has gone past right edge? ; TEMP1 = max. X coordinate
		bcc yboundscheck	;branch if lower/same
warptoleft
		lda #0
		ldy #xpos_int
		sta (REGISTER_X_L),y
		ldy #xpos_frac
		sta (REGISTER_X_L),y		
		bra yboundscheck
warptoright
		lda TEMP1		; TEMP1 = max. X coordinate
		dec
		ldy #xpos_int
		sta (REGISTER_X_L),y
		lda #0
		ldy #xpos_frac
		sta (REGISTER_X_L),y		
yboundscheck
		ldy #ypos_int
		lda (REGISTER_X_L),y
		cmp #$F8		;object has gone past top edge?
		bcs warptobottom	;branch if higher/same
		beq warptobottom		
		cmp TEMP2		;object has gone past bottom edge? ; TEMP2 = max. Y coordinate
		bcc bchkdone		;branch if lower/same
warptotop
		ldy #ypos_int
		lda #0
		sta (REGISTER_X_L),y
		ldy #ypos_frac
		sta (REGISTER_X_L),y		
		bra bchkdone		

warptobottom
		lda TEMP2		; max. Y coordinate
		dec
		ldy #ypos_int
		sta (REGISTER_X_L),y
		ldy #ypos_frac
		lda #0
		ldy #ypos_frac
		sta (REGISTER_X_L),y		
bchkdone
		rts

; decrement the life of the object pointed to by X, if its life value
; is between $01 and $7F.
OBJ_UPDATELIFE
		ldy #life
		lda (REGISTER_X_L),y
		beq infinitelife	;0 life? already dead, skip
		bmi infinitelife	;negative life? invincible
		ldy #life
		lda (REGISTER_X_L),y
		dec
		sta (REGISTER_X_L),y
		ldy #pattern
		lda (REGISTER_X_L),y	;update explosion animation?
		tax			;save pattern
		cmp #SPR_EXPL1_START
		bcc noanim
OBJ_UPDATELIFE2	
		ldy #life
		lda (REGISTER_X_L),y
		and #%00000011
		bne noanim
		txa			; store old value
		clc
		adc #4
		ldy #pattern
		sta (REGISTER_X_L),y
noanim
infinitelife
		rts
		
; for the 16-bit value v pointed to by X,
; compute 0.984375*v and store it back
; (done by subtracting v/64 from v.)

DECELERATE			
		lda (REGISTER_X_L)
		sta REGISTER_D_L
		ldy #1
		lda (REGISTER_X_L),y
		sta REGISTER_D_H	

		lda REGISTER_D_H
		beq DECELERATE2
		bra DECELERATE3
DECELERATE2
		lda REGISTER_D_L
		bne DECELERATE3
		rts			; if zero velocitie exit 
DECELERATE3
		lda REGISTER_D_H
		asl
		ror REGISTER_D_H
		ror REGISTER_D_L		;1
		lda REGISTER_D_H
		asl
		ror REGISTER_D_H
		ror REGISTER_D_L		;2
		lda REGISTER_D_H
		asl
		ror REGISTER_D_H
		ror REGISTER_D_L		;4		
		lda REGISTER_D_H
		asl
		ror REGISTER_D_H
		ror REGISTER_D_L		;8
		lda REGISTER_D_H
		asl
		ror REGISTER_D_H
		ror REGISTER_D_L		;16
		lda REGISTER_D_H
		asl
		ror REGISTER_D_H
		ror REGISTER_D_L		;32
		lda REGISTER_D_L
DECELERATE4				
		sta TEMP1
		bne DECELERATE5			; Patch: work not perfect in plus directions
		lda #$1				; minimal velocitie for positiv direction
		sta TEMP1
DECELERATE5		 
		lda REGISTER_D_H
		sta TEMP2
		lda (REGISTER_X_L)				
		sec 
		sbc TEMP1
		sta (REGISTER_X_L)		
		lda (REGISTER_X_L),y
		sbc TEMP2
		sta (REGISTER_X_L),y
		lda (REGISTER_X_L),y
DECELERATE3_exit
		rts				
	
;------------------------------------------------------------------------------
; vertical blanking interrupt handler
;------------------------------------------------------------------------------
VBLANK	
		lda REGISTER_X_L
		pha
		lda REGISTER_X_H
		pha				
		lda TMS9918REG		;read status, clear interrupt flag
; send new sprite attributes
		lda #<SPRATTABLE
		sta TMS9918REG
		lda #VRAM OR >SPRATTABLE
		sta TMS9918REG
		lda FRAMECOUNTER
		and #01
		bne drawbackwards
drawsprites
		lda #<OBJECTS
		sta REGISTER_X_L
		lda #>OBJECTS
		sta REGISTER_X_H
spriteloop
		ldy #life
		lda (REGISTER_X_L),y
		beq nextsprite		;skip dead objects
		ldy #ypos_int
		lda (REGISTER_X_L),y	
		sta TMS9918RAM		;y position
		ldy #xpos_int
		lda (REGISTER_X_L),y	
		sta TMS9918RAM		;x position
		ldy #pattern
		lda (REGISTER_X_L),y
		sta TMS9918RAM		;sprite name
		nop
		nop
		nop
		nop
		lda #$0F		
		sta TMS9918RAM		;color
nextsprite
		lda REGISTER_X_L
		clc
		adc #<OBJ_SIZE 		;advance to next
		sta REGISTER_X_L
		lda REGISTER_X_H
		adc #>OBJ_SIZE
		sta REGISTER_X_H		
		lda REGISTER_X_H
		cmp #>OBJECTS_END
		bne spriteloop		
		lda REGISTER_X_L
		cmp #<OBJECTS_END
		bne spriteloop
		lda #$D0		;store end-of-sprite-list marker
		sta TMS9918RAM
		jmp printscore
; on odd frames, reverse the sprite priorities to reduce fifth-sprite clipping
drawbackwards
		lda #<(OBJECTS_END-OBJ_SIZE)
		sta REGISTER_X_L
		lda #>(OBJECTS_END-OBJ_SIZE)
		sta REGISTER_X_H
bspriteloop
		ldy #life
		lda (REGISTER_X_L),y
		beq bnextsprite		;skip dead objects
		ldy #ypos_int
		lda (REGISTER_X_L),y	
		sta TMS9918RAM		;y position
		ldy #xpos_int
		lda (REGISTER_X_L),y	
		sta TMS9918RAM		;x position
		ldy #pattern
		lda (REGISTER_X_L),y
		sta TMS9918RAM		;sprite name
		nop
		nop
		nop
		nop
		lda #$0F		
		sta TMS9918RAM		;color
bnextsprite
		lda REGISTER_X_L
		sec
		sbc #<OBJ_SIZE 		;advance to next
		sta REGISTER_X_L
		lda REGISTER_X_H
		sbc #>OBJ_SIZE
		sta REGISTER_X_H		
		lda REGISTER_X_H
		cmp #>(OBJECTS-OBJ_SIZE)
		bne bspriteloop	
		lda REGISTER_X_L
		cmp #<(OBJECTS-OBJ_SIZE)
		bne bspriteloop
		lda #$D0		;store end-of-sprite-list marker
		sta TMS9918RAM
; print score
printscore
		lda #<SCORETEMP1	;convert BCD to ASCII
		sta REGISTER_X_L
		lda #>SCORETEMP1
		sta REGISTER_X_H		
		lda SCORE
		jsr BCD_TO_ASCII	;convert the packed BCD number in A to two ASCII characters in A and X
		ldy #0
		sta (REGISTER_X_L),y
		iny
		txa
		sta (REGISTER_X_L),y
		lda SCORE+1
		jsr BCD_TO_ASCII
		iny 
		sta (REGISTER_X_L),y
		iny
		txa						
		sta (REGISTER_X_L),y
		ldy #0
lzerocheck		
		lda (REGISTER_X_L),y	;is digit zero?
		cmp #"0"		;zero character for comparison
		bne notzero		;no, done
		lda #" "		;yes, replace with blank
		sta (REGISTER_X_L),y
		iny
		cpy #3
		bne lzerocheck
notzero
		lda #<NAMETABLE+32+1	;print score
		sta TMS9918REG		
		lda #VRAM OR >NAMETABLE
		sta TMS9918REG	
		ldy #0
		lda #<SCORETEMP1
		sta REGISTER_X_L
		lda #>SCORETEMP1
		sta REGISTER_X_H
digitloop
		lda (REGISTER_X_L),y	;get character
printdigit
		sta TMS9918RAM
		iny
		cpy #4 
		bne digitloop
		lda #"0"		;print final zero
		sta TMS9918RAM
; print lives
printlives
		lda #<NAMETABLE+64	;print lives
		sta TMS9918REG		
		lda #VRAM OR >NAMETABLE
		sta TMS9918REG	
		ldy #6			;max number of lives to print
printlifeloop	
		lda #"^"		;life character
		cpy LIVES		;print life character or blank?
		bmi printlifechar	;branch lower or same
		beq printlifechar
		lda #" "
printlifechar
		sta TMS9918RAM
		dey
		bne printlifeloop	
; execute display action if necessary
		lda DISPLAY_ACTION
		beq actiondone		;0? no action
		dec
		beq aprinterror		;error action has highest priority
		dec
		beq aprintstart
		dec
		beq aprintgameover
		dec
		beq aclearstrings
actiondone
		stz DISPLAY_ACTION
		jsr READ_JOYSTICK	; read joystick
		sta PAD1STATE		;save controller states
		pla
		sta REGISTER_X_H
		pla
		sta REGISTER_X_L
		lda ARegister
		ldx XRegister
		ldy YRegister
		rti
aprinterror
		lda #<ERROR_MSG_PTR
		sta REGISTER_X_L
		lda #>ERROR_MSG_PTR
		sta REGISTER_X_H
		jsr VDP_PRINTERRSTR
		bra actiondone

aprintstart
		lda #<startstr
		sta REGISTER_X_L
		lda #>startstr
		sta REGISTER_X_H
		jsr VDP_PRINTPSTR
		bra actiondone
aprintgameover
		lda #<gameoverstr	;print both strings
		sta REGISTER_X_L
		lda #>gameoverstr
		sta REGISTER_X_H
		jsr VDP_PRINTPSTR
		lda #<startstr
		sta REGISTER_X_L	
		lda #>startstr			
		sta REGISTER_X_H	
		jsr VDP_PRINTPSTR
		bra actiondone
aclearstrings	
		lda #<clrgameoverstr	; clear both strings
		sta REGISTER_X_L
		lda #>clrgameoverstr
		sta REGISTER_X_H
		jsr VDP_PRINTPSTR
		lda #<clrstartstr
		sta REGISTER_X_L	
		lda #>clrstartstr	
		sta REGISTER_X_H	
		jsr VDP_PRINTPSTR
		jmp actiondone
;------------------------------------------------------------------------------
; end of interrupt handler
;------------------------------------------------------------------------------
	
;------------------------------------------------------------------------------
; subroutines
;------------------------------------------------------------------------------
; convert the packed BCD number in A to two ASCII characters in A and B
; arguments:	BCD number in A
; returns:	ASCII characters in A and X
; destroys:	A,X
BCD_TO_ASCII	;convert the packed BCD number in A to two ASCII characters in A and X
		tax
		lsr
		lsr
		lsr
		lsr
		clc
		adc #$30
		pha
		txa
		and #%00001111
		clc
		adc #$30
		tax
		pla
		rts

; start a new game
; arguments:	none
; returns:	none
; destroys:	A
NEW_GAME
		jsr CLR_OBJECTS
		lda #3
		sta START_ASTEROIDS	;first level will have 4 asteroids
		sta NEXT_LG_SAUCERS	;and 3 large saucers
		sta LIVES		;3 lives
		lda #0
		sta SCORE
		sta SCORE+1		
		sta PAD1STATE
		sta PREVPAD1STATE
		sta PAD1STATE+1
		sta PREVPAD1STATE+1		
		lda #EXTRA_LIFE_AMT
		sta NEXT_LIFE_AT
		lda #0
		sta BGSOUND_BITS	;clear sounds
		sta ATTRACTMODE		;clear attract mode
		sta HYPERSPACE		;clear hyperspace
		lda #$30		;start first level after a delay
		sta RESPAWN_TIMER
		sta NEW_LEVEL_TIMER
		lda #clearstrings	;clear title screen strings
		sta DISPLAY_ACTION
		rts
		
; clear the entire asteroids array, marking all asteroids as dead
; arguments:	none
; returns:	none
; destroys:	A,Y,REGISTER_X
CLR_ASTEROIDS

		lda #<ASTEROIDS		;initialize next asteroid pointer
		sta REGISTER_X_L
		sta NEXT_AST_PTR
		lda #>ASTEROIDS
		sta REGISTER_X_H
		sta NEXT_AST_PTR+1	
clrloop
		ldy #life
		lda #0
		sta (REGISTER_X_L),Y	; clr life
		lda REGISTER_X_L
		clc
		adc #OBJ_SIZE
		sta REGISTER_X_L
		lda REGISTER_X_H
		adc #0
		sta REGISTER_X_H
		lda REGISTER_X_L
		cmp #<ASTEROIDS_END
		bne clrloop
		lda REGISTER_X_H
		cmp #>ASTEROIDS_END
		bne clrloop
		rts

; clears the entire objects array, marking all objects as dead
; arguments:	none
; returns:	none
; destroys:	A

CLR_OBJECTS
		lda #<OBJECTS
		sta REGISTER_X_L
		lda #>OBJECTS
		sta REGISTER_X_H
		bra clrloop

; vend a new asteroid and advance next asteroid pointer
; arguments:	none
; returns:	asteroid pointer in X
;		halts if no free asteroids
; destroys:	A

AST_NEW
		lda NEXT_AST_PTR
		sta REGISTER_X_L
		lda NEXT_AST_PTR+1
		sta REGISTER_X_H
		lda REGISTER_X_L		
		cmp #<ASTEROIDS_END
		bne AST_NEW2
		lda REGISTER_X_H
		cmp #>ASTEROIDS_END	
		beq no_asteroids
AST_NEW2		
		lda REGISTER_X_L
		sta REGISTER_U_L		
		lda REGISTER_X_H
		sta REGISTER_U_H
		lda REGISTER_U_L
		clc
		adc #<OBJ_SIZE
		sta REGISTER_U_L
		lda REGISTER_U_H
		adc #>OBJ_SIZE
		sta REGISTER_U_H
		lda REGISTER_U_L
		sta NEXT_AST_PTR
		lda REGISTER_U_H
		sta NEXT_AST_PTR+1				
		rts

; nothing we can do, halt		
no_asteroids		
		lda #printerror	
		sta DISPLAY_ACTION
		lda #<objerror		; < low byte of expression
		sta ERROR_MSG_PTR
		sta REGISTER_X_L
		lda #>objerror		; > high byte of expression
		sta ERROR_MSG_PTR+1	
		sta REGISTER_X_H
		lda #$86		; Change frame color : TODO Remove if finish
		sta TMS9918REG
		lda #$87
		sta TMS9918REG	
		bra *			; TODO: realy endless loop ?

; create a new set of asteroids (i.e. for level start)
; arguments:	number of asteroids in REGISTER_B
; returns:	
; destroys:	A,X,Y
AST_NEW_SET
		jsr CLR_ASTEROIDS
		lda #<ASTEROIDS
		sta REGISTER_X_L
		lda #>ASTEROIDS
		sta REGISTER_X_H
		lda #0
		ldx REGISTER_B
		clc
AST_NEW_SET2
		adc #7			; compute total number of asteroids
		dex			; big --> 2 medium --> 4 small : 7 asteroids to destroy		
		bne AST_NEW_SET2	; B + 2*B + 4*B = 7*B ( D = A * B)
		sta ASTEROIDS_LEFT
AST_NEW_SET3
		jsr AST_NEW
		jsr AST_INIT
		lda REGISTER_X_L
		clc
		adc #OBJ_SIZE		; x = x + OBJ_SIZE
		sta REGISTER_X_L
		lda REGISTER_X_H
		adc #0
		sta REGISTER_X_H
		dec REGISTER_B
		bne AST_NEW_SET3
		rts 			

; split a large asteroid into two smaller asteroids, or remove it
; if it is a small asteroid
; arguments:	pointer to asteroid in X
; returns:	none
; destroys:	A,X,Y,TEMP1,TEMP2,TEMP3,TEMP4

AST_SPLIT

		lda REGISTER_X_L
		pha 
		lda REGISTER_X_H
		pha
		ldy #siz		;compute center point of asteroid
		lda (REGISTER_X_L),y
		asl			;(offset by half of size)
		lda (REGISTER_X_L),y
		ror			;asra 6502 replacement
		tax			
		ldy #xpos_int
		clc
		adc (REGISTER_X_L),y
		sta TEMP1		;save center pointset x position
		txa 
		ldy #ypos_int
		clc
		adc (REGISTER_X_L),y
		sta TEMP2		;save center point set y position
		ldy #siz		;what's the new size?
		lda (REGISTER_X_L),y
		cmp #AST_LG_SIZE	;large?		
		beq split_med		;split into two medium asteroids
		cmp #AST_MED_SIZE	;medium?
		beq split_sm		;split into two small asteroids
		pla
		sta REGISTER_X_H
		pla
		sta REGISTER_X_L
		rts 			;small? return

split_med
		lda #SPR_AST1_MED
		sta TEMP3
		lda #AST_MED_SIZE
		sta TEMP4
		bra do_split
split_sm
		lda #SPR_AST1_SM
		sta TEMP3
		lda #AST_SM_SIZE
		sta TEMP4		;asteroids size	

; spawn two new asteroids
do_split
		ldx #2
		stx REGISTER_D_H	
		lda TEMP4		;asteroids size ; get new size offset (divide in half)
		sta REGISTER_D_L	;register B
		asl			;asrb (MC6809) replacement
		ror REGISTER_D_L	;get new size offset (divide in half)
do_split2
		jsr AST_NEW
		lda TEMP3		;sprite pattern
		ldy #pattern
		sta (REGISTER_X_L),y
		lda TEMP4		;asteroids size	
		ldy #siz
		sta (REGISTER_X_L),y
		lda TEMP1		;set x position
		sec
		sbc REGISTER_D_L
		ldy #xpos_int
		sta (REGISTER_X_L),y
		ldy #xpos_frac
		lda #0
		sta (REGISTER_X_L),y		
		lda TEMP2		;set y position
		sec
		sbc REGISTER_D_L
		ldy #ypos_int
		sta (REGISTER_X_L),y
		ldy #ypos_frac
		lda #0
		sta (REGISTER_X_L),y
		lda #$FF
		ldy #life
		sta (REGISTER_X_L),y
		lda #asteroid
		ldy #type
		sta (REGISTER_X_L),y
		phx				
		jsr ast_randomize	;randomize velocity and pattern
		plx
		dex
		bne do_split2	
		pla
		sta REGISTER_X_H
		pla
		sta REGISTER_X_L
		rts			
		
; generates a new large asteroid, positions it on one of the 4 screen edges,
; and gives it a random direction
; arguments:	asteroid pointer in REGISTER_X
; returns:	asteroid structure initialized
; destroys:	A,Y
AST_INIT
		lda #$FF		;mark as alive  ; asteroid = object type = 16
		ldy #life
		sta (REGISTER_X_L),y	;life = offset = 0
		lda #asteroid
		ldy #type
		sta (REGISTER_X_L),y
		jsr RANDBYTE		;get a random byte
		and #%00111110		;convert to position table offset
		tay
		lda #<AST_POSITIONS	;read x and y positions
		sta REGISTER_U_L
		lda #>AST_POSITIONS
		sta REGISTER_U_H		
		lda (REGISTER_U_L),y
		phy			; save offset 
		ldy #xpos_int
		sta (REGISTER_X_L),y
		ldy #xpos_frac		
		lda #0
		sta (REGISTER_X_L),y	; store xpos_frac
		ply
		iny		
		lda (REGISTER_U_L),y	; read y position
		ldy #ypos_int
		sta (REGISTER_X_L),y	; store ypos_int position
		ldy #ypos_frac
		lda #0
		sta (REGISTER_X_L),y	; store y position		
		lda #SPR_AST1_LG
		ldy #pattern		; set pattern offset 
		sta (REGISTER_X_L),y	
		lda #AST_LG_SIZE
		iny			; set size offset
		sta (REGISTER_X_L),y
ast_randomize
		jsr RANDBYTE		;get a random byte
		pha			;save it
		and #%01111100		;convert to direction table offset
		tay			;save it
		lda #<AST_DIRECTIONS
		sta REGISTER_U_L
		lda #>AST_DIRECTIONS
		sta REGISTER_U_H 
		tya
		clc
		adc REGISTER_U_L	
		sta REGISTER_U_L
		lda REGISTER_U_H
		adc #0
		sta REGISTER_U_H	;advance to table entry
		lda (REGISTER_U_L)
		ldy #xvel	
		sta (REGISTER_X_L),y	;get x direction
		jsr IncPTR_U		;advance to table entry
		lda (REGISTER_U_L)
		iny
		sta (REGISTER_X_L),y
		jsr IncPTR_U
		lda (REGISTER_U_L)
		ldy #yvel
		sta (REGISTER_X_L),y	;get y direction
		jsr IncPTR_U
		lda (REGISTER_U_L)		
		iny
		sta (REGISTER_X_L),y	
		pla 			;get the random number back 
		and #%00000011		;asteroid shape: 0 to 3
		asl			;multiply by 4 to get sprite number
		asl
		ldy #pattern		;add to existing pattern number
		clc
		adc (REGISTER_X_L),y	
		sta (REGISTER_X_L),y		;add to existing pattern number
		jsr RANDBYTE
		and #%00000001			;randomize speed
		beq ast_randomize_exit
		ldy #yvel+1
		lda (REGISTER_X_L),y
		asl
		lda (REGISTER_X_L),y
		ror
		sta (REGISTER_X_L),y
		dey
		lda (REGISTER_X_L),y
		ror
		sta (REGISTER_X_L),y	
		ldy #xvel+1
		lda (REGISTER_X_L),y
		asl
		lda (REGISTER_X_L),y
		ror
		sta (REGISTER_X_L),y
		dey
		lda (REGISTER_X_L),y
		ror
		sta (REGISTER_X_L),y				
ast_randomize_exit		
		rts

; initialize the player ship
; positions it in the center, facing right
; arguments:	none
; returns:	ship structure and variables initialized
; destroys:	A,Y
SHIP_INIT
		lda #<PLAYERSHIP
		sta REGISTER_X_L
		lda #>PLAYERSHIP
		sta REGISTER_X_H
		lda #$FF		;$ff or opject type
		ldy #life
		sta (REGISTER_X_L),y
		ldy #type
		lda #player1
		sta (REGISTER_X_L),y
		lda HYPERSPACE		;are we hyperspacing? ; 0 = no hyperspace , random number = hyperspace
		beq spawn_center	;no, spawn in the center
		cmp #%11111000		;will hyperspace kill?
		bne SHIP_INIT2
		jmp ship_kill
SHIP_INIT2
spawn_random
		jsr RANDBYTE		;get random x position
		cmp #255-SHIP_SIZE	;clamp to screen
		bmi set_rand_x	
		lda #255-SHIP_SIZE
set_rand_x		
		ldy #xpos_int
		sta (REGISTER_X_L),y
		lda #0
		ldy #xpos_frac
		sta (REGISTER_X_L),y
		jsr RANDBYTE		;get random y position
		cmp #191-SHIP_SIZE	;clamp to screen
		bmi set_rand_y
		lda #191-SHIP_SIZE
set_rand_y
		ldy #ypos_int
		sta (REGISTER_X_L),y
		ldy #ypos_frac
		lda #0
		sta (REGISTER_X_L),y
		bra zero_vel
spawn_center
		lda #$78		;center x
		ldy #xpos_int
		sta (REGISTER_X_L),y
		dey
		lda #00
		sta (REGISTER_X_L),y
		lda #$58		;center y
		ldy #ypos_int
		sta (REGISTER_X_L),y
		dey
		lda #00
		sta (REGISTER_X_L),y		
zero_vel
		lda #0			;zero velocity
		ldy #xvel
		sta (REGISTER_X_L),y
		iny 
		sta (REGISTER_X_L),y
		iny 
		sta (REGISTER_X_L),y
		iny
		sta (REGISTER_X_L),y
		lda HYPERSPACE		;don't set pattern/size if hyperspacing ; 0 = no hyperspace , random number = hyperspace
		bne ship_init_done
		lda #SPR_SHIP
		ldy #pattern		;pattern and size
		sta (REGISTER_X_L),y
		lda #SHIP_SIZE
		ldy #siz
		sta (REGISTER_X_L),y
		stz SHIPANGLE		; set to direction east
ship_init_done
		rts
		
; initializes a saucer
; arguments:	none
; returns:	none
; destroys:	A,X,Y

SAUCER_INIT	
		lda #<SAUCER	
		sta REGISTER_X_L
		lda #>SAUCER	
		sta REGISTER_X_H
		lda #$FF
		ldy #life
		sta (REGISTER_X_L),y
		lda #saucer
		ldy #type
		sta (REGISTER_X_L),y
		lda #0
		ldy #yvel		;zero y velocity initially
		sta (REGISTER_X_L),y	
		iny
		sta (REGISTER_X_L),y	
		ldy #xpos		
		sta (REGISTER_X_L),y	;start on left
		iny
		lda #2
		sta (REGISTER_X_L),y			
		lda LARGE_SAUCERS	;spawn a large saucer?
		beq smallsaucer		;out of large saucers, make it small
largesaucer
		dec LARGE_SAUCERS
		lda #$A0		;slow speed, moving right
		ldy #xvel
		sta (REGISTER_X_L),y
		lda #$00		
		iny
		sta (REGISTER_X_L),y		
		lda #SPR_SAUCER_LG
		ldy #pattern
		sta (REGISTER_X_L),y
		iny
		lda #SAUCER_LG_SIZE
		sta (REGISTER_X_L),y	
		lda #<LARGESAUCER_SOUND	;set up sound effect
		sta REGISTER_U_L
		sta SAUCERSNDPTR
		lda #>LARGESAUCER_SOUND
		sta REGISTER_U_H
		sta SAUCERSNDPTR+1
		bra positionsaucer
smallsaucer
		lda #$D0		;fast speed, moving right
		ldy #xvel
		sta (REGISTER_X_L),y
		iny
		lda #$00
		sta (REGISTER_X_L),y
		lda #SPR_SAUCER_SM
		ldy #pattern
		sta (REGISTER_X_L),y
		iny
		lda #SAUCER_SM_SIZE
		sta (REGISTER_X_L),y
		
		lda #<SMALLSAUCER_SOUND	;set up sound effect
		sta REGISTER_U_L
		sta SAUCERSNDPTR
		lda #>SMALLSAUCER_SOUND
		sta REGISTER_U_H
		sta SAUCERSNDPTR+1
positionsaucer
		jsr RANDBYTE		;get a random byte
		tax			;save it
		lsr			;vertical position: reduce to 0-127 ; TODO: reduce to 0-127 means LSR not ASL
		clc			
		adc #30			;roughly center the range onscreen
		ldy #ypos_int
		sta (REGISTER_X_L),y	;set y position
		lda #0
		ldy #ypos_frac
		sta (REGISTER_X_L),y	;swap to right side?
		txa
		bpl saucerdone		;if random byte msb clear, keep on left
		ldy #xvel+1
		lda (REGISTER_X_L),y
		eor #$FF
		sta (REGISTER_X_L),y	
		ldy #xvel		
		lda (REGISTER_X_L),y	;right side: negate x velocity
		eor #$FF
		sta (REGISTER_X_L),y
		ldy #xpos_int
		lda (REGISTER_X_L),y
		eor #$FF
		sta (REGISTER_X_L),y	;and move to right edge
saucerdone
		jsr SAUCERSND_ON	;play sound
		rts
		
; make the saucer switch directions or fire if appropriate
SAUCER_ACTION
		lda FRAMECOUNTER	
		and #%00111111
		beq saucerfire		;fire every 64 frames
saucerchkmove
		lda FRAMECOUNTER
		and #%01111111		
		beq saucermove		;move every 128 frames
sauceractdone
		rts

saucermove
		jsr RANDBYTE		;get a random byte
		and #%00000110		;set one of four y velocities
		tay
		lda #<saucer_yvels	;with a table lookup
		sta REGISTER_U_L		
		lda #>saucer_yvels
		sta REGISTER_U_H	
		lda (REGISTER_U_L),y
		sta SAUCER+yvel
		iny
		lda (REGISTER_U_L),y
		sta SAUCER+yvel+1				
		bra sauceractdone
saucerfire
		jsr GET_SCRBULLET	;get a bullet ; clc : no bullet
		bcc saucerchkmove	;bail if none available
		jsr RANDBYTE		;get a random byte for direction
		and #%00011111
		tax			; save to x for function call
		lda #<SAUCER
		sta REGISTER_Y_L
		lda #>SAUCER
		sta REGISTER_Y_H
		txa
		jsr FIRE_BULLET		;fire the bullet
		lda #<SAUCERFIRE_SOUND	;play sound
		sta SNDPTR
		lda #>SAUCERFIRE_SOUND
		sta SNDPTR+1
		bra saucerchkmove	

; set up the next saucer spawn
; arguments:	none
; returns:	none
; destroys:	A
SAUCER_NEXT
		lda ATTRACTMODE		;don't respawn saucers in attract mode ; 1 = attract-mode
		bne nonextsaucer	; TODO: arcarde has saucer in attract-mode
		lda NEXT_SAUCER_INT	;decrement time to next saucer?
		cmp #90			;not if it's at the minimum of 3 secs
		bcs resetstimer		;branch if lower/same
		sec
		sbc #15			;shorten by half a second
		sta NEXT_SAUCER_INT
resetstimer
		sta SAUCER_TIMER	;reload saucer timer
nonextsaucer
		rts

; checks if if is safe for the ship to spawn
; (i.e. there are no asteroids near the center of the screen)
; arguments:	none
; returns:	C flag set if it is safe, C flag reset otherwise
; destroys:	A,Y
SHIP_CAN_SPAWN
		lda HYPERSPACE		;are we hyperspacing? ; 0 = no hyperspace , random number = hyperspace
		bne canspawn		;if yes, skip the check
		lda #<ASTEROIDS
		sta REGISTER_X_L
		lda #>ASTEROIDS
		sta REGISTER_X_H
scs_astloop
		ldy #life
		lda (REGISTER_X_L),y
		bpl scs_nextast
		ldy #xpos_int		
		lda (REGISTER_X_L),y
		cmp #$50		;check min x
		bmi scs_nextast		;Branch if Lower 
		cmp #$A0		;check max x
		bpl scs_nextast		;Branch if Higher
		ldy #ypos_int		
		lda (REGISTER_X_L),y	;check min y
		cmp #$30
		bmi scs_nextast
		cmp #$80
		bpl scs_nextast
		clc		;can't spawn, clear C
		rts 		;TODO: function call check for "beq" ?
scs_nextast
		lda REGISTER_X_L	;asteroid is clear, check next
		clc
		adc #OBJ_SIZE
		sta REGISTER_X_L
		lda REGISTER_X_H
		adc #0
		sta REGISTER_X_H
		lda REGISTER_X_L
		cmp #<ASTEROIDS_END
		bne scs_astloop
		lda REGISTER_X_H
		cmp #>ASTEROIDS_END		
		bne scs_astloop
canspawn
		sec		;can spawn, set C
		rts		;TODO: function call check for "beq"
		
; initialize the bullets
; just sets them all to be dead
BULLETS_INIT
		lda #<BULLETS
		sta REGISTER_X_L
		lda #>BULLETS
		sta REGISTER_X_H
BULLETS_INIT2
		lda #0
		ldy #life
		sta (REGISTER_X_L),y
		lda REGISTER_X_L
		clc
		adc #OBJ_SIZE
		sta REGISTER_X_L
		lda REGISTER_X_H
		adc #0
		sta REGISTER_X_H
		lda REGISTER_X_L
		cmp #<BULLETS_END
		bne BULLETS_INIT2
		lda REGISTER_X_H
		cmp #>BULLETS_END		
		bne BULLETS_INIT2		
		rts
		
; finds an available bullet
; returns a pointer to it in X or resets C if no bullets available
GET_BULLET
		lda #<SHIPBULLETS
		sta REGISTER_X_L
		lda #>SHIPBULLETS
		sta REGISTER_X_H
GET_BULLET2
		ldy #life
		lda (REGISTER_X_L),y
		beq foundbullet
		lda REGISTER_X_L
		clc
		adc #OBJ_SIZE
		sta REGISTER_X_L
		lda REGISTER_X_H
		adc #0
		sta REGISTER_X_H	
		lda REGISTER_X_L
		cmp #<SHIPBULLETS_END
		bne GET_BULLET2
		lda REGISTER_X_H
		cmp #>SHIPBULLETS_END		
		bne GET_BULLET2
nobullet
		clc		; clear C flag, no bullets available
		rts		; clc : no bullet
foundbullet
		sec
		rts		; sec : bullet available
		
; finds an available bullet for the saucer
; returns a pointer to it in X or sets Z if no bullets available
GET_SCRBULLET
		lda #<SAUCERBULLETS
		sta REGISTER_X_L
		lda #>SAUCERBULLETS
		sta REGISTER_X_H
GET_SCRBULLET2
		ldy #life
		lda (REGISTER_X_L),y
		beq foundbullet
		lda REGISTER_X_L
		clc
		adc #OBJ_SIZE
		sta REGISTER_X_L
		lda REGISTER_X_H
		adc #0
		sta REGISTER_X_H	
		lda REGISTER_X_L
		cmp #<SCRBULLETS_END
		bne GET_SCRBULLET2
		lda REGISTER_X_H
		cmp #>SCRBULLETS_END		
		bne GET_SCRBULLET2				
		bra nobullet		;clc : no bullet
		
; fires a bullet
; arguments:	pointer to bullet in X
; 		pointer to object firing bullet in Y
;		bullet angle in B
; returns:	none
; destroys:	A,X,Y

FIRE_BULLET
		tax			;save angle in A
		lda #<TRIGTABLE		;get bullet velocity vector		
		sta REGISTER_U_L
		lda #>TRIGTABLE
		sta REGISTER_U_H
		txa
		asl
		asl
		tay			;get entry in trig table
		lda (REGISTER_U_L),y	;get cosine value ;frac value
		asl			
		phy
		ldy #xvel
		sta (REGISTER_X_L),y
		ply
		iny
		lda (REGISTER_U_L),y	;int value
		phy
		rol
		ldy #xvel+1
		sta (REGISTER_X_L),y	;set x velocity	
		ply 
		iny
		lda (REGISTER_U_L),y	;get sine value ;frac value
		asl
		phy
		ldy #yvel
		sta (REGISTER_X_L),y	;set y velocity
		ply
		iny
		lda (REGISTER_U_L),y	;int value
		rol
		ldy #yvel+1
		sta (REGISTER_X_L),y			
		ldy #xpos
		lda (REGISTER_Y_L),y
		ldy #xvel
		clc
		adc (REGISTER_X_L),y
		ldy #xpos		
		sta (REGISTER_X_L),y
		ldy #xpos+1
		lda (REGISTER_Y_L),y
		ldy #xvel+1
		adc (REGISTER_X_L),y
		adc #$06
		ldy #xpos+1		
		sta (REGISTER_X_L),y
		ldy #ypos
		lda (REGISTER_Y_L),y
		clc
		ldy #yvel
		adc (REGISTER_X_L),y
		ldy #ypos		
		sta (REGISTER_X_L),y
		ldy #ypos+1
		lda (REGISTER_Y_L),y
		ldy #yvel+1
		adc (REGISTER_X_L),y
		adc #$06
		ldy #ypos+1		
		sta (REGISTER_X_L),y
		lda #SPR_BULLET		
		ldy #pattern
		sta (REGISTER_X_L),y		;set bullet pattern and size
		lda #0
		ldy #siz
		sta (REGISTER_X_L),y
		lda #bullet
		ldy #type
		ora (REGISTER_Y_L),y
		sta (REGISTER_X_L),y
		lda #$50		
		ldy #life
		sta (REGISTER_X_L),y	;set type and owner
		rts

; bullet/object collision detection
; arguments:	pointer to object in X
;		pointer to bullet in Y
; returns:	none
; destroys:	A,Y

BULLET_COLL
		ldy #life
		lda (REGISTER_X_L),y
		bpl nocoll		;skip dead objects
		ldy #xpos_int
		lda (REGISTER_X_L),y	;check min x position
		cmp (REGISTER_Y_L),y
		bpl nocoll		;branch if Higher
		beq nocoll
		ldy #ypos_int
		lda (REGISTER_X_L),y
		cmp (REGISTER_Y_L),y	;check min y position
		bpl nocoll
		beq nocoll
		ldy #xpos_int
		lda (REGISTER_X_L),y	;check max x position
		clc
		ldy #siz
		adc (REGISTER_X_L),y
		ldy #xpos_int
		cmp (REGISTER_Y_L),y
		bmi nocoll
		ldy #ypos_int
		lda (REGISTER_X_L),y
		clc
		ldy #siz
		adc (REGISTER_X_L),y
		ldy #ypos_int
		cmp (REGISTER_Y_L),y
		bmi nocoll
; collision: kill the bullet and object
; no collision, return
collision
		ldy #life
		lda #0
		sta (REGISTER_Y_L),y
		jsr OBJ_SCORE
		bra OBJ_KILL
nocoll		
		rts

; object/object collision detection
; arguments:	for ship/asteroid:
;		  asteroid pointer in REGISTER_X, ship pointer in REGISTER_Y
;		for saucer/asteroid:
;		  asteroid pointer in XREGISTER_X, saucer pointer in REGISTER_Y
;		for ship/saucer:
;		  saucer pointer in REGISTER_X, ship pointer in REGISTER_Y
; returns:	none
; destroys:	A,X,Y,TEMP1,TEMP2

OBJ_OBJ_COLL
		ldy #siz		
		lda (REGISTER_Y_L),y	;compute center point of object
		asl
		lda (REGISTER_Y_L),y
		ror			;(offset by half of size) ; asra 6502 replacement		
		tax
		ldy #xpos_int
		clc
		adc (REGISTER_Y_L),y
		sta TEMP1		;save center point OBJ_OBJ_COLL ; x center point saucer or ship
		txa
		ldy #ypos_int
		clc
		adc (REGISTER_Y_L),y
		sta TEMP2		;y center point saucer or ship	
		ldy #xpos_int
		lda (REGISTER_X_L),y	;check min x position
		sec
		sbc #3
		cmp TEMP1		;x center point saucer or ship
		bpl ao_nocoll
		ldy #ypos_int
		lda (REGISTER_X_L),y	;check min y position
		sec
		sbc #3
		cmp TEMP2		;y center point saucer or ship
		bpl ao_nocoll
		ldy #xpos_int
		lda (REGISTER_X_L),y	;check max x position
		clc
		ldy #siz
		adc (REGISTER_X_L),y
		adc #3
		cmp TEMP1		;x center point saucer or ship
		bmi ao_nocoll			
		ldy #ypos_int
		lda (REGISTER_X_L),y	;check max y position
		clc
		ldy #siz
		adc (REGISTER_X_L),y
		adc #3
		cmp TEMP2		; y center point saucer or ship
		bmi ao_nocoll	
; collision: kill both objects
		jsr OBJ_SCORE
		jsr OBJ_KILL
		lda REGISTER_Y_L
		sta REGISTER_X_L
		lda REGISTER_Y_H
		sta REGISTER_X_H
		jsr OBJ_KILL	; TODO: arcarde game split aststeroids 	
; no collision: return
ao_nocoll
		rts

; kill an object, replace it with an explosion animation
; arguments:	pointer to object in REGISTER_X
; returns:	none
; destroys:	A,Y,TEMP1,TEMP2
OBJ_KILL
		lda #0
		ldy #xvel
		sta (REGISTER_X_L),y	;set velocity to 0
		iny
		sta (REGISTER_X_L),y
		ldy #yvel		
		sta (REGISTER_X_L),y
		iny
		sta (REGISTER_X_L),y
		lda #EXPLOSION_SIZE	;make sure explosion is centered
		ldy #siz		;(offset by half of size difference)
		sec
		sbc (REGISTER_X_L),y
		sta TEMP1
		lda TEMP1
		asl
		lda TEMP1
		ror			; asra 6502 replacement
		sta TEMP1			
		lda #0
		sta TEMP2
		ldy #xpos
		lda (REGISTER_X_L),y
		sec
		sbc TEMP2
		sta (REGISTER_X_L),y
		ldy #xpos+1
		lda (REGISTER_X_L),y
		sbc TEMP1
		sta (REGISTER_X_L),y		
		ldy #ypos
		lda (REGISTER_X_L),y
		sec
		sbc TEMP2
		sta (REGISTER_X_L),y
		ldy #ypos+1
		lda (REGISTER_X_L),y
		sbc TEMP1
		sta (REGISTER_X_L),y	
		ldy #type		;is it a player ship?
		lda (REGISTER_X_L),y
		and #player_mask
		bne ship_kill
		lda (REGISTER_X_L),y
		cmp #saucer		;is it a saucer?
		beq saucer_kill
		cmp #asteroid		;is it an asteroid?
		bne obj_kill		;no, something else
		jsr AST_SPLIT		;if it's an asteroid, split it
		dec ASTEROIDS_LEFT	;and decrement asteroid count
		beq nextlevel		;last one destroyed? new level!
obj_kill
		lda #SPR_EXPL1_START
		ldy #pattern
		sta (REGISTER_X_L),y
		lda #$10		;set countdown timer
		ldy #life
		sta (REGISTER_X_L),y 
		ldy #siz
		lda (REGISTER_X_L),y 	; TODO: sound frequency for explode sound
 		jsr SND_EXPLODE
		jsr THRUSTSND_OFF	;turn off thrust sound
		rts

saucer_kill
		jsr SAUCERSND_OFF	;stop sound
		jsr SAUCER_NEXT		;set up the next one
		bra obj_kill
ship_kill
		lda #SPR_EXPL2_START
		ldy #pattern
		sta (REGISTER_X_L),y
		stz HYPERSPACE
		stz SAUCER_TIMER	;clear saucer timer until respawn
		lda #$30
		ldy #life
		sta (REGISTER_X_L),y
		jsr SND_EXPLODE		;explosion sound
		jsr THUMPSND_OFF	;turn off thump sound
		dec LIVES		;lose a life
		beq do_gameover		;last life lost? it's game over man
		lda #$60
		sta RESPAWN_TIMER	;set respawn timer
		rts
nextlevel
		lda #$60
		sta NEW_LEVEL_TIMER
		stz SAUCER_TIMER
		jsr THUMPSND_OFF	;turn off thump sound
		bra obj_kill

; add points for a killed object to score
; arguments:	pointer to killed object in REGISTER_X
;		pointer to bullet/object that did the killing in REGISTER_Y
; returns:	none
; destroys:	A,Y
OBJ_SCORE
		ldy #type		;who killed the object?
		lda (REGISTER_Y_L),y
		and #player_mask	;was it a player?
		beq noscore		;if not, no score
		ldy #siz
		lda (REGISTER_X_L),y
; points determined by size, it's a hack i know
		cmp #AST_LG_SIZE
		beq score_lg
		cmp #SAUCER_LG_SIZE
		beq score_lgsaucer
		cmp #AST_MED_SIZE
		beq score_med
		cmp #SAUCER_SM_SIZE
		beq score_smsaucer
		cmp #AST_SM_SIZE
		beq score_sm
noscore
		rts

; add to score
score_lgsaucer
		lda #$20
		bra addscore
	
score_smsaucer
		lda #$99		;1000 points (990+carry)
		sec
		bra addscore
score_lg
		lda #$02
		bra addscore	
score_med
		lda #$05
		bra addscore
score_sm
		lda #$10
addscore
		sei 	; disable interrupt
		sed	; set decimal mode
		sec
		adc SCORE+1
		sta SCORE+1
		lda #0
		adc SCORE
		sta SCORE
		cld
		cli 	; enable interrupt
; check for extra life
; A should contain score MSB
chkextralife
		cmp NEXT_LIFE_AT	;score >= extra life score?
		bmi noextralife		;no, return
		lda LIVES		;less than 255 lives?
		cmp #$FF
		beq noextralife		;no, return to prevent overflow
		inc LIVES		;add life
		lda #EXTRA_LIFE_AMT	;advance amount for next life
		sei 			;disable interrupt
		sed			;set decimal mode
		clc
		adc NEXT_LIFE_AT
		sta NEXT_LIFE_AT			
		cld
		cli 			;enable interrupt
		jsr LIFESND_ON		;play extra life sound
noextralife
		rts	
do_gameover
		lda #printgameover
		sta DISPLAY_ACTION
		sta ATTRACTMODE		;reenable attract mode
		lda #$C0
		sta RESPAWN_TIMER
		rts

;------------------------------------------------------------------------------
; sound routines
;------------------------------------------------------------------------------

SND_UPDATE
		lda SNDPTR		;null pointer? if so, skip
		bne SND_UPDATE2
		lda SNDPTR+1		
		beq SND_UPDATE_SHIP
SND_UPDATE2
		lda (SNDPTR)		
		beq MUTE_CH4		;is it $00? if so, mute
		tay
		inc SNDPTR
		bne SND_UPDATE3
		inc SNDPTR+1
SND_UPDATE3
		lda (SNDPTR)		
		ora #$06<<2		;YMF262 Block 6
		tax
		inc SNDPTR		;otherwise, store advanced pointer
		bne SND_UPDATE4
		inc SNDPTR+1
SND_UPDATE4
		jsr OPL3_SET_CH4FREQ	;and play the sound; YMF262 channel 4, register X MSB and Y LSB
		bra SND_UPDATE_SHIP
MUTE_CH4
		stz SNDPTR
		stz SNDPTR+1
		lda #$b3		;channel 4 : turn the voice off
		sta YMF262_ADDR1
		lda #$00
		sta YMF262_DATA1

SND_UPDATE_SHIP
		lda SNDPTR_SHIP		;null pointer? if so, skip
		bne SND_UPDATE_SHIP2
		lda SNDPTR_SHIP+1		
		beq update_bgsnd
SND_UPDATE_SHIP2
		lda (SNDPTR_SHIP)		
		beq MUTE_CH7		;is it $00? if so, mute
		tay
		inc SNDPTR_SHIP
		bne SND_UPDATE_SHIP3
		inc SNDPTR_SHIP+1
SND_UPDATE_SHIP3
		lda (SNDPTR_SHIP)		
		ora #$06<<2		;YMF262 Block 6
		tax
		inc SNDPTR_SHIP		;otherwise, store advanced pointer
		bne SND_UPDATE_SHIP4
		inc SNDPTR_SHIP+1
SND_UPDATE_SHIP4
		jsr OPL3_SET_CH7FREQ	;and play the sound; YMF262 channel 7, register X MSB and Y LSB
		bra update_bgsnd
MUTE_CH7
		stz SNDPTR_SHIP
		stz SNDPTR_SHIP+1
		lda #$b6		;channel 7 : turn the voice off
		sta YMF262_ADDR1
		lda #$00
		sta YMF262_DATA1
		
update_bgsnd
		lda BGSOUND_BITS
		bit #thumpsnd
		beq update_bgsnd2
		jsr playthumpsnd
		lda BGSOUND_BITS
update_bgsnd2
		bit #lifesnd
		beq update_bgsnd3
		jsr playlifesnd
		lda BGSOUND_BITS		
update_bgsnd3
		bit #saucersnd
		bne playsaucersnd
	
nobgsnd					;no background sounds?
		lda #$b1		;silence channel ; channel 2: turn the voice off
		sta YMF262_ADDR1
		lda #$00
		sta YMF262_DATA1
nobgsnd2
		rts

playlifesnd
		lda LIFESND_TIMER	;play 3 kHz or silence (channel 6) ?
		and #%00000100
		bne play3khz		;bit 2 set? play 3 kHz
		lda #$b5		;otherwise, silence ; channel 2: turn the voice off
		sta YMF262_ADDR1
		lda #$00
		sta YMF262_DATA1		

declifesnd
		dec LIFESND_TIMER
		bne declifesnd2
		jmp LIFESND_OFF		;halt timer at 0
declifesnd2
		rts		

play3khz	
		ldx #$03+($06<<2)		; YMF262 3KHz = FNUM $3D7, Block 6
		ldy #$D7
		jsr OPL3_SET_CH6FREQ
		bra declifesnd

playsaucersnd		
		lda (SAUCERSNDPTR)	;read 16-bit value
		tay			;save low byte to Y
		inc SAUCERSNDPTR	;advance saucer sound pointer
		bne playsaucersnd3
		inc SAUCERSNDPTR+1
playsaucersnd3
	
		lda (SAUCERSNDPTR)	
		bmi saucersndrept	;is it negative? if so, repeat
		ora #$06<<2		;YMF262 Block 6	
		tax
		inc SAUCERSNDPTR	;advance saucer sound pointer
		bne playsaucersnd4
		inc SAUCERSNDPTR+1
playsaucersnd4
		jsr OPL3_SET_CH2FREQ	;and play the sound
		rts

saucersndrept
		dec SAUCERSNDPTR
		lda SAUCERSNDPTR
		cmp #$FF
		bne saucersndrept2
		dec SAUCERSNDPTR+1
saucersndrept2		
		lda (SAUCERSNDPTR)
		eor #$FF
		inc
		sta TEMP1
		lda SAUCERSNDPTR
		sec
		sbc TEMP1		
		sta SAUCERSNDPTR
		lda SAUCERSNDPTR+1
		sbc #0
		sta SAUCERSNDPTR+1
		bra playsaucersnd	;play new sample

playthumpsnd

		lda THUMP_TIMER	
		cmp #4			;thump sound lasts for 4 frames
		bpl mutethumpsound	;otherwise, silence channel, Branch if Higher ; low pitch = 87Hz, high pitch = 93Hz
		lda #$01+($02<<2)	;YMF26: 93Hz = $1E7, block 2; 87Hz = $1C8, block 2
		tax
		lda #$C8
		tay
		lda THUMP_PITCH		;high or low pitch?
		bne setthumpfreq
		tya
		clc
		lda #$E7
		tay

setthumpfreq
		jsr OPL3_SET_CH5FREQ
		rts 

mutethumpsound
		lda #$b4		;silence channel ; channel 4: turn the voice off
		sta YMF262_ADDR1
		lda #$00
		sta YMF262_DATA1
		rts

THRUSTSND_ON	
		lda BGSOUND_BITS
		and #thrustsnd		;check if thrust sound already on
		bne THRUSTSND_ON2
		lda BGSOUND_BITS
		ora #thrustsnd
		sta BGSOUND_BITS
		lda #$b0
		sta YMF262_ADDR1
		lda #$33
		sta YMF262_DATA1	;turn the voice on; set the octave and freq MSB
THRUSTSND_ON2		
		rts

THRUSTSND_OFF
		lda BGSOUND_BITS
		and #!thrustsnd
		sta BGSOUND_BITS
		lda #$b0
		sta YMF262_ADDR1
		lda #$03
		sta YMF262_DATA1
		rts

THUMPSND_ON
		lda BGSOUND_BITS
		and #thumpsnd		;don't reset thump if it's already on
		bne thumpalreadyon
		ora #thumpsnd
		sta BGSOUND_BITS
		stz THUMP_PITCH
		lda #4
		sta THUMP_TIMER
		lda #$b4
		sta YMF262_ADDR1
		lda #$28
		sta YMF262_DATA1	;turn the voice on; set the octave and freq MSB
thumpalreadyon
		rts

THUMPSND_OFF
		lda BGSOUND_BITS
		and #!thumpsnd
		sta BGSOUND_BITS
		lda #$b4
		sta YMF262_ADDR1
		lda #$03
		sta YMF262_DATA1		
		rts

LIFESND_ON	
		lda BGSOUND_BITS
		ora #lifesnd
		sta BGSOUND_BITS
		lda #88
		sta LIFESND_TIMER
		rts

LIFESND_OFF
		lda BGSOUND_BITS
		and #$7F 			;"!lifesnd" doesn't work with ACME: number does not fit in 8 bits. !?
		sta BGSOUND_BITS
		rts


SAUCERSND_ON
		lda BGSOUND_BITS
		ora #saucersnd
		sta BGSOUND_BITS
		rts

SAUCERSND_OFF
		lda BGSOUND_BITS
		and #!saucersnd  
		sta BGSOUND_BITS
		rts

SND_EXPLODE
		tax
		jsr YMF262_Trig_Explode_ON
		ldx #10
SND_EXPLODE_2
		dex
		bne SND_EXPLODE_2	; DELAY
		jsr YMF262_Trig_Explode_OFF
		rts

WaitKey	
		ldx #PS2_GetKey_FN
		jsr PS2_Function_Call
		rts

IncPTR_U
		php
		inc REGISTER_U_L
		bne IncPTR_U2
		inc REGISTER_U_H
IncPTR_U2
		plp
		rts

IncPTR_X
		php
		inc REGISTER_X_L
		bne IncPTR_X2
		inc REGISTER_X_H
IncPTR_X2
		plp
		rts
	
VDP_PRINTPSTR
		lda (REGISTER_X_L)
		sta TMS9918REG
		jsr IncPTR_X
		lda (REGISTER_X_L)
		ora VRAM
		sta TMS9918REG
		jsr IncPTR_X
PrintString2
		lda (REGISTER_X_L)
		beq PrintString3
		sta TMS9918RAM
		jsr IncPTR_X
		bra PrintString2
PrintString3	
		rts

;used for error message

VDP_PRINTERRSTR
		jsr VDP_PRINTPSTR
		rts
	
READ_JOYSTICK
		lda VIA2ORB ;Joystick port: bit 0 = up, bit 1 = down, bit 2 = left, bit 3 = right, bit 4 = fire		
		and #$1F
		eor #$1F
		rts
	
;------------------------------------------------------------------------------
; includes
;------------------------------------------------------------------------------

;	.include "../include/random.asm"
;	.include "../include/ym2149.asm"
!src "random.asm"
!src "ymf262.asm"
; patterns
SPRITEPATS
;	.include "sprites.inc"
!src "sprites.inc"
SPRITEPATS_END

TEXTPATS
;	.include "text.inc"
!src "text.inc"
TEXTPATS_END

; trig tables
; 32 entries each, 8.8 signed fixed point (2 bytes/entry, 64 bytes/table)
!src "trig.inc"
!src "directions.inc"
!src "positions.inc"

; sound effect frequencies
!src "sounds.inc"
;------------------------------------------------------------------------------
; static data
;------------------------------------------------------------------------------

; positioned strings, prefixed with VRAM addresses (screen positions)
authorstr
	!word	NAMETABLE+10+(32*22)
	!text	$22,"#","$","%","&","'","(",")","*","+",",","-",0

authorstr2
	!word	NAMETABLE+1+(32*23)
	!text "65C02 VERSION UWE GOTTSCHLING",0

gameoverstr
	!word NAMETABLE+11+(32*6)
	!text "GAME OVER",0

startstr
	!word NAMETABLE+11+(32*18)
	!text "PUSH START",0

clrgameoverstr
	!word NAMETABLE+11+(32*6)
	!text "         ",0

clrstartstr
	!word NAMETABLE+11+(32*18)
	!text "          ",0

; plain strings
objerror
	!word NAMETABLE
	!text "OUT OF OBJECTS",0

; saucer data
saucer_yvels
!word $0000,$FF60,$00A0,$0000

;------------------------------------------------------------------------------
; data structures
;------------------------------------------------------------------------------
; Asteroids array.
; Since we have enough memory, asteroid slots are not reused.
; New asteroids are spawned at the end of the list, we don't
; bother to reclaim old slots.


OBJECTS			= *
BULLETS 		= *
SHIPBULLETS 		!fill OBJ_SIZE*NUM_SHIPBULLETS	; 12 * 4 = 48
SHIPBULLETS_END		= *
SAUCERBULLETS		!fill OBJ_SIZE*NUM_SCRBULLETS	; 12 * 2 = 24
SCRBULLETS_END		= *
BULLETS_END		= *

PLAYERSHIP		!fill OBJ_SIZE
SAUCER			!fill OBJ_SIZE
SHIPS_END		= *

ASTEROIDS		!fill OBJ_SIZE*NUM_ASTEROIDS	; 12 * 6 * 6 = 432 ($1b0)
ASTEROIDS_END		= *
OBJECTS_END		= *
CODE_END		!BYTE 00


		
					
