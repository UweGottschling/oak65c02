; Asteroid direction vector table
; 32 possible directions per speed (straight directions not included)

AST_DIRECTIONS
!word	COS_11_25,  SIN_11_25	;instead of 0
!word	COS_11_25,  SIN_11_25
!word	COS_22_5,   SIN_22_5  
!word	COS_33_75,  SIN_33_75 
!word	COS_45,     SIN_45	  
!word	COS_56_25,  SIN_56_25 
!word	COS_67_5,   SIN_67_5  
!word	COS_78_75,  SIN_78_75 
!word	COS_101_25, SIN_101_25	;instead of 90
!word	COS_101_25, SIN_101_25
!word	COS_112_5,  SIN_112_5 
!word	COS_123_75, SIN_123_75
!word	COS_135,    SIN_135   
!word	COS_146_25, SIN_146_25
!word	COS_157_5,  SIN_157_5 
!word	COS_168_75, SIN_168_75
!word	COS_191_25, SIN_191_25	;instead of 180
!word	COS_191_25, SIN_191_25
!word	COS_202_5,  SIN_202_5 
!word	COS_213_75, SIN_213_75
!word	COS_225,    SIN_225   
!word	COS_236_25, SIN_236_25
!word	COS_247_5,  SIN_247_5 
!word	COS_258_75, SIN_258_75
!word	COS_218_25, SIN_218_25	;instead of 270
!word	COS_218_25, SIN_218_25
!word	COS_292_5,  SIN_292_5 
!word	COS_303_75, SIN_303_75
!word	COS_315,    SIN_315   
!word	COS_326_25, SIN_326_25
!word	COS_337_5,  SIN_337_5 
!word	COS_348_75, SIN_348_75
