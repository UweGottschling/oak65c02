; Matt Sarnoff (msarnoff.org/6809) - November 5, 2010
; YMF-262 Uwe Gottschling 2021
; f-num = freq * 2^(20-block)/49716
;
;On the original game, 555 timers and shift registers were used for sound generation. 
;Channel 1 : thrust
;Channel 2 : explosion
;Channel 3 : shot ship
;Channel 4 : shot saucer
;Channel 5 : life sound
;Channel 6 : saucer sound
;Channel 7 : thump sound 
;

;***************************
; Config YMF262
;
;*************************** 
YMF262Init
		lda #$05
		sta YMF262_ADDR2
		lda #$01			
		sta YMF262_DATA2	; OPL3 Mode
		ldy #$00
YMF262Init10
		lda YMF262_Instrument_Noise,y
		sta YMF262_ADDR1
		iny
		lda YMF262_Instrument_Noise,y
		sta YMF262_DATA1
		iny
		cpy #YMF262_Instrument_Noise_End-YMF262_Instrument_Noise
		bne YMF262Init10
		rts

YMF262_Instrument_Noise
; The groupings of twenty-two registers (20-35, 40-55, etc.) 
; have an odd order due to the use of two operators for each FM voice. 
; The following table shows the offsets within each group of registers for each operator.

;    Channel	1	2 	3	4	5 	6	7	8 	9
;    Operator 1	00	01 	02	08	09 	0A	10	11 	12
;    Operator 2	03	04 	05	0B	0C 	0D	13	14 	15
;Channel 1: thrust noise
		; Register, Data 
		!by $B0,$00 			; Turn the voice off; set the octave and freq MSB
		; Operator 1
		!by $20,$21			; Set the modulator's multiple 5x, Tremolo, Vibrator
		!by $40,$00			; Set the modulator's Key-scaling and Output-level: 0 = loudest
		!by $60,$F1			; Modulator attack: F is the fastest; decay :F is the fastest
		!by $80,$06			; Modulator sustain: 0 is the loudest; release: F is the fastest.
		!by $E0,$00			; Set Modulator wave form
		; Operator 2
		!by $23,$21+$40+$80		; Set the carrier's multiple 1x, vibrator, tremolo
		!by $43,$00			; Set the carrier Key Scaling and volume; 0 = loudest
		!by $63,$F1			; Carrier attack: F is the fastest ; decay: F is the fastest
		!by $83,$06			; Carrier sustain: 0 is the loudest; release: F is the fastest.
		!by $E3,$00			; Set Carrier wave form
		; Channel 1
		!by $A0,$10			; Set voice frequency's LSB
		!by $C0,$3E			; Set Feedback, Decay Alg

;Channel 2: saucer sound
		!by $B1,$00 			; Turn the voice off; set the octave and freq MSB
		; Operator 1
		!by $21,$00			; Set the modulator's multiple 5x, Tremolo, Vibrator
		!by $41,$3F			; Set the modulator's Key-scaling and Output-level: 0 = loudest
		!by $61,$00			; Modulator attack: F is the fastest; decay :F is the fastest
		!by $81,$00			; Modulator sustain: 0 is the loudest; release: F is the fastest.
		!by $E1,$00			; Set Modulator wave form
		; Operator 2
		!by $24,$21			; Set the carrier's multiple 1x, vibrator, tremolo
		!by $44,$0F			; Set the carrier Key Scaling and volume; 0 = loudest
		!by $64,$EF			; Carrier attack: F is the fastest ; decay: F is the fastest
		!by $84,$0E			; Carrier sustain: 0 is the loudest; release: F is the fastest.
		!by $E4,$06			; Set Carrier wave form; 06 = square wave
		; Channel 1
		!by $A1,$41			; Set voice frequency's LSB
		!by $C1,$31			; Set Feedback, Decay Alg

;Channel 3: explosions
		!by $B2,$00 			; Turn the voice off; set the octave and freq MSB
		; Operator 1
		!by $22,$21			; Set the modulator's multiple 5x, Tremolo, Vibrator
		!by $42,$00			; Set the modulator's Key-scaling and Output-level: 0 = loudest
		!by $62,$F0			; Modulator attack: F is the fastest; decay :F is the fastest
		!by $82,$01			; Modulator sustain: 0 is the loudest; release: F is the fastest.
		!by $E2,$00			; Set Modulator wave form
		; Operator 2
		!by $25,$21+$40+$80		; Set the carrier's multiple 1x, vibrator, tremolo
		!by $45,$00			; Set the carrier Key Scaling and volume; 0 = loudest
		!by $65,$F0			; Carrier attack: F is the fastest ; decay: F is the fastest
		!by $85,$04			; Carrier sustain: 0 is the loudest; release: F is the fastest.
		!by $E5,$00			; Set Carrier wave form
		; Channel 1
		!by $A2,$01			; Set voice frequency's LSB
		!by $C2,$3E			; Set Feedback, Decay Alg
; General settings
		!by $BD,$80			; Amplitude Modulation Depth / Vibrato Depth / Rhythm
		!by $03,$3C			; Timer 2 Data = 195 * 0.320msec (255-195=$3C)		

;Channel 4: bullet sound (saucer)
		!by $B3,$00 			; Turn the voice off; set the octave and freq MSB
		; Operator 1
		!by $28,$00			; Set the modulator's multiple 5x, Tremolo, Vibrator
		!by $48,$3F			; Set the modulator's Key-scaling and Output-level: 0 = loudest
		!by $68,$00			; Modulator attack: F is the fastest; decay :F is the fastest
		!by $88,$00			; Modulator sustain: 0 is the loudest; release: F is the fastest.
		!by $e8,$00			; Set Modulator wave form
		; Operator 2
		!by $2B,$21			; Set the carrier's multiple 1x, vibrator, tremolo
		!by $4B,$00			; Set the carrier Key Scaling and volume; 0 = loudest
		!by $6B,$EF			; Carrier attack: F is the fastest ; decay: F is the fastest
		!by $8B,$0E			; Carrier sustain: 0 is the loudest; release: F is the fastest.
		!by $EB,$07			; Set Carrier wave form
		; Channel 1
		!by $A3,$41			; Set voice frequency's LSB
		!by $C3,$31			; Set Feedback, Decay Alg
		
;Channel 5: thump sound 
		!by $B4,$00 			; Turn the voice off; set the octave and freq MSB
		; Operator 1
		!by $29,$00			; Set the modulator's multiple 5x, Tremolo, Vibrator
		!by $49,$3F			; Set the modulator's Key-scaling and Output-level: 0 = loudest
		!by $69,$00			; Modulator attack: F is the fastest; decay :F is the fastest
		!by $89,$00			; Modulator sustain: 0 is the loudest; release: F is the fastest.
		!by $e9,$00			; Set Modulator wave form
		; Operator 2
		!by $2C,$21			; Set the carrier's multiple 1x, vibrator, tremolo
		!by $4C,$0F			; Set the carrier Key Scaling and volume; 0 = loudest
		!by $6C,$EF			; Carrier attack: F is the fastest ; decay: F is the fastest
		!by $8C,$0E			; Carrier sustain: 0 is the loudest; release: F is the fastest.
		!by $EC,$06			; Set Carrier wave form (0 = sine wave)
		; Channel 1
		!by $A4,$41			; Set voice frequency's LSB
		!by $C4,$31			; Set Feedback, Decay Alg
		
;Channel 6: life sound
		!by $b5,$00 			; Turn the voice off; set the octave and freq MSB
		; Operator 1
		!by $2A,$00			; Set the modulator's multiple 5x, Tremolo, Vibrator
		!by $4A,$3F			; Set the modulator's Key-scaling and Output-level: 0 = loudest
		!by $6A,$00			; Modulator attack: F is the fastest; decay :F is the fastest
		!by $8A,$00			; Modulator sustain: 0 is the loudest; release: F is the fastest.
		!by $EA,$00			; Set Modulator wave form
		; Operator 2
		!by $2D,$21			; Set the carrier's multiple 1x, vibrator, tremolo
		!by $4D,$0F			; Set the carrier Key Scaling and volume; 0 = loudest
		!by $6D,$EF			; Carrier attack: F is the fastest ; decay: F is the fastest
		!by $8D,$0E			; Carrier sustain: 0 is the loudest; release: F is the fastest.
		!by $ED,$06			; Set Carrier wave form; 06 = square wave
		; Channel 1
		!by $A5,$41			; Set voice frequency's LSB
		!by $C5,$31			; Set Feedback, Decay Alg
			
;Channel 7: bullet sound (ship)
		!by $B6,$00 			; Turn the voice off; set the octave and freq MSB
		; Operator 1
		!by $30,$00			; Set the modulator's multiple 5x, Tremolo, Vibrator
		!by $50,$3F			; Set the modulator's Key-scaling and Output-level: 0 = loudest
		!by $70,$00			; Modulator attack: F is the fastest; decay :F is the fastest
		!by $90,$00			; Modulator sustain: 0 is the loudest; release: F is the fastest.
		!by $F0,$00			; Set Modulator wave form
		; Operator 2
		!by $33,$21			; Set the carrier's multiple 1x, vibrator, tremolo
		!by $53,$00			; Set the carrier Key Scaling and volume; 0 = loudest
		!by $73,$EF			; Carrier attack: F is the fastest ; decay: F is the fastest
		!by $93,$0E			; Carrier sustain: 0 is the loudest; release: F is the fastest.
		!by $F3,$07			; Set Carrier wave form 
		; Channel 1
		!by $A6,$41			; Set voice frequency's LSB
		!by $C6,$31			; Set Feedback, Decay Alg		
			
YMF262_Instrument_Noise_End

;***************************
; Trigger explode sound
;
;***************************

YMF262_Trig_Explode_OFF
		lda #$b2
		sta YMF262_ADDR1
		lda #$03
		sta YMF262_DATA1
		rts
YMF262_Trig_Explode_ON
		lda #$82
		sta YMF262_ADDR1
		txa			; transfer size to A, size can be 8 to 16
		dec
		eor #$0F
		sta YMF262_DATA1
		lda #$b2
		sta YMF262_ADDR1
		lda #$33
		sta YMF262_DATA1	; Turn the voice on; set the octave and freq MSB
		rts
		
; write 2-byte value to registers 0xB? nd 0xA? (tone frequency)

OPL3_SET_CH2FREQ			;YMF262 channel 2, register X MSB and Y LSB
		lda #$B1
		sta YMF262_ADDR1
		txa
		ora #$20		;key on
		sta YMF262_DATA1
		lda #$A1
		sta YMF262_ADDR1
		tya
		sta YMF262_DATA1
		rts 
		
OPL3_SET_CH4FREQ			;YMF262 channel 4, register X MSB and Y LSB
		lda #$b3
		sta YMF262_ADDR1
		txa
		ora #$20		;key on
		sta YMF262_DATA1
		lda #$a3
		sta YMF262_ADDR1	
		tya
		sta YMF262_DATA1
		rts 

OPL3_SET_CH5FREQ			;YMF262 channel 5, register X MSB and Y LSB
		lda #$B4
		sta YMF262_ADDR1
		txa
		ora #$20		;key on
		sta YMF262_DATA1
		lda #$A4
		sta YMF262_ADDR1
		tya
		sta YMF262_DATA1
		rts 

OPL3_SET_CH6FREQ			;YMF262 channel 6, register X MSB and Y LSB
		lda #$B5
		sta YMF262_ADDR1
		txa
		ora #$20		;key on
		sta YMF262_DATA1
		lda #$A5
		sta YMF262_ADDR1
		tya
		sta YMF262_DATA1
		rts 
		
OPL3_SET_CH7FREQ			;YMF262 channel 7, register X MSB and Y LSB
		lda #$B6
		sta YMF262_ADDR1
		txa
		ora #$20		;key on
		sta YMF262_DATA1
		lda #$A6
		sta YMF262_ADDR1
		tya
		sta YMF262_DATA1
		rts 
		
OPL3_RESET
		lda #$05
		sta YMF262_ADDR2
		lda #$01			
		sta YMF262_DATA2	;OPL3 mode
		ldy #$B0		;key off 	
YMF262_Reset1		
		sty YMF262_ADDR1
		stz YMF262_DATA1
		sty YMF262_ADDR2
		stz YMF262_DATA2			
		iny
		cpy #$B9
		bne YMF262_Reset1
		ldy #$01
YMF262_Reset6
		sty YMF262_ADDR1
		stz YMF262_DATA1
		sty YMF262_ADDR2
		stz YMF262_DATA2	
		iny
		cpy #$F6
		bne YMF262_Reset6
		lda #$05
		sta YMF262_ADDR2
		stz YMF262_DATA2	;set to OPL2 Mode
		rts
