; Matt Sarnoff (msarnoff.org/6809) - July 5, 2010
; Uwe Gottschling 2021 : 65C02 version
;************ Pseudorandom number generator ************
;Uses a 32-bit Galois linear feedback shift register.
;Not that great, but at least it doesn't require multiplication or division.

SETRANDOMSEED
		lda #$12
		sta RANDSEED
		lda #$34
		sta RANDSEED+1
		lda #$56
		sta RANDSEED+2
		lda #$78
		sta RANDSEED+3
		rts

; generate a pseudorandom bit
; arguments:	none
; returns:	bit in C flag
; destroys:	A

RANDBIT
		lsr	RANDSEED	;shift right one bit
		ror	RANDSEED+1
		ror	RANDSEED+2
		ror	RANDSEED+3
		php			;save carry (output) bit	
		bcc	randbitdone	;don't xor if lsb is 0
		lda 	RANDSEED	;xor with 0xD0000001
		eor	#$D0		;(x^32 + x^31 + x^29 + x + 1)
		sta 	RANDSEED
		lda 	RANDSEED+3
		eor	#$01		
		sta 	RANDSEED+3
randbitdone	plp			;pull carry back to status
		rts
;		
; generate a pseudorandom byte from 8 bits
; arguments:	none
; returns:	byte in A
; destroys:	A,X

RANDBYTE
	ldx #8
RANDBYTE2
	jsr RANDBIT
	ror REGISTER_RND
	dex
	bne RANDBYTE2
RANDBYTEExit
	rts



