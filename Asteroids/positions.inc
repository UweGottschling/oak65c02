; Asteroid initial position table
; 32 possible initial positions around edge of screen

AST_POSITIONS
!byte	0,0	;top edge
!byte	24,0
!byte	48,0
!byte	72,0
!byte	96,0
!byte	120,0
!byte	144,0
!byte	168,0
!byte	192,0
!byte	216,0

!byte	0,25	;left edge
!byte	0,50
!byte	0,75
!byte	0,100
!byte	0,125
!byte	0,150

!byte	239,25	;right edge
!byte	239,50
!byte	239,75
!byte	239,100
!byte	239,125
!byte	239,150

!byte	0,175	;bottom edge
!byte	24,175
!byte	48,175
!byte	72,175
!byte	96,175
!byte	120,175
!byte	144,175
!byte	168,175
!byte	192,175
!byte	216,175

