; Sine, unscaled (generated by tablegen.py)

SIN
!byte	SIN_0,   SIN_11_25,  SIN_22_5,  SIN_33_75
!byte	SIN_45,  SIN_56_25,  SIN_67_5,  SIN_78_75
!byte	SIN_90,  SIN_101_25, SIN_112_5, SIN_123_75
!byte	SIN_135, SIN_146_25, SIN_157_5, SIN_168_75
!byte	SIN_180, SIN_191_25, SIN_202_5, SIN_213_75
!byte	SIN_225, SIN_236_25, SIN_247_5, SIN_258_75
!byte	SIN_270, SIN_218_25, SIN_292_5, SIN_303_75
!byte	SIN_315, SIN_326_25, SIN_337_5, SIN_348_75

SIN_0     	=	$0000	;x=0, y=0
SIN_11_25 	=	$0032	;x=0.19635, y=0.19509
SIN_22_5  	=	$0062	;x=0.392699, y=0.382683
SIN_33_75 	=	$008E	;x=0.589049, y=0.55557
SIN_45	   	=	$00B5	;x=0.785398, y=0.707107
SIN_56_25 	=	$00D5	;x=0.981748, y=0.83147
SIN_67_5  	=	$00ED	;x=1.1781, y=0.92388
SIN_78_75 	=	$00FB	;x=1.37445, y=0.980785
SIN_90	   	=	$0100	;x=1.5708, y=1
SIN_101_25	=	$00FB	;x=1.76715, y=0.980785
SIN_112_5 	=	$00ED	;x=1.9635, y=0.92388
SIN_123_75	=	$00D5	;x=2.15984, y=0.83147
SIN_135   	=	$00B5	;x=2.35619, y=0.707107
SIN_146_25	=	$008E	;x=2.55254, y=0.55557
SIN_157_5 	=	$0062	;x=2.74889, y=0.382683
SIN_168_75	=	$0032	;x=2.94524, y=0.19509
SIN_180   	=	$0000	;x=3.14159, y=1.22465e-16
SIN_191_25	=	$FFCE	;x=3.33794, y=-0.19509
SIN_202_5 	=	$FF9E	;x=3.53429, y=-0.382683
SIN_213_75	=	$FF72	;x=3.73064, y=-0.55557
SIN_225   	=	$FF4B	;x=3.92699, y=-0.707107
SIN_236_25	=	$FF2B	;x=4.12334, y=-0.83147
SIN_247_5 	=	$FF13	;x=4.31969, y=-0.92388
SIN_258_75	=	$FF05	;x=4.51604, y=-0.980785
SIN_270   	=	$FF00	;x=4.71239, y=-1
SIN_218_25	=	$FF05	;x=4.90874, y=-0.980785
SIN_292_5 	=	$FF13	;x=5.10509, y=-0.92388
SIN_303_75	=	$FF2B	;x=5.30144, y=-0.83147
SIN_315   	=	$FF4B	;x=5.49779, y=-0.707107
SIN_326_25	=	$FF72	;x=5.69414, y=-0.55557
SIN_337_5 	=	$FF9E	;x=5.89049, y=-0.382683
SIN_348_75	=	$FFCE	;x=6.08684, y=-0.19509
