; Compiler : ACME 
; UTF-8 character encoding
;

!cpu 65c02

;***************************
; Wozmon from Steve Wozniak
; Mods (C) 2019 Uwe Gottschling:
; - no dummy read after write a byte,
; - no case sensitivity hex input
; - type "X" to exit
;***************************

Exit_WOZMON
		rts			; exit Wozmon
Wozmon	
RESET           cld             	; clear decimal arithmetic mode.
Init
		lda #<Startwozmontext	; < low byte of expression
		sta PTR_String_L
		lda #>Startwozmontext	; > high byte of expression
		sta PTR_String_H
		jsr Print_String
		lda #ESC		; ESC
ESCAPE          lda #"\\"   		; 
                jsr ECHO		; output it.
GETLINE         lda #LF 		; CR
                jsr ECHO        	; output it.
		jsr LEditor
                ldy #$FF		; reset text index.
                lda #$00		; for XAM mode.
                tax			; 0->X.
SETSTOR         asl			; leaves $7B if setting STOR mode.
SETMODE         sta Mon_MODE        	; B7,B6: %00=XAM %01=STORE %10=BLOCK XAM
BLSKIP          iny			; advance text index.
NEXTITEM        lda InBuffer,Y		; get character.
                cmp #00    		; EOL?
                beq GETLINE     	; yes, done this line.
                cmp #"."  		; "."?
                bcc BLSKIP      	; skip everything below ASCII "."
                bne NOBLOCK     	; not equal? Skip set BLOCK mode.
		lda #$80 		; BLOCK XAM
		jmp SETMODE
NOBLOCK         
                cmp #"R"		; "R"?
                beq RUN			; yes. Run user program.
		cmp #"X"
		beq Exit_WOZMON
                stz Mon_L           	; $00-> L.
                stz Mon_H           	; and H.
                sty Mon_YSAV        	; save Y for comparison.
NEXTHEX         lda InBuffer,Y        	; get character for hex test.
		jsr Hex2bin

		bcs NOTHEX
DIG             asl
                asl             	; hex digit to MSD of A.
                asl
                asl
                ldx #$04        	; shift count.
HEXSHIFT        
		asl             	; hex digit left, MSB to carry.
                rol Mon_L           	; rotate into LSD.
                rol Mon_H           	; rotate into MSD’s.
                dex             	; done 4 shifts?
                bne HEXSHIFT    	; no, loop.
                iny             	; advance text index.
                bne NEXTHEX     	; always taken. Check next char for hex.
NOTHEX
		jmp CHECKSTORE
NOTHEX2
		cpy Mon_YSAV        	; check if L, H empty (no hex digits).
                beq ESCAPE      	; yes, generate ESC sequence.
                bit Mon_MODE        	; test MODE byte.
                bvc NOTSTOR     	; B6=1 STOR B6=0 for XAM & BLOCK XAM
                lda Mon_L           	; LSD’s of hex data.
                sta (PTR_W_L)     	; store at current ‘store index’.
                inc PTR_W_L         	; increment store index.
                bne NEXTITEM    	; get next item. (no carry).
                inc PTR_W_H         	; add carry to ‘store index’ high order.
TONEXTITEM      jmp NEXTITEM    	; get next command item.
RUN             jmp (PTR_String_L)      ; run at current XAM index.
NOTSTOR         bmi XAMNEXT     	; B7=0 for XAM, 1 for BLOCK XAM.
                ldx #$02        	; byte count.
SETADR          
		lda Mon_L-1,X       	; copy hex data to
                sta PTR_W_L-1,X     	; ‘store index’.
                sta PTR_String_L-1,X    ; and to ‘XAM index’.
                dex             	; next of 2 bytes.
                bne SETADR      	; loop unless X=0.
NXTPRNT         
		bne PRDATA      	; NE means no address to print.
                lda #LF       		; CR.
                jsr ECHO        	; output it.
                lda PTR_String_H        ; ‘examine index’ high-order byte.
                jsr PRBYTE      	; output it in hex format.
                lda PTR_String_L        ; low-order ‘examine index’ byte.
                jsr PRBYTE      	; output it in hex format.
                lda #":"		; ":".
                jsr ECHO        	; output it.
PRDATA          
		lda #SP			; blank.
                jsr ECHO        	; output it.
                lda (PTR_String_L)    	; get data byte at ‘examine index’.
                jsr PRBYTE      	; output it in hex format.
XAMNEXT         
		stz Mon_MODE		; 0->MODE (XAM mode).
                lda PTR_String_L
                cmp Mon_L		; compare ‘examine index’ to hex data.
                lda PTR_String_H
                sbc Mon_H
                bcs TONEXTITEM		; not less, so no more data to output.
                inc PTR_String_L
                bne MOD8CHK     	; increment ‘examine index’.
                inc PTR_String_H
MOD8CHK         
		lda PTR_String_L	; check low-order ‘examine index’ byte
                and #$07		; for MOD 8=0
                bpl NXTPRNT     	; always taken.
PRBYTE                       		; save A for LSD.
		phy
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call
;		jsr Print_Bin2Hex
		ply
		rts
ECHO 
		phy
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
;		jsr Print_CHR
		ply
                rts             	; return.
CHECKSTORE
		lda InBuffer,Y
		cmp #":" 
		bne CHECKSTORE_exit
	        lda Mon_L
		sta PTR_W_L
		lda Mon_H
		sta PTR_W_H
		lda #$40
		sta Mon_MODE 
		iny
		jmp NEXTHEX
CHECKSTORE_exit
		jmp NOTHEX2


Startwozmontext
!text LF,"Wozmon V1.0",LF
!text "<Address><Enter>",LF,"Display content of memory",LF
!text "<Start Address>.<End Address><Enter>",LF,"Display a range of memory",LF
!text "<Address>:<Data> <Data>...<Enter>",LF,"Write data to memory",LF
!text "<Address>R<Enter>",LF,"Run from an address",LF
!text "X Exit",LF
!text 00


