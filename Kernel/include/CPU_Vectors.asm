
*=$FFE0		; (Reserved) 65c816
!word BRKService816

*=$FFE2		; (Reserved) 65c816
!word BRKService816

*=$FFE4		; COP 65c816
!word BRKService816	

*=$FFE6		; BRK 65c816
!word BRKService816

*=$FFE8		; ABORTB 65c816
!word BRKService816

*=$FFEA		; NMIB 65c816
!word BRKService816

*=$FFEC		; (Reserved) 65c816
!word BRKService816	

*=$FFEE		; IRQB 65c816
!word BRKService816


; Vector Locations
; Emulation Mode Vector Locations (8-bit Mode)

*=$FFF0		; Reserved 65c816
!word BRKService816

*=$FFF2		; Reserved 65c816
!word BRKService816

*=$FFF4		; COP Service 65c816
!word BRKService816

*=$FFF6		; Reserved 65c816
!word BRKService816

*=$FFF8		; ABORT Vector 65c816
!word BRKService816

*=$FFFA		; NMI Vector
!word NMIService		

*=$FFFC		; RESET Vector
!word Reset  	

*=$FFFE		; Vector BRK/IRQB Software/Hardware
!word IRQService

