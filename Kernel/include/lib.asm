; Compiler : ACME 
; UTF-8 character encoding
; (C) 2019 Uwe Gottschling
;***************************
; Multiply 16bit x 8Bit = 16Bit
; input: Operand1 = Low  Operand2 = High x Operand3
; output A Low, Y high
; used: a,x,y
;***************************

Multiply16
		lda #$00		
		tay
		bra Multiply16_3
Multiply16_1
		clc
		adc Operand1
		tax
		tya
		adc Operand2
		tay
		txa
Multiply16_2
		asl Operand1	; calculate First R
		rol Operand2
Multiply16_3  			; accumulating multiply entry point (enter with .A=lo, .Y=hi)
		lsr Operand3
		bcs Multiply16_1
		bne Multiply16_2
		rts

;***************************
; Divide 16bit / 16Bit = 16Bit
; input: Operand1,2 (dividend low/high) Operand3,4 (divisor low/high)
; output: Operand5,6 (reminder low/high)
; output: Operand1,2 (result low/high)
; used: A,X,Y
;***************************

Divide16
		lda #0	        ; preset remainder to 0
		sta Operand5
		sta Operand6
		ldx #16	        ; set bit length
Divide16_1
		asl Operand1	; dividend low & high *2, msb -> Carry
		rol Operand2
		rol Operand5	; remainder low & high * 2 + msb from carry
		rol Operand6
		lda Operand5
		sec
		sbc Operand3	; substract divisor to see if it fits in
		tay	        ; low result -> Y, for we may need it later
		lda Operand6
		sbc Operand4
		bcc Divide16_2	; if carry=0 then divisor didn't fit in yet

		sta Operand6	; else save substraction result as new remainder,
		sty Operand5
		inc Operand1	; result low = divident low
Divide16_2
		dex
		bne Divide16_1	; branch to do next bit
		rts

;***************************
; Converts two ASCII-HEX characters into binary byte
; input: A (1. Char) Y (2. Char)  
; output: A
; used: A,Y
;***************************

Hex2byte
    		jsr Hex2bin	; convert to value
		bcs Hex2byte2	; end if error
    		sta Operand1	; store >Operand1
    		tya 		; get the second char
    		jsr Hex2bin     ; convert
		bcs Hex2byte2	; end if error
    		asl 		; shift 4 bits left
    		asl
    		asl
    		asl
    		clc
    		adc Operand1	; add hi nibble
		clc
Hex2byte2
    		rts

;***************************
; Converts an ASCII character (0 to 9 or A to F or a to f)
; into a binary value.
; input: A 
; output: A
; used: A
;***************************

Hex2bin
    		sec			; subtract without carry
    		sbc #'0'                ; subtract $30 gives the binary result for numbers
    		cmp #10                 ; is the value below decimal 10 ?
    		bcc Hex2bin3		; if yes then ready
		sec			; subtract without carry
		sbc #'A'-'0'-10         ; subtract difference to ASCII "A
		cmp #10                	; is the result under 10 dec ?
		bcc Hex2bin2		; if yes then error
		cmp #$10		; if the result is below 10 hex 
		bcc Hex2bin3		; then Done
		sec			; subtract without carry
		sbc #'a'-'A'         	; subtract the difference from the lower case letters
		cmp #10			; is the result under 10 dec ??
		bcc Hex2bin2		; then error
		cmp #$10		; is the result under $10 hex ?
		bcc Hex2bin3		; if yes, then done.
Hex2bin2		
		sec
Hex2bin3
		rts			; error flag = carry

;***************************
; Convert accumulator content to HEX Value and print it to STDIO
; input: A
; output: stdio
; used: A,X
;***************************

Print_Bin2Hex 				; convert A to HEX
		jsr Bin2Hex1
		phx
		jsr OS_Internal_Print_CHR		
		plx
		txa
		jsr OS_Internal_Print_CHR	
		rts
Bin2Hex1
		pha                     ; save A; , output a,x
		and #$0F
		jsr BintoHex2
		tax			; X = high nibble
		pla			; from here on, the bytes for the lower byte half
		lsr                    	; push 4 times to the right
		lsr
		lsr
		lsr
		jsr BintoHex2		; convert to ASCIIn
		rts			; A= low nibble

;***************************
; Converts a binary value in the accumulator into an ASCII hex character
; input: A
; output: A
;***************************

BintoHex2
		cmp #10			; Check whether in the number range
		bpl BintoHex3
		ora #$30		; Convert to ASCII space
		rts
BintoHex3
		adc #'A'-10-1		; Convert to ASCII letter range
		rts

;***************************
; Get one line from std input
; input: -
; output: IN String
; used: A,X,Y
;***************************

LEditor					
		ldy #00			; set input buffer to start
LEditor2
		phy
		jsr Get_CHR 		; get character in A and set cursor
		ply
		cmp #BS			; backspace ?
		beq LEbackspace
		cmp #CR			; return ?
		beq LEReturn
		cmp #LF			; some serial terminals send LF instead of CR
		beq LEReturn
		cmp #HT			; tab Key ?
		beq LEditor2		
		cpy #$7F		; test for maximum length
		beq LEditor2
		jsr LEEcho
		sta InBuffer,y
		iny
		jmp LEditor2
LEExit
		rts
LEbackspace
 		cpy #0			; back up text index.
		beq LEditor2
		dey
		lda #BS			; ASCII Backspace Command
		jsr LEEcho	
		lda #SP
		jsr LEEcho
		lda #BS			; ASCII Backspace Command
		jsr LEEcho	
		jmp LEditor2
LEReturn
		lda #NUL		; mark string end with "00" 
		sta InBuffer,y
		lda #LF
		jsr LEEcho
		rts
LEEcho
		pha
		phy
		jsr OS_Internal_Print_CHR
		ply
		pla
                rts
LEGetChar
		phy
		phx
		lda STDOUT
		cmp #3		
		bne LEGetChar2
		ldx #VDP_Cursor_On_FN
		jsr VDP_Function_Call
		ldx #PS2_GetKey_FN
		jsr PS2_Function_Call
		pha
		ldx #VDP_Cursor_Off_FN
		jsr VDP_Function_Call
		pla
		plx
		ply
		rts
LEGetChar2
		ldx #PS2_GetKey_FN
		jsr PS2_Function_Call
		plx
		ply
		rts

;***************************
; Read byte from default input
; input: stdin
; output: A
; used: A,X,Y
;***************************

Get_CHR	
		lda STDIN
		cmp #0			; 0 = Keyboard
		bne Get_CHR2
		jmp LEGetChar
Get_CHR2
		cmp #2			; 2 = UART 
		bne Get_CHR3
		ldx #UART_CHAR_Receive_FN
		jsr ACIA_Function_Call
Get_CHR3	
		rts

;***************************
; Write byte to default output
; input: A
; output: stdout
; used: A,X,Y
;***************************

Print_CHR
		pha
		lda STDOUT
		cmp #3 			; 3 = Display
		bne Print_CHR2
		pla
		cmp #BEL
		bne Print_CHR1
		ldx #OS_Bell_FN
		jsr OS_Function_Call
		rts		
Print_CHR1
		ldx #VDP_print_CHR_FN
		jsr VDP_Function_Call
		rts 
Print_CHR2
		cmp #2			; 2 = UART
		bne Print_CHR3 
		pla
		ldx #UART_CHAR_Transmit_FN
		jsr ACIA_Function_Call
		rts
Print_CHR3
		pla
		sec			; set error flag
		rts

;***************************
; Write String to Default Output
; input: PTR_String
; output: stdio
; Used: A,X
;***************************

Print_String
		lda (PTR_String_L)
		beq Print_String3	; end if zero
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
Print_String1
		inc PTR_String_L
	    	bne Print_String2
	    	inc PTR_String_H
Print_String2
		bra Print_String
Print_String3
		rts

;***************************
; 32-Bit Binary to BCD
; Example $4a,$a1,$00,$00 Dezimal = 41.290 Bytes
; (Alternaive: double dabble (shift-and-add-3 algorithm))
; input: Operand 1,2,3,4
; output: Operand 5,6,7,8,9
; used: A,X
;***************************

Bin2BCD32
		clc			
Bin2BCD32_3
		stz Operand5	; erase result (BCD Outbuffer)
		stz Operand6
		stz Operand7
		stz Operand8
		stz Operand9
		ldx #32		; number of bits
		sed		; set decimal mode
Bin2BCD32_4
		asl Operand1	; shift out one bit
		rol Operand2
		rol Operand3
		rol Operand4

		lda Operand9
		adc Operand9
		sta Operand9
		lda Operand8
		adc Operand8
		sta Operand8
		lda Operand7
		adc Operand7
		sta Operand7
		lda Operand6
		adc Operand6
		sta Operand6
		lda Operand5
		adc Operand5
		sta Operand5
		dex			; and repeat for next bit
		bne Bin2BCD32_4
		cld			; clear decimal mode
		rts

;***************************
; Print BCD 
; 0 fill with space except the last
; input = Operand 5,6,7,8,9
; output: stdio
; used: A,X
;***************************

Print_BCD
		ldx #0
Print_BCD1
		lda Operand5,x
		and #$F0
		bne Print_BCD7
		jsr Print_BCD_Space
Print_BCD3
		lda Operand5,x
		and #$0F
		bne Print_BCD9
		cpx #4
		beq Print_BCD9
		jsr Print_BCD_Space
Print_BCD4
		inx
		cpx #5
		bne Print_BCD1
		rts

Print_BCD6	;*************** normal output
		lda Operand5,x
		and #$F0
Print_BCD7
		lsr
		lsr
		lsr
		lsr
		ora #$30
		jsr Print_BCD11
Print_BCD8
		lda Operand5,x
		and #$0F
Print_BCD9
		ora #$30
		jsr Print_BCD11
		inx
		cpx #5
		bne Print_BCD6
		rts

Print_BCD_Space
		phx
		lda #SP
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		plx
		rts
Print_BCD11
		phx
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		plx
		rts

;***************************
; Write Binary Digits to Default Output
; input: A
; output: stdio
; used: A,X
;***************************

Print_BIN2DIG				; print binary digits to stdout
		ldx #08
Print_BIN4
		rol
		pha
		bcc Print_BIN5
		lda #"1"
		jmp Print_BIN6
Print_BIN5
		lda #"0"
Print_BIN6
		phx
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		plx
		pla
		dex
		bne Print_BIN4
		rts

;***************************
; Get two ASCII characters from stdin 
; Input: Y=offset for InBuffer
; Output: Operand 1, Operand 2
; Used: A,Y
;***************************

STDIN_ASCII_TO_BCD
		ldx #OS_LineEditor_FN		; get one line for date / time set
		jsr OS_Function_Call
		ldy #$FF
STDIN_ASCII_TO_BCD_2
		iny
		lda InBuffer,y
		cmp #' '			; skip space
		beq STDIN_ASCII_TO_BCD_2
		cmp #00
		beq STDIN_ASCII_TO_BCD_Exit_Error
		sta Operand1
		iny
		lda InBuffer,y
		sta Operand2
		jsr ASCII_to_BCD
		rts
STDIN_ASCII_TO_BCD_Exit_Error
		sec
		rts

;***************************
; Convert ASCII to BCD
; input: Operand1 (left ASCII), Operand2 (right ASCII)
; output: A Binary data
; used: A,X
;***************************

ASCII_to_BCD
		lda Operand2
		jsr ASCII2BCD		
		bcs ASCII_to_BCD_one_digit_Exit	; if 2. ASCII Byte is no number then branch
		sta Operand2
		lda Operand1
		jsr ASCII2BCD		
		bcs ASCII_to_BCD_Error_Exit
		asl
		asl	
		asl
		asl
		sta Operand1
		clc
		lda Operand1
		adc Operand2
		jsr BCD2BIN_8
		clc
		rts
ASCII_to_BCD_Error_Exit
		sec
		rts
ASCII_to_BCD_one_digit_Exit
		lda Operand1
		jsr ASCII2BCD		
		bcs ASCII_to_BCD_Error_Exit
		clc
		rts

;***************************
; BCD to binary conversion 
; input: A: BCD data
; output: A; Binary data
; used: A,X
;***************************

BCD2BIN_8
		tax		; save value
		and #$F0	; mask value
		lsr 		; divide 2 = upper nibble *8
		sta Operand1	; save *8
		lsr		; divide by 4
		lsr		; divide by 8 = upper nibble *2
		clc			
		adc Operand1
		sta Operand1
		txa
		and #$0F
		clc
		adc Operand1
		rts

;***************************
; Convert 1 Byte ASCII to BCD (0...9)
; input: A
; output: A
; CARRY = out of range
; used: A
;***************************

ASCII2BCD
		cmp #'0'
		bmi ASCII2BCDError
		cmp #'9'+1
		bpl ASCII2BCDError
ASCII2BCD2
		sec
		sbc #'0'
ASCII2BCDExit
		clc
		rts
ASCII2BCDError
		sec
		rts

;***************************
; 8-Bit Binary to BCD and Print in Time Format (2 Digits)
; input: A
; output: stdio
; used: A 
;***************************

Print_Bin2BCD_2D
		jsr Bin2BCD8
		lda Operand4
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call
		rts 

;***************************
; 8-Bit Binary to BCD
; input: A
; output: Operand 3,4
; used: A,X
;***************************

Bin2BCD8
		clc
		sta Operand1
Bin2BCD8_3
		stz Operand3	; erase result (BCD Outbuffer)
		stz Operand4	; erase result (BCD Outbuffer)
		ldx #8		; number of bits
		sed		; set decimal mode
Bin2BCD8_4
		asl Operand1	; shift out one bit
		lda Operand4
		adc Operand4
		sta Operand4
		lda Operand3
		adc Operand3
		sta Operand3
		dex		; and repeat for next bit
		bne Bin2BCD8_4
		cld		; clear decimal mode
		rts

