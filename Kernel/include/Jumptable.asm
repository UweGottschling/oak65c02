

Reset 				= $e000 ;e000 4c47e0             		jmp Reset
ACIA_Function_Call		= $e003 ;e003 4c52f7             		jmp ACIA_Function_Call
VIA2_Init			= $e006 ;e006 4c58ea             		jmp VIA2_Init
FDC_Function_Call		= $e009 ;e009 4c28e9             		jmp FDC_Function_Call
VDP_Function_Call		= $e00c ;e00c 4c88eb             		jmp VDP_Function_Call
PS2_Function_Call		= $e00f ;e00f 4c12f7             		jmp PS2_Function_Call	
;DOS_Function_Call		= $e012 ;e012 6c9202             		jmp (DOS_Function_Vector_L)
DOS_Function_Call		= $e012 ;e012 6c9202             		jmp (DOS_Function_Vector_L)
OS_Function_Call		= $e015

; Einsprungadressen ROM
;Reset				= $e000;    10  e000 4c4be0             		jmp Reset
;UART_Init 			= $e003;    11  e003 4c4be3             		jmp UART_Init
;UART_CHAR_Receive		= $e006;    12  e006 4c33e3             		jmp UART_CHAR_Receive
;UART_CHAR_Transmit		= $e009;    13  e009 4c3ee3             		jmp UART_CHAR_Transmit
;VIA2_Init			= $e00c;    14  e00c 4c36e4             		jmp VIA2_Init
;TMS9918_Init_Textmode1		= $e00f;    15  e00f 4ce4e3             		jmp TMS9918_Init_Textmode1
;TMS9918_Init_Graphic1Mode	= $e012;    16  e012 4c0ce4             		jmp TMS9918_Init_Graphic1Mode
;TMS9918_Init_Clear_Screen	= $e015;    17  e015 4c5de4             		jmp TMS9918_Init_Clear_Screen
;TMS9918_Init_Pattern		= $e018;    18  e018 4c8fe4             		jmp TMS9918_Init_Pattern
;TMS9918_print_CHR		= $e01b;    19  e01b 4cb7e4             		jmp TMS9918_print_CHR
;TMS9918_Scoll_up		= $e01e;    20  e01e 4cd5e5             		jmp TMS9918_Scoll_up
;PS2_GetKey			= $e021;    21  e021 4c52e4             		jmp PS2_GetKey
;PS2_ChkKey			= $e024;    22  e024 4c28e6             		jmp VRAM_Clear_Pattern_Name_Table
;Bin2BCD			= $e027;    23  e027 4c15e7             		jmp Bin2BCD
;WD1770_Restore			= $e02a;    24  e02a 4c50e7             		jmp WD1770_Restore
;WD1770_SeekTrack		= $e02d;    25  e02d 4c64e7             		jmp WD1770_SeekTrack
;WD1770_Addr			= $e030;    26  e030 4c7be7             		jmp WD1770_Addr
;WD1770_RTrack			= $e033;    27  e033 4c84e7             		jmp WD1770_RTrack
;WD1770_RSect			= $e036;    28  e036 4c8de7             		jmp WD1770_RSect
;Bin2Hex			= $e039;    29  e039 4cb5e6             		jmp Bin2Hex
;Hex2byte			= $e03c;    30  e03c 4c86e6             		jmp Hex2byte
;ChangeSTDOUT			= $e03f;    31  e03f 4c53e2             		jmp ChangeSTDOUT
;ChangeSTDIN			= $e042;    32  e042 4c63e2             		jmp ChangeSTDIN
;Print_CHR			= $e045;    33  e045 4c84e2             		jmp Print_CHR
;Get_CHR			= $e048;    34  e048 4c73e2             		jmp Get_CHR
;Print_String			= $e04b;    35  e04b 4c9de2             		jmp Print_String
