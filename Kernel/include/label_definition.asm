;Standard - label - definition
; I/O registers addresses

IO_BASE		= $DF00

; UART
UARTDATA 	= IO_BASE + 0
UARTSTATUS 	= IO_BASE + 1
UARTCMD 	= IO_BASE + 2
UARTCTRL 	= IO_BASE + 3

; VIA 1
VIA1ORB		= IO_BASE +$10 ; DB0 = RAM_ENABLE; 0=RAM ENABLE 1= ROM ENABLE 
VIA1ORA		= IO_BASE +$11
VIA1DDRB	= IO_BASE +$12 ; 0=input 1=output
VIA1DDRA	= IO_BASE +$13 ; 0=input 1=output 
VIA1T1CL	= IO_BASE +$14
VIA1T1CH	= IO_BASE +$15
VIA1T1LL	= IO_BASE +$16
VIA1T1LH	= IO_BASE +$17
VIA1T2CL	= IO_BASE +$18
VIA1T2CH	= IO_BASE +$19
VIA1SR		= IO_BASE +$1A
VIA1ACR		= IO_BASE +$1B
VIA1PCR		= IO_BASE +$1C
VIA1IFR		= IO_BASE +$1D
VIA1IER		= IO_BASE +$1E
VIA1ORA2	= IO_BASE +$1F

; VIA 2 (also Keyboad input)
VIA2ORB		= IO_BASE +$20
VIA2ORA		= IO_BASE +$21
VIA2DDRB	= IO_BASE +$22 ; 0=input 1=output 
VIA2DDRA	= IO_BASE +$23 ; 0=input 1=output 
VIA2T1CL	= IO_BASE +$24
VIA2T1CH	= IO_BASE +$25
VIA2T1LL	= IO_BASE +$26
VIA2T1LH	= IO_BASE +$27
VIA2T2CL	= IO_BASE +$28
VIA2T2CH	= IO_BASE +$29
VIA2SR		= IO_BASE +$2A
VIA2ACR		= IO_BASE +$2B
VIA2PCR		= IO_BASE +$2C
VIA2IFR		= IO_BASE +$2D
VIA2IER		= IO_BASE +$2E
VIA2ORA2	= IO_BASE +$2F

EF9367CMD 	= IO_BASE +$30
EF9367CTRL1 	= IO_BASE +$31
EF9367CTRL2 	= IO_BASE +$32
EF9367XMSB 	= IO_BASE +$38
EF9367XLSB 	= IO_BASE +$39
EF9367YMSB 	= IO_BASE +$3A
EF9367YLSB 	= IO_BASE +$3B
EF9367PAGE 	= IO_BASE +$40

TMS9918RAM	= IO_BASE +$50
TMS9918REG	= IO_BASE +$51
V9938PALREG	= IO_BASE +$52
V9938INDREG	= IO_BASE +$53

WD177XCMD	= IO_BASE +$60
WD177XTRK	= IO_BASE +$61
WD177XSEC	= IO_BASE +$62
WD177XDAT	= IO_BASE +$63
FLOPPYLATCH	= IO_BASE +$64	; Bit 0 = side select (0 = top 1 = bottom), bit 1 = drive select (0=a 1=b)

WD37C65_STAT	= IO_BASE +$70
WD37C65_DATA	= IO_BASE +$71
WD37C65_CONF	= IO_BASE +$72
WD37C65_OPER	= IO_BASE +$73

DS1685_DATA 	= IO_BASE +$80
DS1685_ADDR 	= IO_BASE +$81

YMF262_ADDR1 	= IO_BASE +$84
YMF262_DATA1 	= IO_BASE +$85
YMF262_ADDR2 	= IO_BASE +$86
YMF262_DATA2 	= IO_BASE +$87


; WD177X commands
WD177XREST	= $00		; Restore (Seec Track 0)
WD177XSEEK	= $10		; Seek Track
WD177XREADS	= $80		; Read Sector
WD177XREADI	= $C8		; Read Adress 
WD177XREADT	= $E0		; Read Track
WD177XWRITET	= $F0		; Write Track
WD177XWRITES	= $A0		; Write Sector

; DS1685 register addresses
DS1685_SECONDS	= $00
DS1685_MINUTES	= $02
DS1685_HOURS	= $04
DS1685_DAY	= $06
DS1685_DATE	= $07
DS1685_MONTH	= $08
DS1685_YEAR	= $09
DS1685_REG_A	= $0A
DS1685_REG_B	= $0B
DS1685_REG_C	= $0C
DS1685_REG_D	= $0D
DS1685_CENTURY	= $48

; OS function numbers
OS_Print_CHR_FN		= 0	; Write byte to default output
OS_Get_CHR_FN		= 1	; Read byte from default input
OS_Print_String_FN	= 2	; Print a zero-terminated string to stdout	
OS_Bin2BCD32_FN		= 3	; 32-Bit Binary to BCD, input: Operand 1,2,3,4 output: Operand 5,6,7,8,9
OS_Print_Bin2Hex_FN 	= 4	; Convert accumulator content to HEX Value and print it to STDIO.
OS_Hex2byte_FN		= 5	; Converts two ASCII-HEX characters into binary byte
OS_ChangeSTDOUT_FN	= 6	
OS_ChangeSTDIN_FN	= 7	
OS_XModem_FN		= 8	
OS_LineEditor_FN	= 9	; Get one line from std input
OS_Print_BCD_FN		= 10 	; Print BCD, input = Operand 5,6,7,8,9
OS_Multiply16_FN	= 11
OS_Divide16_FN		= 12
OS_Bell_FN		= 13
OS_RTC_Print_Date_FN	= 14
OS_RTC_Print_Time_FN	= 15
OS_STDIN_ASCII_TO_BCD_FN= 16
OS_Print_Bin2BCD_2D_FN	= 17	; 2 digits
OS_ASCII_to_BCD_FN	= 18
OS_Print_BIN2DIG_FN	= 19

; FDC Function numbers
FDC_Reset_FN		= 0
FDC_Restore_FN		= 1
FDC_Addr_FN		= 2
FDC_RTrack_FN		= 3
FDC_RSect_FN		= 4
FDC_WaitBusy_FN		= 5
FDC_WTrack_FN		= 6
FDC_WSect_FN		= 7
FDC_Status_FN		= 8

; VDP function numbers (X reg. parameter)
VDP_Set_Video_Mode_FN		= 0
VDP_Init_Pattern_FN		= 1
VDP_print_CHR_FN		= 2
VDP_Cursor_On_FN		= 3
VDP_Cursor_Off_FN		= 4
VDP_Clear_Text_Screen_FN	= 5
VDP_Detect_CTRL_Type_FN		= 6
VDP_Wait_V_SYNC_FN		= 7

;VDP screen numbers (MSX screen numbering scheme, A reg. parameter)
TMS9918_Set_Screen0		= 0	; 40 x 25 patterns
TMS9918_Set_Screen1		= 1	; 32 x 24 patterns, 256 types, 256x192 pixel
TMS9918_Set_Screen2		= 2	; 32 x 24 patterns, 768 types, 256x192 pixel
TMS9918_Set_Screen3		= 3	; 64 x 48 pixel 16 color 
V9938_Set_Screen5		= 5	; 256 x 212 pixels 16 colors out of 512
V9938_Set_Screen6		= 6	; 512 x 212 pixels 4 colors out of 512
V9938_Set_Screen7		= 7	; 512 x 212 pixels 16 colors out of 512
V9938_Set_Screen8		= 8	; 256 x 212 pixels 256 colors
V9958_Set_Screen12		= 12	; 256 x 212 pixels 19268 YJK color
V9938_Set_Screen0_W80		= 13	; 80 x 26 patterns

; DOS function numbers
DOS_Open_File_FN	= 0
DOS_Load_Byte_FN	= 1
DOS_Close_File_FN	= 2
DOS_Search_FN		= 3
DOS_Dir_FN		= 4
DOS_Init_FN		= 5
DOS_LEditor_FN		= 6

; PS2 function numbers
PS2_GetKey_FN		= 0
PS2_ChkKey_FN 		= 1	; carry flag set if key was pressed
PS2_DummyRead_FN 	= 2

; ACIA function numbers
UART_Init_FN		= 0
UART_CHAR_Transmit_FN	= 1
UART_CHAR_Receive_FN	= 2

; ASCII control character

NUL		= $00 		; ASCII null character
SOH		= $01		; ASCII start of heading
ETX		= $03		; ASCII end of text or CTRL-C on keyboard
EOT		= $04		; ASCII end of transmission
ACK		= $06		; ASCII acknowledgement
BEL		= $07		; ASCII bell
BS		= $08		; ASCII backspace
HT		= $09		; ASCII horizontal tab
LF		= $0A		; ASCII line feed
VT		= $0B		; ASCII vertical tab
FF		= $0C		; ASCII form feed	
CR		= $0D		; ASCII carriage return
NAK		= $15		; ASCII negative acknowledgement
CAN 		= $18		; ASCII cancel
SP		= $20		; ASCII space
ESC		= $1B		; ASCII escape

;Device numbers for STDIO

STDIO_KEYB	= 0	; 0 = Keyboard
STDIO_UART	= 2	; 2 = UART 
STDIO_DISP	= 3	; 3 = Display

;******************************
;Zero page 
;******************************
!addr{
STDOUT		= $1	; Standard output device 
STDIN		= $2	; Standard input device 
CPU_Type	= $3	; Detected CPU Type. 0=6502NMOS, 1=65C02, 2=65C816

VDP_CURSOR_H	= $4	; Current cursor position horizontal
VDP_CURSOR_V	= $5	; Current cursor position vertical

VDP_Text_MaxH	= $6	; Column number text mode
VDP_Text_MaxV	= $7	; Line count text mode
VDP_Cursor_Back	= $8	; character at cursor position / Zeichen unter dem Cursor 


TMS9918_TEMP1	= $9	; temporary 1 register for VDP calculation
TMS9918_TEMP2	= $10	; temporary 2 register for VDP calculation

	
; Start WOZMON variables
;Mon_XAML --> PTR_String_L
;Mon_XAMH --> PTR_String_L
;Mon_STL --> PTR_W_L
;Mon_STH --> PTR_W_H
Mon_L		= $1A	;  WOZMON hex value parsing low	
Mon_H		= $1B	;  WOZMON Hex value parsing high	
Mon_YSAV	= $1C	;  WOZMON Used to see if hex value is given
Mon_MODE	= $1D	;  WOZMON $00=XAM, $7F=STOR, $AE=BLOCK XAM
; End WOZMON variables

Operand1	= $1E	; Operand 1 (need to exchange data with OS functions calls)
Operand2	= $1F	; Operand 2
Operand3	= $20	; Operand 3
Operand4	= $21	; Operand 4
Operand5	= $22	; Operand 5 to 9 used for BCD output
Operand6	= $23	; Operand 6
Operand7	= $24	; Operand 7
Operand8	= $25	; Operand 8
Operand9	= $26	; Operand 9

; For caclulationg floppy logical block addressing to drive parameters
Floppy_Track		= $27	; Floppy Track (0..79)
Floppy_Head		= $28	; Floppy head (0..1)
Floppy_Sector		= $29	; Floppy Sector (1...9 for 720kb)
Floppy_Blk_Low		= $2A	; LBA to track, head, sector. block nr. low byte
Floppy_Blk_High		= $2B	; LBA to track, head, sector. block nr. high byte
Floppy_DriveNo		= $2C	; Current floppy drive number 0 = Drive A, 1 = Drive B.

PTR_String_L		= $2D	; String pointer low byte
PTR_String_H		= $2E	; String pointer high byte

PTR_TMS9918W_L		= $2F	; Pointer for write operation video memory low
PTR_TMS9918W_H		= $30	; Pointer for write operation video memory high

PTR_TMS9918R_L		= $31	; Pointer for read operation video memory low
PTR_TMS9918R_H		= $32	; Pointer for read operation video memory high

PTR_S_L			= $33	; Programm start pointer low (run address for loaded programm)
PTR_S_H			= $34	; Programm start pointer high 

PTR_R_L			= $35	; Read pointer low 
PTR_R_H			= $36	; Read pointer high

PTR_W_L			= $37	; Write pointer low
PTR_W_H			= $38	; Write pointer high

PTR_J_L			= $39	; Pointer jump low, used for function call jump
PTR_J_H			= $3A	; Pointer jump high, used for function call jump

PTR_FLOPPY_L		= $3B	; Floppy sector data pointer low
PTR_FLOPPY_H		= $3C	; Floppy sector data pointer high

PTR_DOS_DPB_L		= $3D	; Pointer to current DPB
PTR_DOS_DPB_H		= $3E

PTR_DOS_FCB_L		= $3F	; Pointer to current FCB
PTR_DOS_FCB_H		= $40

; Stack $0100 to $01FF

InBuffer		= $0200	;  Input buffer for line editor $0200 to $027F = 127 Bytes

; Vectors $200-$297
IRQVector_L		= $0280	; Vector: IRQ
IRQVector_H		= $0281	; Vector: IRQ
BRKVector_L		= $0282	; Vector: BRK
BRKVector_H		= $0283	; Vector: BRK
NMIVector_L		= $0284	; Vector: NMI
NMIVector_H		= $0285	; Vector: NMI
ABORT_E_Vector_L	= $0286	; Vector: ABORT in 816 emulation mode
ABORT_E_Vector_H	= $0287	; Vector: ABORT in 816 emulation mode
COP_E_Vector_L		= $0288	; Vector: COP in 816 emulation mode
COP_E_Vector_H		= $0289	; Vector: COP in 816 emulation mode
BRKService816Vector_L 	= $028A ; Vector: BRK in 816 emulation mode
BRKService816Vector_H	= $028B	; Vector: BRK in 816 emulation mode

ACIA_Function_Vector_L	= $028C	
ACIA_Function_Vector_H	= $028D	
FDC_Function_Vector_L	= $028E
FDC_Function_Vector_H	= $028F	
VDP_Function_Vector_L	= $0290	
VDP_Function_Vector_H	= $0291	
PS2_Function_Vector_L	= $0292		
PS2_Function_Vector_H	= $0293	
OS_Function_Vector_L	= $0294
OS_Function_Vector_H	= $0295
DOS_Function_Vector_L	= $0296
DOS_Function_Vector_H	= $0297


MEMTOP_L		= $02B0 ; Pointer to end of RAM after memory test low address
MEMTOP_H		= $02B1 ; Pointer to end of RAM after memory test high address
OS_Version_L		= $02B2	; Operating system version number minor
OS_Version_H		= $02B3	; Operating system version number major
VDP_Type		= $02B4 ; 0 = TMS99X8 4 = V9958
VDP_Video_Mode		= $02B5 ; TMS 99XX / V9938 / V9958 screen mode (MSX numbering scheme)
VDP_Pattern_L		= $02B6 ; Pattern generator base low address
VDP_Pattern_H		= $02B7 ; Pattern generator base high address
VDP_Layout_L		= $02B8	; Pattern layout table base low address
VDP_Layout_H		= $02B9	; Pattern layout table base high address

; IRQ / BRK routine save registers ($02BA ... $02C8)

PRegister		= $02BA	; Processor Status Register “P”
DRegister		= $02BB ; Direct register low (65C816 only)
DRegister_H		= $02BC ; Direct register high (65C816 only) 
PBRegister		= $02BD ; Program bank register (65C816 only)
DBRegister		= $02BE ; Data bank register high (65C816 only)
PCLRegister		= $02BF	; Program counter PC
PCHRegister		= $02C0	; Program counter PC
SPRegister		= $02C1	; Stack pointer register
SPRegister_H		= $02C2	; Stack pointer register high (65C816 only)
YRegister		= $02C3	; Y register
YRegister_H		= $02C4	; Y register (65C816 only)
XRegister		= $02C5	; X register
XRegister_H		= $02C6	; X register (65C816 only)
ARegister		= $02C7	; Accu
BRegister		= $02C8	; B-accu (65C816 only)

; DOS variables
DOS			= $2C9
DOS_CurrentDirEntry	= DOS + $00	; Actual entry number of directory, 2 Byte
DOS_CurrentAttr		= DOS + $02	; 1 Byte, $002CB, current directory attribute
DOS_CTRL_Error_Code	= DOS + $03	; Floppy controller error code; 0 = no error, $002CC
DOS_FATSectorBuffLBA_L	= DOS + $04	; Status of stored FAT sector LBA (2 Byte), $002CD  
DOS_FATSectorBuffLBA_H	= DOS + $05	; Current sector in sector buffer, $002CE 
DOS_FATSectorBuffDrive	= DOS + $06	; Current drive numer in sector buffer and buffer status, $FF = invalid, Adr.: $002CF
DOS_TotalFreeClusters_L	= DOS + $07	; $02D0 --> e.g. to calculate the free space on the data medium 
DOS_TotalFreeClusters_H	= DOS + $08	; $02D1
DOS_CurrentCluster_L	= DOS + $09	; $02D2
DOS_CurrentCluster_H	= DOS + $0A	; $02D3
DOS_DataSectorBuffLBA_L	= DOS + $0B	; Status of stored DATA sector LBA (2 Byte), $02D4
DOS_DataSectorBuffLBA_H	= DOS + $0C	; Current sector in sector buffer, $02D5
DOS_DataSectorBuffDrive	= DOS + $0D	; Current drive numer in sector buffer and buffer status, $FF = invalid, Adr.: $02D6
DOS_DirFilesSum		= DOS + $0E 	; Number of files in directory
DOS_ErrorCode		= DOS + $0F	; Error code number

; TODO: more DOS variables:
; DOS_CurrentDrive, DOS_CommandSwitches, DOS_BootDrive, DOS_DirDisplayedEntrys
;

	

; FAT disk parameter block in RAM for floppy drive. Drive 1. The usually values are for 720kByte floppy 
DPB_1			=  $300	; TODO: Logical drive #: A=0,B=1,...
DPB_BytesPerSector	=  $00 	; Bytes per logical sector in powers of two; the most common value is 512. Length = 2 Byte. Usually 512byte = $0200
DPB_SectorsPerCluster	=  $02	; Logical sectors per cluster. Allowed values are 1, 2, 4, 8, 16, 32, 64, and 128. Length = 1 Byte. Usually = 2
DPB_ReservedSectors	=  $03	; The number of logical sectors before the first FAT in the file system image. Length = 2 Byte. Usually = 1
DPB_FatCopies		=  $05	; Number of File Allocation Tables. Almost always 2. Length = 1 Byte. Usually = 2
DPB_RootDirEntries	=  $06	; Maximum number of FAT12 or FAT16 root directory entries (32 Bytes), 0 for FAT32. 
				; Length = 2 Byte. Usually = $70 (7 Sectors)
DPB_NumSectors		=  $08	; Total logical sectors. Length = 2 Byte. Usually = $05A0 (80 Tracks, 9 sectors per track, 2 sides)
DPB_MediaType		=  $0A	; Media, length = 1 Byte. Usually F9 for 3.5-inch Double sided (720 KB). 
DPB_SectorsPerFAT	=  $0B	; Logical sectors per file allocation table for FAT12/FAT16. Length = 2 Byte. Usually = $0003
DPB_SectorsPerTrack	=  $0D	; Physical sectors per track for disks with CHS (cylinder, head, sector) geometry. Length = 2 Byte. Usually = $0009
DPB_NumberOfHeads	=  $0F	; Number of heads for disks with CHS geometry, 2 for a double sided floppy. Length = 2 Byte. Usually = $0002
DPB_FirstRootDirSecNum	=  $11 	; First sector of the root directory, Lenth = 2 Byte. Calculated, usually = 7
DPB_FirstDataSector	=  $13	; FirstDataSector = BPB_ResvdSecCnt + (BPB_NumFATs * FATSz) + RootDirSectors; Length = 2 Byte. Usually $000E
DPB_TotalDataSectors	=  $15	; 2 Bytes = DPB_NumSectors - DPB_FirstDataSector
DPB_CountofClusters	=  $17	; 2 Bytes = DPB_TotalDataSectors / DPB_SectorsPerCluster (it is also the maximum count of clusters)
				; reserve ... $30
DPB_SIZE			=  $30
; Size = $17
; TODO: DPB directory string ; DPB cluster number of start of current directory from sub-directorys

DPB_2			= DPB_1 + DPB_SIZE	; $330
DPB_3			= DPB_2 + DPB_SIZE	; $360 Drive 3
DPB_4			= DPB_3 + DPB_SIZE	; $390 Drive 4 to $3C0   

;  1680  4da0 0003               	!word 	DPB_1
;  1681  4da2 3003               	!word 	DPB_2
;  1682  4da4 6003               	!word 	DPB_3
;  1683  4da6 9003               	!word 	DPB_4

; FAT file control block
FCB_1			= $3C0	; start
; Offsets from FCB:
FCB_Drive_Number	= $00	; Drive number: 0 current drive, 1 = A:, 2 = B: ...; current drive is converted to real drive (01H to 04H)
FCB_File_Name		= $01	; File name and extension — together these form a 8.3 file name, 11 bytes
FCB_FstClusHI		= $0C	; High word of this entry’s first cluster number (always 0 for a FAT12 or FAT16 volume), 2 Byte
FCB_WrtTime		= $0E	; Time of last write, 2 Byte
FCB_WrtDate		= $10	; Date of last write, 2 Byte
FCB_FstClusLO		= $12	; Low word of this entry’s first cluster number, 2 Byte
FCB_FileSize		= $14	; Holding this file size in bytes, 4 Byte
FCB_Current_Cluster	= $18	; Current cluster number; 2 Byte
FCB_Sector_Count	= $1A	; Number of sectors already read from cluster, 1 Byte
FCB_FirstSecOfCluster= $1B	; FirstSectorofCluster = ((N – 2) * BPB_SecPerClus) + FirstDataSector; 2 Byte
FCB_FATOffset		= $1D	; Offset of FAT entry, FCB_FATOffset(Operand1,2) = FCB_Current_Cluster (2Byte) + (FCB_Current_Cluster / 2), 2 Byte
FCB_ThisFATSecNum	= $1F	; The sector number of the FAT sector that contains the entry for cluster N in the first FAT, 2 Byte ThisFATSecNum = BPB_ResvdSecCnt + (FATOffset / BPB_BytsPerSec)
FCB_ThisFATEntOffset	= $21	; Offset in FAT to this entry, FCB_ThisFATEntOffset = (Remainder)(FCB_FATOffset (2Byte) / DPB_BytesPerSector (2Byte)) 2 Byte
FCB_CurrentByteNum	= $23	; Current byte position in file. Count up from 0 to FileSize, 4 Byte
				; Reserve $27 ... $30 : TODO current LBA
FCB_Size		= $30	; TODO: 32 Byte for CP/M compatibility ?
				; TODO: 64 Byte for path ?

FCB_2			= FCB_1 + FCB_Size ; $03F0
FCB_3			= FCB_2 + FCB_Size ; $0420
FCB_4			= FCB_3 + FCB_Size ; $0450

;  1690  4da8 c003      !word 	FCB_1
;  1691  4daa f003      !word 	FCB_2
;  1692  4dac 2004      !word 	FCB_3
;  1693  4dae 5004      !word 	FCB_4

; Buffers
DOS_DataSectorBuff	= $B00		; Data sector buffer; normal 512 byte --> end $CFF
Rbuff			= $B00      	; Temp 132 byte receive buffer (for XMODEM, also used for SectorBuffer)
DOS_FATSectorBuff 	= $D00		; Sector buffer for last FAT read sector; normaly 512 byte --> end $EFF
CRCLO			= $D00		; Temp 256 byte CRC lookup table (for XMODEM, also used with FatSectorBuffer)
CRCHI			= $E00		; Temp 256 byte CRC lookup table (for XMODEM, also used with FatSectorBuffer)
}
; END : $F00

