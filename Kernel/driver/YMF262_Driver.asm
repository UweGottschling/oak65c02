; Compiler : ACME 
; UTF-8 character encoding
; OPL3 (Yamaha YMF262) Sound Driver
; Bell Sound 
; (C) 2019 Uwe Gottschling
;!src "Oak65c02_GIT/Kernel/include/label_definition.asm"
;!src "Oak65c02_GIT/Kernel/include/Jumptable.asm"
;!src "Oak65c02_GIT/Kernel/include/DOS_FAT_Constant.asm"
!cpu 65c02


;*=$0FFE
;!word $1000

YMF262_Main
		jsr YMF262_Start_Message
		ldx #OS_Get_CHR_FN
		jsr OS_Function_Call
		pha
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call	
		jsr OS_Internal_Print_CHR	; Echo character
		pla
		cmp #"x"
		beq YMF262_Back_Exit
		ldx #End_YMF262_Commands-YMF262_Commands
YMF262_Main1
		dex
		bmi YMF262_Main2
		cmp YMF262_Commands,x
		bne YMF262_Main1
		txa
		asl
		tax
		lda YMF262_Command_LUT,x
		sta PTR_J_L
		lda YMF262_Command_LUT+1,x
		sta PTR_J_H
		jsr YMF262_Main_Call
		jmp YMF262_Main
YMF262_Main2
		jsr UnknownCommand
		jmp YMF262_Main
YMF262_Back_Exit
		rts
YMF262_Main_Call
 		jmp (PTR_J_L)
	
YMF262_Commands
!text "b"
!text "r"
!text "x"
!text CR
!text LF
End_YMF262_Commands

YMF262_Command_LUT
!word YMF262_Bell		
!word YMF262_Reset		
!word YMF262_Exit
!word YMF262_Main				;CR
!word YMF262_Main				;LF

YMF262_Exit
		rts


YMF262_Start_Message_Text
!text LF
!text "YMF262 Bell Sound Test. Press Key:",LF
!text "b = Bell",LF
!text "r = Reset OPL3",LF
!text "x = Exit",LF
!text 00


;***************************
; Reset YMF262
; input: -
; output: -
; used: A,Y 
;***************************

;YMF262_Reset				; TODO: dont't work always!
;		lda #$05
;		sta YMF262_ADDR2
;		lda #$00		; set to OPL2 Mode
;		sta YMF262_DATA2
;		lda #$0
;		ldy #$0
;YMF262_Reset1
;		sty YMF262_ADDR1
;		sta YMF262_DATA1
;		iny
;		bne YMF262_Reset1
;		rts
;	; Register, Data
;YMF262__Reset				; TODO : remove this part
;		!by $04,$00		; timer Control Byte
;		!by $08,$00		; CSM Mode / keyboard Split.
;YMF262__Reset_End


YMF262_Reset
		lda #$05
		sta YMF262_ADDR2
		lda #$01			
		sta YMF262_DATA2	; OPL3 mode
		ldy #$B0		; key off 	
YMF262_Reset1		
		sty YMF262_ADDR1
		stz YMF262_DATA1
		sty YMF262_ADDR2
		stz YMF262_DATA2			
		iny
		cpy #$B9
		bne YMF262_Reset1
		ldy #$01
YMF262_Reset6
;		jsr debug
		sty YMF262_ADDR1
		stz YMF262_DATA1
	
		sty YMF262_ADDR2
		stz YMF262_DATA2	
		iny
		cpy #$F6
		bne YMF262_Reset6

		lda #$05
		sta YMF262_ADDR2
		stz YMF262_DATA2	; set to OPL2 Mode
;		jsr VGM_Wait_62
		rts



;***************************
; Bell Sound
; input: -
; output: -
; used: A,X,Y 
;***************************

YMF262_Bell
		ldy #0
YMF262_Bell1
		lda YMF262_Instrument_Bell,y
		sta YMF262_ADDR1
		iny
		lda YMF262_Instrument_Bell,y
		sta YMF262_DATA1
		iny
		cpy #YMF262_Instrument_Bell_End-YMF262_Instrument_Bell
		bne YMF262_Bell1
		rts
YMF262_Start_Message
		lda #<YMF262_Start_Message_Text	; < low byte of expression
		sta PTR_String_L
		lda #>YMF262_Start_Message_Text	; > high byte of expression
		sta PTR_String_H
;		ldx #OS_Print_String_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_String
		rts

YMF262_Instrument_Bell
		; Register, Data
		; Operator 1
		!by $b0,$00 		; turn the voice off; set the octave and freq MSB
		!by $20,$B5		; set the modulator's multiple 5x, Tremolo, Vibrator
		!by $40,$40+$0B		; set the modulator's Key-scaling and Output-level: 0 = loudest
		!by $60,$EA		; modulator attack: F is the fastest; decay :F is the fastest
		!by $80,$22		; modulator sustain: 0 is the loudest; release: F is the fastest.
		!by $e0,$00		; set Modulator wave form
		; Operator 2
		!by $23,$01		; set the carrier's multiple 1x
		!by $43,$00		; set the carrier Key Scaling and volume; 0 = loudest
		!by $63,$E9		; carrier attack: F is the fastest ; decay: F is the fastest
		!by $83,$14		; carrier sustain: 0 is the loudest; release: F is the fastest.
		!by $e3,$00		; set Carrier wave form
		; Channel 1
		!by $a0,$98		; set voice frequency's LSB
		!by $c0,$0A		; set Feedback, Decay Alg
		; Global Settings
		!by $03,$fe		; timer 2 Data
		!by $b0,$31		; turn the voice on; set the octave and freq MSB
		!by $BD,$B0		; amplitude Modulation Depth / Vibrato Depth / Rhythm
YMF262_Instrument_Bell_End



;UnknownCommand				; print unknown key 
;		phx
;		pha
;		lda #<UnknownCommand_text
;		sta PTR_String_L
;		lda #>UnknownCommand_text
;		sta PTR_String_H
;		ldx #OS_Print_String_FN
;		jsr OS_Function_Call
;		pla
;		plx
;		rts

;UnknownCommand_text
;!text "Unknown key",CR,00



