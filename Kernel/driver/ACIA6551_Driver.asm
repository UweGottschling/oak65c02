; Compiler : ACME 
; UTF-8 character encoding
; TODO: numbers more identical to keyboard read 
; TODO: check receive buffer

!cpu 65c02

ACIA_Function_Call
		jmp (ACIA_Function_Vector_L)

ACIA_Function_Call1
		pha
		txa
		asl
		tax
		cpx #ACIA_Function_Call_LUT_End-ACIA_Function_Call_LUT
		bcs ACIA_Function_Call_Error_Exit
		lda ACIA_Function_Call_LUT,x	
		sta PTR_J_L
		lda ACIA_Function_Call_LUT+1,x
		sta PTR_J_H
		pla
		jsr ACIA_Function_Call_2
		rts

ACIA_Function_Call_2

		jmp (PTR_J_L)

ACIA_Function_Call_Error_Exit
		pla
		sec
		rts

ACIA_Function_Call_LUT

!word ACIA_Init			;0
!word ACIA_CHAR_Transmit	;1
!word ACIA_CHAR_Receive		;2

ACIA_Function_Call_LUT_End


;***************************
; UART Routinen Receive Char
; input: -
; output: A
;***************************
ACIA_CHAR_Receive
		lda UARTSTATUS		; wait for new character in latch and store it in accumulator
		and #$08
		beq ACIA_CHAR_Receive
		lda UARTDATA
		rts

;***************************
; UART Routinen Transmit Char
; input: A
; output: 
;***************************
ACIA_CHAR_Transmit
		pha
ACIA_CHAR_Wait_Transmit
		lda UARTSTATUS		; wait for send buffer empty
		and #$10
		beq ACIA_CHAR_Wait_Transmit
		pla
		sta UARTDATA
		rts
;W65C51 LOT: A6A749.1 , 1016G015 has a bug - Xmit bit in status register is stuck on

;***************************
; UART Routinen Init
; input: 
; output:
;***************************
ACIA_Init			; initialises UART 6551
		lda #$1F	; 19200 baud, baud generator intern, 1 stop, 8 data bits
		sta UARTCTRL
		lda #$0b	; no parity, no echo, IRQ disable, High -RTS, Interrupt disable, -DTR to high disable, Trans+Rel
		sta UARTCMD
		rts

