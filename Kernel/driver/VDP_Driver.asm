; (C) 2018 Uwe Gottschling
; Driver for Video Display Processor (VDP) TMS 99x8, V9938 and V9958
; TODO: prevent (SEI) or enable (CLI) while access VDP Registers
; TODO: Function Call "WAIT VSYNC"
; TODO: ASCII CR ($0D) cursor left, LF ($0A) new line + cursor left, VT ($0B) cursor one line down, BEL ($07) --> more linux like.
; --> see also CONSOLE_CODES(4) linux man page
; TODO: Textmode2 scroll colortable, set Interval register 14 to $f0, set register #13 with differnt color
; TODO: Graphic mode 1 only scrolling too fast for TMS9918 but V9958 work! DONE: 02.10.2021
; TODO: Graphic mode 1 with 16kRAM flag for TMS9918! DONE: 02.10.2021
; TODO: Remove CLI and SEI; not optimal after all 
; TODO: activate /WAIT output 

;***************************
; VDP Function Call
; input: A = parameter, X- function number
; output: -
; used: A,X,Y
;***************************

VDP_Function_Call
		jmp (VDP_Function_Vector_L)
VDP_Function_Call1
		pha
		txa
		asl
		tax
		cpx #VDP_Function_Call_LUT_End-VDP_Function_Call_LUT
		bcs VDP_Function_Call_Error_Exit
		lda VDP_Function_Call_LUT,x
		sta PTR_J_L
		lda VDP_Function_Call_LUT+1,x
		sta PTR_J_H
		pla
		jsr VDP_Function_Call_2
		rts
VDP_Function_Call_2
		jmp (PTR_J_L)
VDP_Function_Call_Error_Exit
		pla
		rts

VDP_Function_Call_LUT

!word VDP_Set_Video_Mode	;0
!word VDP_Init_Pattern		;1
!word VDP_print_CHR		;2
!word VDP_Cursor_On		;3
!word VDP_Cursor_Off		;4
!word VDP_Clear_Text_Screen	;5
!word VDP_Detect_CTRL_Type	;6
!word VDP_Wait_V_Sync		;7
VDP_Function_Call_LUT_End

;***************************
; VDP_Set_Video_Mode
; A = VDP Screen Mode
; input: A
; output: -
; used: A,X,Y
;***************************

VDP_Set_Video_Mode
		ldx #End_VDP_Diplay_Mode_Number-VDP_Diplay_Mode_Number
VDP_Set_Video_Mode1
		dex
		bmi VDP_Set_Video_Mode2
		cmp VDP_Diplay_Mode_Number,x
		bne VDP_Set_Video_Mode1
		sta VDP_Video_Mode	; if valid screen mode then store
		txa
		asl
		tax
		lda VDP_Diplay_Mode_Loc,x
		sta PTR_R_L
		lda VDP_Diplay_Mode_Loc+1,x
		sta PTR_R_H
		jsr VDP_Init_Video_Mode
		clc
		rts

VDP_Set_Video_Mode2
		sec
		rts

VDP_Diplay_Mode_Number			; (MSX Screen Numbering Scheme)
!by TMS9918_Set_Screen0			; 0 40 x 25 patterns
!by TMS9918_Set_Screen1			; 1 32 x 24 patterns, 256 types, 256x192 pixel
!by TMS9918_Set_Screen2			; 2 32 x 24 patterns, 768 types, 256x192 pixel
!by TMS9918_Set_Screen3			; 3 64 x 48 pixel 16 color 
!by V9938_Set_Screen5			; 5 256 x 212 pixels 16 colors out of 512
!by V9938_Set_Screen6			; 6 512 x 212 pixels 4 colors out of 512
!by V9938_Set_Screen7			; 7 512 x 212 pixels 16 colors out of 512
!by V9938_Set_Screen8			; 8 256 x 212 pixels 256 colors
!by V9958_Set_Screen12			; 12 256 x 212 pixels 19268 YJK colors 
!by V9938_Set_Screen0_W80		; 13 80 x 26.5 patterns
End_VDP_Diplay_Mode_Number

VDP_Diplay_Mode_Loc			; VDP data sheet names
!word TMS9918_Reginit_Textmode1		; 0 40 x 25 patterns
!word TMS9918_Reginit_Graphic1		; 1 32 x 24 patterns, 256 types, 256x192 pixel
!word TMS9918_Reginit_Graphic2		; 2 32 x 24 patterns, 768 types, 256x192 pixel
!word TMS9918_Reginit_MultiColor	; 3 64 x 48 pixel 16 color 
!word V9938_Reginit_Graphic4		; 5 256 x 212 pixels 16 colors out of 512
!word V9938_Reginit_Graphic5		; 6 512 x 212 pixels 4 colors out of 512
!word V9938_Reginit_Graphic6		; 7 512 x 212 pixels 16 colors out of 512
!word V9938_Reginit_Graphic7		; 8 256 x 212 pixels 256 colors
!word V9958_Reginit_Graphic7YJK		; 12 256 x 212 pixels 19268 YJK colors 	
!word V9938_Reginit_Textmode2		; 13 80 x 26.5 patterns

;***************************
; Init VDP Video Mode
; input: PTR_R --> Config table
; output: -
; used: A,Y
;***************************
VDP_Init_Video_Mode
		jsr V9958_Reset_Reg	
		lda (PTR_R_L)		; get number of parameter to copy
		tay			; and set it to Y
		dey
		lda (PTR_R_L),y
		sta VDP_Text_MaxH
		dey
		lda (PTR_R_L),y
		sta VDP_Text_MaxV
		dey
		lda (PTR_R_L),y
		sta VDP_Layout_L
		dey
		lda (PTR_R_L),y
		sta VDP_Layout_H
		dey
		lda (PTR_R_L),y
		sta VDP_Pattern_L
		dey
		lda (PTR_R_L),y
		sta VDP_Pattern_H
		dey
		sei
VDP_Init_Video_Mode1
		lda (PTR_R_L),y
		sta TMS9918REG
		dey
		bne VDP_Init_Video_Mode1
VDP_Init_Video_Mode2			; check for cursor pos. fit to graphic mode
		lda VDP_CURSOR_H
		cmp VDP_Text_MaxH
		bcc VDP_Init_Video_Mode3
		lda VDP_Text_MaxH
		dec
		sta VDP_CURSOR_H
VDP_Init_Video_Mode3
		lda VDP_CURSOR_V
		cmp VDP_Text_MaxV
		bcc VDP_Init_Video_Mode_exit
		lda VDP_Text_MaxV
		dec
		sta VDP_CURSOR_V
;VDP_Init_Video_Mode4
;		lda VDP_Video_Mode
;		cmp #13			; special function for clear line 26.5 in text mode 2 
;		bne VDP_Init_Video_Mode_exit
;		jsr VDP_Clear_Line_26_5

VDP_Init_Video_Mode_exit
		cli
		rts

TMS9918_Reginit_Textmode1	; 40 x 24 characters, MSX screen 0 width = 40	
!by End_TMS9918_Reginit_Textmode1-TMS9918_Reginit_Textmode1
!by $80+0,$00 	; Register 0
!by $80+1,$d0 	; Register 1 16K-ram - blank off - M1 = 1 (Textmode) 40x24
!by $80+2,$00 	; Register 2 Name table base  --> value*$400 --> $000 ... $3BF
!by $80+3,$80 	; Register 3 Color table base  -->  value*$40 --> unused
!by $80+4,$01 	; Register 4 Pattern generator base --> value*$800 --> $800 ... $FFF
!by $80+5,$36 	; Register 5 Sprite Attribute table base address --> value*$80 -->  unused
!by $80+6,$07 	; Register 6 Sprite pattern generator base address --> value*$800 -->  unused
!by $80+7,$f4 	; Register 7 Colour font = yellow and background = dark blue
!by $08,$00	; pattern generator base address (H,L)
!by $00,$00	; name table base address (H,L)
!by 24 		; VDP_Text_MaxV
!by 40 		; VDP_Text_MaxH
End_TMS9918_Reginit_Textmode1

TMS9918_Reginit_Graphic1	; 32 x 24 characters/tiles 256x192 pixel, MSX screen 1
!by End_TMS9918_Reginit_Graphic1-TMS9918_Reginit_Graphic1
!by $80+0,$00 	; Register 0 M4 = 0
!by $80+1,$C2 	; Register 1 16K-Ram - blank off - M1,M2,M3 = 0 (Graphic 1 Mode), 16x16 pxl. Sprites 
!by $80+2,$06 	; Register 2 Name table base --> value*$400 --> $1800 ... $1AFF
!by $80+3,$80 	; Register 3 Color table base --> value*$40 --> $2000 ... $201F
!by $80+4,$00 	; Register 4 Pattern generator base --> value*$800 --> $0000 ... $17FF
!by $80+5,$36 	; Register 5 Sprite attribute table base address --> value*$80--> $1B00 ... $1B7F
!by $80+6,$07 	; Register 6 Sprite pattern generator base address --> value*$800 --> $3800 ... $3FFF
!by $80+7,$00 	; Register 7 Background color
!by $00,$00	; pattern generator base address (H,L)
!by $18,$00	; name table base address (H,L)
!by 24 		; VDP_Text_MaxV
!by 32 		; VDP_Text_MaxH
End_TMS9918_Reginit_Graphic1

TMS9918_Reginit_Graphic2	; 32 x 24 characters/tiles 256x192 pixel, MSX screen 2. Differenz to MSX screen 4 = sprite mode
!by End_TMS9918_Reginit_Graphic2-TMS9918_Reginit_Graphic2
!by $80+0,$02 	; Register 0 M3=1
!by $80+1,$C2 	; Register 1 16K-Ram - blank off - M1,M2,M3 = 0 (graphic 2 mode), 16x16 pxl. sprites
!by $80+2,$06 	; Register 2 Name table base --> value*$400 --> value*$400 --> $1800 ... $1AFF
!by $80+3,$FF 	; Register 3 Color table base --> value*$40, bit 0 ... 6 must be 1 --> $2000 ... $37FF
!by $80+4,$03 	; Register 4 Pattern generator base --> value*$800, bit 0 ... 1 must be 1 --> $0000 ... $17FF
!by $80+5,$36 	; Register 5 Sprite attribute table base address --> value*$80 --> $1B00 ... $1B7F
!by $80+6,$07 	; Register 6 Sprite pattern generator base address --> value*$800 --> $3800 ... $3FFF
!by $80+7,$00 	; Register 7 Background color 
!by $00,$00	; pattern generator base address (H,L)
!by $18,$00	; name table base address (H,L)
!by 24 		; VDP_Text_MaxV
!by 32 		; VDP_Text_MaxH
End_TMS9918_Reginit_Graphic2

TMS9918_Reginit_MultiColor	; 64 x 48 pixel 16 colors, MSX screen 3. 
!by End_TMS9918_Reginit_MultiColor-TMS9918_Reginit_MultiColor
!by $80+0,$00 	; Register 0 MSX screen 3; 
!by $80+1,$CA 	; Register 1 16K-Ram - blank off - M1,M2,M3 = 0, 16x16 pxl. sprites
!by $80+2,$02 	; Register 2 Name table base --> value*$400 --> $800 ... $AFF
!by $80+3,$00 	; Register 3 Color table base --> value*$40 --> unused
!by $80+4,$00 	; Register 4 Pattern generator base --> value*$800 --> $000 ... $5FF 
!by $80+5,$36 	; Register 5 Sprite attribute table base address --> value*$80--> $1B00 ... $1B7F
!by $80+6,$07 	; Register 6 Sprite pattern generator base address --> value*$800--> $3800 ... $3FFF
!by $80+7,$00 	; Register 7 Background color 
!by $00,$00	; pattern generator base address (H,L)
!by $08,$00	; name table base address (H,L)
!by 24 		; VDP_Text_MaxV
!by 32 		; VDP_Text_MaxH
End_TMS9918_Reginit_MultiColor

V9938_Reginit_Graphic4		; 256 x 212 pixels 16 colors, MSX screen 5
!by End_V9938_Reginit_Graphic4-V9938_Reginit_Graphic4
!by $80+0,$06	; Register 0 ($80) M3=M4=1 (Graphic Mode 4); 
!by $80+1,$42	; Register 1 ($81) Blank off - M1 = M2 = 0, sprite 16x16
!by $80+2,$1F	; Register 2 ($82) Pattern name table base --> value*$400, bit 0 ... 4 must be 1--> $0000 ... $69FF
!by $80+3,$80	; Register 3 ($83) Color table base --> value*$40 --> unused
!by $80+4,$00	; Register 4 ($84) Pattern generator base --> value*$800 --> unused
!by $80+5,$EF	; Register 5 ($85) Sprite attribute table base address --> value*$80, bit 0 ... 2 must be 1 --> color $7400 ... $75FF attributes $7600...$767F
!by $80+6,$0F	; Register 6 ($86) Sprite pattern generator base address --> value*$800 --> $7800 ... $7FFF
!by $80+7,$00	; Register 7 ($87) Background Color
!by $80+8,$0A	; Register 8 Selects the type and organization of VRAM.1 = 64Kx1Bit or 64Kx4bits, Sprite disable
!by $80+9,$82	; Register 9 Vertical dot count is set to 212
!by $00,$00	; pattern generator base address (H,L)
!by $00,$00	; name table base address (Bitmap H,L)
!by 24 		; VDP_Text_MaxV
!by 32 		; VDP_Text_MaxH
End_V9938_Reginit_Graphic4

V9938_Reginit_Graphic5		; 512 x 212 pixels 4 colors, MSX Screen 6
!by End_V9938_Reginit_Graphic5-V9938_Reginit_Graphic5
!by $80+0,$08	; Register 0 ($80) M5=1 (Graphic Mode 5); 
!by $80+1,$42	; Register 1 ($81) Blank off - M1 = M2 = 0, Sprite 16x16
!by $80+2,$1F	; Register 2 ($82) Pattern Name table base --> value*$400 bit 0 ... 4 must be 1 --> $0000 ... $69FF
!by $80+3,$27	; Register 3 ($83) Color table base --> value*$40 --> unused
!by $80+4,$00	; Register 4 ($84) Pattern Generator base --> value*$800 --> unused
!by $80+5,$EF	; Register 5 ($85) Sprite Attribute table base address --> value*$80 bit 0 ... 2 must be 1 --> color $7400 ... $7fff attribute $7600 ... $767F
!by $80+6,$0F	; Register 6 ($86) Sprite pattern Generator base address --> value*$800 --> $7800 ... $7FFF
!by $80+7,$04	; Register 7 ($87) Background Color
!by $80+8,$0A	; Register 8 Selects the type and organization of VRAM.1 = 64Kx1Bit or 64Kx4bits, Sprite disable
!by $80+9,$82	; Register 9 Vertical dot count is set to 212
!by $00,$00	; pattern generator base address (H,L)
!by $00,$00	; name table base address (Bitmap H,L)
!by 24 		; VDP_Text_MaxV
!by 64 		; VDP_Text_MaxH
End_V9938_Reginit_Graphic5

V9938_Reginit_Graphic6		; 512 x 212 pixels 16 colors,  MSX BASIC screen 7
!by End_V9938_Reginit_Graphic6-V9938_Reginit_Graphic6
!by $80+0,$0A	; Register 0 ($80) M5,M3=1 (Graphic Mode 4);
!by $80+1,$42	; Register 1 ($81) Blank off - M1 = M2 = 0, Sprite 16x16
!by $80+2,$1F	; Register 2 ($82) Pattern Name table base  --> value*$400 bit 0 ... 4 must be 1 --> $0000 ... $D3FF
!by $80+3,$80	; Register 3 ($83) Color table base --> value*$40 --> unused
!by $80+4,$00	; Register 4 ($84) Pattern Generator base --> value*$800 --> unused 
!by $80+5,$F7	; Register 5 ($85) Sprite Attribute table base address --> value*$80, bit 0 ... 3 must be 1 --> $FA00 ... $FA7F
!by $80+6,$1E	; Register 6 ($86) Sprite pattern Generator base address --> value*$800 --> $F000 ...$F7FF, color: $F800 ... $F9FF
!by $80+7,$04	; Register 7 ($87) Background Color
!by $80+8,$0A	; Register 8 Selects the type and organization of VRAM.1 = 64Kx1Bit or 64Kx4bits, Sprite disable
!by $80+9,$82	; Register 9 Vertical dot count is set to 212
;!by $80+10,$00	; Register 10 Color table High
!by $80+11,$01	; Register 11 Sprite attribute table high
!by $00,$00	; pattern generator base address (H,L)
!by $00,$00	; name table base address (Bitmap H,L)
!by 24 		; VDP_Text_MaxV
!by 64 		; VDP_Text_MaxH
End_V9938_Reginit_Graphic6

V9938_Reginit_Graphic7		; 256 pixel x 212 pixel x 256 color, MSX screen 8
!by End_V9938_Reginit_Graphic7-V9938_Reginit_Graphic7
!by $80+0,$0e	; Register 0 GRAPHIC7 (G7) mode: M5,M4,M3=1. 
!by $80+1,$42	; Register 1 16K-Ram - Blank off - M1,M2,M3 = 0 (Graphic 1 Mode), 16x16 pxl. Sprites
!by $80+2,$1f	; Register 2 pattern layout table base address --> value*$400 bit 0 ... 4 must be 1 --> $0000 ... $D3FF
!by $80+3,$80	; Register 3 Color table base --> value*$40 --> unused
!by $80+4,$00	; Register 4 Pattern generator base --> value*$800 --> unused
!by $80+5,$F7	; Register 5 Sprite attribute table low base address --> value*$80, bit 0 ... 3 must be 1 --> $FA00 ... $FA7F
!by $80+6,$1E	; Register 6 Sprite pattern generator base address --> value*$800 --> $F000 ... $F7FF, color: $F800 ... $F9FF
!by $80+7,$00	; Register 7 Background color
!by $80+8,$0A	; Register 8 Selects the type and organization of VRAM.1 = 64Kx1Bit or 64Kx4bits, Sprite disable
!by $80+9,$82	; Register 9 Vertical dot count is set to 212, Set PAL mode
!by $80+11,$01	; Register 11 Sprite attribute table high base address --> $FA00
!by $00,$00	; pattern generator base address (H,L)
!by $00,$00	; name table base address (Bitmap H,L)
!by 26		; VDP_Text_MaxV
!by 40		; VDP_Text_MaxH
End_V9938_Reginit_Graphic7

V9958_Reginit_Graphic7YJK	; 256 x 212, 19268 YJK colors 	
!by End_V9958_Reginit_Graphic7YJK-V9958_Reginit_Graphic7YJK
!by $80+0,$0e	; Register 0 GRAPHIC7 (G7) mode: M5,M4,M3=1.
!by $80+1,$42	; Register 1 16K-Ram - blank off - M1,M2,M3 = 0 (Graphic 1 Mode), 16x16 pxl. Sprites
!by $80+2,$1f	; Register 2 pattern layout table base address --> value*$400 bit 0 ... 4 must be 1 --> $0000 ... $D3FF
!by $80+3,$00	; Register 3 Color table base: --> value*$40 --> not used
!by $80+4,$00	; Register 4 Pattern Generator base --> value*$800 --> not used
!by $80+5,$F7	; Register 5 Sprite attribute table low base address --> value*$80, bit 0 ... 3 must be 1 --> $FA00 ... $FA7F
!by $80+6,$1E	; Register 6 Sprite pattern Generator base address --> value*$800 --> $F000 ...$F7FF, color: $F800 ... $F9FF
!by $80+7,$00	; Register 7 Screen margin color: black
!by $80+8,$0A	; Register 8 Selects the type and organization of VRAM.1 = 64Kx1Bit or 64Kx4bits, Sprite disable
!by $80+9,$82	; Register 9 Vertical dot count is set to 212, Set PAL Mode
!by $80+11,$01	; Register 11 Sprite attribute table high base address --> 
!by $80+25,$08	; Register 25 YJK-Type Data Display Function: YJK=1 ; GRAPHIC12 Mode; handles the data on VRAM as YJK type data
!by $00,$00	; pattern generator base address (H,L)
!by $00,$00	; name table base address (H,L)
!by 26 		; VDP_Text_MaxV
!by 32 		; VDP_Text_MaxH
End_V9958_Reginit_Graphic7YJK

V9938_Reginit_Textmode2		; text mode with 80x26.5, MSX screen 0; width = 80
!by End_V9938_Reginit_Textmode2-V9938_Reginit_Textmode2
!by $80+0,$04	; Register 0 M4=1
!by $80+1,$50	; Register 1 16K-Ram - Blank off - M1 = 1 (Textmode) 80x26.5
!by $80+2,$03	; Register 2 Name table base  --> value*$400 --> $0000 ... $086F
!by $80+3,$27	; Register 3 Color table base  -->  value*$40 --> $0800h ... $090D; overlap with name table (because 26.5 lines) 
!by $80+4,$02	; Register 4 Pattern generator base --> value*$800 -->$1000 ... $17FF
!by $80+5,$36	; Register 5 Sprite attribute table base address --> value*$80 --> $1B00 (unused)
!by $80+6,$07	; Register 6 Sprite pattern Generator base address --> value*$800 --> $3800 (unused)
!by $80+7,$f4	; Register 7 Colour font = white and background = dark blue
!by $80+9,$82	; Register 8 Line:212, PAL mode 50Hz
!by $80+12,$f4	; Register 12 Text color / back color register --> for blinkable character, currently not supported
!by $80+13,$F0	; Register 13 Blinking period register: always on 
!by $10,$00	; pattern generator base address (H,L)
!by $00,$00	; name table base address (H,L)
!by 26 		; VDP_Text_MaxV
!by 80 		; VDP_Text_MaxH
End_V9938_Reginit_Textmode2

;***************************
; V9938 Clear Line 26.5 
; Special Function for Text2 Mode
; input: -
; output: -
; used: A,Y
;***************************

VDP_Clear_Line_26_5
		clc
		lda VDP_Layout_L
		adc #$20		
		sei
		sta TMS9918REG
		lda VDP_Layout_H
		adc #$08		; 26*80 = $820
		ora #$40
		sta TMS9918REG
		lda #SP
		ldy VDP_Text_MaxH
VDP_Clear_Line_26_5_2
		nop
		nop
		nop
		sta TMS9918RAM
		dey
		bne VDP_Clear_Line_26_5_2
		cli
		rts

;***************************
; Set V9958 Register 8 to 20 and 23,45,46 to 00; 
; Set R25 to 02 (PAL Mode 50Hz), Set 64kBit VRAM
; input: -
; output: -
; used: A,X
;***************************

V9958_Reset_Reg	
		sei
		ldx #$89
V9958_Reset_Reg_1			; Reset V9938 / 58: write zero to register 9 to 20 and 23,25
		stz TMS9918REG
		stx TMS9918REG
		inx
		cpx #$80+21
		bne V9958_Reset_Reg_1
		ldx #$80+23
		stz TMS9918REG
		stx TMS9918REG		; R#23
		ldx #$80+25
		stz TMS9918REG
		stx TMS9918REG		; R#25
		lda #$02		; set PAL Mode
		sta TMS9918REG
		lda #$80+09		; set to register R#9
		sta TMS9918REG
		lda #$08		; set VRAM 64kBit
		sta TMS9918REG
		lda #$80+08		; set to register R#8
		sta TMS9918REG
		stz TMS9918REG
		lda #$80+45		; set $00 (banks) to R#45
		sta TMS9918REG
		stz TMS9918REG
		lda #$80+46		; set $00 (stop command) to R#46
		sta TMS9918REG
		cli
		rts

;***************************
; Copy pattern to TMS9918 VRAM
; input: base address: VDP_Pattern_L/H
; output:
; used: A,X,Y
;***************************

VDP_Init_Pattern
		sei
		lda VDP_Pattern_L
		sta TMS9918REG			; set start address VRAM low Byte
		lda VDP_Pattern_H
		ora #$40			; set address maker and high Byte --> pattern generator sub-block
		sta TMS9918REG			; set start address VRAM
		lda #<VDP_ASCII_Pattern		; Low Byte
		sta PTR_R_L
		lda #>VDP_ASCII_Pattern		; High Byte
		sta PTR_R_H
		ldy #$00			; number of pattern (256)
VDP_Init_Pattern1
		ldx #$08			; number of bytes each character
VDP_Init_Pattern2
		lda (PTR_R_L)
		inc PTR_R_L
               	bne VDP_Init_Pattern3
               	inc PTR_R_H
VDP_Init_Pattern3
		sta TMS9918RAM
		dex
		bne VDP_Init_Pattern2
		dey
		bne VDP_Init_Pattern1
		lda VDP_Video_Mode
VDP_Init_Pattern4		
		cmp #1
		bne VDP_Init_Pattern_Exit		
		jsr VDP_Clear_Screen1		; special function for screen mode 1

VDP_Init_Pattern_Exit
		cli
		rts

;***************************
; TMS9918a Print Character
; input: A 
; output: -
; used: A,X,Y	TODO check for LF,BEL,VT
;***************************
VDP_print_CHR
		tay			; save character to Y
		cmp #LF			; is it a carriage line feed ?
		bne VDP_print_CHR_BS 	; if not then continue
VDP_print_CHR2
		stz VDP_CURSOR_H
		inc VDP_CURSOR_V
		lda VDP_CURSOR_V
		cmp VDP_Text_MaxV
		bmi VDP_print_CHR_LF	; if not bigger then no line feed
		jsr VDP_Scoll_up
		lda VDP_Text_MaxV
		dec
		sta VDP_CURSOR_V	; place the cursor on the bottom line
VDP_print_CHR_LF
		rts
VDP_print_CHR_BS
		cmp #BS			; is it a backspace ?
		bne VDP_print_CHR_Tab	; if not then continue
		lda VDP_CURSOR_H
		cmp #0			; compare current horizontal cursor position with 0 horizontalen
		bne VDP_print_CHR_BS3 	; if not 0 then branch
		lda VDP_CURSOR_V	; compare current cursor position with 0 vertical
		cmp #0			;
		bne VDP_print_CHR_BS2	; if not then continue
		rts			; end, as already position 0.0

VDP_print_CHR_BS2
		lda VDP_Text_MaxH
		dec
		sta VDP_CURSOR_H
		ldy #SP			; replace backspace with space
		jsr VDP_print_CHR4
		dec VDP_CURSOR_V
		rts
VDP_print_CHR_BS3
		ldy #SP			; replace backspace with space
		jsr VDP_print_CHR4
		dec VDP_CURSOR_H	; cursor one more to the left
		jmp VDP_print_CHR10
VDP_print_CHR_Tab
		cmp #HT			; is it tabulator ?
		bne VDP_print_CR	; if not then continue
VDP_print_CHR_Tab2			
		inc VDP_CURSOR_H	
		lda VDP_CURSOR_H
		cmp VDP_Text_MaxH
		bpl VDP_print_CHR2	; if too far to the right then CR
		lda VDP_CURSOR_H
		and #07
		bne VDP_print_CHR_Tab2
		rts
VDP_print_CR				
		cmp #CR			; is it a carriage return ?
		bne VDP_print_CHR3
		stz VDP_CURSOR_H	; set cursor to left border		
		rts		

VDP_print_CHR3
		jsr VDP_print_CHR4
		jsr VDP_print_CHR10
		jmp VDP_print_CHR8

VDP_print_CHR4
		; calculate the video RAM address from the cursor position and store it to PTR_TMS9918W
		lda VDP_Text_MaxH
		sta TMS9918_TEMP1	; MPD (multiplicand)
		lda VDP_CURSOR_V
		sta TMS9918_TEMP2	; MPR (multiplier)
		lda #0
		sta PTR_TMS9918W_H
		sta PTR_TMS9918W_L
		ldx #8
VDP_print_CHR5
		lsr TMS9918_TEMP2	; MPR (multiplier)
		bcc VDP_print_CHR6
		clc
		adc TMS9918_TEMP1	; MPD (multiplicand)
VDP_print_CHR6
		ror
		ror PTR_TMS9918W_L
		dex
		bne VDP_print_CHR5
		sta PTR_TMS9918W_H
VDP_print_CHR7
		lda VDP_CURSOR_H
		clc
		adc PTR_TMS9918W_L
		sta PTR_TMS9918W_L
		lda #00			; add carry 
		adc PTR_TMS9918W_H
		sta PTR_TMS9918W_H
		clc
		lda VDP_Layout_L	; add offset	
		adc PTR_TMS9918W_L
		sta PTR_TMS9918W_L
		lda VDP_Layout_H	
		adc PTR_TMS9918W_H
		sta PTR_TMS9918W_H	
		rts
VDP_print_CHR10				; write character to VRAM
		sei
		lda PTR_TMS9918W_L
		sta TMS9918REG		; set start address VRAM low Byte
		lda PTR_TMS9918W_H
		ora #$40
		sta TMS9918REG
		tya			; get ASCII character back
		sta TMS9918RAM		; write character	
		cli
		rts
VDP_print_CHR8
		inc VDP_CURSOR_H	; increase cursor horizontal
		lda VDP_CURSOR_H
		cmp VDP_Text_MaxH	; compare with maximum number of columns
		bcc VDP_print_CHR9	; if less than then end
		lda #0			; else line feed
		sta VDP_CURSOR_H
		inc VDP_CURSOR_V
		lda VDP_CURSOR_V
		cmp VDP_Text_MaxV	; compare with max number lines
		bcc VDP_print_CHR9 	; if not greater equal further
		jsr VDP_Scoll_up	; scroll up otherwise
		dec VDP_CURSOR_V
VDP_print_CHR9
		rts

;***************************
; TMS99xx scroll one line up
; input: -
; output: -
; used: A,X,Y
;***************************

VDP_Scoll_up
		lda VDP_Text_MaxH	
		clc
		adc VDP_Layout_L
		sta PTR_TMS9918R_L
		lda #0
		adc VDP_Layout_H
		sta PTR_TMS9918R_H	; VRAM source
		lda VDP_Layout_L	; name table base address low
		sta PTR_TMS9918W_L
		lda VDP_Layout_H	; name table base address high
		sta PTR_TMS9918W_H	; VRAM destination
		ldx VDP_Text_MaxH	; calculate the number of characters to move
		sei
VDP_Scoll_up2
		ldy VDP_Text_MaxV
		dey
VDP_Scoll_up3
		lda PTR_TMS9918R_L	; get start address VRAM low byte
		sta TMS9918REG
		lda PTR_TMS9918R_H	; get start address VRAM high byte
		and #$3f
		sta TMS9918REG
	   	inc PTR_TMS9918R_L	; increments PTR_TMS9918R; inc zp = 5 cyl.
               	bne VDP_Scoll_up5	; bne = 2 cyl. +1 or +2 if branch
               	inc PTR_TMS9918R_H	; inc zp = 5 cyl.
VDP_Scoll_up5
		nop			; delay optimized for 2MHz CPU
		nop			; nop = 2 cyl.
		lda TMS9918RAM		; read source
		nop			; 4 x 2 cyl.	
		nop			
		nop
		nop
		pha			; pha = 3 cyl.
		lda PTR_TMS9918W_L	; lda zp = 3 cyl.
		sta TMS9918REG		; set start address VRAM low Byte; sta zp = 3 cyl.
		lda PTR_TMS9918W_H
		ora #$40
		sta TMS9918REG
		pla
	    	inc PTR_TMS9918W_L	; increments PTR_TMS9918W
               	bne VDP_Scoll_up6
               	inc PTR_TMS9918W_H
VDP_Scoll_up6
		sta TMS9918RAM		; write destination
		nop
		nop
		dey
		bne VDP_Scoll_up3
		dex
		bne VDP_Scoll_up2
		ldx VDP_Text_MaxH
		lda #SP			; ASCII: space
VDP_Scoll_up4				; fill last line with spaces
		nop			; delay
		nop
		nop
		nop
		sta TMS9918RAM
		dex
		bne VDP_Scoll_up4
		cli
		rts

;***************************
; TMS9918 Cursor on
; input: -
; output: -
; used: A,Y
;***************************

VDP_Cursor_On
		; Cursor_Back = Zeichen hinter dem Curor
		sei
		jsr VDP_print_CHR4	; calculate character position and store in PTR_TMS9918W
		lda PTR_TMS9918W_L
		sta TMS9918REG		; set start address VRAM low Byte
		lda PTR_TMS9918W_H
		and #$3f
		sta TMS9918REG
		nop
		nop
		nop
		nop
		nop
		nop			; delay minimum 6 * NOP = 12 cycl.
		lda TMS9918RAM
		cmp #$FF		; is already cursor: Exit
		beq VDP_Cursor_On_Error_Exit
		sta VDP_Cursor_Back	; save character at cursor position
		lda PTR_TMS9918W_L
		sta TMS9918REG		; set start address VRAM low Byte
		lda PTR_TMS9918W_H
		ora #$40
		sta TMS9918REG
		lda #$FF		; set cursor
		sta TMS9918RAM
		lda #<VDP_ASCII_Pattern
		sta PTR_R_L
		lda #>VDP_ASCII_Pattern
		sta PTR_R_H
		lda VDP_Cursor_Back	; multiply character at cursor position by 8
		sta TMS9918_TEMP1		
		stz TMS9918_TEMP2
		asl TMS9918_TEMP1
		rol TMS9918_TEMP2
		asl TMS9918_TEMP1
		rol TMS9918_TEMP2
		asl TMS9918_TEMP1
		rol TMS9918_TEMP2
		clc
		lda TMS9918_TEMP1  	; and add to the pointer
		adc PTR_R_L
		sta PTR_R_L
		lda TMS9918_TEMP2
		adc PTR_R_H
		sta PTR_R_H
		
		clc
		lda VDP_Pattern_L
		adc #$F8		; set pattern TMS9918 to 255 * 8 = $7F8
		sta TMS9918REG
		lda VDP_Pattern_H
		adc #$07
		ora #$40		; $0f and ora #$40 for write access 
		sta TMS9918REG
		ldy #0
VDP_Cursor_On2
		lda (PTR_R_L),y
		eor #$FF
		sta TMS9918RAM
		nop			; 3 * 2 cycl.
		nop			
		nop
		iny
		cpy #8
		bne VDP_Cursor_On2
		rts
VDP_Cursor_On_Error_Exit
		cli
		sec
		rts

;***************************
; TMS9918 Cursor off
; input: -
; output: -
; used: A
;***************************

VDP_Cursor_Off
		sei
		jsr VDP_print_CHR4	; calculate character position and store in PTR_TMS9918W
		lda PTR_TMS9918W_L
		sta TMS9918REG		; set start address VRAM low Byte
		lda PTR_TMS9918W_H
		ora #$40
		sta TMS9918REG
		lda VDP_Cursor_Back
		sta TMS9918RAM
		cli
		rts

;***************************
; Clear text display (VDP Modes Text1 Text2 Screen1)
; input: -
; output: -
; used: A,X,Y
;***************************

VDP_Clear_Text_Screen
		sei
		lda VDP_Layout_L
		sta TMS9918REG		; set start address VRAM low Byte
		lda VDP_Layout_H	; set address marker and high Byte
		ora #$40
		sta TMS9918REG		; set start address VRAM
		ldx VDP_Text_MaxV
		lda #SP
VDP_Clear_Text_Screen1
		ldy VDP_Text_MaxH
VDP_Clear_Text_Screen2			; cycle time = 8µs
		sta TMS9918RAM		
		nop			; delay 3*2 cycl.
		nop			
		nop			
		dey				; 2 cycl.			
		bne VDP_Clear_Text_Screen2	; 2 cycl. +1 cycl. if branching + 1 cycl. if over page limit
		dex					
		bne VDP_Clear_Text_Screen1
		stz VDP_CURSOR_H
		stz VDP_CURSOR_V
		lda VDP_Video_Mode
		cmp #13				; special function for VDP Text mode 2: clear text line 26.5
		bne VDP_Clear_Text_Screen_Exit
		jsr VDP_Clear_Line_26_5 
VDP_Clear_Text_Screen_Exit
		cli
		rts

;***************************
; Clear Screen Mode 1, Clear Color table and Sprite Attributes
; input: -
; output: -
; used: A,Y
;***************************

VDP_Clear_Screen1		
		lda #$00			
		sei	
		sta TMS9918REG		; set start address VRAM low Byte
		lda #$20		; set address marker and high Byte $2000
		ora #$40
		sta TMS9918REG		; set start address VRAM
		ldy #$20
		lda #$F4		; clear text color / background color
VDP_Clear_Screen1_2
		sta TMS9918RAM
		nop			; 3 x 2 cycl.
		nop			
		nop			
		dey
		bne VDP_Clear_Screen1_2
		lda #00
		sta TMS9918REG		; set start address VRAM low Byte
		lda #$1B		; set address marker and high Byte $1B00
		ora #$40
		sta TMS9918REG		; set start address VRAM
		lda #208		; Y-Pos 208 : sprite are not displayed
		ldy #$80		; set sprites attribute, VRAM $1B00 to $3B7F 
VDP_Clear_Screen1_3
		nop			; 3 x 2 cycl.		 
		nop			 
		nop			 
		sta TMS9918RAM
		dey
		bne VDP_Clear_Screen1_3
		cli
		rts

;***************************
; Detect typ of VDP : TMS99xx or V9958
; Set zero page variable VDP_Type
; input: -
; output: -
; used: A,X
;***************************

VDP_Detect_CTRL_Type
		sei
		lda #01			; set Status Register #1 for V9958 reading ID
		sta TMS9918REG
		lda #$80+15		; status register pointer
		sta TMS9918REG
		lda TMS9918REG		; read Status Register
		sta VDP_Type
		cli
		ldx #VDP_Set_Video_Mode_FN
		lda #0
		jsr VDP_Function_Call	; init textmode1 again for TMS99X8 or set subroutine after reset.
		lda VDP_Type
		cmp #04
		bne VDP_Detect_CTRL_Type2
		rts
VDP_Detect_CTRL_Type2
		stz VDP_Type
		rts

;***************************
; Wait Vertical Sync Impulse. Warning: only works reliably in interrupt mode
; input: -
; output: -
; used: A
;***************************

VDP_Wait_V_Sync				
		lda TMS9918REG
		and #$80
		beq VDP_Wait_V_Sync
		rts


;VDP9958 Register:
;----------------------------------------------------------------
;|      |                                                       |
;| R#n  |                        Function                       |
;|      |                                                       |
;|------+-------------------------------------------------------|
;| R#0  | Mode register #0                                      |
;| R#1  | Mode register #1                                      |
;| R#2  | Pattern layout table                                  |
;| R#3  | Colour table (LOW)                                    |
;| R#4  | Pattern generator table                               |
;| R#5  | Sprite attribute table (LOW)                          |
;| R#6  | Sprite pattern generator table                        |
;| R#7  | Text and screen margin colore                         |
;| R#8  | Mode register #2                                      |
;| R#9  | Mode register #3                                      |
;| R#10 | Colour table (HIGH)                                   |
;| R#11 | Sprite attribute table (HIGH)                         |
;| R#12 | Text and background blink color                       |
;| R#13 | Blinking period register                              |
;| R#14 | VRAM access base register                             |
;| R#15 | Status register pointer                               |
;| R#16 | Color palette address register                        |
;| R#17 | Control register pointer                              |
;| R#18 | Display adjust register                               |
;| R#19 | Interrupt line register                               |
;| R#20 | Colour burst register 1                               |
;| R#21 | Colour burst register 2                               |
;| R#22 | Colour burst register 3                               |
;| R#23 | Vertical offset register                              |
;| R#25 | Type data display function                            |
;| R#26 | Horizontal scroll function HO3...HO8                  |
;| R#27 | Horizontal scroll function HO0...HO2                  |
;| R#32 | SX: Source X low register                             |
;| R#33 | SX: Source X high register                            |
;| R#34 | SY: Source Y low register                             |
;| R#35 | SY: Source Y high register                            |
;| R#36 | DX: Destination X low register                        |
;| R#37 | DX: Destination X high register                       |
;| R#38 | DY: Destination Y low register                        |
;| R#39 | DY: Destination Y high register                       |
;| R#40 | NX: Number of dots X low register                     |
;| R#41 | NX: Number of dots X high register                    |
;| R#42 | NY: Number of dots Y low register                     |
;| R#43 | NY: Number of dots Y high register                    |
;| R#44 | CLR: Color register                                   |
;| R#45 | ARG: Argument register                                |
;| R#46 | CMR: Command register                                 |
;----------------------------------------------------------------


