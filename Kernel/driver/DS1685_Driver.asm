; Compiler : ACME 
; UTF-8 character encoding
; (C) 2019 Uwe Gottschling
; RTC DS1685 Real-Time Clock Driver and Set	
;!src "Oak65c02_GIT/Kernel/include/label_definition.asm"
;!src "Oak65c02_GIT/Kernel/include/Jumptable.asm"
;!src "Oak65c02_GIT/Kernel/include/DOS_FAT_Constant.asm"
!cpu 65c02
;TODO: Set crystal select bit to 1 for 12.5pf load ?
;*=$0FFE
;!word $1000

DS1685_Main
		jsr DS1685_Init
		jsr DS1685_Start_Message
		ldx #OS_Get_CHR_FN
		jsr OS_Function_Call
		pha
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call		; echo character
		jsr OS_Internal_Print_CHR
		lda #LF
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call	
		jsr OS_Internal_Print_CHR
		pla
		cmp #'x'
		beq DS1685_Back_Exit
		ldx #End_DS1685_Commands-DS1685_Commands
DS1685_Main1
		dex
		bmi DS1685_Main2
		cmp DS1685_Commands,x
		bne DS1685_Main1
		txa
		asl
		tax
		lda DS1685_Command_LUT,x
		sta PTR_J_L
		lda DS1685_Command_LUT+1,x
		sta PTR_J_H
;		lda #LF
;		jsr Print_CHR
		jsr DS1685_Main_Call
		jmp DS1685_Main
DS1685_Main2
		jsr UnknownCommand
		jmp DS1685_Main
DS1685_Back_Exit
		rts
DS1685_Main_Call
 		jmp (PTR_J_L)
	
DS1685_Commands
!text "t"
!text "r"
!text "d"
!text "s"
!text "e"
!text CR
!text LF
End_DS1685_Commands

DS1685_Command_LUT
!word DS1685_Print_Time				; t
!word DS1685_NVRAM				; r 
!word DS1685_Print_Date				; d
!word DS1685_Set_Date				; s
!word DS1685_Set_Time				; e
!word DS1685_Main				; CR
!word DS1685_Main				; LF

DS1685_Exit
		rts

DS1685_Start_Message
		lda #<DS1685_Start_Message_Text	; < low byte of expression
		sta PTR_String_L
		lda #>DS1685_Start_Message_Text	; > high byte of expression
		sta PTR_String_H
;		ldx #OS_Print_String_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_String
		rts

DS1685_Start_Message_Text
!text LF
!text "DS1685 RTC Test. Press Key:",LF
!text "t = Print Time",LF
!text "r = Print NVRAM",LF
!text "d = Print Date",LF
!text "s = Set Date RTC",LF
!text "e = Set Time RTC",LF
!text "x = Exit",LF
!text 00

;***************************
; Dump DS1686 NVRAM 
; input: -
; output: stdout
; used: A,X,Y
;***************************

DS1685_NVRAM
		lda #LF
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR
		ldy #$0E		
DS1685_NVRAM_2
		sty DS1685_ADDR
		phy
		tya
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call	
		jsr OS_Internal_Print_Bin2Hex
		lda #":"
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR	
		lda DS1685_DATA
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call			
		jsr OS_Internal_Print_Bin2Hex
		lda #SP
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR
		ply 
		iny
		tya
		and #$03
		bne DS1685_NVRAM_3
		lda #LF
;		ldx #OS_Print_CHR_FN	
		phy
		jsr OS_Internal_Print_CHR
;		jsr OS_Function_Call	
		ply
DS1685_NVRAM_3
		cpy #$40
		bne DS1685_NVRAM_2
		rts

;***************************
; Init DS1685 RTC
; input: -
; output: -
; used: A
;***************************

DS1685_Init
		lda #DS1685_REG_A	; register A
		sta DS1685_ADDR
		lda #$20		; turn  the  oscillator  on  and  allow  the  RTC  to  keep  time.
		lda DS1685_DATA
		lda #DS1685_REG_B	; register B
		sta DS1685_ADDR
		lda #$06		; binary Mode, 24-hour mode
		lda DS1685_DATA
		lda #DS1685_REG_C	; register C
		sta DS1685_ADDR
		lda #$00		 
		lda DS1685_DATA
		rts

;***************************
; Print RTC Time
; input: -
; output: stdout
; used: A,X,Y
;***************************

DS1685_Print_Time	
		jsr DS1685_Print_Time_2
		lda #LF
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR		
		rts
DS1685_Print_Time_2
		lda #DS1685_HOURS	; hours
		sta DS1685_ADDR
		lda DS1685_DATA
		jsr Print_Bin2BCD_2D
		lda #":"
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR
		lda #DS1685_MINUTES	; minutes
		sta DS1685_ADDR
		lda DS1685_DATA
		jsr Print_Bin2BCD_2D
		lda #":"
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR
		lda #DS1685_SECONDS	; seconds
		sta DS1685_ADDR
		lda DS1685_DATA
		jsr Print_Bin2BCD_2D
		rts

;***************************
; Print RTC Date
; input: -
; output: stdout
; used: A,X,Y
;***************************

DS1685_Print_Date
		jsr DS1685_Print_Date_2
		lda #LF
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR
		rts
DS1685_Print_Date_2
		lda #DS1685_CENTURY	; century
		sta DS1685_ADDR
		lda DS1685_DATA
		jsr Print_Bin2BCD_2D
		lda #DS1685_YEAR	; year
		sta DS1685_ADDR
		lda DS1685_DATA
		jsr Print_Bin2BCD_2D
		lda #"-"
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR
		lda #DS1685_MONTH	; month
		sta DS1685_ADDR
		lda DS1685_DATA
		jsr Print_Bin2BCD_2D
		lda #"-"
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR
		lda #DS1685_DATE	; date
		sta DS1685_ADDR
		lda DS1685_DATA
		jsr Print_Bin2BCD_2D
		lda #SP
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR
		lda #DS1685_DAY		; weekday
		sta DS1685_ADDR
		lda DS1685_DATA
		jsr DS1685_Weekday
		rts

;***************************
; Set Date RTC Date
; input: stdin
; output: stdout
; used: A,X,Y,Operand1
;***************************

DS1685_Set_Date
		lda #<SetDate_Message_Text	; < low byte of expression 
		sta PTR_String_L
		lda #>SetDate_Message_Text	; > high byte of expression 
		sta PTR_String_H
;		ldx #OS_Print_String_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_String
		ldy #00
DS1685_Set_Date_2
		phy
		lda Date_Input_Sign,y
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR
		lda #':'
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR
		jsr OS_Internal_STDIN_ASCII_TO_BCD
		ply
		bcs DS1685_Set_Date_Error_Exit
		cmp Date_Lower_Limit,y
		bmi DS1685_Set_Date_Error_Exit	
		cmp Date_Upper_Limit,y
		beq DS1685_Set_Date_3
		bcs DS1685_Set_Date_Error_Exit
DS1685_Set_Date_3
		pha
		lda Date_Reg,y		
		sta DS1685_ADDR
		pla
		sta DS1685_DATA	
		lda DS1685_DATA	
		iny
		cpy #Date_Reg_end-Date_Reg	
		beq DS1685_Set_Date_Exit
		bra DS1685_Set_Date_2

DS1685_Set_Date_Exit
		lda #LF
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR
		clc
		rts
DS1685_Set_Date_Error_Exit
		jsr Print_Bin2BCD_2D
		lda #"?"
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR
		lda #LF
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR
		rts


; DS1685 registers for date
; century, year, month, date, day
Date_Reg
!byte DS1685_CENTURY,DS1685_YEAR,DS1685_MONTH,DS1685_DATE,DS1685_DAY
Date_Reg_end
; Lower Limit 
Date_Lower_Limit
!byte 20,00,01,01,01 
Date_Upper_Limit
!byte 20,99,12,31,07 
Date_Input_Sign
!byte 'C','Y','M','D','W'

SetDate_Message_Text
!text LF,"Set Date:",LF,"Century, Year, Month, Data, Working Day",LF,00

;***************************
; Set RTC Time Version
; input: stdin
; output: stdout
; Used: A,X,Y,Operand1
;***************************

DS1685_Set_Time
		lda #<Set_Time_Message_Text	; < low byte of expression
		sta PTR_String_L
		lda #>Set_Time_Message_Text	; > high byte of expression
		sta PTR_String_H
;		ldx #OS_Print_String_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_String
		ldy #00
DS1685_Set_Time_2
		phy
		lda Time_Input_Sign,y
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR
		lda #':'
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR
		jsr OS_Internal_STDIN_ASCII_TO_BCD
		ply
		bcs DS1685_Set_Time_Error_Exit
		cmp Time_Lower_Limit,y
		bmi DS1685_Set_Time_Error_Exit	
		cmp Time_Upper_Limit,y
		beq DS1685_Set_Time_3
		bcs DS1685_Set_Time_Error_Exit
DS1685_Set_Time_3
		pha
		lda Time_Reg,y		
		sta DS1685_ADDR
		pla
		sta DS1685_DATA	
		lda DS1685_DATA	
		iny
		cpy #Time_Reg_end-Time_Reg	
		beq DS1685_Set_Time_Exit
		bra DS1685_Set_Time_2
DS1685_Set_Time_Exit
		lda #LF
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR
		clc
		rts
DS1685_Set_Time_Error_Exit
		jsr Print_Bin2BCD_2D
		lda #"?"
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR
		lda #LF
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR
		rts

; DS1685 registers for time
Time_Reg
!byte DS1685_HOURS,DS1685_MINUTES,DS1685_SECONDS
Time_Reg_end
; Lower Limit 
Time_Lower_Limit
!byte 00,00,00 
Time_Upper_Limit
!byte 23,59,59 
Time_Input_Sign
!byte 'H','M','S'
Set_Time_Message_Text
!text LF,"Set Time: Hours, Minutes, Seconds",LF,00

;***************************
; Print Weekday
; input: A (01...07)
; input: -
; output: stdio
; used: A,X,Y,Operand1
;***************************

DS1685_Weekday
		and #7			; mask wrong value
		beq DS1685_Weekday_Exit
		dec			; sub 1
		sta Operand1
		clc
		asl			; *2 (calc offset for text)
		adc Operand1		; A=A*2+A
		tay
		ldx#3
DS1685_Weekday_2
		lda DS1685_Weekday_Text,y
		phx
		phy
;		ldx #OS_Print_CHR_FN	
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR
		ply
		plx
		iny
		dex
		bne DS1685_Weekday_2
DS1685_Weekday_Exit
		rts

DS1685_Weekday_Text
!text "MonTueWedThuFriSatSun"



