; Compiler : ACME (Compiler - Umstellung ab 10.03.2017)
; UTF-8 character encoding
; (C) 2019 Uwe Gottschling
; TODO: rename to keyboard_driver

!cpu 65c02

PS2_Function_Call
		jmp (PS2_Function_Vector_L)

PS2_Function_Call1
		pha
		txa
		asl
		tax
		cpx #PS2_Function_Call_LUT_End-PS2_Function_Call_LUT
		bcs PS2_Function_Call_Error_Exit
		lda PS2_Function_Call_LUT,x	
		sta PTR_J_L
		lda PS2_Function_Call_LUT+1,x
		sta PTR_J_H
		pla
		jsr PS2_Function_Call_2
		rts
PS2_Function_Call_2
		jmp (PTR_J_L)
PS2_Function_Call_Error_Exit
		pla
		sec
		rts

PS2_Function_Call_LUT

!word PS2_GetKey		;0
!word PS2_ChkKey		;1
!word PS2_DummyRead		;2
PS2_Function_Call_LUT_End



;***************************
; PS/2 Keyboard Routinen
; input: -
; output: A
; used: A
;***************************
PS2_GetKey
		lda VIA2IFR
		and #$02
		beq PS2_GetKey
		lda VIA2ORA
		rts

;***************************
; PS/2 Keyboard, carry flag set if key was pressed
; input: -
; output: A
; used: A
;***************************
PS2_ChkKey
		lda VIA2IFR
		and #$02
		beq PS2_ChkKey2
		sec
		rts
PS2_ChkKey2
		clc
		rts

;***************************
; PS/2 Dummy Read
; input: -
; output: -
; used: A
;***************************
PS2_DummyRead
		lda VIA2ORA		; PS2 keyboard dummy read
		rts 


