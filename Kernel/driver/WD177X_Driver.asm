; Compiler : ACME (Compiler - change since 10.03.2017)
; UTF-8 character encoding
; (C) 2018 Uwe Gottschling

!cpu 65c02

;***************************
; Floppy Disc Controller WD177X Function Table
; input: A = parameter, X- function number
; used: A,X,Y
;***************************

FDC_Function_Call
		jmp (FDC_Function_Vector_L)
FDC_Function_Call1
		pha
		txa
		asl
		tax
		cpx #FDC_Function_Call_LUT_End-FDC_Function_Call_LUT
		bcs FDC_Function_Call_Error_Exit
		lda FDC_Function_Call_LUT,x	
		sta PTR_J_L
		lda FDC_Function_Call_LUT+1,x
		sta PTR_J_H
		pla
		jsr FDC_Function_Call_2
		rts
FDC_Function_Call_2
		jmp (PTR_J_L)
;		rts
FDC_Function_Call_Error_Exit
		pla
		sec
		rts

FDC_Function_Call_LUT
!word FDC_Reset			;0
!word FDC_Restore		;1
!word FDC_Addr			;2
!word FDC_RTrack		;3
!word FDC_RSect			;4
!word FDC_WaitBusy		;5
!word FDC_WTrack		;6
!word FDC_WSect			;7
!word FDC_Status		;8
FDC_Function_Call_LUT_End ; TODO: seek Track

;***************************
; Reset Disk System
; input: Floppy_DriveNo, Floppy_Head
; output: -
; used: A
;***************************
FDC_Reset
		lda Floppy_DriveNo
		clc
		rol		
		ora Floppy_Head		; BIT0 = side select. Side = 0 for boot sector
		sta FLOPPYLATCH		; Bit =0
		rts			; TODO: Reset force interrupt register

;***************************
; Restore (Track0)
; input: -
; output: -
; used: A,X
;***************************
FDC_Restore
		lda #WD177XREST  	; resore (seek track 0)
		sta WD177XCMD
		jsr FDC_WaitBusy
		lda WD177XCMD
		tax			; Save register A to X
		clc
		and #$59
		beq FDC_Restore1
		sec			; Carry = 1 = error
FDC_Restore1
		txa
		rts

;***************************
; Seek Track
; input: A = track
; output: A = status, C=1=error
; used: A,X
;***************************

FDC_SeekTrack
		sta WD177XDAT		; Track number to track register
		lda #WD177XSEEK  	; Seek command
		sta WD177XCMD
		jsr FDC_WaitBusy
		lda WD177XCMD
		tax
		clc
		and #$59		; $A6 is no error
		beq FDC_SeekTrack1
		sec
FDC_SeekTrack1
		txa
		rts

;***************************
; Read ID
; input: CHS
; output: 
; used: A
;***************************
FDC_Addr
;		phy
		jsr FDC_Set_CHS
		lda #WD177XREADI	; read Adress. The next ID field is read
		sta WD177XCMD
		jmp FDC_RData

;***************************
; Read Track
; input: CHS
; output: 
; used: A
;***************************
FDC_RTrack
;		phy
		jsr FDC_Set_CHS
		lda #WD177XREADT	; read track
		sta WD177XCMD
		jmp FDC_RData

;***************************
; Read Sector
; input: A=sector number
; output: 
; used: A
;***************************
FDC_RSect
;		phy
		jsr FDC_Set_CHS
		lda Floppy_Sector
		sta WD177XSEC		; set sector register
		lda #WD177XREADS	; read sector
		sta WD177XCMD

;***************************
; Read Data
; input: A=sector number
; output: A=status, C=1=error
; used: A,X,Y,PTR_FLOPPY
;***************************
FDC_RData
		ldy #$0
FDC_RData2
		bit FLOPPYLATCH		; bit 7 = DRQ, Bit 6 = INTRQ
		bvs FDC_RData4		; check if bit 6 set
		bpl FDC_RData2		; check if bit 7 clear
FDC_RData3
		lda WD177XDAT
		sta (PTR_FLOPPY_L),y
		iny
      	bne FDC_RData2
     	inc PTR_FLOPPY_H
		jmp FDC_RData2
FDC_RData4
		tya
		clc
		adc PTR_FLOPPY_L	; set pointer correctly using Y registers
		sta PTR_FLOPPY_L
		lda #0
		adc PTR_FLOPPY_H
		sta PTR_FLOPPY_H
		lda WD177XCMD
		tax
		clc
		and #$3F		; $80 is no error
		beq FDC_RData5
		sec
FDC_RData5
		txa
		rts

;***************************
; WD177X wait busy
;
;***************************
FDC_WaitBusy
		bit FLOPPYLATCH
		bvc FDC_WaitBusy
		jmp FDC_WData

;***************************
; WD177X Write Track
;
;***************************
FDC_WTrack

		jsr FDC_Set_CHS
		lda #WD177XWRITET	; write Track
		sta WD177XCMD
		jmp FDC_WData

;***************************
; WD177X Write Sector
;
;***************************
FDC_WSect
		jsr FDC_Set_CHS		; TODO: add "lda Floppy_Sector"
		sta WD177XSEC		; set sector register
		lda #WD177XWRITES	; write sector
		sta WD177XCMD
		jmp FDC_WData
		

;***************************
; WD177X Read Status of Last Operation
;
;***************************
FDC_Status
		lda WD177XCMD
		rts

;***************************
; WD177X Set Head, drive and Track number
;
;***************************
FDC_Set_CHS
		lda Floppy_DriveNo
		clc
		rol		
		ora Floppy_Head		; BIT0 = side select. Side = 0 for boot sector
		sta FLOPPYLATCH
		lda Floppy_Track
		jsr FDC_SeekTrack
		rts

;***************************
; Write data from PTR_FLOPPY to disk
; Output A: Status; C=1=Error
;***************************
FDC_WData
		ldy #$0
FDC_WData2
		bit FLOPPYLATCH		; Bit 7 = DRQ, Bit 6 = INTRQ
		bvs FDC_WData4
		bpl FDC_WData2
FDC_WData3
		lda (PTR_FLOPPY_L),y
		sta WD177XDAT
		iny
		bne FDC_WData2
		inc PTR_FLOPPY_H
		jmp FDC_WData2
FDC_WData4
		tya
		clc
		adc PTR_FLOPPY_L	; set pointer correctly using Y registers
		sta PTR_FLOPPY_L
		lda #0
		adc PTR_FLOPPY_H
		sta PTR_FLOPPY_H
;		ply
		lda WD177XCMD
		tax
		clc
		and #$3F		; $80 is no error TODO: should be and #$7F
		beq FDC_WData5
		sec
FDC_WData5
		txa
		rts

