; Compiler : ACME (Compiler - Umstellung ab 10.03.2017)
; UTF-8 character encoding
; (C) 2019 Uwe Gottschling

!cpu 65c02

;***************************
; VIA1 Init
; input: -
; output: -
; used: A,X
;***************************

VIA1_Init
		ldx #$0f
VIA1_Init2	lda VIA1_Init_Data,x
		sta VIA1ORB,x
		dex
		bne VIA1_Init2
		rts		

VIA1_Init_Data
	!by $00	; Register 0 VIA1ORB  = $DF10 DB0 = RAM_ENABLE; 0=RAM ENABLE 1= ROM ENABLE 
	!by $00	; Register 1 VIA1ORA  = $DF11
	!by $00	; Register 2 VIA1DDRB = $DF12 0=input 1=output
	!by $FF	; Register 3 VIA1DDRA = $DF13 0=input 1=output
	!by $00	; Register 4 VIA1T1CL = $DF14
	!by $00	; Register 5 VIA1T1CH = $DF15
	!by $00	; Register 6 VIA1T1LL = $DF16
	!by $00	; Register 7 VIA1T1LH = $DF17
	!by $00	; Register 8 VIA1T2CL = $DF18
	!by $00	; Register 9 VIA1T2CH = $DF19
	!by $00	; Register A VIA1SR   = $DF1A
	!by $00	; Register B VIA1ACR  = $DF1B 1= latch Port A enable
	!by $00	; Register C VIA1PCR  = $DF1C
	!by $00	; Register D VIA1IFR  = $DF1D
	!by $00	; Register E VIA1IER  = $DF1E
	!by $00	; Register F VIA1ORA2 = $DF1F

;***************************
; VIA2 Init
; input: -
; output: -
; used: A,X
;***************************

VIA2_Init
		ldx #$0f
VIA2_Init2	lda VIA2_Init_Data,x
		sta VIA2ORB,x
		dex
		bne VIA2_Init2
		rts		

VIA2_Init_Data
	!by $00	; Register 0 VIA2ORB  = $DF20 joystick interface
	!by $00	; Register 1 VIA2ORA  = $DF21 keyboard interface
	!by $00	; Register 2 VIA2DDRB = $DF22 0=input 1=output
	!by $00	; Register 3 VIA2DDRA = $DF23 0=input 1=output
	!by $00	; Register 4 VIA2T1CL = $DF24
	!by $00	; Register 5 VIA2T1CH = $DF25
	!by $00	; Register 6 VIA2T1LL = $DF26
	!by $00	; Register 7 VIA2T1LH = $DF27
	!by $00	; Register 8 VIA2T2CL = $DF28
	!by $00	; Register 9 VIA2T2CH = $DF29
	!by $00	; Register A VIA2SR   = $DF2A
	!by $01	; Register B VIA2ACR  = $DF2B 1= latch Port A enable
	!by $08	; Register C VIA2PCR  = $DF2C
	!by $00	; Register D VIA2IFR  = $DF2D
	!by $00	; Register E VIA2IER  = $DF2E
	!by $00	; Register F VIA2ORA2 = $DF2F

