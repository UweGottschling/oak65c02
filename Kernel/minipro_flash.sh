#!/bin/bash

echo
echo Flash Oak65C02OS.o65 to AT28C64B
echo

minipro --get_info -dTL866CS	# get device info
minipro -p AT28C64B -w Oak65C02OS.o65

