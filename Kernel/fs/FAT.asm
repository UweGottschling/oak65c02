;***************************
; FAT12 File System support
; currently only 720kb single floppy drive support
;***************************

DPB_PTR_Table

.word 	DPB_1
.word 	DPB_2
.word 	DPB_3
.word 	DPB_4

FCB_PTR_Table
.word 	FCB_1
.word 	FCB_2
.word 	FCB_3
.word 	FCB_4

;******************
; FAT_Load_Next_File_Sector
;******************
FAT_Load_Next_File_Sector
		lda FCB_FirstSecOfCluster
		clc
		adc FCB_Sector_Count
		sta Floppy_Blk_Low
		lda FCB_FirstSecOfCluster+1
		adc #0
		sta Floppy_Blk_High
FAT_Load_Next_File_Sector2
		jsr Floppy_LBA
		lda #<SectorBuffer
		sta PTR_FLOPPY_L
		lda #>SectorBuffer	; set sector buffer start address
		sta PTR_FLOPPY_H
		jsr Floppy_CHS_Read	; read sector
		rts

;******************
; Check Boot Sector Signature
;******************
FAT_CheckBootSec
		lda #<SectorBuffer
		sta PTR_FLOPPY_L
		lda #>SectorBuffer	; Startadresse wieder auf Anfang setzen
		sta PTR_FLOPPY_H

		ldy #DOS_BPB_Offset_Signature		; Offset Bootsector Kennung
		inc PTR_FLOPPY_H
		lda (PTR_FLOPPY_L),y
		cmp #$55
		beq FAT_CheckBootSec4
		jmp FAT_CheckBootSec_Error
FAT_CheckBootSec4
		iny
		lda (PTR_FLOPPY_L),y
		cmp #$AA
		beq FAT_CheckBootSec5
		jmp FAT_CheckBootSec_Error
FAT_CheckBootSec5
		ldy #DOS_BPB_Offset_Media
		lda (PTR_FLOPPY_L),y
		cmp #DOS_Media_Signature_720kb
		beq FAT_CheckBootSec5_2
		jmp FAT_CheckBootSec_Error
FAT_CheckBootSec5_2
		dec PTR_FLOPPY_H
; Copy sector Bootsector-data to Disk Parameter Block DPB
		ldy #DOS_BPB_Offset_BytsPerSec	; start at BPB_BytsPerSec (Offset Byte 11)
		ldx #0
FAT_CheckBootSec6
		lda (PTR_FLOPPY_L),y
		sta DPB,x
		iny
		inx
		cpx #$11	; end of data
		bcc FAT_CheckBootSec6
; Compare if media decriptor = F9 (720kByte Floppy)

; Calculated first sector of the root directory
; FirstRootDirSecNum = DPB_ReservedSectors + (DPB_FatCopies (1 Byte) * DPB_SectorsPerFAT (2Byte)) = 01 + (02 * 03) = 7 for 720kB
FAT_CheckBootSec7
		lda DPB_SectorsPerFAT
		sta Operand1
		lda DPB_SectorsPerFAT+1
		sta Operand2
		lda DPB_FatCopies
		sta Operand3

		jsr Multiply16

		sta DPB_FirstRootDirSecNum
		sty DPB_FirstRootDirSecNum+1

		lda DPB_FirstRootDirSecNum
		clc
		adc DPB_ReservedSectors
		sta DPB_FirstRootDirSecNum
		lda DPB_ReservedSectors+1
		adc DPB_FirstRootDirSecNum+1
		sta DPB_FirstRootDirSecNum+1

; FirstDataSector = BPB_ResvdSecCnt + (DPB_FatCopies * DPB_SectorsPerFAT) + RootDirSectors;

		lda DPB_RootDirEntries+1
		sta DPB_FirstDataSector+1
		lda DPB_RootDirEntries
		sta DPB_FirstDataSector

		ldx #4
FAT_CheckBootSec8
		clc				; calculate number of root dir sectors. Work only with 512byte sectors size
		lda DPB_FirstDataSector+1
		ror
		sta DPB_FirstDataSector+1
		lda DPB_FirstDataSector
		ror
		sta DPB_FirstDataSector
		dex
		bne FAT_CheckBootSec8

		clc
		lda DPB_FirstRootDirSecNum+1
		adc DPB_FirstDataSector+1
		sta DPB_FirstDataSector+1
		lda DPB_FirstRootDirSecNum
		adc DPB_FirstDataSector
		sta DPB_FirstDataSector

		stz DPB_FatSectorBufferBlk	; Restore FatSectorBuffer status
		stz DPB_FatSectorBufferBlk+1
		clc
		rts
FAT_CheckBootSec_Error
		sec
		rts

;******************
; DOS calculate next FAT sector
;******************

FAT_Calc_Next_File_Sector
		inc FCB_Sector_Count
;FAT_Calc_Next_File_Sector1
		lda FCB_Sector_Count
		cmp DPB_SectorsPerCluster
		bne FAT_Calc_Next_File_Sector3
;FAT_Calc_Next_File_Sector2
		stz FCB_Sector_Count
		jsr FAT_GetNextFat12Cluster
		jsr FAT_Cluster_to_Block
FAT_Calc_Next_File_Sector3
		rts 

;******************
; Fill FCB
; PTR_FLOPPY has to point to the directory entry
;******************
FAT_Fill_FCB
		lda #$01
		sta FCB_Drive_Number		; t.b.d. current drive

		ldy #$00			; Offet: start at first dir entry (name)
		ldx #$00
FAT_Fill_FCB2
		lda (PTR_FLOPPY_L),y
		sta FCB_File_Name,x
		iny
		inx
		cpx #$11			; end at of file name 8+3 = 11
		bcc FAT_Fill_FCB2

		ldy #DOS_Offset_DIR_FstClusHI	; offset DIR_FstClusHI
		ldx #0
FAT_Fill_FCB4
		lda (PTR_FLOPPY_L),y		; copy data: FstClusHI, WrtTime, WrtDate, FstClusLo, FileSize
		sta FCB_FstClusHI,x
		iny
		inx
		cpx #$12
		bcc FAT_Fill_FCB4

		lda FCB_FstClusLO		; Copy Start Cluster to current Cluster
		sta FCB_Current_Cluster
		lda FCB_FstClusLO+1
		sta FCB_Current_Cluster+1

		stz FCB_FirstSecOfCluster
		stz FCB_FirstSecOfCluster+1
		stz FCB_Sector_Count
		stz FCB_Sector_Count+1

		stz FCB_CurrentByteNum
		stz FCB_CurrentByteNum+1
		stz FCB_CurrentByteNum+2
		stz FCB_CurrentByteNum+3
		stz FCB_Sector_Count
		rts

;******************
; Clear FCB
; PTR_FLOPPY has to point to the directory entry
;******************
FAT_Clear_FCB
		ldx #FCB_Size
FAT_Clear_FCB1
		stz FCB,x
		dex
		bpl FAT_Clear_FCB1 	
		rts


;******************
; Calculate Cluster to First Block from FCB_Current_Cluster to Flopy_Blk_Low and Floppy_Blk_High
; FCB_FirstSecOfCluster = ((N – 2) * DPB_SectorsPerCluster(1Byte)) + DPB_FirstDataSector (2Byte)
;******************

FAT_Cluster_to_Block
; Calculate N-2
		sec				; set carry
		lda FCB_Current_Cluster
		sbc #2				; Substract 2
		sta FCB_FirstSecOfCluster
		lda FCB_Current_Cluster+1
		sbc #0
		sta FCB_FirstSecOfCluster+1

; FCB_FirstSecOfCluster (2Byte) * DPB_SectorsPerCluster(1Byte)
		lda FCB_FirstSecOfCluster
		sta Operand1
		lda FCB_FirstSecOfCluster+1
		sta Operand2
		lda DPB_SectorsPerCluster
		sta Operand3
; Operand1 = Low  Operand2 = High x Operand3
		jsr Multiply16
		sta FCB_FirstSecOfCluster
		sty FCB_FirstSecOfCluster+1
; DPB_FirstDataSector + FCB_FirstSecOfCluster
		clc				; clear carry
		lda FCB_FirstSecOfCluster
		adc DPB_FirstDataSector
		sta FCB_FirstSecOfCluster	; store sum of LSBs
		lda FCB_FirstSecOfCluster+1
		adc DPB_FirstDataSector+1	; add the MSBs using carry from
		sta FCB_FirstSecOfCluster+1	; the previous calculation
		rts

;******************
; Get next FAT12 cluster
; Calculate position in fat and load sector
; write next cluster to FCB_Current_Cluster
; if it is the last cluster set carry 
;
;******************
FAT_GetNextFat12Cluster

; FCB_FATOffset(Operand1,2) = FCB_Current_Cluster (2Byte) + (FCB_Current_Cluster / 2) --> only FAT12

		lda FCB_Current_Cluster+1 ;(FCB_Current_Cluster / 2)
		clc
		ror
		sta Operand2
		lda FCB_Current_Cluster
		ror
		sta Operand1
		clc
		adc FCB_Current_Cluster	; + FCB_Current_Cluster (2Byte)
		sta FCB_FATOffset
		sta Operand1
		lda Operand2
		adc FCB_Current_Cluster+1
		sta FCB_FATOffset+1
		sta Operand2

; FCB_ThisFATSecNum (Operand1,2) = DPB_ReservedSectors (2 Byte) + (FCB_FATOffset(Operand1,2) / DPB_BytesPerSector(Operand3,4));

		lda DPB_BytesPerSector	; (FATOffset(Operand1,2) / DPB_BytesPerSector(Operand3,4)
		sta Operand3
		lda DPB_BytesPerSector+1
		sta Operand4
		jsr Divide16

		lda DPB_ReservedSectors	; + DPB_ReservedSectors (2 Byte)
		clc
		adc Operand1
		sta FCB_ThisFATSecNum
		lda DPB_ReservedSectors+1
		adc Operand2
		sta FCB_ThisFATSecNum+1

; FCB_ThisFATEntOffset = (Remainder)(FCB_FATOffset (2Byte) / DPB_BytesPerSector (2Byte));

		lda FCB_FATOffset
		sta Operand1
		lda FCB_FATOffset+1
		sta Operand2
		lda DPB_BytesPerSector
		sta Operand3
		lda DPB_BytesPerSector+1
		sta Operand4
		jsr Divide16
		lda Operand5
		sta FCB_ThisFATEntOffset
		lda Operand6
		sta FCB_ThisFATEntOffset+1

; Load FAT Sector into buffer
		
		lda #<FatSectorBuffer
		sta PTR_FLOPPY_L
		lda #>FatSectorBuffer		; Set sector buffer start address
		sta PTR_FLOPPY_H
		lda FCB_ThisFATSecNum
		cmp DPB_FatSectorBufferBlk	; check if sector already loaded 
		bne FAT_GetNextFat12Cluster1
		lda FCB_ThisFATSecNum+1
		cmp DPB_FatSectorBufferBlk+1
		beq FAT_GetNextFat12Cluster3
FAT_GetNextFat12Cluster1
		lda FCB_ThisFATSecNum
		sta Floppy_Blk_Low
		lda FCB_ThisFATSecNum+1
		sta Floppy_Blk_High		; TODO: check if sector already loaded 
		jsr Floppy_LBA			; Parameter berechnen 
		jsr Floppy_CHS_Read		; Read Sector. 
		bcc FAT_GetNextFat12Cluster2
		rts
FAT_GetNextFat12Cluster2
		lda Floppy_Blk_Low
		sta DPB_FatSectorBufferBlk
		lda Floppy_Blk_High
		sta DPB_FatSectorBufferBlk+1		
FAT_GetNextFat12Cluster3
		lda #<FatSectorBuffer	
		sta PTR_FLOPPY_L
		lda #>FatSectorBuffer		; Set sector buffer start address again 
		sta PTR_FLOPPY_H

; Set Pointer:  FAT12ClusEntryVal = *((WORD *) &SecBuff[ThisFATEntOffset]); 
		lda PTR_FLOPPY_L
		clc
		adc FCB_ThisFATEntOffset
		sta PTR_FLOPPY_L
		lda PTR_FLOPPY_H
		adc FCB_ThisFATEntOffset+1
		sta PTR_FLOPPY_H
; check for boundary case
; If(FCB_ThisFATEntOffset == (DPB_BytesPerSector – 1))
		lda DPB_BytesPerSector
		sec
		sbc #1
		sta Operand1
		lda DPB_BytesPerSector+1
		sbc #0
		sta Operand2
		lda Operand1
		cmp FCB_ThisFATEntOffset
		bne FAT_GetNextFat12Cluster4
		lda Operand2
		cmp FCB_ThisFATEntOffset+1
		bne FAT_GetNextFat12Cluster4
		jmp FAT_GetNextFat12ClusterBoundary

; If(FCB_Current_Cluster & 0x0001) --> Only for FAT12, check odd / even
FAT_GetNextFat12Cluster4
		lda FCB_Current_Cluster
		and #$01
		bne FAT_GetNextFat12ClusterOdd
		jmp FAT_GetNextFat12ClusterEven

;FAT12ClusEntryVal = FAT12ClusEntryVal >> 4;	/* Cluster number is ODD */
FAT_GetNextFat12ClusterOdd
		ldy #0
		lda (PTR_FLOPPY_L),y
		sta FCB_Current_Cluster
		iny
		lda (PTR_FLOPPY_L),y
		sta FCB_Current_Cluster+1
FAT_GetNextFat12ClusterOdd2
		ror FCB_Current_Cluster+1
		ror FCB_Current_Cluster
		ror FCB_Current_Cluster+1
		ror FCB_Current_Cluster
		ror FCB_Current_Cluster+1
		ror FCB_Current_Cluster
		ror FCB_Current_Cluster+1
		ror FCB_Current_Cluster
		lda FCB_Current_Cluster+1
		and #$0F
		sta FCB_Current_Cluster+1
		jmp FAT_GetNextFat12ClusterCheckEnd
;Else
;FAT12ClusEntryVal = FAT12ClusEntryVal & 0x0FFF;	/* Cluster number is EVEN */
FAT_GetNextFat12ClusterEven
		ldy #0
		lda (PTR_FLOPPY_L),y
		sta FCB_Current_Cluster
		iny
		lda (PTR_FLOPPY_L),y
		sta FCB_Current_Cluster+1
FAT_GetNextFat12ClusterEven2
		lda FCB_Current_Cluster+1
		and #$0f
		sta FCB_Current_Cluster+1
		sta FCB_Current_Cluster+1
		jmp FAT_GetNextFat12ClusterCheckEnd

; If sector boundary case
FAT_GetNextFat12ClusterBoundary
		lda FCB_Current_Cluster
		pha				; Save old FCB_Current_Cluster for later comparing for odd /even
		ldy #0
		lda (PTR_FLOPPY_L),y
		sta FCB_Current_Cluster
		lda #<FatSectorBuffer
		sta PTR_FLOPPY_L
		lda #>FatSectorBuffer		; Set sector buffer start address
		sta PTR_FLOPPY_H
		lda FCB_ThisFATSecNum
		clc
		adc #1
		sta Floppy_Blk_Low
		lda FCB_ThisFATSecNum+1
		adc #0
		sta Floppy_Blk_High
		jsr Floppy_LBA			; Calculate parameters
		jsr Floppy_CHS_Read		; Read Sector 
		bcc FAT_GetNextFat12ClusterBoundary2
		rts
FAT_GetNextFat12ClusterBoundary2
		lda Floppy_Blk_Low
		sta DPB_FatSectorBufferBlk
		lda Floppy_Blk_High
		sta DPB_FatSectorBufferBlk+1
		lda #<FatSectorBuffer		; TODO: FAT sector buffer
		sta PTR_FLOPPY_L
		lda #>FatSectorBuffer		; Set sector buffer start address again
		sta PTR_FLOPPY_H
		ldy #0
		lda (PTR_FLOPPY_L),y
		sta FCB_Current_Cluster+1
		pla
		and #$01
		beq FAT_GetNextFat12ClusterBoundary3
		jmp FAT_GetNextFat12ClusterOdd2
FAT_GetNextFat12ClusterBoundary3
		jmp FAT_GetNextFat12ClusterEven2

FAT_GetNextFat12ClusterCheckEnd
; Check for EOF
		lda FCB_Current_Cluster+1
		cmp #$0F
		bne FAT_GetNextFat12ClusterExit
		lda FCB_Current_Cluster
		cmp #$F7
		bcs FAT_GetNextFat12ClusterErrorExit

FAT_GetNextFat12ClusterExit
		clc
		rts
FAT_GetNextFat12ClusterErrorExit
		sec
		rts


;******************
; FAT_LoadRootDirSec
; each entry has 32 Byte, every sector has 16 entrys
; Vector "FAT_CurrentDirEntry" is the current directory entry
; A 720kB Floppy has 112 directory entrys = 7 sectors
; FAT_CurrentDirEntry:
; 7 6 5 4 3 2 1 0
; | | | | | | | |
; | | |	| | -------> 8-bit index, 8*32 = 256 byte = 8-bit index register y
; | | | | ---------> 512 byte = low / high sector buffer
; | ---------------> Block number 0..8 (+ dir start)
; -----------------> not used
;******************


FAT_LoadRootDirSec
		lda #<SectorBuffer
		sta PTR_FLOPPY_L
		lda #>SectorBuffer
		sta PTR_FLOPPY_H
		lda FAT_CurrentDirEntry
		and #$70			; Maskiere LBA Nummer aus FAT_CurrentDirEntry
		lsr
		lsr
		lsr
		lsr

		adc DPB_FirstRootDirSecNum	; 8 = start sector for Directory at 720kB Floppys = 7 LBA
		sta Floppy_Blk_Low		; Floppy-Block nummer 0
		stz Floppy_Blk_High		; Floppy-Block nummer 0
		jsr Floppy_LBA			; Parameter berechnen

		jsr Floppy_CHS_Read		; Read Sector
		bcc FAT_LoadRootDirSec2
		rts
FAT_LoadRootDirSec2
		lda #<SectorBuffer		; Set pointer to start again
		sta PTR_FLOPPY_L
		lda #>SectorBuffer
		sta PTR_FLOPPY_H

FAT_LoadRootDirSec3
		rts
;******************
; Set directory pointer
;******************

FAT_SetDirectoryPTR
		lda FAT_CurrentDirEntry	; calculate PTR-FLOOPY based on FAT_CurrentDirEntry
 		and #$07		; mask 256/8 byte index
		asl			; *2 Indexzeiger berechnen
		asl			; *4
		asl			; *8
		asl			; *16
		asl			; *32
		clc
		adc #<SectorBuffer
		sta PTR_FLOPPY_L
		lda FAT_CurrentDirEntry
		and #$08
		ror
		ror
		ror
		adc #>SectorBuffer
		sta PTR_FLOPPY_H
		rts

;******************
; Print Dir Entry
;******************

FAT_PrintDirectoryEntry
		ldx #8				; 8 character filename
		ldy #$0B
		lda (PTR_FLOPPY_L),y		; load attribute byte
		cmp #DOS_ATTR_LONG_NAME		; if "ATTR_LONG_NAME" then next entry
		beq FAT_PrintDirectoryEntry2
		and #%00000110			; mask Attr hiden or attr system
		beq FAT_PrintDirectoryEntry3
FAT_PrintDirectoryEntry2
		jmp FAT_PrintDirectoryEntryExit
FAT_PrintDirectoryEntry3
		ldy #0
		lda (PTR_FLOPPY_L),y		; Test first character for $e5 or $00
		cmp #$e5			; if $e5 then next entry
		beq FAT_PrintDirectoryEntry2
		cmp #$00			; if $00 it is the last entry
		bne FAT_PrintDirectoryEntry6
FAT_PrintDirectoryEntry5
		jmp FAT_PrintDirectoryEntryEndExit
FAT_PrintDirectoryEntry6
		lda (PTR_FLOPPY_L),y		; print first 8 characters
		phx
		phy
		jsr Print_CHR
		ply
		plx
		iny
		dex
		bne FAT_PrintDirectoryEntry6
		lda #"."
		phy
		jsr Print_CHR
		ply
		ldx #3
FAT_PrintDirectoryEntry7
		lda (PTR_FLOPPY_L),y		; print 3 characters extension
		phx
		phy
		jsr Print_CHR
		ply
		plx
		iny
		dex
		bne FAT_PrintDirectoryEntry7

		lda #SP
		jsr Print_CHR
; Attribute anzeigen
FAT_PrintDirectoryEntry8
		ldy #DOS_Offset_DIR_Attr
		lda (PTR_FLOPPY_L),y		; Get attribute
		sta FAT_CurrentAttr		; temp. save to FAT_CurrentAttr
		bit #0x10			; ATTR_DIRECTORY
		beq FAT_PrintDirectoryEntry9
		jmp FAT_PrintDirectoryEntry23	; then skip the rest
FAT_PrintDirectoryEntry9
		bit #DOS_ATTR_READ_ONLY		; ATTR_READ_ONLY
		beq FAT_PrintDirectoryEntry10
		lda #"R"
		jsr Print_CHR
		jmp FAT_PrintDirectoryEntry11
FAT_PrintDirectoryEntry10
		lda #"-"
		jsr Print_CHR
FAT_PrintDirectoryEntry11
		lda FAT_CurrentAttr
		bit #DOS_ATTR_HIDDEN		; ATTR_HIDDEN
		beq FAT_PrintDirectoryEntry12
		lda #"H"
		jsr Print_CHR
		jmp FAT_PrintDirectoryEntry13
FAT_PrintDirectoryEntry12
		lda #"-"
		jsr Print_CHR
FAT_PrintDirectoryEntry13
		lda FAT_CurrentAttr
		bit #DOS_ATTR_SYSTEM		; ATTR_SYSTEM
		beq FAT_PrintDirectoryEntry14
		lda #"S"
		jsr Print_CHR
		jmp FAT_PrintDirectoryEntry15
FAT_PrintDirectoryEntry14
		lda #"-"
		jsr Print_CHR
FAT_PrintDirectoryEntry15
		lda FAT_CurrentAttr
		bit #DOS_ATTR_VOLUME_ID		;ATTR_VOLUME_ID
		beq FAT_PrintDirectoryEntry16
		lda #"V"
		jsr Print_CHR
		jmp FAT_PrintDirectoryEntry17
FAT_PrintDirectoryEntry16
		lda #"-"
		jsr Print_CHR
FAT_PrintDirectoryEntry17
		lda FAT_CurrentAttr
		bit #DOS_ATTR_ARCHIVE		;ATTR_ARCHIVE
		beq FAT_PrintDirectoryEntry18
		lda #"A"
		jsr Print_CHR
		jmp FAT_PrintDirectoryEntry19
FAT_PrintDirectoryEntry18
		lda #"-"
		jsr Print_CHR
FAT_PrintDirectoryEntry19
		lda #SP
		jsr Print_CHR

; Dateigröße berechnen
		ldy #DOS_Offset_DIR_FileSize	; Pointer to file size
		ldx #0
FAT_PrintDirectoryEntry20
		lda (PTR_FLOPPY_L),y		; cnvert file size to BCD
		sta BCDINBuffer0,x
		iny
		inx
		cpx #4				; 4 Byte file size
		bne FAT_PrintDirectoryEntry20
		jsr Bin2BCD
FAT_PrintDirectoryEntry21
		jsr Print_File_Size_SP		; print file size
		lda #CR
		jsr Print_CHR
		clc
		rts
FAT_PrintDirectoryEntry22
		jmp FAT_PrintDirectoryEntryExit
FAT_PrintDirectoryEntry23
		lda #<DIR_Text
		sta PTR_String_L
		lda #>DIR_Text
		sta PTR_String_H
		jsr Print_String

FAT_PrintDirectoryEntryExit
		clc
		rts
FAT_PrintDirectoryEntryEndExit
		sec
		rts


DIR_Text
!text "<DIR>",CR,00


