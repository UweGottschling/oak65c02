;*************************
; Disk error handling code
; (c) 2020 Uwe Gottschling
;*************************
; Display text message for error code

; TODO: Print Error Message and current Drive: Floppy_DriveNo
; TODO: If Read Error try to read again ?
; TODO: Check and print: DOS_CTRL_Error_Code 
; 

DOS_PrintError
		lda #<DOS_ErrorMessageText
		sta PTR_String_L
		lda #>DOS_ErrorMessageText
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		jsr DOS_PrintCurrentDrive
		lda #LF
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		lda DOS_ErrorCode
		sec
		sbc #1
		asl
		cmp #DOS_Erro_LUT_End-DOS_Error_LUT
		bcs DOS_PrintError2
		tax
		lda DOS_Error_LUT,x
		sta PTR_String_L
		lda DOS_Error_LUT+1,x
		sta PTR_String_H
		ldx #OS_Print_String_FN				
		jsr OS_Function_Call
		stz DOS_ErrorCode	; Clear current error code
		rts
DOS_PrintError2
		lda #<DOS_InternalErrorText
		sta PTR_String_L
		lda #>DOS_InternalErrorText
		sta PTR_String_H
		ldx #OS_Print_String_FN				
		jsr OS_Function_Call
		lda DOS_ErrorCode
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call		
		stz DOS_ErrorCode	; Clear current error code
		rts

DOS_ErrorMessageText
!text LF,"DOS error drive ",00
DOS_ReadErrorText
!text "Read error",LF,00 		;1
DOS_MediaIDErrorText 
!text "Unknown media ID",LF,00		;2
DOS_FileNotFoundErrorText
!text "File not found",LF,00		;3
DOS_NoFATBootSectorText 
!text "No FAT boot sector",LF,00	;4
DOS_FATBadErrorText	
!text "File allocation table bad",LF,00 ;5
DOS_Wrong_Name_Text
!text "Wrong delimiter in name",LF,00	;6
DOS_InternalErrorText 
!text "DOS internal error",LF,00

DOS_Error_LUT
!word DOS_ReadErrorText			;1
!word DOS_MediaIDErrorText		;2
!word DOS_FileNotFoundErrorText		;3
!word DOS_NoFATBootSectorText		;4
!word DOS_FATBadErrorText		;5
!word DOS_Wrong_Name_Text		;6
DOS_Erro_LUT_End



