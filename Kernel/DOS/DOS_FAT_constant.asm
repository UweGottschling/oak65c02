; (C) 2018 Uwe Gottschling
; DOS FAT constant

; Offset directory entry structure
FAT_Offset_DIR_Attr		= 11
; DOS_Offset_DIR_CrtTimeTenth	= 13
FAT_Offset_DIR_FstClusHI	= 20
; DOS_Offset_DIR_WrtTime	= 22
; DOS_Offset_DIR_WrtDate	= 24
; DOS_Offset_DIR_FstClusLO	= 26
FAT_Offset_DIR_FileSize		= 28
; offset to first available cluster
FAT_Offset_FirstAvCluster	= 02	; The first data cluster is cluster 2. 


; Offset BIOS parameter block
FAT_BPB_Offset_BytsPerSec	= 11
FAT_BPB_Offset_Media		= $015
FAT_BPB_Offset_Signature	= $1FE	

;Media ID
FAT_Media_Signature_720kb	= $F9

; File attributes
FAT_ATTR_READ_ONLY		= $01
FAT_ATTR_HIDDEN			= $02
FAT_ATTR_SYSTEM			= $04	
FAT_ATTR_VOLUME_ID		= $08
FAT_ATTR_ARCHIVE		= $20
FAT_ATTR_LONG_NAME		= $0F

; Error codes
DOS_ErrorNo_Read		= 1
DOS_ErrorNo_BadMediaID		= 2
DOS_ErrorNo_FileNotFound	= 3
DOS_ErrorNo_FATBootSector 	= 4
DOS_ErrorNo_FATBad		= 5
DOS_ErrorNo_WrongName		= 6

;
; 
; Media ID, currently only F9 is supported
;----------------------------------------------------------------------
;|                            | 1DD, 9  | 2DD, 9  | 1DD, 8  | 2DD, 8  |
;|                            | sectors | sectors | sectors | sectors |
;|----------------------------+---------+---------+---------+---------|
;| media ID                   |   $F8   |   $F9   |   $FA   |   $FB   |
;| number of sides            |    1    |    2    |    1    |    2    |
;| tracks per side            |   80    |   80    |   80    |   80    |
;| sectors per track          |    9    |    9    |    8    |    8    |
;| bytes per sector           |   512   |   512   |   512   |   512   |
;| cluster size (in sectors)  |    2    |    2    |    2    |    2    |
;| FAT size (in sectors)      |    2    |    3    |    1    |    2    |
;| number of FATs             |    2    |    2    |    2    |    2    |
;| number of recordable files |   112   |   112   |   112   |   112   |
;----------------------------------------------------------------------
;
; 2DD, 9 sectors = capacity = 512bytes*2*9*80 = 737280 bytes
; 737280 bytes - (bootsector, 2xFATx3 Sectors, - 7 sector root dir) = 730112 bytes available for data = 713 clusters
;
; Example full 720kb floppy disc
; FAT12 stucture:
; $0200: $F9 $FF $FF $03 $40 $00 $50 $60 $00 ...
; cluster 0: $FF9 = identic to media ID
; cluster 1: $FFF = internaly reserved 
; 
; $062E : $2C $FF
; $0630 : $0F ; end of FAT, 1/2 byte used
; 
; FAT data: $431 bytes ==> 1072,5 bytes = 715 clusters
; First 2 clusters are reserved = 713 clusters
; First directory entry "DIR_FstClusLO" = 0002 --> first available cluster
; 
; Fist data sector numer = $0E 
; FCB_FirstSecOfCluster = ((N – 2) * DPB_SectorsPerCluster(1Byte)) + DPB_FirstDataSector (2Byte); N = data cluster number 
; $0E (fist data sector numer) * $200 (bytes per sector) = $1C00 --> start of space for user data 
;
; If only one file with 5 bytes is on the disc, the fat look like this:
;
; $0200: $F9 $FF $FF $FF $0F
; cluster 0: $FF9 identic to media ID
; cluster 1: $FFF internaly reserved 
; cluster 2: $FFF EOF; used for first file with 5 bytes
; 
;FAT 32 Byte Directory Entry Structure:
;Byte	Length
;offset	(bytes)		Contents 
;-----------------------------------------------------------------
;0x00	8 			Short file name (padded with spaces) 
;0x08 	3 			Short file extension (padded with spaces) 
;0x0B 	1 			File Attributes 
;					0x01 = read only, 0x02 = hide, 0x04 = system, 0x08 = volume ID, 
;					0x10 =  dir, 0x20 = archive, 0x01|0x02|0x04|0x08 = long filename
;0x0C 	1 			Reserved for use by Windows NT
;0x0E 	2			Creation time hours, minutes, seconds
;0x10 	2			Creation time year, month, day
;0x14 	2 			High word first cluster number (FAT 12 = 0)
;0x16 	2 			Last modified time 
;0x18 	2			Last modified date
;0x1A 	2			Low word of file in clusters 
;0x1C 	4 			File size in bytes.








