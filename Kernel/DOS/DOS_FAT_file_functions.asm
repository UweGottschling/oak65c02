; DOS fat functions 
; (C) 2018 Uwe Gottschling
;
;******************
; Load sector: FCB_FirstSecOfCluster + FCB_Sector_Count
; If it is the root directory: DPB_FirstRootDirSecNum + FCB_Sector_Count
; Carry set if read error
;******************

FAT_Load_Next_File_Sector
		ldy #FCB_Current_Cluster
		lda (PTR_DOS_FCB_L),y
		bne FAT_Load_Next_File_Sector1	; Check for cluster 0 (root directory)
		iny
		lda (PTR_DOS_FCB_L),y
		bne FAT_Load_Next_File_Sector1	
		ldy #FCB_Sector_Count
		lda (PTR_DOS_FCB_L),y	
		ldy #DPB_FirstRootDirSecNum
		clc
		adc (PTR_DOS_DPB_L),y		; 8 = start sector for directory at 720kB Floppys --> 7 LBA
		sta Floppy_Blk_Low		; Floppy-LBA nummer 0
		stz Floppy_Blk_High		
		bra FAT_Load_Next_File_Sector2
FAT_Load_Next_File_Sector1
		ldy #FCB_FirstSecOfCluster	; Add sector count to first sector of cluster
		lda (PTR_DOS_FCB_L),y
		ldy #FCB_Sector_Count		
		clc
		adc (PTR_DOS_FCB_L),y
		sta Floppy_Blk_Low
		ldy #FCB_FirstSecOfCluster+1
		lda (PTR_DOS_FCB_L),y
		adc #0
		sta Floppy_Blk_High
FAT_Load_Next_File_Sector2
		jsr FAT_LoadDataSector	
		rts

;******************
; Calculate next sector
; Increase FCB_Sector_Count. If FCB_Sector_Count > DPB_SectorsPerCluster then set carry and FCB_Sector_Count = 0
; If current cluster = 0 then care about DPB_SectorsPerCluster
;******************

FAT_Calc_Next_File_Sector
		ldy #FCB_Current_Cluster
		lda (PTR_DOS_FCB_L),y
		bne FAT_Calc_Next_File_Sector2
		iny
		lda (PTR_DOS_FCB_L),y
		bne FAT_Calc_Next_File_Sector2	; Check for cluster 0 (root directory)
		ldy #FCB_Sector_Count		; If root directory count only sectors
		lda (PTR_DOS_FCB_L),y
		inc
		sta (PTR_DOS_FCB_L),y
		bra FAT_Calc_Next_File_Sector3
FAT_Calc_Next_File_Sector2
		ldy #FCB_Sector_Count		; Increase sector
		lda (PTR_DOS_FCB_L),y
		inc
		sta (PTR_DOS_FCB_L),y
		ldy #DPB_SectorsPerCluster
		cmp (PTR_DOS_DPB_L),y
		bcc FAT_Calc_Next_File_Sector3	; Compare with sectors per cluster 
		ldy #FCB_Sector_Count
		lda #0
		sta (PTR_DOS_FCB_L),y
		jsr FAT_GetNextFat12Cluster					
		bcs FAT_Calc_Next_File_Sector_Error_Exit
		jsr FAT_Cluster_to_Block
FAT_Calc_Next_File_Sector3
		clc
FAT_Calc_Next_File_Sector_Error_Exit
		rts

; Cluster Values FAT12:
; 000 		--> free cluster
; 001 		--> reserved cluster
; 002...FF6 --> user data
; FF7 		--> bad cluster
; FF8...FFF --> end marker
;******************
; Position 	FAT12 Value
; 	0	aaaaaaaa --> low a
;	1	bbbbaaaa --> low b, high a
;	2	bbbbbbbb --> high b
; Cluster value b is odd and need to be shift 4 times right
; Cluster value a is even and need to mask out with 0x0fff

;******************
; Calculate cluster to first sector
; input: opened FCB
; FCB_FirstSecOfCluster = ((N – 2) * DPB_SectorsPerCluster(1Byte)) + DPB_FirstDataSector (2Byte)
; N = data cluster number 
;******************

FAT_Cluster_to_Block
		ldy #FCB_Current_Cluster	; Calculate N-2
		lda (PTR_DOS_FCB_L),y
		sec				
		sbc #2				
		ldy #FCB_FirstSecOfCluster
		sta (PTR_DOS_FCB_L),y
		ldy #FCB_Current_Cluster+1
		lda (PTR_DOS_FCB_L),y
		sbc #0
		ldy #FCB_FirstSecOfCluster+1
		sta (PTR_DOS_FCB_L),y
		ldy #FCB_FirstSecOfCluster	; FCB_FirstSecOfCluster (2Byte) * DPB_SectorsPerCluster(1Byte)
		lda (PTR_DOS_FCB_L),y
		sta Operand1
		iny
		lda (PTR_DOS_FCB_L),y
		sta Operand2
		ldy #DPB_SectorsPerCluster
		lda (PTR_DOS_DPB_L),y
		sta Operand3
		ldx #OS_Multiply16_FN		
		jsr OS_Function_Call		; Operand1 = Low  Operand2 = High x Operand3
		phy
		ldy #FCB_FirstSecOfCluster
		sta (PTR_DOS_FCB_L),y
		pla
		iny
		sta (PTR_DOS_FCB_L),y
		ldy #FCB_FirstSecOfCluster	
		lda (PTR_DOS_FCB_L),y
		ldy #DPB_FirstDataSector
		clc				
 		adc (PTR_DOS_DPB_L),y		; DPB_FirstDataSector + FCB_FirstSecOfCluster
		ldy #FCB_FirstSecOfCluster	
		sta (PTR_DOS_FCB_L),y		; Store sum of LSBs
		iny
		lda (PTR_DOS_FCB_L),y
		ldy #DPB_FirstDataSector+1
		adc (PTR_DOS_DPB_L),y		; Add the MSBs using carry from
		ldy #FCB_FirstSecOfCluster+1	; the previous calculation
		sta (PTR_DOS_FCB_L),y
		rts

;******************
; Get next FAT12 cluster
; Calculate position in fat and load FAT sector,
; write next cluster to FCB_Current_Cluster,
;******************

FAT_GetNextFat12Cluster
		jsr FAT_GetFAT12Offset
		jsr FAT_GetThisFAT12SecNum	
		jsr FAT_GetThisFAT12EntOffset
		jsr FAT_LoadFATSector
		bcs FAT_GetNextFat12ClusterExit2
		jsr FAT_GetFAT12ClusEntryVal
		bcs FAT_GetNextFat12ClusterExit2
		jsr FAT_CheckFAT12EOF
		clc				; TODO: end of cluster (>= $FF7) no checked !
FAT_GetNextFat12ClusterExit2
		rts

;******************
; GetFAT12Offset
; FCB_FATOffset(Operand1,2) = FCB_Current_Cluster (2Byte) + (FCB_Current_Cluster / 2) 
;******************

FAT_GetFAT12Offset
		ldy #FCB_Current_Cluster+1	; (FCB_Current_Cluster / 2)
		lda (PTR_DOS_FCB_L),y
		clc
		ror
		sta Operand2
		dey
		lda (PTR_DOS_FCB_L),y
		ror
		sta Operand1
		ldy #FCB_Current_Cluster
		clc
		adc (PTR_DOS_FCB_L),y		; + FCB_Current_Cluster (2Byte)
		ldy #FCB_FATOffset
		sta (PTR_DOS_FCB_L),y
		sta Operand1
		lda Operand2
		ldy #FCB_Current_Cluster+1
		adc (PTR_DOS_FCB_L),y
		ldy #FCB_FATOffset+1
		sta (PTR_DOS_FCB_L),y
		sta Operand2
		rts
		
;******************
; GetThisFAT12SecNum
; FCB_ThisFATSecNum (Operand1,2) = DPB_ReservedSectors (2 Byte) + (FCB_FATOffset(Operand1,2) / DPB_BytesPerSector(Operand3,4))
; Calculate where in the FAT(s) is the entry for that cluster number
;******************

FAT_GetThisFAT12SecNum
		ldy #DPB_BytesPerSector 	; (FATOffset(Operand1,2) / DPB_BytesPerSector(Operand3,4)
		lda (PTR_DOS_DPB_L),y
		sta Operand3
		iny
		lda (PTR_DOS_DPB_L),y
		sta Operand4
		ldx #OS_Divide16_FN
		jsr OS_Function_Call
		ldy #DPB_ReservedSectors 	; + DPB_ReservedSectors (2 Byte)
		lda (PTR_DOS_DPB_L),y
		clc
		adc Operand1
		ldy #FCB_ThisFATSecNum
		sta (PTR_DOS_FCB_L),y
		ldy #DPB_ReservedSectors+1
		lda (PTR_DOS_DPB_L),y
		adc Operand2
		ldy #FCB_ThisFATSecNum+1
		sta (PTR_DOS_FCB_L),y
		rts
		
;******************
; GetThisFAT12EntOffset
; FCB_ThisFATEntOffset = (Remainder)(FCB_FATOffset (2Byte) / DPB_BytesPerSector (2Byte))
;******************

FAT_GetThisFAT12EntOffset
		ldy #FCB_FATOffset
		lda (PTR_DOS_FCB_L),y
		sta Operand1
		iny
		lda (PTR_DOS_FCB_L),y
		sta Operand2
		ldy #DPB_BytesPerSector
		lda (PTR_DOS_DPB_L),y
		sta Operand3
		iny
		lda (PTR_DOS_DPB_L),y
		sta Operand4
		ldx #OS_Divide16_FN
		jsr OS_Function_Call
		lda Operand5
		ldy #FCB_ThisFATEntOffset
		sta (PTR_DOS_FCB_L),y
		lda Operand6
		iny
		sta (PTR_DOS_FCB_L),y
		rts
		
;******************
; LoadFATSector
; Load FCB_ThisFATSecNum into DOS_FATSectorBuffer
; TODO: if FCB_ThisFATSecNum > DPB_SectorsPerFAT + DPB_ReservedSectors --> FAT Error
;******************

FAT_LoadFATSector
		lda Floppy_DriveNo		; check it the same drive number, $FF = invalid
		cmp DOS_FATSectorBuffDrive	
		bne FAT_LoadFATSector1
		ldy #FCB_ThisFATSecNum
		lda (PTR_DOS_FCB_L),y
		cmp DOS_FATSectorBuffLBA_L	; check if sector already loaded
		bne FAT_LoadFATSector1
		ldy #FCB_ThisFATSecNum+1
		lda (PTR_DOS_FCB_L),y
		cmp DOS_FATSectorBuffLBA_H	
		beq FAT_LoadFATSector3		; If this is all true jump without loading
FAT_LoadFATSector1
		lda #<DOS_FATSectorBuff
		sta PTR_FLOPPY_L
		lda #>DOS_FATSectorBuff	
		sta PTR_FLOPPY_H		; set sector buffer start address
		ldy #FCB_ThisFATSecNum
		lda (PTR_DOS_FCB_L),y
		sta Floppy_Blk_Low
		iny
		lda (PTR_DOS_FCB_L),y
		sta Floppy_Blk_High		
		jsr Floppy_LBA2CHS		; LBA to CHS
		jsr FloppyReadCHS		; read Sector
		bcc FAT_LoadFATSector2	
		lda #$FF			; set FAT buffer blk illegal in case of floppy read error
		sta DOS_FATSectorBuffDrive
		rts
FAT_LoadFATSector2
		lda Floppy_Blk_Low
		sta DOS_FATSectorBuffLBA_L 	
		lda Floppy_Blk_High
		sta DOS_FATSectorBuffLBA_H 	; store current sector in sector buffer
		lda Floppy_DriveNo
		sta DOS_FATSectorBuffDrive	; store current drive
FAT_LoadFATSector3
		clc
		rts

;******************
; Load data sector from Floppy_blk into DOS_DataSectorBuff
;
;******************

FAT_LoadDataSector
		lda Floppy_DriveNo		
		cmp DOS_DataSectorBuffDrive	; check drive number; FF = empty
		bne FAT_LoadDataSector1
		lda Floppy_Blk_Low
		cmp DOS_DataSectorBuffLBA_L	; check if sector already loaded
		bne FAT_LoadDataSector1
		lda Floppy_Blk_High
		cmp DOS_DataSectorBuffLBA_H	
		beq FAT_LoadDataSector3		; if it is true jump without loading
FAT_LoadDataSector1
		lda #<DOS_DataSectorBuff
		sta PTR_FLOPPY_L
		lda #>DOS_DataSectorBuff	
		sta PTR_FLOPPY_H		; set sector buffer start address	
		jsr Floppy_LBA2CHS		; LBA to CHS
		jsr FloppyReadCHS		; read Sector
		bcc FAT_LoadDataSector2
		lda #$FF			; set buffer status to illegal in case of floppy read error
		sta DOS_DataSectorBuffDrive
		rts
FAT_LoadDataSector2
		lda Floppy_Blk_Low
		sta DOS_DataSectorBuffLBA_L 	
		lda Floppy_Blk_High
		sta DOS_DataSectorBuffLBA_H 	; store current sector in sector buffer
		lda Floppy_DriveNo
		sta DOS_DataSectorBuffDrive	; store current drive
FAT_LoadDataSector3
		clc
		rts

;******************
; Get the next FAT12 cluster entry value
; Set pointer: FAT12ClusEntryVal = *((WORD *) &SecBuff[ThisFATEntOffset]) 
;******************

FAT_GetFAT12ClusEntryVal
		lda #<DOS_FATSectorBuff
		sta PTR_FLOPPY_L
		lda #>DOS_FATSectorBuff		; set FAT sector buffer PRT to DOS_FATSectorBuffer start address
		sta PTR_FLOPPY_H
		lda PTR_FLOPPY_L
		ldy #FCB_ThisFATEntOffset	; add FCB_ThisFATEntOffset to PTR_FLOPPY
		clc
		adc (PTR_DOS_FCB_L),y
		sta PTR_FLOPPY_L
		lda PTR_FLOPPY_H
		iny
		adc (PTR_DOS_FCB_L),y
		sta PTR_FLOPPY_H
; check for sector boundary case
; If(FCB_ThisFATEntOffset == (DPB_BytesPerSector – 1))
		ldy #DPB_BytesPerSector
		lda (PTR_DOS_DPB_L),y
		sec
		sbc #1
		sta Operand1
		iny
		lda (PTR_DOS_DPB_L),y
		sbc #0
		sta Operand2
		ldy #FCB_ThisFATEntOffset+1
		cmp (PTR_DOS_FCB_L),y		
		bne FAT_GetFAT12ClusEntryVal4
		lda Operand1				; TODO: Check first Operand 2, direct with A ?
		dey
		cmp (PTR_DOS_FCB_L),y
		bne FAT_GetFAT12ClusEntryVal4
FAT_GetFAT12ClusEntryValBoundary			; in case of sector boundary
		ldy #FCB_Current_Cluster
		lda (PTR_DOS_FCB_L),y
		pha					; save old FCB_Current_Cluster for later comparing for odd /even!
		lda (PTR_FLOPPY_L)
		ldy #FCB_Current_Cluster
		sta (PTR_DOS_FCB_L),y
		ldy #FCB_ThisFATSecNum
		lda (PTR_DOS_FCB_L),y
		clc
		adc #1
		sta (PTR_DOS_FCB_L),y
		iny
		lda (PTR_DOS_FCB_L),y
		adc #0
		sta (PTR_DOS_FCB_L),y
		jsr FAT_LoadFATSector			; load next FAT sector		
		bcc FAT_GetFAT12ClusEntryValBoundary2
		plx					; stack pointer correction, not to A because there is the errror code 		
		rts					; if something goes wrong then RTS
FAT_GetFAT12ClusEntryValBoundary2
		lda #<DOS_FATSectorBuff
		sta PTR_FLOPPY_L
		lda #>DOS_FATSectorBuff			; reset the Pointer
		sta PTR_FLOPPY_H
		lda (PTR_FLOPPY_L)			; load high byte of the next sector
		ldy #FCB_Current_Cluster+1
		sta (PTR_DOS_FCB_L),y
;		jsr DebugOutput2		
		pla					; restore value for comparing for odd /even
		and #$01				; check if cluster number is odd or even
		beq FAT_GetFAT12ClusEntryValEven2
		bra FAT_GetFAT12ClusEntryValOdd2
FAT_GetFAT12ClusEntryVal4
		ldy #FCB_Current_Cluster
		lda (PTR_DOS_FCB_L),y
		and #$01
		bne FAT_GetFAT12ClusEntryValOdd
		bra FAT_GetFAT12ClusEntryValEven
;FAT12ClusEntryVal = FAT12ClusEntryVal >> 4 if cluster number is odd 
FAT_GetFAT12ClusEntryValOdd
		lda (PTR_FLOPPY_L)
		ldy #FCB_Current_Cluster
		sta (PTR_DOS_FCB_L),y
		ldy #1
		lda (PTR_FLOPPY_L),y
		ldy #FCB_Current_Cluster+1
		sta (PTR_DOS_FCB_L),y
FAT_GetFAT12ClusEntryValOdd2
		ldx #4					; shift FCB_Current_Cluster 4 times right 
FAT_GetFAT12ClusEntryValOdd3
		clc
		ldy #FCB_Current_Cluster+1
		lda (PTR_DOS_FCB_L),y
		ror
		sta (PTR_DOS_FCB_L),y
		dey
		lda (PTR_DOS_FCB_L),y
		ror
		sta (PTR_DOS_FCB_L),y
		dex
		bne FAT_GetFAT12ClusEntryValOdd3
		iny
		lda (PTR_DOS_FCB_L),y
		and #$0F
		sta (PTR_DOS_FCB_L),y
		clc
		rts
;Else if cluster number is even FAT12ClusEntryVal = FAT12ClusEntryVal & 0x0FFF
FAT_GetFAT12ClusEntryValEven
		lda (PTR_FLOPPY_L)
		ldy #FCB_Current_Cluster
		sta (PTR_DOS_FCB_L),y
		ldy #1
		lda (PTR_FLOPPY_L),y
		ldy #FCB_Current_Cluster+1
		sta (PTR_DOS_FCB_L),y
FAT_GetFAT12ClusEntryValEven2
		ldy #FCB_Current_Cluster+1
		lda (PTR_DOS_FCB_L),y
		and #$0f
		sta (PTR_DOS_FCB_L),y
		clc
		rts

;******************
; CheckFAT12EOF
; check for end of clusterchain (>=$FF7)
; TODO: bad cluster mark is $FF7 --> need extra handling ?
; Should not be reached as the file size is checked!
;******************

FAT_CheckFAT12EOF
		ldy #FCB_Current_Cluster+1 		; check for end of clusterchain
		lda (PTR_DOS_FCB_L),y
		cmp #$0F
		bne FAT_CheckFAT12EOFExit
		dey
		lda (PTR_DOS_FCB_L),y
		cmp #$F7
		bcs FAT_CheckFAT12EOFLastExit
FAT_CheckFAT12EOFExit
		rts
FAT_CheckFAT12EOFLastExit
		sec
		rts

; FAT12 values
; FAT12 FAT[0] = 0x0FF9 = Media ID 
; FAT12 FAT[1] = 0x0FFF = EOC
; 0x0FF7 = bad cluster


;******************
; GetFAT12FreeCluster	
; CountofClusters = DPB_NumSectors / DPB_SectorsPerCluster
;
;******************

FAT_GetFAT12FreeCluster				
		jsr DOS_Close_File
		stz DOS_TotalFreeClusters_L
		stz DOS_TotalFreeClusters_H
		stz DOS_CurrentCluster_L
		stz DOS_CurrentCluster_H
		lda #FAT_Offset_FirstAvCluster		; set offset to first available cluster
		ldy #FCB_Current_Cluster
		sta (PTR_DOS_FCB_L),y	
		lda #00
		iny			
		sta (PTR_DOS_FCB_L),y			; FCB_Current_Cluster high byte
FAT_GetFAT12FreeCluster_2
		jsr FAT_GetFAT12Offset
		jsr FAT_GetThisFAT12SecNum	
		jsr FAT_GetThisFAT12EntOffset
		jsr FAT_LoadFATSector
		bcs FAT_GetFAT12FreeCluster_Error_Exit
		jsr FAT_GetFAT12ClusEntryVal
		bcs FAT_GetFAT12FreeCluster_Error_Exit
;		jsr DebugOutput
		ldy #FCB_Current_Cluster
		lda (PTR_DOS_FCB_L),y	
		cmp #$00
		bne FAT_GetFAT12FreeCluster_3
		iny					; #FCB_Current_Cluster+1
		lda (PTR_DOS_FCB_L),y	
		cmp #$00
		bne FAT_GetFAT12FreeCluster_3
		inc DOS_TotalFreeClusters_L
		bne FAT_GetFAT12FreeCluster_3		
		inc DOS_TotalFreeClusters_H
		beq FAT_GetFAT12FreeCluster_Exit
FAT_GetFAT12FreeCluster_3
		jsr FAT_GetFAT12FreeClusterINC
		bcc FAT_GetFAT12FreeCluster_2
FAT_GetFAT12FreeCluster_Exit
		clc
		rts
FAT_GetFAT12FreeCluster_Error_Exit
		lda #DOS_ErrorNo_FATBad
		sta DOS_ErrorCode
		rts

;******************
; GetFAT12FreeClusterINC
; Increase DOS_CurrentCluster 
; Set carry if DOS_CurrentCluster == DPB_CountofClusters
;******************

FAT_GetFAT12FreeClusterINC
		inc DOS_CurrentCluster_L
		bne FAT_GetFAT12FreeClusterINC_1
 		inc DOS_CurrentCluster_H
FAT_GetFAT12FreeClusterINC_1
		lda DOS_CurrentCluster_L
		ldy #DPB_CountofClusters
		cmp (PTR_DOS_DPB_L),y
		bne FAT_GetFAT12FreeClusterINC_2
		lda DOS_CurrentCluster_H
		iny			
		cmp (PTR_DOS_DPB_L),y				
		beq FAT_GetFAT12FreeClusterINC_End
FAT_GetFAT12FreeClusterINC_2
 		lda DOS_CurrentCluster_L
		ldy #FCB_Current_Cluster
		clc
		adc #FAT_Offset_FirstAvCluster		; set offset to first available cluster  		
		sta (PTR_DOS_FCB_L),y
		lda DOS_CurrentCluster_H	
		iny					; #FCB_Current_Cluster+1
		adc #00					; add carry to high byte
		sta (PTR_DOS_FCB_L),y

		clc
		rts
FAT_GetFAT12FreeClusterINC_End
		rts
		
;******************
; NumClusttoBytes
; do the math DOS_TotalFreeClusters * DPB_SectorsPerFAT * DPB_SectorsPerCluster
; and print free bytes
; input: valid PTR_DOS_DPB, valid DOS_TotalFreeClusters
;******************

FAT_NumClusttoBytes
		ldy #DPB_BytesPerSector
		lda (PTR_DOS_DPB_L),y
		sta Operand1
		iny
		lda (PTR_DOS_DPB_L),y
		sta Operand2			
		ldy #DPB_SectorsPerCluster
		lda (PTR_DOS_DPB_L),y
		sta Operand3
		ldx #OS_Multiply16_FN
		jsr OS_Function_Call		; DPB_BytesPerSector * DPB_SectorsPerCluster = output A Low, Y high
		sta Operand5
		sty Operand6
		lda DOS_TotalFreeClusters_L
		sta Operand7
		lda DOS_TotalFreeClusters_H
		sta Operand8
		jsr Multiply32			; (DPB_BytesPerSector * DPB_SectorsPerCluster) * DOS_TotalFreeClusters_L
		rts

;******************
; 16-bit multiply with 32-bit product 
; took from 6502.org
; multiplier x multiplicand = product
; Operand 5,6 x Operand 7,8 = Operand 1,2,3,4
; TODO: Move to lib.asm
;******************
 
Multiply32	
 		lda	#$00
		sta	Operand3	; clear upper bits of product
		sta	Operand4 
		ldx	#$10		; set binary count to 16 
Multiply32s_r	
		lsr	Operand6	; divide multiplier by 2 
		ror	Operand5
		bcc	Multiply32r_r 
		lda	Operand3	; get upper half of product and add multiplicand
		clc
		adc	Operand7
		sta	Operand3
		lda	Operand4
		adc	Operand8
Multiply32r_r	
		ror			; rotate partial product 
		sta	Operand4
		ror	Operand3
		ror	Operand2 
		ror	Operand1 
		dex
		bne	Multiply32s_r 
		rts



