; (C) 2018 Uwe Gottschling
; Only support 720kB disk, 512 bytes per sector
; not supported sub directory and long names
; LBA 16 bit support = 64kbyte*512byte = 32MByte
; TODO: 32 Bit LBA address
; TODO: Pointer for FCB and DPB
; TODO: Support more than one drive
; TODO: Separate FAT and DOS routines
; TODO: Support FAT 12,16 and 32
; TODO: Support partition table
; TODO: Support functions: DOS_Mount, DOS_Open, DOS_Close, DOS_Read, DOS_Write, DOS_Seek, DOS_OpenDir, DOS_Readdir, DOS_FindFirst,
; TODO: Set Start Vector from File-Header
; TODO: Device name: COM1


debug=0	; Set to 1 for RAM segment

!src "../include/label_definition.asm"
!src "../include/Jumptable.asm"
!src "DOS_FAT_constant.asm"

!cpu 65c02
DOSVersionNoL = $0A ; Minor version number
DOSVersionNoH = $00 ; Major version number

; Fixed entry points:
; $C006: DOS_Function_Vector
; $C000: Start from OS main menu

!if debug {
*=$4000-2
!word $4000	; Start vector --> only for testing, normally =$c000
} else {
*=$c000
}
		jmp DOS_Main	; $C000

DOS_Function_Call_Entry
		jmp (DOS_Function_Vector_L)	; C003
DOS_Function_Call1			; Fixed $C006 for DOS_Function_Vector_L
		pha
		txa
		asl
		tax
		cpx #DOS_Function_Call_LUT_End-DOS_Function_Call_LUT	; DOS function dispatcher
		bcs DOS_Function_Call_Error_Exit
		lda DOS_Function_Call_LUT,x
		sta PTR_J_L
		lda DOS_Function_Call_LUT+1,x
		sta PTR_J_H
		pla
		jsr DOS_Function_Call_2
		rts
DOS_Function_Call_2
		jmp (PTR_J_L)
DOS_Function_Call_Error_Exit
		pla
		rts

DOS_Function_Call_LUT
!word	DOS_Open_File		; 0
!word	FAT_Load_Byte		; 1
!word	DOS_Close_File		; 2
!word	DOS_Search		; 3
!word	DOS_Dir			; 4
!word	DOS_Init		; 5 
!word	DOS_LEditor 		; 6
!word	DOS_Dir 		; 7
DOS_Function_Call_LUT_End	;TODO: Get file size

;***************************
; DOS main menu
;
;***************************

DOS_Init:
		lda DPB_PTR_Table
		sta PTR_DOS_DPB_L
		lda DPB_PTR_Table+1
		sta PTR_DOS_DPB_H
		lda FCB_PTR_Table
		sta PTR_DOS_FCB_L
		lda FCB_PTR_Table+1
		sta PTR_DOS_FCB_H
		stz Floppy_DriveNo
		lda #$FF
		sta DOS_DataSectorBuffDrive
		lda #$FF
		sta DOS_FATSectorBuffDrive
		ldx #FDC_Reset_FN
		jsr FDC_Function_Call
		rts
DOS_Main
		jsr DOS_Init
DOS_Main0		
		jsr Print_StartDostext
		ldx #OS_Get_CHR_FN
		jsr OS_Function_Call
		pha
		ldx #OS_Print_CHR_FN ; Echo command
		jsr OS_Function_Call
		lda #LF
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		pla
		ldx #End_Commands-Commands
DOS_Main1
		dex
		bmi DOS_Main2
		cmp Commands,x
		bne DOS_Main1

		txa
		asl
		tax
		lda Command_LUT,x
		sta PTR_J_L
		lda Command_LUT+1,x
		sta PTR_J_H
		jsr DOS_Main3
		bcs DOS_Main_Error
		jmp DOS_Main0
DOS_Main2
		cmp #'x'
		beq DOS_Main_Exit
		jsr UnknownCommand
		jmp DOS_Main0
DOS_Main3
		jmp (PTR_J_L)
		jmp DOS_Main0
DOS_Main_Exit
		rts
DOS_Main_Error
		jsr DOS_PrintError
		jmp DOS_Main0

Commands
!byte 'd'
!byte 't'
!byte 'l'
!byte 's'
!byte 'a'
!byte 'f'
!byte CR
!byte LF
End_Commands

Command_LUT
!word DOS_Dir				;d
!word DOS_Type				;t
!word DOS_Load				;l
!word Start_Programm			;s
!word DOS_Change_Floppy_Drive		;a
!word DOS_PrintFreeDiscSpace		;f
!word DOS_Main_Exit			;CR
!word DOS_Main_Exit			;LF

;***************************
; Print DOS start message
;***************************

Print_StartDostext
		lda #<StartDostext	; < Low byte of expression
		sta PTR_String_L
		lda #>StartDostext	; > High byte of expression
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		lda #DOSVersionNoH
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call
		lda #"."
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		lda #DOSVersionNoL
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call
		lda #<StartDosCurrentDrive	; < Low byte of expression
		sta PTR_String_L
		lda #>StartDosCurrentDrive	; > High byte of expression
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		jsr DOS_PrintCurrentDrive
		lda #<StartDostext2	; < Low byte of expression
		sta PTR_String_L
		lda #>StartDostext2	; > High byte of expression
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		rts

StartDostext
!text LF
!text "65C02 DOS",LF
!text "Version: ",00

StartDosCurrentDrive
!text LF,"Current floppy drive ",00

StartDostext2
!text LF,"Menu:",LF
!text "d = Directory",LF
!text "t = Type text file",LF
!text "l = Load binary file",LF
!text "s = Start programm",LF
!text "a = Toggle floppy A: / B:",LF
!text "f = Free disc space",LF
!text "x = Exit menu",LF
!text 00

;***************************
; Print Unknown key
;
;***************************

UnknownCommand				
		phx
		pha
		lda #<UnknownCommand_text
		sta PTR_String_L
		lda #>UnknownCommand_text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		pla
		plx
		rts

UnknownCommand_text
!text "Unknown key",LF,00

;***************************
; Print current floppy drive letter
; input: Floppy_DriveNo
;***************************

DOS_PrintCurrentDrive
		lda Floppy_DriveNo
		clc
		adc #"A"
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		lda #":"
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		rts

;******************
; Count free clusters and print free space in bytes
; Need a valid DPB and a free FCB
;******************

DOS_PrintFreeDiscSpace
		jsr FAT_GetFAT12FreeCluster
		bcs DOS_CountFreeClusters_Exit
		lda #<FreeBytes_text
		sta PTR_String_L
		lda #>FreeBytes_text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		jsr FAT_NumClusttoBytes
		ldx #OS_Bin2BCD32_FN
		jsr OS_Function_Call
		jsr Print_File_Size_SP
		lda #LF
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
DOS_CountFreeClusters_Exit
		rts

FreeBytes_text
!text "Bytes free: ",00

;******************
; Start programm at PTR_S_
;******************

Start_Programm
		jsr DOS_Print_Start_Address
		jmp (PTR_S_L)

;******************
; Change floppy drive
;******************

DOS_Change_Floppy_Drive
		lda Floppy_DriveNo
		eor #01
		sta Floppy_DriveNo	; Toggle drive number A: / B: 
		clc
		rol			; *2
		tay
		lda DPB_PTR_Table,y
		sta PTR_DOS_DPB_L
		iny
		lda DPB_PTR_Table,y
		sta PTR_DOS_DPB_H
		ldx #FDC_Reset_FN
		jsr FDC_Function_Call
		rts

;******************
; DOS print directory 
;******************

DOS_Dir
		lda #<DirStart_Text
		sta PTR_String_L
		lda #>DirStart_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		jsr DOS_PrintCurrentDrive
		lda #LF
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call	
		jsr DOS_Print_Dir
		rts

DirStart_Text
!text "Directory of ",00

;******************
; FAT print dir loop
;******************

DOS_Print_Dir
		jsr DOS_Clear_FCB
		jsr FAT_CheckBootSec
		bcs DOS_Print_Dir_Error_Exit
DOS_Print_Dir2
		stz DOS_CurrentDirEntry 	
		stz DOS_CurrentDirEntry+1
		stz DOS_DirFilesSum			
DOS_Print_Dir3
		jsr FAT_Load_Next_File_Sector
		bcs DOS_Print_Dir_Error_Exit
DOS_Print_Dir4
		jsr DOS_SetDirectoryPTR		
		jsr DOS_PrintDirEntry		
		bcs DOS_Print_Dir_Exit		; carry set = end of dir
DOS_Print_Dir5
		inc DOS_CurrentDirEntry		; next entry
		bne DOS_Print_Dir6
		inc DOS_CurrentDirEntry
DOS_Print_Dir6
		jsr DOS_CheckBreakPauseKey	
		bcs DOS_Print_Dir_Exit
DOS_Print_Dir6_1
		jsr DOS_CheckDirMax		; check for end of DOS_CurrentDirEntry
		bcs DOS_Print_Dir_Exit
DOS_Print_Dir7
		lda DOS_CurrentDirEntry
		and #$0F
		bne DOS_Print_Dir4
		jsr FAT_Calc_Next_File_Sector
		bcs DOS_Print_Dir_Error_Exit
		bra DOS_Print_Dir3
DOS_Print_Dir_Exit
		jsr DOS_PrintNumFiles
		jsr DOS_PrintFreeDiscSpace	; count free clusters and print free space in bytes
DOS_Print_Dir_Error_Exit
		rts

;******************
; Prints the number of files found in directory.
;*****************

DOS_PrintNumFiles
		lda #<DirSumFiles_Text
		sta PTR_String_L
		lda #>DirSumFiles_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		lda DOS_DirFilesSum
		sta Operand1
		stz Operand2
		stz Operand3
		stz Operand4
		ldx #OS_Bin2BCD32_FN
		jsr OS_Function_Call
		jsr Print_File_Size_SP
		ldx #OS_Print_CHR_FN
		lda #LF
		jsr OS_Function_Call
		rts 
DirSumFiles_Text
!text "File(s):     ",00

;******************
; DebugOutput
;******************

DebugOutput
		pha
		phx
		phy
		php


;		lda #<Debug_Text
;		sta PTR_String_L
;		lda #>Debug_Text
;		sta PTR_String_H
;		ldx #OS_Print_String_FN
;		jsr OS_Function_Call

		lda DOS_CurrentCluster_L+1
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call

		lda DOS_CurrentCluster_L
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call

		ldx #OS_Print_CHR_FN
		lda #" "
		jsr OS_Function_Call

		ldy #FCB_Current_Cluster+1
		lda (PTR_DOS_FCB_L),y	
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call

		ldy #FCB_Current_Cluster
		lda (PTR_DOS_FCB_L),y	
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call

		ldx #OS_Print_CHR_FN
		lda #LF
		jsr OS_Function_Call
		
		jsr DOS_CheckBreakPauseKey

		plp
		ply
		plx
		pla
		rts

;Debug_Text
;!text "DOS_CurrentDirEntry," 
;!text "DOS_CurrentCluster_L, FCB_Current_Cluster",LF,00

;******************
; DebugOutput2
;******************

DebugOutput2
		pha
		phx
		phy
		php


		lda #<Debug2_Text
		sta PTR_String_L
		lda #>Debug2_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call

		ldy #FCB_Current_Cluster+1
		lda (PTR_DOS_FCB_L),y	
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call

		ldy #FCB_Current_Cluster
		lda (PTR_DOS_FCB_L),y	
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call

		ldx #OS_Print_CHR_FN
		lda #LF
		jsr OS_Function_Call
		
		jsr DOS_CheckBreakPauseKey

		plp
		ply
		plx
		pla
		rts

Debug2_Text
;!text "DOS_CurrentDirEntry," 
!text "Boundary ",LF,00

;******************
; Search text file for type
;******************

DOS_Type
		lda #<TypeStart_Text
		sta PTR_String_L
		lda #>TypeStart_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		jsr DOS_LEditor			; get file name
		bcs DOS_Type_Error_Exit
		jsr DOS_Open_File
		bcs DOS_Type_Error_Exit
DOS_Type_File_Found
		lda #<TypeFound_Text
		sta PTR_String_L
		lda #>TypeFound_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
DOS_Type_File_Found2
		jsr DOS_Type_File
DOS_Type_Error_Exit
		rts

TypeStart_Text
!text "Type text file",LF,"Name (8.3):",00

TypeFound_Text
!text "File found",LF,00

;******************
; DOS type text file
;******************

DOS_Type_File
		jsr FAT_Load_Byte
		bcc DOS_Type_File3
		cmp #00					; A = 00 = EOF
		beq DOS_Type_File_exit
		bra DOS_Type_File_error
;DOS_Type_File2
;		cmp #CR
;		bne DOS_Type_File3
;		lda #LF
DOS_Type_File3
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		jsr DOS_CheckBreakPauseKey
		bcs DOS_Type_File_exit
		bra DOS_Type_File
DOS_Type_File_exit
		clc
		rts
DOS_Type_File_error
		rts

; DEBUG Output

;Debug_Output_ByteNum
;		pha
;		phx
;		phy
;		php
;		lda #LF
;		ldx #OS_Print_CHR_FN
;		jsr OS_Function_Call

;;		ldy #FCB_Sector_Count
;;		lda (PTR_DOS_FCB_L),y
;;		ldx #OS_Print_Bin2Hex_FN
;;		jsr OS_Function_Call

;		ldy #FCB_CurrentByteNum+3
;		lda (PTR_DOS_FCB_L),y
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		ldy #FCB_CurrentByteNum+2
;		lda (PTR_DOS_FCB_L),y
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		ldy #FCB_CurrentByteNum+1
;		lda (PTR_DOS_FCB_L),y
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		ldy #FCB_CurrentByteNum
;		lda (PTR_DOS_FCB_L),y
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		lda #LF
;		ldx #OS_Print_CHR_FN
;		jsr OS_Function_Call

;		ldy #FCB_FileSize+3
;		lda (PTR_DOS_FCB_L),y
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		ldy #FCB_FileSize+2
;		lda (PTR_DOS_FCB_L),y
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		ldy #FCB_FileSize+1
;		lda (PTR_DOS_FCB_L),y
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		ldy #FCB_FileSize
;		lda (PTR_DOS_FCB_L),y
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		lda #LF
;		ldx #OS_Print_CHR_FN
;		jsr OS_Function_Call

;		plp
;		ply
;		plx
;		pla
;		rts

;******************
; Load binary file
;******************

DOS_Load
		lda #<LoadStart_Text
		sta PTR_String_L
		lda #>LoadStart_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		jsr DOS_LEditor		; Get file name
		bcs DOS_Load_Error_Exit
		jsr DOS_Open_File
		bcs DOS_Load_Error_Exit
DOS_Load_File_Found
		lda #<TypeFound_Text
		sta PTR_String_L
		lda #>TypeFound_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		jsr DOS_Load_File
		bcs DOS_Load_Error_Exit
		rts
DOS_Load_Error_Exit
		rts

LoadStart_Text
!text "Load binary file",LF,"Name (8.3):",00

;******************
; Set Programm_ptr for start address
; and load binary file
;******************

DOS_Load_File
		jsr DOS_Set_Programm_PTR
		bcs DOS_Load_File_error
		jsr DOS_Copy_Data_Sector
DOS_Load_File_error
		rts

;******************
; Set write-PTR and start-PTR from first sector and print address
;******************

DOS_Set_Programm_PTR
		jsr FAT_Load_Byte
		bcs DOS_Set_Programm_PTR_Exit
		sta PTR_W_L		; set low byte from header
		sta PTR_S_L
		jsr FAT_Load_Byte
		bcs DOS_Set_Programm_PTR_Exit
		sta PTR_W_H		; set high byte from header
		sta PTR_S_H
		jsr DOS_Print_Start_Address
DOS_Set_Programm_PTR_Exit
		rts

;******************
; Copy data from PTR_FLOPPY to PTR_W
;******************

DOS_Copy_Data_Sector
		jsr FAT_Load_Byte
		bcc DOS_Copy_Data_Sector2
		cmp #00			; end of file ?
		beq DOS_Copy_Data_Sector_Exit2
		bra DOS_Copy_Data_Sector_Error_Exit
DOS_Copy_Data_Sector2
		sta (PTR_W_L)
	   	inc PTR_W_L
       	bne DOS_Copy_Data_Sector3
       	inc PTR_W_H
DOS_Copy_Data_Sector3
		bra DOS_Copy_Data_Sector
DOS_Copy_Data_Sector_Error_Exit
		rts				; if A not zero then error
DOS_Copy_Data_Sector_Exit2
		clc
		rts

;******************
; FAT sequential read next byte from file
;******************

FAT_Load_Byte
		ldy #FCB_CurrentByteNum		; check for first byte to read
		lda (PTR_DOS_FCB_L),y
		bne FAT_Load_Byte4
		iny
		lda (PTR_DOS_FCB_L),y
		bne FAT_Load_Byte4
		iny
		lda (PTR_DOS_FCB_L),y
		bne FAT_Load_Byte4
		iny
		lda (PTR_DOS_FCB_L),y
		bne FAT_Load_Byte4		; if it is then load first file sector
		jsr FAT_Load_Next_File_Sector
		bcc FAT_Load_Byte5
		rts
FAT_Load_Byte4
		lda PTR_FLOPPY_L		; check for sector bondary
		sec
		sbc #<DOS_DataSectorBuff
		ldy #DPB_BytesPerSector
		cmp (PTR_DOS_DPB_L),y
		bne FAT_Load_Byte6
		lda PTR_FLOPPY_H
		sbc #>DOS_DataSectorBuff
		iny
		cmp (PTR_DOS_DPB_L),y
		bne FAT_Load_Byte6
		jsr FAT_Load_Next_File_Sector
		bcc FAT_Load_Byte5
		rts
FAT_Load_Byte5
		jsr FAT_Calc_Next_File_Sector
		bcs FAT_Load_Byte_Error_Exit			
		lda #<DOS_DataSectorBuff
		sta PTR_FLOPPY_L
		lda #>DOS_DataSectorBuff	; set PTR_FLOPPY to start of buffer
		sta PTR_FLOPPY_H
FAT_Load_Byte6
		jsr FAT_Read_Byte_Sector
FAT_Load_Byte_Error_Exit
		rts
		
;******************		
; Read one byte from sector buffer		
; and increment PTR_FLOPPY_L
;******************		
		
FAT_Read_Byte_Sector				
		lda (PTR_FLOPPY_L)
		pha
		jsr DOS_INCCurrentByteNum
		pla
		bcs FAT_Read_Byte_Sector_Exit_EOF
	   	inc PTR_FLOPPY_L		; increment read buffer pointer
        bne FAT_Read_Byte_Sector_Exit
       	inc PTR_FLOPPY_H
FAT_Read_Byte_Sector_Exit
		rts
FAT_Read_Byte_Sector_Exit_EOF
		lda #0
		rts

;******************
; Print programm start address
;******************

DOS_Print_Start_Address
		lda #<DOS_Start_address		; < low byte of expression
		sta PTR_String_L
		lda #>DOS_Start_address		; > high byte of expression
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		lda PTR_S_H			; set start address for both pointer
		jsr DOS_Bin2Hex
		lda PTR_S_L
		jsr DOS_Bin2Hex
		lda #LF
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		rts

DOS_Start_address
!text "Start address:",LF,00

;******************
; Increment FCB_CurrentByteNum
; Check FCB_CurrentByteNum with FCB_FileSize for end of file
;******************

DOS_INCCurrentByteNum
		ldy #FCB_CurrentByteNum		; compare FCB_CurrentByteNum with FCB_FileSize
		lda (PTR_DOS_FCB_L),y
		ldy #FCB_FileSize
		cmp (PTR_DOS_FCB_L),y
		bne DOS_INCCurrentByteNum2
		ldy #FCB_CurrentByteNum+1
		lda (PTR_DOS_FCB_L),y
		ldy #FCB_FileSize+1
		cmp (PTR_DOS_FCB_L),y
		bne DOS_INCCurrentByteNum2
		ldy #FCB_CurrentByteNum+2
		lda (PTR_DOS_FCB_L),y
		ldy #FCB_FileSize+2
		cmp (PTR_DOS_FCB_L),y
		bne DOS_INCCurrentByteNum2
		ldy #FCB_CurrentByteNum+3
		lda (PTR_DOS_FCB_L),y
		ldy #FCB_FileSize+3
		cmp (PTR_DOS_FCB_L),y
		bne DOS_INCCurrentByteNum2 	; if FCB_CurrentByteNum equal with FCB_FileSize end
		sec							; C=1 mean end of file, not an error
		rts
DOS_INCCurrentByteNum2
		ldy #FCB_CurrentByteNum		; increment FCB_CurrentByteNum
		lda (PTR_DOS_FCB_L),y
		inc
		sta (PTR_DOS_FCB_L),y
		bne DOS_INCCurrentByteNum_Exit
		iny
		lda (PTR_DOS_FCB_L),y
		inc
		sta (PTR_DOS_FCB_L),y
		bne DOS_INCCurrentByteNum_Exit
		iny
		lda (PTR_DOS_FCB_L),y
		inc
		sta (PTR_DOS_FCB_L),y
		bne DOS_INCCurrentByteNum_Exit
		iny
		lda (PTR_DOS_FCB_L),y
		inc
		sta (PTR_DOS_FCB_L),y
		bne DOS_INCCurrentByteNum_Exit
DOS_INCCurrentByteNum_Exit
		clc
		rts

;******************
; Open file
;
;******************

DOS_Open_File
		jsr DOS_Search
		bcs DOS_Open_File_exit
		jsr FAT_Fill_FCB
		jsr FAT_Cluster_to_Block
		clc
		rts
DOS_Open_File_exit
		jsr DOS_Close_File
		lda #DOS_ErrorNo_FileNotFound
		sta DOS_ErrorCode
		rts

;******************
; Close file
;
;******************

DOS_Close_File
		jsr DOS_Clear_FCB
		; TODO: Flush sector data buffers
		rts

;******************
; Search filename in "InBuffer"
;
;******************

DOS_Search
		jsr DOS_Clear_FCB
		jsr FAT_CheckBootSec
		bcs DOS_Search_Error_Exit
		stz DOS_CurrentDirEntry 	; reset DOS_CurrentDirEntry
		stz DOS_CurrentDirEntry+1
DOS_Search3
;		jsr DOS_LoadRootDirSec
		jsr FAT_Load_Next_File_Sector
		bcs DOS_Search_Error_Exit
DOS_Search4
		jsr DOS_SetDirectoryPTR
		ldy #$00
		lda (PTR_FLOPPY_L),y	; test first character for $00
		cmp #$00		; 0x00 indicate free entry, there are no allocated directory entries after this one
		bne DOS_Search4_1
		jmp DOS_Search_Error_Exit
DOS_Search4_1
		cmp #$E5		; 0xE5 indicate free entry
		beq DOS_Search5
		ldy #$0B
		lda (PTR_FLOPPY_L),y
		and #$1B		; ATTR directory + Volume_ID + System
		bne DOS_Search5
		lda (PTR_FLOPPY_L),y
		cmp #$0F		; if "ATTR_LONG_NAME" then skip
		beq DOS_Search5
		jsr DOS_STRCMP
		bcc DOS_Search_Exit
DOS_Search5
		inc DOS_CurrentDirEntry		; set next entry
		bne DOS_Search6
		inc DOS_CurrentDirEntry+1
DOS_Search6
		jsr DOS_CheckDirMax		; check for end of dir entry
		bcs DOS_Search_Error_Exit
		lda DOS_CurrentDirEntry
		and #$0f
		bne DOS_Search4
		jsr FAT_Calc_Next_File_Sector
		bra DOS_Search3
DOS_Search_Error_Exit
		sec
		rts
DOS_Search_Exit
		rts

;******************
; Fill FCB
; PTR_FLOPPY has to point to the directory entry
;******************

FAT_Fill_FCB
		jsr DOS_Clear_FCB
		lda #Floppy_DriveNo
		inc
		ldy #FCB_Drive_Number
		sta (PTR_DOS_FCB_L),y
		ldy #$00			; offet: start at first dir entry (name)
		ldx #FCB_File_Name
FAT_Fill_FCB2
		lda (PTR_FLOPPY_L),y
		phy				; copy x to y
		phx
		ply
		sta (PTR_DOS_FCB_L),y
		ply
		iny
		inx
		cpx #FCB_FstClusHI		; end at of file name 8+3 = 11
		bcc FAT_Fill_FCB2
		ldy #FAT_Offset_DIR_FstClusHI	; Offset DIR_FstClusHI
		ldx #FCB_FstClusHI
FAT_Fill_FCB4
		lda (PTR_FLOPPY_L),y		; copy data: FstClusHI, WrtTime, WrtDate, FstClusLo, FileSize
		phy							; Copy x to y
		phx
		ply
		sta (PTR_DOS_FCB_L),y
		ply
		iny
		inx
		cpx #FCB_Current_Cluster
		bcc FAT_Fill_FCB4
		ldy #FCB_FstClusLO		; copy start cluster to current cluster
		lda (PTR_DOS_FCB_L),y
		ldy #FCB_Current_Cluster
		sta (PTR_DOS_FCB_L),y
		ldy #FCB_FstClusLO+1
		lda (PTR_DOS_FCB_L),y
		ldy #FCB_Current_Cluster+1
		sta (PTR_DOS_FCB_L),y
		lda #0
		ldy #FCB_FirstSecOfCluster
		sta (PTR_DOS_FCB_L),y		; TODO: clear FCB
		iny
		sta (PTR_DOS_FCB_L),y
		ldy #FCB_Sector_Count
		sta (PTR_DOS_FCB_L),y
		iny
		sta (PTR_DOS_FCB_L),y
		ldy #FCB_CurrentByteNum
		sta (PTR_DOS_FCB_L),y
		iny
		sta (PTR_DOS_FCB_L),y
		iny
		sta (PTR_DOS_FCB_L),y
		iny
		sta (PTR_DOS_FCB_L),y
		rts

;******************
; Clear FCB
; Fill FCB with zero
;******************

DOS_Clear_FCB
		lda #0
		ldy #FCB_Size+1
DOS_Clear_FCB1
		dey
		sta (PTR_DOS_FCB_L),y
		bne DOS_Clear_FCB1
		rts


;******************
; each entry has 32 Byte, every sector has 16 entrys
; Vector "DOS_CurrentDirEntry" is the current directory entry
; A 720kB Floppy has 112 directory entrys = 7 sectors
; DOS_CurrentDirEntry:
; 7 6 5 4 3 2 1 0
; | | | | | | | |
; | | |	| | -------> 8-bit index, 8*32 = 256 byte = 8-bit index register y
; | | | | ---------> 512 byte = low / high sector buffer
; | ---------------> Block number 0..8 (+ dir start)
; -----------------> not used
;******************

;******************
; Set directory pointer
;******************

DOS_SetDirectoryPTR
		lda DOS_CurrentDirEntry	; Calculate PTR-FLOPPY based on DOS_CurrentDirEntry
 		and #$07		; Mask 256/8 byte index
		asl			; *2 Calculate index pointer
		asl			; *4
		asl			; *8
		asl			; *16
		asl			; *32
		clc
		adc #<DOS_DataSectorBuff
		sta PTR_FLOPPY_L
		lda DOS_CurrentDirEntry
		and #$08
		ror
		ror
		ror
		adc #>DOS_DataSectorBuff
		sta PTR_FLOPPY_H
		rts

;******************
; Check for max. entrys
; TODO: Manage sub directories
;******************

DOS_CheckDirMax
		ldy #FCB_Current_Cluster
		lda (PTR_DOS_FCB_L),y
		bne DOS_CheckDirMax_Exit
		iny
		lda (PTR_DOS_FCB_L),y
		bne DOS_CheckDirMax_Exit	; check for cluster 0 (root directory)					
		lda DOS_CurrentDirEntry+1	; check for max. entrys high byte
		ldy #DPB_RootDirEntries
		cmp (PTR_DOS_DPB_L),y
		bne DOS_CheckDirMax_Exit
		lda DOS_CurrentDirEntry		; check for max. entrys low byte
		ldy #DPB_RootDirEntries+1
		cmp (PTR_DOS_DPB_L),y
		beq DOS_CheckDirMax_End_Exit
DOS_CheckDirMax_Exit
		clc
		rts
DOS_CheckDirMax_End_Exit
		sec
		rts

;******************
; Print dir entry
; PTR_FLOPPY to directory entry
;******************

DOS_PrintDirEntry
		jsr DOS_CheckDirAttrValid
		bcs DOS_PrintDirEntry_End
		bne DOS_PrintDirEntry_End
		jsr DOS_PrintDIRFileName
;		jsr DOS_PrintDIRAttr
		jsr DOS_PrintDirFileSize
		lda #LF
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		inc DOS_DirFilesSum
DOS_PrintDirEntry_End
		rts

;******************
; Check dir entry attribute, and EOF
; PTR_FLOPPY to directory entry
;
;******************

DOS_CheckDirAttrValid
		ldy #0
		lda (PTR_FLOPPY_L),y		; test first character
		cmp #$00			; if $00 it is the last entry
		beq DOS_CheckDirAttrValid_END
		cmp #$e5			; if $e5 then skip entry
		beq DOS_CheckDirAttrValid_Skip
		ldy #$0B
		lda (PTR_FLOPPY_L),y		; load attribute byte
		and #FAT_ATTR_LONG_NAME	OR FAT_ATTR_HIDDEN OR FAT_ATTR_SYSTEM OR FAT_ATTR_VOLUME_ID	
		bne DOS_CheckDirAttrValid_Skip
		lda #0
		clc
		rts
DOS_CheckDirAttrValid_Skip
		lda #$FF
		clc
		rts 
DOS_CheckDirAttrValid_END
		sec
		rts

;******************
; Print file name 
; PTR_FLOPPY to directory entry
;******************

DOS_PrintDIRFileName
		ldx #8				; 8 character filename
		ldy #00
DOS_PrintDIRFileName6
		lda (PTR_FLOPPY_L),y		; print first 8 characters
		phx
		phy
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		ply
		plx
		iny
		dex
		bne DOS_PrintDIRFileName6
		lda #"."
		phy
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		ply
		ldx #3
DOS_PrintDIRFileName7
		lda (PTR_FLOPPY_L),y		; print 3 characters extension
		phx
		phy
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		ply
		plx
		iny
		dex
		bne DOS_PrintDIRFileName7
		lda #SP
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		clc
		rts

;******************
; Print file attribute 
; PTR_FLOPPY to directory entry
;******************

DOS_PrintDIRAttr
		ldy #FAT_Offset_DIR_Attr
		lda (PTR_FLOPPY_L),y		; get attribute
		sta DOS_CurrentAttr		; temp. save to DOS_CurrentAttr
		bit #0x10			; ATTR_DIRECTORY
		beq DOS_PrintDIRAttr9
		bra DOS_PrintDIRAttr20		; then skip the rest
DOS_PrintDIRAttr9
		bit #FAT_ATTR_READ_ONLY		; ATTR_READ_ONLY
		beq DOS_PrintDIRAttr10
		lda #"R"
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		jmp DOS_PrintDIRAttr11
DOS_PrintDIRAttr10
		lda #"-"
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
DOS_PrintDIRAttr11
		lda DOS_CurrentAttr
		bit #FAT_ATTR_HIDDEN		; ATTR_HIDDEN
		beq DOS_PrintDIRAttr12
		lda #"H"
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		jmp DOS_PrintDIRAttr13
DOS_PrintDIRAttr12
		lda #"-"
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
DOS_PrintDIRAttr13
		lda DOS_CurrentAttr
		bit #FAT_ATTR_SYSTEM		; ATTR_SYSTEM
		beq DOS_PrintDIRAttr14
		lda #"S"
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		jmp DOS_PrintDIRAttr15
DOS_PrintDIRAttr14
		lda #"-"
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
DOS_PrintDIRAttr15
		lda DOS_CurrentAttr
		bit #FAT_ATTR_VOLUME_ID		;ATTR_VOLUME_ID
		beq DOS_PrintDIRAttr16
		lda #"V"
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		jmp DOS_PrintDIRAttr17
DOS_PrintDIRAttr16
		lda #"-"
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
DOS_PrintDIRAttr17
		lda DOS_CurrentAttr
		bit #FAT_ATTR_ARCHIVE		;ATTR_ARCHIVE
		beq DOS_PrintDIRAttr18
		lda #"A"
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		jmp DOS_PrintDIRAttr19
DOS_PrintDIRAttr18
		lda #"-"
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
DOS_PrintDIRAttr19
		lda #SP
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
DOS_PrintDIRAttr20
		clc
		rts

;******************
; Print dir entry file size
; PTR_FLOPPY to directory entry
;******************

; Calculate file size
DOS_PrintDirFileSize
		ldy #FAT_Offset_DIR_FileSize	; offset to file size
		ldx #0
DOS_PrintDirFileSize20
		lda (PTR_FLOPPY_L),y		; convert file size to BCD
		sta Operand1,x
		iny
		inx
		cpx #4				; 4 Byte file size
		bne DOS_PrintDirFileSize20
		ldx #OS_Bin2BCD32_FN
		jsr OS_Function_Call
DOS_PrintDirFileSize21
		jsr Print_File_Size_SP		; print file size
		clc
		rts


;******************
; DEBUG OUTPUT
;******************

;Debug_Output_Cluster
;		pha
;		phx
;		phy
;		php
;		lda #LF
;		ldx #OS_Print_CHR_FN
;		jsr OS_Function_Call

;		ldy #FCB_Sector_Count
;		lda (PTR_DOS_FCB_L),y
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		ldy #FCB_Current_Cluster+1
;		lda (PTR_DOS_FCB_L),y
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		ldy #FCB_Current_Cluster
;		lda (PTR_DOS_FCB_L),y
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		ldy #FCB_CurrentByteNum+1
;		lda (PTR_DOS_FCB_L),y
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		lda #LF
;		ldx #OS_Print_CHR_FN
;		jsr OS_Function_Call

;		lda Floppy_Blk_High
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		lda Floppy_Blk_Low
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		lda #LF
;		ldx #OS_Print_CHR_FN
;		jsr OS_Function_Call
;		plp
;		ply
;		plx
;		pla
;		rts

;******************
; DEBUG OUTPUT
;******************

;Debug_Output_CHS
;		pha
;		phx
;		phy
;		php
;		lda #LF
;		ldx #OS_Print_CHR_FN
;		jsr OS_Function_Call

;		ldy #FCB_Sector_Count
;		lda (PTR_DOS_FCB_L),y
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		lda #"T"
;		ldx #OS_Print_CHR_FN
;		jsr OS_Function_Call

;		lda Floppy_Track
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		lda #"H"
;		ldx #OS_Print_CHR_FN
;		jsr OS_Function_Call

;		lda Floppy_Head
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		lda #"S"
;		ldx #OS_Print_CHR_FN
;		jsr OS_Function_Call

;		lda Floppy_Sector
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		lda #LF
;		ldx #OS_Print_CHR_FN
;		jsr OS_Function_Call

;		plp
;		ply
;		plx
;		pla
;		rts

;******************
; Translation logical block addressing (LBA) to cylinder, head, sector (CHS)
; Input: Floppy_Blk_Low, High. Bootsector is blk=0
; Putput: Floppy_Sector, Floppy_Head, Floppy_Track
; Used: a,x,y,Operand1,2,3,4,5
;******************

Floppy_LBA2CHS
		ldy #DPB_SectorsPerTrack ; divisor, normaly 720Kb floppy (9 Sektoren each track)
		lda (PTR_DOS_DPB_L),y
		sta Operand3
		stz Operand4
		lda Floppy_Blk_Low
		sta Operand1
		lda Floppy_Blk_High
		sta Operand2
		ldx #OS_Divide16_FN
		jsr OS_Function_Call
		lda Operand5
		sta Floppy_Sector	; remainder is the sector number
		inc Floppy_Sector	; first sector number is 1
		lda Operand1
		and #$01		; mask head number  --> DPB_NumberOfHeads-1
		sta Floppy_Head
		lda Operand1
		lsr
		sta Floppy_Track
		rts

;******************
; Read sector by given CHS
;******************

FloppyReadCHS
		ldx #FDC_RSect_FN
		jsr FDC_Function_Call	; read sector to PTR_FLOPPY
		bcs FloppyReadCHS2
		lda #0
		sta DOS_CTRL_Error_Code
		rts
FloppyReadCHS2
		sta DOS_CTRL_Error_Code
		lda #DOS_ErrorNo_Read
		sta DOS_ErrorCode
		rts

;******************
; Print BCD (32-bit), 0 fill with space except the last, special for file size
; input: BCD format Operant5 .. Operand 9
;******************

Print_File_Size_SP
		ldx #0
Print_File_Size_SP1
		lda Operand5,x
		and #$F0
		bne Print_File_Size_SP7
		jsr Print_File_Size_Space
		lda Operand5,x
		and #$0F
		bne Print_File_Size_SP9
		cpx #4
		beq Print_File_Size_SP9
		jsr Print_File_Size_Space
		inx
		cpx #5
		bne Print_File_Size_SP1
		rts
Print_File_Size_SP6		; normal output
		lda Operand5,x
		and #$F0
Print_File_Size_SP7
		lsr
		lsr
		lsr
		lsr
		ora #$30
		jsr Print_File_Size_SP11
		lda Operand5,x
		and #$0F
Print_File_Size_SP9
		ora #$30
		jsr Print_File_Size_SP11
		inx
		cpx #5
		bne Print_File_Size_SP6
		rts
Print_File_Size_Space
		phx
		lda #SP
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		plx
		rts

Print_File_Size_SP11
		phx
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		plx
		rts

;***************************
; Get one line from std input and make uppercase letters
; and check for wrong file delimiters
; Input: -
; Output: PTR_String
; Destroy: A,X,Y
;***************************

DOS_LEditor
		ldx #OS_LineEditor_FN
		jsr OS_Function_Call
		lda #<InBuffer		; < Low byte of expression
		sta PTR_String_L
		lda #>InBuffer		; > High byte of expression
		sta PTR_String_H
		ldy #0
DOS_LEditor1
		lda (PTR_String_L),y
		beq DOS_LEditor_Exit
		and #$7F		; mask for ASCII character set
		cmp #$61		; cmp with "a"
		bmi DOS_LEditor2	; if > then next
		cmp #'z'+1		; cmp with "z"
		bpl DOS_LEditor2	; if < then next
		sec
		sbc #$20		; make uppercase letters
		cmp #$7F		; if > ASCII error
		bcs DOS_LEditor_Error_Exit
DOS_LEditor2
		beq DOS_LEditor_Error_Exit
		cmp #"+"
		beq DOS_LEditor_Error_Exit
		cmp #","
		beq DOS_LEditor_Error_Exit
		cmp #"/"
		beq DOS_LEditor_Error_Exit
		cmp #":"
		beq DOS_LEditor_Error_Exit
		cmp #";"
		beq DOS_LEditor_Error_Exit
		cmp #"<"
		beq DOS_LEditor_Error_Exit
		cmp #"="
		beq DOS_LEditor_Error_Exit
		cmp #">"
		beq DOS_LEditor_Error_Exit
		cmp #"["
		beq DOS_LEditor_Error_Exit
		cmp #"]"
		beq DOS_LEditor_Error_Exit
		cmp #"|"
		beq DOS_LEditor_Error_Exit
		sta (PTR_String_L),y
		iny
		bne DOS_LEditor1
DOS_LEditor_Exit
		clc
		rts
DOS_LEditor_Error_Exit
		lda #DOS_ErrorNo_WrongName
		sta DOS_ErrorCode 
		sec
		rts

; TODO: check for valid delimiter.
;---------------------------------------------------------------------------------------
;| Available   |  A to Z   0 to 9   $  &  #  % (  )  -  @  ^  {  }  '  `  !            |
;| characters  |  characters corresponding to character codes  $80 to $FE              |
;|-------------+-----------------------------------------------------------------------|
;| Unavailable |  ~  *  +  ,  .  /  :  ;  =  ?  [  ]                                   |
;| characters  |  characters corresponding to character codes $00 to $20 and $7F, $FF  |
;---------------------------------------------------------------------------------------
; ASCII code:
;Code 	…0 	…1 	…2 	…3 	…4 	…5 	…6 	…7 	…8 	…9 	…A 	…B 	…C 	…D 	…E 	…F
;0… 	NUL 	SOH 	STX 	ETX 	EOT 	ENQ 	ACK 	BEL 	BS 	HT 	LF 	VT 	FF 	CR 	SO 	SI
;1… 	DLE 	DC1 	DC2 	DC3 	DC4 	NAK 	SYN 	ETB 	CAN 	EM 	SUB 	ESC 	FS 	GS 	RS 	US
;2… 	SP 	! 	" 	# 	$ 	% 	& 	' 	( 	) 	* 	+ 	, 	- 	. 	/
;3… 	0 	1 	2 	3 	4 	5 	6 	7 	8 	9 	: 	; 	< 	= 	> 	?
;4… 	@ 	A 	B 	C 	D 	E 	F 	G 	H 	I 	J 	K 	L 	M 	N 	O
;5… 	P 	Q 	R 	S 	T 	U 	V 	W 	X 	Y 	Z 	[ 	\ 	] 	^ 	_
;6… 	` 	a 	b 	c 	d 	e 	f 	g 	h 	i 	j 	k 	l 	m 	n 	o
;7… 	p 	q 	r 	s 	t 	u 	v 	w 	x 	y 	z 	{ 	| 	} 	~ 	DEL

;***************************
; Compare two strings (case-sensitive)
; Input: PTR_String,PTR_W
; Output: A
; Destroy: A,Y
;***************************

DOS_STRCMP
		ldx #$00		; offset input string
		ldy #$00		; offset disk buffer name string
		lda InBuffer,x
		cmp (PTR_FLOPPY_L),Y	; first byte has to be a valid value
		bne DOS_STRCMPError
DOS_STRCMP1
		inx
		iny
		lda InBuffer,X		; input string
		cmp #"."
		beq DOS_STRCMP3
		cmp #00
		beq DOS_STRCMP11
		cmp (PTR_FLOPPY_L),Y	; compare with dir name
		bne DOS_STRCMPError
		cpy #7
		bne DOS_STRCMP1
		bra DOS_STRCMP8
DOS_STRCMP2
		iny
DOS_STRCMP3
		lda #SP			; check if it is filled with space
		cmp (PTR_FLOPPY_L),Y
		bne DOS_STRCMPError
		cpy #7
		bne DOS_STRCMP2
		bra DOS_STRCMP9
DOS_STRCMP8
		inx
DOS_STRCMP9
		inx
		iny
		lda InBuffer,X		; check file extension
		cmp #"."		; only one point in input string else error
		beq DOS_STRCMPError
		cmp #00			; if it is end of string ?
		beq DOS_STRCMP11
		cmp (PTR_FLOPPY_L),Y
		bne DOS_STRCMPError
		cpy #10
		bne DOS_STRCMP9
		bra DOS_STRCMPExit
DOS_STRCMP10
		iny
DOS_STRCMP11
		lda #SP			; check if it is filled with space
		cmp (PTR_FLOPPY_L),Y
		bne DOS_STRCMPError
		cpy #10
		bne DOS_STRCMP10
DOS_STRCMPExit
		clc
		rts
DOS_STRCMPError
		sec
		rts

;***************************
; Bin2Hex output with register save
;***************************

DOS_Bin2Hex
		phx
		phy
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call
		ply
		plx 
		rts

;***************************
; Check for break key press (CTRL+C)
; "p" for pause
; C=1 if break key 
;***************************

DOS_CheckBreakPauseKey			; TODO: move function to kernel
		ldx #PS2_ChkKey_FN
		jsr PS2_Function_Call
		bcc DOS_CheckBreakPauseKey3
		ldx #PS2_GetKey_FN
		jsr PS2_Function_Call
		cmp #ETX		; ETX or CTRL-C on keyboard
		beq DOS_CheckBreakPauseKey2
		cmp #"p" 		; p = pause
		beq DOS_CheckBreakPauseKey4
		clc
		bra DOS_CheckBreakPauseKey3
DOS_CheckBreakPauseKey2
		lda #<Break_Text
		sta PTR_String_L
		lda #>Break_Text
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		sec
DOS_CheckBreakPauseKey3
		rts
DOS_CheckBreakPauseKey4
		ldx #PS2_ChkKey_FN
		jsr PS2_Function_Call
		bcc DOS_CheckBreakPauseKey4
		clc
		rts

Break_Text
!text LF,"Break",LF,00


;******************
; Clear all buffer status
;******************
DOS_ClearSectorBuffStat
		stz DOS_DataSectorBuffLBA_L
		stz DOS_DataSectorBuffLBA_H
		lda #$FF
		sta DOS_DataSectorBuffDrive
		stz DOS_FATSectorBuffLBA_L
		stz DOS_FATSectorBuffLBA_H
		sta DOS_FATSectorBuffDrive
		rts

!src "DOS_FAT_MBR_functions.asm"
!src "DOS_FAT_file_functions.asm"
!src "DOS_error.asm"

;******************
; Table for disk parameter block
;******************

DPB_PTR_Table
	!word 	DPB_1
	!word 	DPB_2
	!word 	DPB_3
	!word 	DPB_4

;******************
; Table for file control block
;******************

FCB_PTR_Table
	!word 	FCB_1
	!word 	FCB_2
	!word 	FCB_3
	!word 	FCB_4

; Keep the area clear for IO address space
!if debug {	
} else {
*=$DF00
!fi 256, $ff
}




