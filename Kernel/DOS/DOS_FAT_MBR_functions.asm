; DOS FAT MBR functions 
; (C) 2018 Uwe Gottschling
;
;******************
; Get on byte from DOS_DataSectorBuff
; input low byte A, high byte Y.
; return A
;******************

DOS_GetBufferwOffset
		ldx #<DOS_DataSectorBuff
		stx PTR_FLOPPY_L
		ldx #>DOS_DataSectorBuff	; set sector buffer start address
		stx PTR_FLOPPY_H
		clc
		adc PTR_FLOPPY_L
		sta PTR_FLOPPY_L
		tya
		adc PTR_FLOPPY_H
		sta PTR_FLOPPY_H
		lda (PTR_FLOPPY_L)
		rts

;******************
; Check boot sector signature
;******************

FAT_CheckBootSec
		jsr FAT_LoadBootSec
		bcc FAT_CheckBootSec3
		rts
FAT_CheckBootSec3
		lda #<FAT_BPB_Offset_Signature	; offset boot sector magic byte at 511 and 512
		ldy #>FAT_BPB_Offset_Signature
		jsr	DOS_GetBufferwOffset
		cmp #$55
		beq FAT_CheckBootSec4
		jmp FAT_CheckBootSec_Magic_Error
FAT_CheckBootSec4
		lda #<FAT_BPB_Offset_Signature
		inc
		jsr	DOS_GetBufferwOffset
		cmp #$AA
		beq FAT_CheckBootSec5
		jmp FAT_CheckBootSec_Magic_Error
FAT_CheckBootSec5
		lda #<FAT_BPB_Offset_Media	; offset boot sector magic byte at 511 and 512
		ldy #>FAT_BPB_Offset_Media
		jsr	DOS_GetBufferwOffset
		cmp #FAT_Media_Signature_720kb	; compare if media decriptor = F9 (720kByte Floppy)
		beq FAT_CheckBootSec5_1
		jmp FAT_CheckBootSec_IDError
FAT_CheckBootSec5_1
	;******************
	; Copy Sector Bootsector-Data to Disk Parameter Block DPB
	;******************
		lda #<DOS_DataSectorBuff
		sta PTR_FLOPPY_L
		lda #>DOS_DataSectorBuff	; set sector buffer start address
		sta PTR_FLOPPY_H
		ldy #FAT_BPB_Offset_BytsPerSec	; start at BPB_BytsPerSec (Offset Byte 11)
		ldx #0
FAT_CheckBootSec6
		lda (PTR_FLOPPY_L),y
		phy				; copy x to y
		phx
		ply
		sta (PTR_DOS_DPB_L),y
		ply
		iny
		inx
		cpx #DPB_FirstRootDirSecNum	; end of data
		bcc FAT_CheckBootSec6
	;******************
	; Calculated First Sector of the Root Directory
	; FirstRootDirSecNum = DPB_ReservedSectors + (DPB_FatCopies (1 Byte) * DPB_SectorsPerFAT (2Byte)) = 01 + (02 * 03) = 7 for 720kB
	;******************
FAT_CheckBootSec7
		ldy #DPB_SectorsPerFAT
		lda (PTR_DOS_DPB_L),y
		sta Operand1
		iny
		lda (PTR_DOS_DPB_L),y
		sta Operand2
		ldy #DPB_FatCopies
		lda (PTR_DOS_DPB_L),y
		sta Operand3
		ldx #OS_Multiply16_FN
		jsr OS_Function_Call	
		phy			; Save y
		ldy #DPB_FirstRootDirSecNum
		sta (PTR_DOS_DPB_L),y
		ldy #DPB_FirstRootDirSecNum+1	; DPB_FirstRootDirSecNum = DPB_SectorsPerFAT * DPB_FatCopies
		pla			; copy y to a
		sta (PTR_DOS_DPB_L),y
		ldy #DPB_FirstRootDirSecNum
		lda (PTR_DOS_DPB_L),y
		ldy #DPB_ReservedSectors
		clc
		adc (PTR_DOS_DPB_L),y
		ldy #DPB_FirstRootDirSecNum	; add DPB_ReservedSectors to DPB_FirstRootDirSecNum
		sta (PTR_DOS_DPB_L),y
		ldy #DPB_ReservedSectors+1
		lda (PTR_DOS_DPB_L),y
		ldy #DPB_FirstRootDirSecNum+1
		adc (PTR_DOS_DPB_L),y
		sta (PTR_DOS_DPB_L),y
; (DPB_FirstDataSector = BPB_ResvdSecCnt + (DPB_FatCopies * DPB_SectorsPerFAT) + RootDirSectors)
		ldy #DPB_RootDirEntries
		lda (PTR_DOS_DPB_L),y
		sta Operand1
		ldy #DPB_RootDirEntries+1
		lda (PTR_DOS_DPB_L),y
		sta Operand2
; RootDirSectors = ((BPB_RootEntCnt * 32) + (BPB_BytsPerSec – 1)) / BPB_BytsPerSec;
		ldx #5
FAT_CheckBootSec8
		clc
		lda Operand1
		rol
		sta Operand1
		lda Operand2
		rol
		sta Operand2
		dex
		bne FAT_CheckBootSec8
		clc
		ldy #DPB_BytesPerSector
		lda (PTR_DOS_DPB_L),y
		sta Operand3
		ldy #DPB_BytesPerSector+1
		lda (PTR_DOS_DPB_L),y
		sta Operand4
		ldx #OS_Divide16_FN
		jsr OS_Function_Call	
; DPB_FirstDataSector = RootDirSectors + DPB_FirstRootDirSecNum
 		ldy #DPB_FirstRootDirSecNum
		lda (PTR_DOS_DPB_L),y	
		clc
		adc Operand1	
		ldy #DPB_FirstDataSector		
		sta (PTR_DOS_DPB_L),y	
 		ldy #DPB_FirstRootDirSecNum+1
		lda (PTR_DOS_DPB_L),y
		adc Operand2			
		ldy #DPB_FirstDataSector+1
		sta (PTR_DOS_DPB_L),y	
; DPB_TotalDataSectors = DPB_NumSectors-DPB_FirstDataSector
		ldy #DPB_NumSectors
		lda (PTR_DOS_DPB_L),y
		ldy #DPB_FirstDataSector
		sec
		sbc (PTR_DOS_DPB_L),y
		ldy #DPB_TotalDataSectors
		sta (PTR_DOS_DPB_L),y
		ldy #DPB_NumSectors+1
		lda (PTR_DOS_DPB_L),y
		ldy #DPB_FirstDataSector+1
		sbc (PTR_DOS_DPB_L),y
		ldy #DPB_TotalDataSectors+1
		sta (PTR_DOS_DPB_L),y
; DPB_CountofClusters = DPB_TotalDataSectors / DPB_SectorsPerCluster
		ldy #DPB_TotalDataSectors
		lda (PTR_DOS_DPB_L),y
		sta Operand1
		ldy #DPB_TotalDataSectors+1
		lda (PTR_DOS_DPB_L),y
		sta Operand2
		ldy #DPB_SectorsPerCluster
		lda (PTR_DOS_DPB_L),y
		sta Operand3
		stz Operand4
		ldx #OS_Divide16_FN
		jsr OS_Function_Call
		lda Operand1
		ldy #DPB_CountofClusters
		sta (PTR_DOS_DPB_L),y
		lda Operand2
		ldy #DPB_CountofClusters+1
		sta (PTR_DOS_DPB_L),y
; Reset FatSectorBuffer status
		stz DOS_FATSectorBuffLBA_L	
		stz DOS_FATSectorBuffLBA_H
		lda #$FF
		sta DOS_FATSectorBuffDrive
		clc
		rts
FAT_CheckBootSec_IDError
		lda #DOS_ErrorNo_BadMediaID
		sta DOS_ErrorCode
		sec
		rts
FAT_CheckBootSec_Magic_Error	
		lda #DOS_ErrorNo_FATBootSector
		sta DOS_ErrorCode
		sec
		rts	
		
;******************
; Load boot sector
;******************

FAT_LoadBootSec
		lda #$FF			; set sector data buffer to invalid 
		sta DOS_DataSectorBuffDrive
		ldx #FDC_Restore_FN
		jsr FDC_Function_Call
		bcs FAT_LoadBootSec4
FAT_LoadBootSec2
		lda #<DOS_DataSectorBuff
		sta PTR_FLOPPY_L
		lda #>DOS_DataSectorBuff	; set start address
		sta PTR_FLOPPY_H
FAT_LoadBootSec4

		lda #0
		sta Floppy_Head			; BIT0 = side select. boot sector is on side = 0
		sta Floppy_Track
		lda #1
		sta Floppy_Sector		; set CHS to fist sector
		jsr FloppyReadCHS
		rts

