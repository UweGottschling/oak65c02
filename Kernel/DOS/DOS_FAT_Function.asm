;******************
; Load Next File Sector
;******************

FAT_Load_Next_File_Sector
		ldy #FCB_Current_Cluster
		lda (PTR_DOS_FCB_L),y
		bne FAT_Load_Next_File_Sector1
		iny
		lda (PTR_DOS_FCB_L),y
		bne FAT_Load_Next_File_Sector1	; check for cluster 0 (root directory)
		ldy #FCB_Sector_Count
		lda (PTR_DOS_FCB_L),y	
		ldy #DPB_FirstRootDirSecNum
		clc
		adc (PTR_DOS_DPB_L),y		; 8 = start sector for Directory at 720kB Floppys = 7 LBA
		sta Floppy_Blk_Low		; Floppy-LBA nummer 0
		stz Floppy_Blk_High		
		bra FAT_Load_Next_File_Sector2
;		jsr Floppy_LBA2CHS		; parameter berechnen
;		jsr FloppyReadCHS		; read Sector
;		jsr FAT_LoadDataSectorLBA
FAT_Load_Next_File_Sector1
		ldy #FCB_FirstSecOfCluster
		lda (PTR_DOS_FCB_L),y
		ldy #FCB_Sector_Count
		clc
		adc (PTR_DOS_FCB_L),y
		sta Floppy_Blk_Low
		ldy #FCB_FirstSecOfCluster+1
		lda (PTR_DOS_FCB_L),y
		adc #0
		sta Floppy_Blk_High
FAT_Load_Next_File_Sector2
;		jsr Floppy_LBA2CHS
;		lda #<DOS_DataSectorBuff
;		sta PTR_FLOPPY_L
;		lda #>DOS_DataSectorBuff	; reset sector buffer to start address
;		sta PTR_FLOPPY_H
;		jsr FloppyReadCHS		; read sector
;		jsr DebugOutput2
		jsr FAT_LoadDataSector
		rts

;******************
; Load Sector to 
;******************
; FAT_LoadDataSectorLBA


;		sta Floppy_Blk_Low		; floppy-Block nummer 0
;		stz Floppy_Blk_High		; floppy-Block nummer 0
;		jsr Floppy_LBA2CHS		; parameter berechnen
;		jsr FloppyReadCHS		; read Sector

;******************
; DebugOutput2
;******************

;DebugOutput2
;		pha
;		phx
;		phy
;		php
;		
;		ldx #OS_ChangeSTDOUT_FN
;		lda #2				; 2 = UART 3 = Display
;		jsr OS_Function_Call

;		lda #<Debug2_Text
;		sta PTR_String_L
;		lda #>Debug2_Text
;		sta PTR_String_H
;		ldx #OS_Print_String_FN	
;		jsr OS_Function_Call

;		lda Floppy_Track
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		ldx #OS_Print_CHR_FN
;		lda #" "
;		jsr OS_Function_Call	

;		lda Floppy_Head
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		ldx #OS_Print_CHR_FN
;		lda #" "
;		jsr OS_Function_Call

;		lda Floppy_Sector
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		ldx #OS_Print_CHR_FN
;		lda #CR
;		jsr OS_Function_Call		

;		ldx #OS_ChangeSTDOUT_FN
;		lda #3				; 2 = UART 3 = Display
;		jsr OS_Function_Call

;		plp
;		ply
;		plx
;		pla
;		rts	

;Debug2_Text
;!text "Floppy_Track, Floppy_Head, Floppy_Sector:",CR,00

;******************
; Check Boot Sector Signature
;******************

FAT_CheckBootSec
		lda #<DOS_DataSectorBuff
		sta PTR_FLOPPY_L
		lda #>DOS_DataSectorBuff	; set sector buffer start address
		sta PTR_FLOPPY_H
		ldy #DOS_BPB_Offset_Signature	; offset boot sector magic byte at 511 and 512
		inc PTR_FLOPPY_H
		lda (PTR_FLOPPY_L),y
		cmp #$55
		beq FAT_CheckBootSec4
		jmp FAT_CheckBootSec_Error
FAT_CheckBootSec4
		iny
		lda (PTR_FLOPPY_L),y
		cmp #$AA
		beq FAT_CheckBootSec5
		jmp FAT_CheckBootSec_Error
		ldy #DOS_BPB_Offset_Media
		lda (PTR_FLOPPY_L),y
		cmp #DOS_Media_Signature_720kb	; compare if media decriptor = F9 (720kByte Floppy)
		beq FAT_CheckBootSec5
		jmp FAT_CheckBootSec_Error
FAT_CheckBootSec5
		dec PTR_FLOPPY_H

;******************
; Copy Sector Bootsector-Data to Disk Parameter Block DPB
;******************

		ldy #DOS_BPB_Offset_BytsPerSec	; start at BPB_BytsPerSec (Offset Byte 11)
		ldx #0
FAT_CheckBootSec6
		lda (PTR_FLOPPY_L),y
		phy				; copy x to y
		phx
		ply
		sta (PTR_DOS_DPB_L),y
		ply
		iny
		inx
		cpx #DPB_FirstRootDirSecNum	; end of data
		bcc FAT_CheckBootSec6

;******************
; Calculated First Sector of the Root Directory
; FirstRootDirSecNum = DPB_ReservedSectors + (DPB_FatCopies (1 Byte) * DPB_SectorsPerFAT (2Byte)) = 01 + (02 * 03) = 7 for 720kB
;******************

FAT_CheckBootSec7
		ldy #DPB_SectorsPerFAT
		lda (PTR_DOS_DPB_L),y
		sta Operand1
		iny
		lda (PTR_DOS_DPB_L),y
		sta Operand2
		ldy #DPB_FatCopies
		lda (PTR_DOS_DPB_L),y
		sta Operand3
		ldx #OS_Multiply16_FN
		jsr OS_Function_Call	
		phy				; save y
		ldy #DPB_FirstRootDirSecNum
		sta (PTR_DOS_DPB_L),y
		ldy #DPB_FirstRootDirSecNum+1	; DPB_FirstRootDirSecNum = DPB_SectorsPerFAT * DPB_FatCopies
		pla				; copy y to a
		sta (PTR_DOS_DPB_L),y
		ldy #DPB_FirstRootDirSecNum
		lda (PTR_DOS_DPB_L),y
		ldy #DPB_ReservedSectors
		clc
		adc (PTR_DOS_DPB_L),y
		ldy #DPB_FirstRootDirSecNum	; add DPB_ReservedSectors to DPB_FirstRootDirSecNum
		sta (PTR_DOS_DPB_L),y
		ldy #DPB_ReservedSectors+1
		lda (PTR_DOS_DPB_L),y
		ldy #DPB_FirstRootDirSecNum+1
		adc (PTR_DOS_DPB_L),y
		sta (PTR_DOS_DPB_L),y
; This is more universal: (DPB_FirstDataSector = BPB_ResvdSecCnt + (DPB_FatCopies * DPB_SectorsPerFAT) + RootDirSectors)
		ldy #DPB_RootDirEntries
		lda (PTR_DOS_DPB_L),y
		sta Operand1
		ldy #DPB_RootDirEntries+1
		lda (PTR_DOS_DPB_L),y
		sta Operand2
; RootDirSectors = ((BPB_RootEntCnt * 32) + (BPB_BytsPerSec – 1)) / BPB_BytsPerSec;
		ldx #5
FAT_CheckBootSec8
		clc
		lda Operand1
		rol
		sta Operand1
		lda Operand2
		rol
		sta Operand2
		dex
		bne FAT_CheckBootSec8
		clc
		ldy #DPB_BytesPerSector
		lda (PTR_DOS_DPB_L),y
		sta Operand3
		ldy #DPB_BytesPerSector+1
		lda (PTR_DOS_DPB_L),y
		sta Operand4
		ldx #OS_Divide16_FN
		jsr OS_Function_Call	
; DPB_FirstDataSector = RootDirSectors + DPB_FirstRootDirSecNum
 		ldy #DPB_FirstRootDirSecNum
		lda (PTR_DOS_DPB_L),y	
		clc
		adc Operand1	
		ldy #DPB_FirstDataSector		
		sta (PTR_DOS_DPB_L),y	
 		ldy #DPB_FirstRootDirSecNum+1
		lda (PTR_DOS_DPB_L),y
		adc Operand2			
		ldy #DPB_FirstDataSector+1
		sta (PTR_DOS_DPB_L),y	
; DPB_TotalDataSectors = DPB_NumSectors-DPB_FirstDataSector
		ldy #DPB_NumSectors
		lda (PTR_DOS_DPB_L),y
		ldy #DPB_FirstDataSector
		sec
		sbc (PTR_DOS_DPB_L),y
		ldy #DPB_TotalDataSectors
		sta (PTR_DOS_DPB_L),y
		ldy #DPB_NumSectors+1
		lda (PTR_DOS_DPB_L),y
		ldy #DPB_FirstDataSector+1
		sbc (PTR_DOS_DPB_L),y
		ldy #DPB_TotalDataSectors+1
		sta (PTR_DOS_DPB_L),y
; DPB_CountofClusters = DPB_TotalDataSectors / DPB_SectorsPerCluster
		ldy #DPB_TotalDataSectors
		lda (PTR_DOS_DPB_L),y
		sta Operand1
		ldy #DPB_TotalDataSectors+1
		lda (PTR_DOS_DPB_L),y
		sta Operand2
		ldy #DPB_SectorsPerCluster
		lda (PTR_DOS_DPB_L),y
		sta Operand3
		stz Operand4
		ldx #OS_Divide16_FN
		jsr OS_Function_Call
		lda Operand1
		ldy #DPB_CountofClusters
		sta (PTR_DOS_DPB_L),y
		lda Operand2
		ldy #DPB_CountofClusters+1
		sta (PTR_DOS_DPB_L),y
; Reset FatSectorBuffer status
		stz DOS_FATSectorBuffLBA_L	
		stz DOS_FATSectorBuffLBA_H
		stz DOS_FATSectorBuffDrive
		clc
		rts
FAT_CheckBootSec_Error
		sec
		rts

;******************
; DOS calculate next sector
;******************

FAT_Calc_Next_File_Sector
		ldy #FCB_Current_Cluster
		lda (PTR_DOS_FCB_L),y
		bne FAT_Calc_Next_File_Sector2
		iny
		lda (PTR_DOS_FCB_L),y
		bne FAT_Calc_Next_File_Sector2	; check for cluster 0 (root directory)
		ldy #FCB_Sector_Count		; if root directory count only sectors
		lda (PTR_DOS_FCB_L),y
		inc
		sta (PTR_DOS_FCB_L),y
		bra FAT_Calc_Next_File_Sector3
FAT_Calc_Next_File_Sector2
		ldy #FCB_Sector_Count
		lda (PTR_DOS_FCB_L),y
		inc
		sta (PTR_DOS_FCB_L),y
;		jsr DebugOutput
		ldy #DPB_SectorsPerCluster
		cmp (PTR_DOS_DPB_L),y
		bne FAT_Calc_Next_File_Sector3
		ldy #FCB_Sector_Count
		lda #0
		sta (PTR_DOS_FCB_L),y
		jsr FAT_GetNextFat12Cluster
		jsr FAT_Cluster_to_Block
FAT_Calc_Next_File_Sector3
		rts



; Cluster Values FAT12:
; 000 		--> free cluster
; 001 		--> reserved cluster
; 002...FF6 	--> user data
; FF7 		--> bad cluster
; FF8...FFF 	--> end marker
;******************
; Position 	FAT12 Value
; 	0	aaaaaaaa --> low a
;	1	bbbbaaaa --> low b, high a
;	2	bbbbbbbb --> high b
; Cluster value b is odd and need to be shift 4 times right
; Cluster value a is even and need to mask out with 0x0fff

;******************
; Calculate Cluster to First Block
; Input: FCB_Current_Cluster
; Output: Flopy_Blk_Low and Floppy_Blk_High
; FCB_FirstSecOfCluster = ((N – 2) * DPB_SectorsPerCluster(1Byte)) + DPB_FirstDataSector (2Byte)
; N = data cluster number 
;******************

FAT_Cluster_to_Block
;		jsr DebugOutput
		ldy #FCB_Current_Cluster	; calculate N-2
		lda (PTR_DOS_FCB_L),y
		sec				
		sbc #2				
		ldy #FCB_FirstSecOfCluster
		sta (PTR_DOS_FCB_L),y
		ldy #FCB_Current_Cluster+1
		lda (PTR_DOS_FCB_L),y
		sbc #0
		ldy #FCB_FirstSecOfCluster+1
		sta (PTR_DOS_FCB_L),y
		ldy #FCB_FirstSecOfCluster	; FCB_FirstSecOfCluster (2Byte) * DPB_SectorsPerCluster(1Byte)
		lda (PTR_DOS_FCB_L),y
		sta Operand1
		iny
		lda (PTR_DOS_FCB_L),y
		sta Operand2
		ldy #DPB_SectorsPerCluster
		lda (PTR_DOS_DPB_L),y
		sta Operand3
		ldx #OS_Multiply16_FN		; Operand1 = Low  Operand2 = high x Operand3
		jsr OS_Function_Call
		phy
		ldy #FCB_FirstSecOfCluster
		sta (PTR_DOS_FCB_L),y
		pla
		iny
		sta (PTR_DOS_FCB_L),y
		ldy #FCB_FirstSecOfCluster	; DPB_FirstDataSector + FCB_FirstSecOfCluster
		lda (PTR_DOS_FCB_L),y
		ldy #DPB_FirstDataSector
		clc				
 		adc (PTR_DOS_DPB_L),y
		ldy #FCB_FirstSecOfCluster	; store sum of LSBs
		sta (PTR_DOS_FCB_L),y
		iny
		lda (PTR_DOS_FCB_L),y
		ldy #DPB_FirstDataSector+1
		adc (PTR_DOS_DPB_L),y		; add the MSBs using carry from
		ldy #FCB_FirstSecOfCluster+1	; the previous calculation
		sta (PTR_DOS_FCB_L),y
		rts



;******************
; Get next FAT12 cluster
; Calculate position in fat and load sector,
; write next cluster to FCB_Current_Cluster,
; if it is the last cluster carry is set
; input: FCB
;******************

FAT_GetNextFat12Cluster
		jsr FAT_GetFAT12Offset
		jsr FAT_GetThisFAT12SecNum	
		jsr FAT_GetThisFAT12EntOffset
		jsr FAT_LoadFATSector
		bcs FAT_GetNextFat12ClusterExit2
		jsr FAT_GetFAT12ClusEntryVal
		jsr FAT_CheckFAT12EOF
FAT_GetNextFat12ClusterExit2
		rts

;******************
; FAT_GetFAT12Offset
; FCB_FATOffset(Operand1,2) = FCB_Current_Cluster (2Byte) + (FCB_Current_Cluster / 2) 
;******************
FAT_GetFAT12Offset
		ldy #FCB_Current_Cluster+1	; (FCB_Current_Cluster / 2)
		lda (PTR_DOS_FCB_L),y
		clc
		ror
		sta Operand2
		dey
		lda (PTR_DOS_FCB_L),y
		ror
		sta Operand1
		ldy #FCB_Current_Cluster
		clc
		adc (PTR_DOS_FCB_L),y		; + FCB_Current_Cluster (2Byte)
		ldy #FCB_FATOffset
		sta (PTR_DOS_FCB_L),y
		sta Operand1
		lda Operand2
		ldy #FCB_Current_Cluster+1
		adc (PTR_DOS_FCB_L),y
		ldy #FCB_FATOffset+1
		sta (PTR_DOS_FCB_L),y
		sta Operand2
		rts
;******************
; FAT_GetThisFAT12SecNum
; FCB_ThisFATSecNum (Operand1,2) = DPB_ReservedSectors (2 Byte) + (FCB_FATOffset(Operand1,2) / DPB_BytesPerSector(Operand3,4))
;******************
FAT_GetThisFAT12SecNum
		ldy #DPB_BytesPerSector 	; (FATOffset(Operand1,2) / DPB_BytesPerSector(Operand3,4)
		lda (PTR_DOS_DPB_L),y
		sta Operand3
		iny
		lda (PTR_DOS_DPB_L),y
		sta Operand4
		ldx #OS_Divide16_FN
		jsr OS_Function_Call
		ldy #DPB_ReservedSectors 	; + DPB_ReservedSectors (2 Byte)
		lda (PTR_DOS_DPB_L),y
		clc
		adc Operand1
		ldy #FCB_ThisFATSecNum
		sta (PTR_DOS_FCB_L),y
		ldy #DPB_ReservedSectors+1
		lda (PTR_DOS_DPB_L),y
		adc Operand2
		ldy #FCB_ThisFATSecNum+1
		sta (PTR_DOS_FCB_L),y
		rts
;******************
; FAT_GetThisFAT12EntOffset
; FCB_ThisFATEntOffset = (Remainder)(FCB_FATOffset (2Byte) / DPB_BytesPerSector (2Byte))
;******************
FAT_GetThisFAT12EntOffset
		ldy #FCB_FATOffset
		lda (PTR_DOS_FCB_L),y
		sta Operand1
		iny
		lda (PTR_DOS_FCB_L),y
		sta Operand2
		ldy #DPB_BytesPerSector
		lda (PTR_DOS_DPB_L),y
		sta Operand3
		iny
		lda (PTR_DOS_DPB_L),y
		sta Operand4
		ldx #OS_Divide16_FN
		jsr OS_Function_Call
		lda Operand5
		ldy #FCB_ThisFATEntOffset
		sta (PTR_DOS_FCB_L),y
		lda Operand6
		iny
		sta (PTR_DOS_FCB_L),y
		rts
;******************
; FAT_LoadFATSector
; Load FCB_ThisFATSecNum into DOS_FATSectorBuffer
; and compare if it is already in buffer
; TODO: if FCB_ThisFATSecNum > DPB_SectorsPerFAT + DPB_ReservedSectors --> FAT Error
;******************
FAT_LoadFATSector
		lda Floppy_DriveNo		; check it the same drive number, $FF = empty
		cmp DOS_FATSectorBuffDrive	
		bne FAT_LoadFATSector1
		ldy #FCB_ThisFATSecNum
		lda (PTR_DOS_FCB_L),y
		cmp DOS_FATSectorBuffLBA_L	; check if sector already loaded
		bne FAT_LoadFATSector1
		ldy #FCB_ThisFATSecNum+1
		lda (PTR_DOS_FCB_L),y
		cmp DOS_FATSectorBuffLBA_H	
		beq FAT_LoadFATSector3		; if this is all true jump without loading
FAT_LoadFATSector1
		lda #<DOS_FATSectorBuff
		sta PTR_FLOPPY_L
		lda #>DOS_FATSectorBuff	
		sta PTR_FLOPPY_H		; set sector buffer start address
		ldy #FCB_ThisFATSecNum
		lda (PTR_DOS_FCB_L),y
		sta Floppy_Blk_Low
		iny
		lda (PTR_DOS_FCB_L),y
		sta Floppy_Blk_High		
		jsr Floppy_LBA2CHS		; LBA to CHS
		jsr FloppyReadCHS		; read Sector
		bcc FAT_LoadFATSector2
;		stz DOS_FATSectorBuffLBA_L	; reset buffer blk in case of floppy read error
;		stz DOS_FATSectorBuffLBA_H
		lda #$FF			; set FAT buffer blk illegal in case of floppy read error
		sta DOS_FATSectorBuffDrive
		rts
FAT_LoadFATSector2
		lda Floppy_Blk_Low
		sta DOS_FATSectorBuffLBA_L 	
		lda Floppy_Blk_High
		sta DOS_FATSectorBuffLBA_H 	; store current sector in sector buffer
		lda Floppy_DriveNo
		sta DOS_FATSectorBuffDrive	; store current drive
FAT_LoadFATSector3
		clc
		rts

;******************
; Load Data Sector from Floppy_blk
;
;******************
FAT_LoadDataSector
		lda Floppy_DriveNo		
		cmp DOS_DataSectorBuffDrive	; check drive number; FF = empty
		bne FAT_LoadDataSector1
		lda Floppy_Blk_Low
		cmp DOS_DataSectorBuffLBA_L	; check if sector already loaded
		bne FAT_LoadDataSector1
		lda Floppy_Blk_High
		cmp DOS_DataSectorBuffLBA_H	
		beq FAT_LoadDataSector3		; if it true jump without loading
FAT_LoadDataSector1
		lda #<DOS_DataSectorBuff
		sta PTR_FLOPPY_L
		lda #>DOS_DataSectorBuff	
		sta PTR_FLOPPY_H		; set sector buffer start address	
		jsr Floppy_LBA2CHS		; LBA to CHS
		jsr FloppyReadCHS		; read Sector
		bcc FAT_LoadDataSector2
;		stz DOS_FATSectorBuffLBA_L	;
;		stz DOS_FATSectorBuffLBA_H
		lda #$FF			; set buffer blk illegal in case of floppy read error
		sta DOS_DataSectorBuffDrive
		rts
FAT_LoadDataSector2
		lda Floppy_Blk_Low
		sta DOS_DataSectorBuffLBA_L 	
		lda Floppy_Blk_High
		sta DOS_DataSectorBuffLBA_H 	; store current sector in sector buffer
		lda Floppy_DriveNo
		sta DOS_DataSectorBuffDrive	; store current drive
FAT_LoadDataSector3
		clc
		rts

;******************
; FAT_GetFAT12ClusEntryVal
; Set Pointer: FAT12ClusEntryVal = *((WORD *) &SecBuff[ThisFATEntOffset]) 
;******************



FAT_GetFAT12ClusEntryVal
		lda #<DOS_FATSectorBuff
		sta PTR_FLOPPY_L
		lda #>DOS_FATSectorBuff		; set FAT sector buffer PRT to DOS_FATSectorBuffer start address
		sta PTR_FLOPPY_H
		lda PTR_FLOPPY_L
		ldy #FCB_ThisFATEntOffset	; add FCB_ThisFATEntOffset to PTR_FLOPPY
		clc
		adc (PTR_DOS_FCB_L),y
		sta PTR_FLOPPY_L
		lda PTR_FLOPPY_H
		iny
		adc (PTR_DOS_FCB_L),y
		sta PTR_FLOPPY_H
; check for sector boundary case
; If(FCB_ThisFATEntOffset == (DPB_BytesPerSector – 1))
		ldy #DPB_BytesPerSector
		lda (PTR_DOS_DPB_L),y
		sec
		sbc #1
		sta Operand1
		iny
		lda (PTR_DOS_DPB_L),y
		sbc #0
		sta Operand2
		lda Operand1
		ldy #FCB_ThisFATEntOffset
		cmp (PTR_DOS_FCB_L),y
		bne FAT_GetFAT12ClusEntryVal4	; no sector boundary case
		lda Operand2
		iny
		cmp (PTR_DOS_FCB_L),y		; no sector boundary case
		bne FAT_GetFAT12ClusEntryVal4
FAT_GetFAT12ClusEntryValBoundary		; in case of sector boundary
		lda FCB_Current_Cluster
		ldy #FCB_Current_Cluster
		lda (PTR_DOS_FCB_L),y
		pha				; save old FCB_Current_Cluster for later comparing for odd /even
		ldy #0
		lda (PTR_FLOPPY_L),y
		ldy #FCB_Current_Cluster
		sta (PTR_DOS_FCB_L),y
;		lda #<DOS_FATSectorBuff
;		sta PTR_FLOPPY_L
;		lda #>DOS_FATSectorBuff		; set FAT sector buffer PRT to DOS_FATSectorBuffer start address
;		sta PTR_FLOPPY_H
		ldy #FCB_ThisFATSecNum
		lda (PTR_DOS_FCB_L),y
		clc
		adc #1
		sta Floppy_Blk_Low
		iny
		lda (PTR_DOS_FCB_L),y
		adc #0
		sta Floppy_Blk_High
;		jsr Floppy_LBA2CHS		; LBA to CHS
;		jsr FloppyReadCHS		; read sector FCB_ThisFATSecNum to DOS_FATSectorBuffer
		jsr FAT_LoadFATSector
		bcc FAT_GetFAT12ClusEntryValBoundary2
		rts				; if something goes wrong then RTS
FAT_GetFAT12ClusEntryValBoundary2
;		lda Floppy_Blk_Low
;		sta DOS_FATSectorBuffLBA_L 	
;		lda Floppy_Blk_High
;		sta DOS_FATSectorBuffLBA_H	; store current sector in sector buffer status
;		lda Floppy_DriveNo
;		sta DOS_FATSectorBuffDrive	; store current drive in sector buffer status 
		lda #<DOS_FATSectorBuff
		sta PTR_FLOPPY_L
		lda #>DOS_FATSectorBuff		; set sector buffer start address again
		sta PTR_FLOPPY_H
		ldy #0
		lda (PTR_FLOPPY_L),y		; load high byte of the next sector
		ldy #FCB_Current_Cluster+1
		sta (PTR_DOS_FCB_L),y
		pla
		and #$01			; check if cluster number is odd or even
		beq FAT_GetFAT12ClusEntryValEven2
		bra FAT_GetFAT12ClusEntryValOdd2
FAT_GetFAT12ClusEntryVal4
		ldy #FCB_Current_Cluster
		lda (PTR_DOS_FCB_L),y
		and #$01
		bne FAT_GetFAT12ClusEntryValOdd
		bra FAT_GetFAT12ClusEntryValEven
;FAT12ClusEntryVal = FAT12ClusEntryVal >> 4;	; cluster number is ODD 
FAT_GetFAT12ClusEntryValOdd
		ldy #0
		lda (PTR_FLOPPY_L),y
		ldy #FCB_Current_Cluster
		sta (PTR_DOS_FCB_L),y
		ldy #1
		lda (PTR_FLOPPY_L),y
		ldy #FCB_Current_Cluster+1
		sta (PTR_DOS_FCB_L),y
FAT_GetFAT12ClusEntryValOdd2
;		jsr Debug_Output_Cluster
		ldx #4				; shift FCB_Current_Cluster 4 times right 
FAT_GetFAT12ClusEntryValOdd3
		clc
		ldy #FCB_Current_Cluster+1
		lda (PTR_DOS_FCB_L),y
		ror
		sta (PTR_DOS_FCB_L),y
		dey
		lda (PTR_DOS_FCB_L),y
		ror
		sta (PTR_DOS_FCB_L),y
		dex
		bne FAT_GetFAT12ClusEntryValOdd3
		iny
		lda (PTR_DOS_FCB_L),y
		and #$0F
		sta (PTR_DOS_FCB_L),y
;		jmp FAT_GetNextFat12ClusterCheckEnd
		rts
;Else
;FAT12ClusEntryVal = FAT12ClusEntryVal & 0x0FFF	; cluster number is EVEN 
FAT_GetFAT12ClusEntryValEven
		ldy #0
		lda (PTR_FLOPPY_L),y
		ldy #FCB_Current_Cluster
		sta (PTR_DOS_FCB_L),y
		ldy #1
		lda (PTR_FLOPPY_L),y
		ldy #FCB_Current_Cluster+1
		sta (PTR_DOS_FCB_L),y
FAT_GetFAT12ClusEntryValEven2
		ldy #FCB_Current_Cluster+1
		lda (PTR_DOS_FCB_L),y
		and #$0f
		sta (PTR_DOS_FCB_L),y
;		jmp FAT_GetNextFat12ClusterCheckEnd	; check for end of clusterchain
		rts

;******************
; FAT_CheckFAT12EOF
; check for end of clusterchain (>= 0x0FF7)
; TODO: Bad Cluster mark is 0x0FF7 --> need extra handling ?
;******************
FAT_CheckFAT12EOF
		ldy #FCB_Current_Cluster+1 	; check for end of clusterchain
		lda (PTR_DOS_FCB_L),y
		cmp #$0F
		bne FAT_CheckFAT12EOFExit
		dey
		lda (PTR_DOS_FCB_L),y
		cmp #$F7
		bcs FAT_CheckFAT12EOFLastExit
FAT_CheckFAT12EOFExit
		clc
		rts
FAT_CheckFAT12EOFLastExit
		sec
		rts

; FAT12 values
; FAT12 FAT[0] = 0x0FF9 = Media ID 
; FAT12 FAT[1] = 0x0FFF = EOC
; 0x0FF7 = bad cluster


;******************
; FAT_CountClustersFAT12
; In Development
; 
; CountofClusters = DPB_NumSectors / DPB_SectorsPerCluster
;******************

FAT_GetFAT12FreeCluster
		stz DOS_TotalFreeClusters_L
		stz DOS_TotalFreeClusters_H
		stz DOS_CurrentCluster_L
		stz DOS_CurrentCluster_H
		lda #02				; set offset to first available cluster
		ldy #FCB_Current_Cluster
		sta (PTR_DOS_FCB_L),y	
		lda #00
		iny				; FCB_Current_Cluster high byte
		sta (PTR_DOS_FCB_L),y	
FAT_GetFAT12FreeCluster_2
		jsr FAT_GetFAT12Offset
		jsr FAT_GetThisFAT12SecNum	
		jsr FAT_GetThisFAT12EntOffset
		jsr FAT_LoadFATSector
;		jsr DebugOutput2
		bcs FAT_GetFAT12FreeCluster_Error_Exit
		jsr FAT_GetFAT12ClusEntryVal
;		jsr DebugOutput
		ldy #FCB_Current_Cluster
		lda (PTR_DOS_FCB_L),y	
		cmp #$00
		bne FAT_GetFAT12FreeCluster_3
		iny				; #FCB_Current_Cluster+1
		lda (PTR_DOS_FCB_L),y	
		cmp #$00
		bne FAT_GetFAT12FreeCluster_3
		inc DOS_TotalFreeClusters_L
		bne FAT_GetFAT12FreeCluster_3		
		inc DOS_TotalFreeClusters_H
		beq FAT_GetFAT12FreeCluster_Error_Exit
FAT_GetFAT12FreeCluster_3
		jsr FAT_GetFAT12FreeClusterINC
		bcc FAT_GetFAT12FreeCluster_2
FAT_GetFAT12FreeCluster_Exit
		clc
FAT_GetFAT12FreeCluster_Error_Exit
		rts

;******************
; FAT_CheckFAT12ClusterINC
; Increase DOS_CurrentCluster 
; Set carry if DOS_CurrentCluster == DPB_CountofClusters
;******************
FAT_GetFAT12FreeClusterINC
		inc DOS_CurrentCluster_L
		bne FAT_GetFAT12FreeClusterINC_1
 		inc DOS_CurrentCluster_H
FAT_GetFAT12FreeClusterINC_1
		lda DOS_CurrentCluster_L
		ldy #DPB_CountofClusters
		cmp (PTR_DOS_DPB_L),y
		bne FAT_GetFAT12FreeClusterINC_2
		lda DOS_CurrentCluster_H
		iny				; #DPB_CountofClusters+1
		cmp (PTR_DOS_DPB_L),y				
		beq FAT_GetFAT12FreeClusterINC_End
FAT_GetFAT12FreeClusterINC_2
 		lda DOS_CurrentCluster_L
		ldy #FCB_Current_Cluster
		clc
		adc #$02	 		; set offset in FAT for first usable cluster
		sta (PTR_DOS_FCB_L),y
		lda DOS_CurrentCluster_H	
		iny				; #FCB_Current_Cluster+1
		adc #00				; add carry to high byte
		sta (PTR_DOS_FCB_L),y
		clc
		rts
FAT_GetFAT12FreeClusterINC_End
		sec
		rts
;******************
; DebugOutput
;******************

;DebugOutput
;		pha
;		phx
;		phy
;		php
;		
;		ldx #OS_ChangeSTDOUT_FN
;		lda #2				; 2 = UART 3 = Display
;		jsr OS_Function_Call

;		lda #<Debug_Text
;		sta PTR_String_L
;		lda #>Debug_Text
;		sta PTR_String_H
;		ldx #OS_Print_String_FN	
;		jsr OS_Function_Call

;		ldy #FCB_Current_Cluster+1
;		lda (PTR_DOS_FCB_L),y
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		ldy #FCB_Current_Cluster
;		lda (PTR_DOS_FCB_L),y
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		ldx #OS_Print_CHR_FN
;		lda #" "
;		jsr OS_Function_Call	

;		lda DOS_CurrentCluster_H
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		lda DOS_CurrentCluster_L
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		ldx #OS_Print_CHR_FN
;		lda #" "
;		jsr OS_Function_Call	

;		lda DOS_TotalFreeClusters_H
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		lda DOS_TotalFreeClusters_L
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		ldx #OS_Print_CHR_FN
;		lda #" "
;		jsr OS_Function_Call	

;		ldy #DPB_CountofClusters+1
;		lda (PTR_DOS_DPB_L),y
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		ldy #DPB_CountofClusters
;		lda (PTR_DOS_DPB_L),y
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call

;		ldx #OS_Print_CHR_FN
;		lda #CR
;		jsr OS_Function_Call		

;		ldx #OS_ChangeSTDOUT_FN
;		lda #3				; 2 = UART 3 = Display
;		jsr OS_Function_Call

;		plp
;		ply
;		plx
;		pla
;		rts	

;Debug_Text
;!text "FCB_Current_Cluster, DOS_CurrentCluster,  DOS_TotalFreeClusters, DPB_CountofClusters ",CR,00

;******************
; FAT_ConvNumClusttoBytes
; In Development
;
; DOS_TotalFreeClusters * DPB_SectorsPerFAT * DPB_SectorsPerCluster
;******************
FAT_NumClusttoBytes
		ldy #DPB_BytesPerSector
		lda (PTR_DOS_DPB_L),y
		sta Operand1
		iny
		lda (PTR_DOS_DPB_L),y
		sta Operand2			
		ldy #DPB_SectorsPerCluster
		lda (PTR_DOS_DPB_L),y
		sta Operand3
		ldx #OS_Multiply16_FN
		jsr OS_Function_Call	; DPB_BytesPerSector * DPB_SectorsPerCluster = output A Low, Y high
		sta Operand5
		sty Operand6
		lda DOS_TotalFreeClusters_L
		sta Operand7
		lda DOS_TotalFreeClusters_H
		sta Operand8
		jsr Multiply32		; (DPB_BytesPerSector * DPB_SectorsPerCluster) * DOS_TotalFreeClusters_L
		rts

;******************
; 16-bit multiply with 32-bit product 
; took from 6502.org
; multiplier x multiplicand = product
; Operand 5,6 x Operand 7,8 = Operand 1,2,3,4
; TODO: Move to lib.asm
;******************
 
Multiply32	
 		lda	#$00
		sta	Operand3	; clear upper bits of product
		sta	Operand4 
		ldx	#$10		; set binary count to 16 
Multiply32s_r	lsr	Operand6	; divide multiplier by 2 
		ror	Operand5
		bcc	Multiply32r_r 
		lda	Operand3	; get upper half of product and add multiplicand
		clc
		adc	Operand7
		sta	Operand3
		lda	Operand4
		adc	Operand8
Multiply32r_r	ror			; rotate partial product 
		sta	Operand4
		ror	Operand3
		ror	Operand2 
		ror	Operand1 
		dex
		bne	Multiply32s_r 
		rts





