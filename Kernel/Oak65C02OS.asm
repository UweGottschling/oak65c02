; Compiler : ACME (Compiler - Umstellung ab 10.03.2017)
; (C) 2017 Uwe Gottschling
; UTF-8 character encoding
; Delay for TMS9918 optimized for 2MHz CPU Clock
;
; TODO: Vectors for the jumptable so that they can be redirected

; Subroutine Register Usage:
; Call:
; A: Parameter Y: Optional Parameter
; A: Low Byte Adress Y: High Byte Adress
; X: Function Number
;
; Return:
; A: Parameter Y: Optional Parameter X: Optional Parameter
; A: Low Byte Adress Y: High Byte Adress
; Success: Carry flag cleared
; Failure: Carry flag set; A: Error Code

!src "include/label_definition.asm"
!src "DOS/DOS_FAT_constant.asm"
!cpu 65c02
OSVersionNoL = $18 ; minor version number
OSVersionNoH = $00 ; major version number
;*=$0ffe ; program counter only for RAM !!!
;!word $1000	; start vector only for RAM !!!

*=$E000 ; program counter $E000 (for 8K Byte, Bank #8)

;jmp Reset	; Only for RAM Start !!!

		; Jump Table
		jmp Reset		; TODO: move jump table to before the end of $FFE0
		jmp ACIA_Function_Call
		jmp VIA2_Init
		jmp FDC_Function_Call
		jmp VDP_Function_Call
		jmp PS2_Function_Call
		jmp $c003			;DOS_Function_Call --> init DOS
		; TODO : YMF Driver 

;***************************
; OS Function call
; A,Y = parameter
; X = function number
;***************************

OS_Function_Call
		jmp (OS_Function_Vector_L)
OS_Function_Call1
		pha
		txa
		asl
		tax
		cpx #OS_Function_Call_LUT_End-OS_Function_Call_LUT
		bcs OS_Function_Call_Error_Exit
		lda OS_Function_Call_LUT,x
		sta PTR_J_L
		lda OS_Function_Call_LUT+1,x
		sta PTR_J_H
		pla
		jsr OS_Function_Call_2
		rts
OS_Function_Call_2
		jmp (PTR_J_L)
OS_Function_Call_Error_Exit
		pla
		rts

OS_Function_Call_LUT

!word Print_CHR		; 0
!word Get_CHR		; 1
!word Print_String	; 2
!word Bin2BCD32		; 3
!word Print_Bin2Hex	; 4
!word Hex2byte		; 5
!word ChangeSTDOUT	; 6
!word ChangeSTDIN	; 7
!word XModem		; 8
!word LEditor		; 9
!word Print_BCD		; 10
!word Multiply16	; 11
!word Divide16		; 12
!word YMF262_Bell	; 13
!word DS1685_Print_Date	; 14
!word DS1685_Print_Time	; 15
!word STDIN_ASCII_TO_BCD; 16
!word Print_Bin2BCD_2D	; 17
!word ASCII_to_BCD	; 18
!word Print_BIN2DIG	; 19
OS_Function_Call_LUT_End

Reset
		sei			; disable interrupts
  		ldx #$FF                ; reset stack pointer to top of page
	    	txs
	    	lda #0                  ; clear A, X, and Y
;	    	tax
;	    	tay
		pha			; push zero to stack
	    	plp                     ; clear all flags, including decimal

		lda #00			; clear zero page
		tay
Reset3
		sta $0000,y		; clear zero-page
		sta $0200,y		; extended zero-page
		sta $0300,y		; extended zero-page
		iny
		bne Reset3
		lda #OSVersionNoL
		sta OS_Version_L
		lda #OSVersionNoH
		sta OS_Version_H

		lda #3				; standard IO = display / keyboard
		sta STDOUT			; standard out 3 = display (CBM-style)
		stz STDIN			; standard in 0 = keyboard (CBM-style)
		jsr OS_Initialise_Vectors	; initialise vectors
		jsr VIA1_Init
		lda #UART_Init_FN
		jsr ACIA_Function_Call		; UART 6551 initialisieren
 		jsr VIA2_Init

		jsr Call_VDP_Set_Text_Mode_0
		jsr Check_CPU_Type
		jsr Print_CPU_Type

		ldx #VDP_Detect_CTRL_Type_FN
		jsr VDP_Function_Call
		jsr VDP_Print_CTRL_Type

		jsr Startmessage
		jsr YMF262_Reset
		jsr YMF262_Bell
;		jsr DS1685_Init
		jsr DS1685_Print_Date
		jsr DS1685_Print_Time
		
		stz PTR_S_L		; set default start address
		lda #$10
		sta PTR_S_H

		jsr RAM_Test		; remove for testing the kernel in RAM !!!
Reset2
		cli			; enable interrupts
		ldx #PS2_DummyRead_FN
		jsr PS2_Function_Call		; PS2 keyboard dummy read
Reset1
		jsr Print_Startmonitortext
		jsr Main
		jmp Reset1

;***************************
; Print monitor message
;
;***************************
Print_Startmonitortext
		lda #<Startmonitortext		; < low byte of expression
		sta PTR_String_L
		lda #>Startmonitortext		; > high byte of expression
		sta PTR_String_H
;		ldx #OS_Print_String_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_String
		jsr Print_Start_Address

		lda #<Startmonitortext2		; < low byte of expression
		sta PTR_String_L
		lda #>Startmonitortext2		; > high byte of expression
		sta PTR_String_H
;		ldx #OS_Print_String_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_String
		rts

Startmonitortext
!text LF
!text "Main Menu:",LF
!text "1 JSR: $",00

Startmonitortext2
!text "2 Set Textmode1 (40x24)",LF
!text "3 Set Graphic1 (32x24)",LF
!text "4 Set Textmode2  (80x26)",LF
!text "5 YMF262 Test",LF
!text "6 RTC Setting",LF
!text "7 DOS",LF
!text "u Set STDIO to UART",LF
!text "d Set STDIO to VDP/Keyboard",LF
!text "h Help Text",LF
!text "m WOZMON",LF
!text "l Line Editor Test",LF
!text "x XModem",LF
!text 00

;***************************
; Main loop
;
;***************************
Main
		ldx #OS_Get_CHR_FN
		jsr OS_Function_Call
		pha
;		ldx #OS_Print_CHR_FN
;		jsr OS_Function_Call	; echo character
		jsr OS_Internal_Print_CHR
		pla
		ldx #End_Commands-Commands
Main1
		dex
		bmi Main2
		cmp Commands,x
		bne Main1

		txa
		asl
		tax
		lda Command_LUT,x
		sta PTR_J_L
		lda Command_LUT+1,x
		sta PTR_J_H
		jmp (PTR_J_L)
Main2
		jsr UnknownCommand
		jmp Main

Back_Main
		rts

Commands
!text "1"
!text "2"
!text "3"
!text "4"
!text "5"
!text "6"
!text "7"
!text "u"
!text "d"
!text "h"
!text "m"
!text "l"
!text "x"
!text CR
!text LF
End_Commands

Command_LUT
!word Start_RAM_Programm		;1 JSR $1000
!word Call_VDP_Set_Text_Mode_0		;2 Screen Mode 0 (40x24)
!word Call_VDP_Set_Video_Mode_1		;3 Screen Mode 1 (32x24)
!word Call_VDP_Set_Text_Mode_13		;4 Screen Mode 13 (80x26)
!word YMF262_Main			;5 OPL3 / YMF262 Test
!word DS1685_Main			;6 Real Time Clock Setting
!word $c000				;7 DOS
!word SetSTDIOtoUART			;u Set STDIO to UART
!word SetSTDIOtoKeyDis			;d Set STDIO to display/keyboard
!word Back_Main				;h Help Text
!word Wozmon				;m WOZMON
!word LEditor_Test			;l Line Editor Test
!word XModem				;x XModem
!word Main				;CR
!word Main				;LF

;***************************
; RAM-Test:
; init zero-page and extended zero-page
; Check low 48 KByte of memory
; TODO : Check  memtop !
;***************************
RAM_Test
		lda #<Text_Memtest	; <    low byte of expression
		sta PTR_String_L
		lda #>Text_Memtest	; >    high byte of expression
		sta PTR_String_H
		jsr OS_Internal_Print_String

		lda #03
		sta PTR_W_H
		lda #00
		sta PTR_W_L
RAM_Test4
		lda (PTR_W_L),y
		tax			; save old value
		lda #$55 		; testpattern $55
		sta (PTR_W_L),y
		cmp (PTR_W_L),y
		bne RAM_Test_Fail
		rol			; testpattern $AA
		sta (PTR_W_L),y
		cmp (PTR_W_L),y
		bne RAM_Test_Fail
		txa
		sta (PTR_W_L),y		; set old value again
		iny
		bne RAM_Test4
		inc PTR_W_H
		lda PTR_W_H
		cmp #$C0		; stop at the end of $BFFF
		bne RAM_Test4
		sta MEMTOP_H		; (High byte end address)
		tya
		sta MEMTOP_L		; save Y-register (low byte end address)
		lda #"O"
;		ldx #OS_Print_CHR_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR
		lda #"K"
;		ldx #OS_Print_CHR_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR
		clc
		jsr RAM_Test_Print_Memtop
		rts
RAM_Test_Fail
		tya
		sta MEMTOP_L		; Save Y-register (low byte end address)
		lda PTR_W_H
		sta MEMTOP_H		; (high byte end address)
		jsr Print_Ram_Test_Fail
		jsr RAM_Test_Print_Memtop
		sec
		rts

RAM_Test_Print_Memtop
		lda #<Text_MEMTOP	; <    low byte of expression
		sta PTR_String_L
		lda #>Text_MEMTOP	; >    high byte of expression
		sta PTR_String_H
		jsr OS_Internal_Print_String		
		lda MEMTOP_H	
		jsr OS_Internal_Print_Bin2Hex
		lda MEMTOP_L
		jsr OS_Internal_Print_Bin2Hex		
		lda #CR
		jsr OS_Internal_Print_CHR
		rts		

Text_Memtest
!text "Memtest: "
!text 00

Text_MEMTOP
!text " MEMTOP: $"
!text 00

;***************************
; Print RAM-Test Fail Message
;
;***************************

Print_Ram_Test_Fail
		lda #<Text_Memtest_Fail		; <    low byte of expression
		sta PTR_String_L
		lda #>Text_Memtest_Fail		; >    high byte of expression
		sta PTR_String_H
		jsr OS_Internal_Print_String
;		ldx #OS_Print_String_FN
;		jsr OS_Function_Call
		rts

Text_Memtest_Fail
!text "failed"
!text 00


;***************************
; Detecting CPU Type
; from http://golombeck.eu
; $00: for a 6502 CPU
; $01: for a 65C02 CPU
; $02: for a 65816 CPU
; Checked with R65C02P2, W65C816S
;***************************
!cpu 65816
Check_CPU_Type

        	bra 	Check_CPU_Type1 ; results in a BRA +4 if 65C02/65816
          	lda #00
          	beq 	Check_CPU_Type2	; a 6502 drops directly through here
Check_CPU_Type1
	  	lda 	#01	  	; 65C02/65816 arrives here
				  	; some 65816 code here
        	xba               	; put $01 in B accu -> =NOP for 65C02
        	dec	         	; A=$00 if 65C02
        	xba               	; get $01 back if 65816
        	inc  	         	; makes $01 (65C02) or $02 (65816)
Check_CPU_Type2
    		sta 	CPU_Type    	; save variable value
!cpu 65C02
		rts

;***************************
; Print CPU Type
;
;***************************

Print_CPU_Type
		lda #<Text_CPU_Type		; <    low byte of expression
		sta PTR_String_L
		lda #>Text_CPU_Type		; >    high byte of expression
		sta PTR_String_H
;		ldx #OS_Print_String_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_String
		lda CPU_Type
		bne Print_CPU_Type1

Print_CPU_Type0
		lda #<Text_CPU_Type_NMOS	; <    low byte of expression
		sta PTR_String_L
		lda #>Text_CPU_Type_NMOS	; >    high byte of expression
		sta PTR_String_H
;		ldx #OS_Print_String_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_String
		rts
Print_CPU_Type1
		cmp #1
		bne Print_CPU_Type2
		lda #<Text_CPU_Type_CMOS	; <    low byte of expression
		sta PTR_String_L
		lda #>Text_CPU_Type_CMOS	; >    high byte of expression
		sta PTR_String_H
;		ldx #OS_Print_String_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_String
		rts

Print_CPU_Type2
		lda #<Text_CPU_Type_65816	; <    low byte of expression
		sta PTR_String_L
		lda #>Text_CPU_Type_65816	; >    high byte of expression
		sta PTR_String_H
;		ldx #OS_Print_String_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_String
		rts

Text_CPU_Type
!text "CPU Type: 65"
!text 00

Text_CPU_Type_NMOS
!text "02",LF
!text 00

Text_CPU_Type_CMOS
!text "C02",LF
!text 00

Text_CPU_Type_65816
!text "C816",LF
!text 00

;***************************
; Print VDP Type
;
;***************************

VDP_Print_CTRL_Type
		lda #<VDPTypeTXT
		sta PTR_String_L
		lda #>VDPTypeTXT
		sta PTR_String_H
;		ldx #OS_Print_String_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_String		

		lda VDP_Type
		cmp #4
		bne VDP_Print_CTRL_TypeTMS

		lda #<VDPTypeV9958TXT
		sta PTR_String_L
		lda #>VDPTypeV9958TXT
		sta PTR_String_H
;		ldx #OS_Print_String_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_String
		rts

VDP_Print_CTRL_TypeTMS
		lda #<VDPTypeTMS99X8TXT
		sta PTR_String_L
		lda #>VDPTypeTMS99X8TXT
		sta PTR_String_H
;		ldx #OS_Print_String_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_String
		rts


VDPTypeTXT
!text "VDP type: ",00

VDPTypeTMS99X8TXT
!text "TMS99X8",LF,00

VDPTypeV9958TXT
!text "V9958",LF,00

;***************************
; VDP Menu Calls
;
;***************************

Call_VDP_Set_Text_Mode_0
		ldx #VDP_Set_Video_Mode_FN
		lda #TMS9918_Set_Screen0
		jsr VDP_Function_Call
		jmp Call_VDP_Init_Text

Call_VDP_Set_Video_Mode_1
		ldx #VDP_Set_Video_Mode_FN
		lda #TMS9918_Set_Screen1
		jsr VDP_Function_Call
		jmp Call_VDP_Init_Text

Call_VDP_Set_Text_Mode_13
		ldx #VDP_Set_Video_Mode_FN
		lda #V9938_Set_Screen0_W80
		jsr VDP_Function_Call

Call_VDP_Init_Text
		ldx #VDP_Init_Pattern_FN
		jsr VDP_Function_Call
		ldx #VDP_Clear_Text_Screen_FN
		jsr VDP_Function_Call
		rts

;Call_BCD_Test

;		lda #$4a
;		sta Operand1
;		lda #$a1
;		sta Operand2
;		stz Operand3
;		stz Operand4
;		ldx #OS_Bin2BCD32_FN
;		jsr OS_Function_Call
;		ldx #OS_Print_BCD_FN
;		jsr OS_Function_Call
;		lda #CR
;		ldx #OS_Print_CHR_FN
;		jsr OS_Function_Call
;		rts


;***************************
; Change Standard Output Device
;
;***************************
ChangeSTDOUT	; 2 = UART 3 = Display
		cmp #2
		bne ChangeSTDOT2
		sta STDOUT
ChangeSTDOT2
		cmp #3
		bne ChangeSTDOT3
		sta STDOUT
		clc
		rts
ChangeSTDOT3
		sec 			; carry 1 = error
		rts

;***************************
; Change Standard Input Device
;
;***************************
ChangeSTDIN
		cmp #0	; 0 = Keyboard
		bne ChangeSTDIN2
		sta STDIN
ChangeSTDIN2
		cmp #2	; 2 = UART
		bne ChangeSTDIN3
		sta STDIN
		clc
		rts
ChangeSTDIN3
		sec 			; carry 1 = error
		rts

;***************************
; Set UART to default IO
;
;***************************

SetSTDIOtoUART
		lda #2
		jsr ChangeSTDOUT
		jsr ChangeSTDIN
		rts

;***************************
; Set Keyboard / Display to default IO
;
;***************************

SetSTDIOtoKeyDis
		lda #3
		jsr ChangeSTDOUT
		lda #0
		jsr ChangeSTDIN
		rts

;***************************
; initialise vectors
;
;***************************

OS_Initialise_Vectors
		ldx #OS_Vectors_LUT_End-OS_Vectors_LUT
OS_Initialise_Vectors2
		lda OS_Vectors_LUT,x
		sta IRQVector_L,x
		dex
		bpl OS_Initialise_Vectors2
		rts
OS_Vectors_LUT
!word IRQExit
!word BRKService1
!word NMIService1
!word ABORT_E_Service_Exit
!word COP_E_Service_Exit
!word BRKService816_2
!word ACIA_Function_Call1
!word FDC_Function_Call1
!word VDP_Function_Call1
!word PS2_Function_Call1
!word OS_Function_Call1
!word $c006				; fixed DOS_Function_Vector
OS_Vectors_LUT_End

;***************************
; Line Editor test
;
;***************************

LEditor_Test
		ldx #OS_LineEditor_FN
		jsr OS_Function_Call
		lda #<InBuffer		; < low byte of expression
		sta PTR_String_L
		lda #>InBuffer		; > high byte of expression
		sta PTR_String_H
;		ldx #OS_Print_String_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_String
		rts

;***************************
; Jump to PTR_S
;
;***************************

Start_RAM_Programm
		jsr Start_RAM_Programm_2
		rts
Start_RAM_Programm_2
		jmp (PTR_S_L)

;******************
; DOS_print binary start address
;******************

Print_Start_Address
		lda PTR_S_H		; set start address for both pointer
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_Bin2Hex
		lda PTR_S_L
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_Bin2Hex
		lda #LF
;		ldx #OS_Print_CHR_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR
		rts

;***************************
; TMS9918_Startmessage
; Input: Output: OS-Version Number, Info Text
;***************************
Startmessage
		lda #<Starttext1	; <    low byte of expression
		sta PTR_String_L
		lda #>Starttext1	; >    high byte of expression
		sta PTR_String_H
		jsr OS_Internal_Print_String
		lda OS_Version_H
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_Bin2Hex
		lda #"."
;		ldx #OS_Print_CHR_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_CHR
		lda OS_Version_L
;		ldx #OS_Print_Bin2Hex_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_Bin2Hex

		lda #<Starttext2	; <    low byte of expression
		sta PTR_String_L
		lda #>Starttext2	; >    high byte of expression
		sta PTR_String_H
;		ldx #OS_Print_String_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_String
		rts

Starttext1
; text block width 31 char
!text $99,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$8B,LF
!text $8A,"Uwe's Oak65C02OS, V:",00
Starttext2
!text "    ",$8A,LF
!text $8A,"RS232: 19200 baud, 8N1       ",$8A,LF
!text $98,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$9D,$8C,LF
!text 00
End_Starttextvideo

;***************************
; Print Error Message to STDOUT
;
;***************************

UnknownCommand				; print unknown key
		phx
		pha
		lda #<UnknownCommand_text
		sta PTR_String_L
		lda #>UnknownCommand_text
		sta PTR_String_H
;		ldx #OS_Print_String_FN
;		jsr OS_Function_Call
		jsr OS_Internal_Print_String
		pla 
		plx
		rts

UnknownCommand_text
!text "Unknown key",LF,00

;***************************
; ABORT Service Routine
;
;***************************

ABORT_E_Service
		jmp (ABORT_E_Vector_L)
ABORT_E_Service_Exit
		rti

;***************************
; COP Service Routine
;
;***************************

COP_E_Service
		jmp (COP_E_Vector_L)
COP_E_Service_Exit
		rti

;***************************
; BRK Service Routine
;
;***************************

BRKService
		jmp (BRKVector_L)
BRKService1
		pla
		sta PRegister
		pla
		sta PCLRegister
		pla
		sta PCHRegister
		pha
		lda PCLRegister
		pha
		lda PRegister
		pha
		lda #<BRKtext		; < low byte of expression
		sta PTR_String_L
		lda #>BRKtext		; > high byte of expression
		sta PTR_String_H
;		jsr Print_String
		jsr OS_Internal_Print_String
		lda ARegister
		jsr Print_Bin2Hex	; print Hex Byte to stdout
		lda #SP
		jsr Print_CHR
		lda XRegister
		jsr Print_Bin2Hex	; print Hex Byte to stdout
		lda #SP
		jsr Print_CHR
		lda YRegister
		jsr Print_Bin2Hex	; print Hex Byte to stdout
		lda #SP
		jsr Print_CHR
		lda PRegister
		jsr Print_BIN2DIG
		lda #SP
		jsr Print_CHR
		lda SPRegister		; get status register
		jsr Print_Bin2Hex	; print Hex Byte to stdout
		lda #SP
		jsr Print_CHR
		lda PCHRegister
		jsr Print_Bin2Hex	; print Hex Byte to stdout
		lda PCLRegister
		jsr Print_Bin2Hex	; print Hex Byte to stdout
		lda #LF
		jsr Print_CHR

		ldx #$ff
		txs			; init stackpointer
		cli
		jmp Reset1 		; back to system

;BRKService2
;		jsr Print_Bin2Hex	; print Hex Byte to stdout
;		rts

BRKtext
!text LF,"BRK",LF,"Register content",LF
!text "A  X  Y  NV1BDIZC S  PC",LF,00

;***************************
; IRQ Service Routine
;
;***************************

;TODO: faster IRQ with push and pull registers to stack

IRQService
		sta ARegister		; save A 3 Zykl.
		stx XRegister		; save X
		sty YRegister		; save Y
		tsx			; transfer stackPTR to x
		stx SPRegister		; save stack pointer
		pla			; get status register from stack
		pha
;		sta PRegister		; store status register (flags)
		and #$10		; mask BRK bit (only 65(C)02)
		beq IRQService2		; if no BRK jump
		jmp BRKService
IRQService2
		jmp (IRQVector_L)
IRQExit
		lda ARegister
		ldx XRegister
		ldy YRegister
		rti			; return from interrupt
		
; Alternative IRQService:
;IRQ      STA   ACC			; 3 Zykl.
;         PLA				; 4 Zykl
;         PHA				; 3 Zykl
;         ASL				; 2 Zykl.
;         ASL				; 2 Zykl.
;         ASL				; 2 Zykl.
;         BMI   BRKService
;         JMP   (IRQService2)		

;***************************
; OS internal functions
;
;***************************		

OS_Internal_Print_String
		ldx #OS_Print_String_FN
		jsr OS_Function_Call	
		rts
OS_Internal_Print_CHR	
		ldx #OS_Print_CHR_FN
		jsr OS_Function_Call
		rts
OS_Internal_Print_Bin2Hex				
		ldx #OS_Print_Bin2Hex_FN
		jsr OS_Function_Call				
		rts
OS_Internal_STDIN_ASCII_TO_BCD
		ldx #OS_STDIN_ASCII_TO_BCD_FN
		jsr OS_Function_Call				
		rts

;***************************
; NMI Service Routine
;
;***************************
NMIService
		jmp (NMIVector_L)
NMIService1
		sta ARegister		; save A 3 Zykl.
		stx XRegister		; save X
		sty YRegister		; save Y
		tsx			; transfer stackPTR to x
		stx SPRegister
		pla
		pha 			; get Status register from stack
		sta PRegister
		jmp BRKService
;***************************
; BRK 65C816 Service Routine
;
;***************************


BRKService816
		!CPU 65816		; push direct register to stack and set direct register to 0
		rep #$30		; 16 bit all registers
		!al			; ACME compiler: accumulator long
		!rl			; ACME compiler: index register long
		phb			; push data bank register on stack (8-Bit)
		phd			; push direct register on stack (16-Bit)
		pea $0000
		pld
		jmp (BRKService816Vector_L)
BRKService816_2
		sta $000000+ARegister	; $000000+ --> for telling ACME that this is a 24 bit address
		txa
		sta $000000+XRegister
		tya
		sta $000000+YRegister
		pla
		sta $000000+DRegister
		sep #$20		; 8 bit accumulator
		!as
		lda #$00
		pha
		plb			; set data bank register to $00
		pla
		sta DBRegister
		pla
		sta PRegister
		plx
		stx PCLRegister
		pla
		sta PBRegister
		tsx
		stx SPRegister
		sec 			; SEt Carry bit
		xce 			; eXchange Carry and Emulation bit
		!CPU 65c02
		lda #<BRKTXT65816	; < low byte of expression
		sta PTR_String_L
		lda #>BRKTXT65816	; > high byte of expression
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		ldy #BRegister-PRegister
BRKService816_3
		lda PRegister,y
		phy
		jsr Print_Bin2Hex	; print Hex Byte to stdout
		lda #SP
		jsr Print_CHR
		ply
		dey
		bne BRKService816_3
		lda #<SRF65816		; < low byte of expression
		sta PTR_String_L
		lda #>SRF65816		; > high byte of expression
		sta PTR_String_H
		ldx #OS_Print_String_FN
		jsr OS_Function_Call
		lda PRegister		; print processor status register (P)
		jsr Print_BIN2DIG
BRKService816_Exit
		ldx #$ff
		txs			; init stackpointer
		cli
		jmp Reset1 		; back to system

BRKTXT65816
!text LF,"Native mode break routine, enter emulation mode",LF
!text "Register content",LF
!text "B  A  XH XL YH YL SH SL PH PL DB PB DH DL",LF,00
SRF65816
!text LF,"NVMXDIZC",LF,00

!src "include/XMODEM.asm"
!src "include/WOZMON.asm"
;!src "include/New_DOS_65C02.asm"
!src "driver/WD177X_Driver.asm"
!src "driver/VIA6522_Driver.asm"
!src "driver/VDP_Driver.asm"
!src "driver/VDP_Font.asm"
!src "driver/PS2KEY_Driver.asm"
!src "driver/ACIA6551_Driver.asm"
!src "driver/YMF262_Driver.asm"
!src "driver/DS1685_Driver.asm"
;!src "fs/FAT.asm"
!src "include/lib.asm"
!src "include/CPU_Vectors.asm" ; Remove for testing the kernel in RAM !!!



