EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "Oak-65XX-WD1772"
Date "2020-01-06"
Rev "0.2"
Comp "Uwe Gottschling"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 65xx-WD1772-rescue:D_TVS_UNI D1
U 1 1 59EFA4EC
P 14150 1250
F 0 "D1" H 14150 1350 50  0000 C CNN
F 1 "P6KE6,8A" H 14150 1150 50  0000 C CNN
F 2 "" H 14150 1250 50  0001 C CNN
F 3 "" H 14150 1250 50  0001 C CNN
	1    14150 1250
	0    -1   -1   0   
$EndComp
$Comp
L 65xx-WD1772-rescue:C64AC_DIN41612_BUS X1
U 1 1 59EFA531
P 15300 4850
F 0 "X1" H 15200 8150 50  0000 C CNN
F 1 "C64AC_DIN41612_BUS" V 15700 4800 50  0000 C CNN
F 2 "" H 15200 4850 50  0001 C CNN
F 3 "" H 15200 4850 50  0001 C CNN
	1    15300 4850
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:+5V #PWR01
U 1 1 59EFB08C
P 14150 950
F 0 "#PWR01" H 14150 800 50  0001 C CNN
F 1 "+5V" H 14150 1090 50  0000 C CNN
F 2 "" H 14150 950 50  0001 C CNN
F 3 "" H 14150 950 50  0001 C CNN
	1    14150 950 
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:GNDD #PWR02
U 1 1 59EFB0C4
P 14150 1600
F 0 "#PWR02" H 14150 1350 50  0001 C CNN
F 1 "GNDD" H 14150 1475 50  0000 C CNN
F 2 "" H 14150 1600 50  0001 C CNN
F 3 "" H 14150 1600 50  0001 C CNN
	1    14150 1600
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:74HCT245 U10
U 1 1 59EFB954
P 12700 2450
F 0 "U10" H 13000 2950 50  0000 L CNN
F 1 "74HCT245" H 12800 1850 50  0000 L CNN
F 2 "" H 12700 2450 60  0001 C CNN
F 3 "" H 12700 2450 60  0001 C CNN
	1    12700 2450
	1    0    0    -1  
$EndComp
Text GLabel 14550 2700 0    50   Output ~ 0
R_~W
Text GLabel 11850 2000 0    50   Input ~ 0
R/~W
Text GLabel 11350 2100 0    50   Input ~ 0
DATABUS
Text Label 12000 2200 0    50   ~ 0
D0
Text Label 12000 2300 0    50   ~ 0
D1
Text Label 12000 2400 0    50   ~ 0
D2
Text Label 12000 2500 0    50   ~ 0
D3
Text Label 12000 2600 0    50   ~ 0
D4
Text Label 12000 2700 0    50   ~ 0
D5
Text Label 12000 2800 0    50   ~ 0
D6
Text Label 12000 2900 0    50   ~ 0
D7
Entry Wire Line
	11700 2100 11800 2200
Entry Wire Line
	11700 2200 11800 2300
Entry Wire Line
	11700 2300 11800 2400
Entry Wire Line
	11700 2400 11800 2500
Entry Wire Line
	11700 2500 11800 2600
Entry Wire Line
	11700 2600 11800 2700
Entry Wire Line
	11700 2700 11800 2800
Entry Wire Line
	11700 2800 11800 2900
Text GLabel 14550 2900 0    50   Output ~ 0
PHI2
Entry Wire Line
	13100 3300 13200 3400
Entry Wire Line
	13100 3400 13200 3500
Entry Wire Line
	13100 3500 13200 3600
Entry Wire Line
	13100 3600 13200 3700
Text GLabel 12850 3300 0    50   Output ~ 0
ADDRESSBUS
Text Label 13250 3400 0    50   ~ 0
A0
Text Label 13250 3500 0    50   ~ 0
A1
Text Label 13250 3600 0    50   ~ 0
A2
Text Label 13250 3700 0    50   ~ 0
A3
Entry Wire Line
	13100 3700 13200 3800
Entry Wire Line
	13100 3800 13200 3900
Entry Wire Line
	13100 3900 13200 4000
Entry Wire Line
	13100 4100 13200 4200
Text Label 13250 3800 0    50   ~ 0
A4
Text Label 13250 3900 0    50   ~ 0
A5
Text Label 13250 4000 0    50   ~ 0
A6
Text Label 13250 4200 0    50   ~ 0
A7
$Comp
L 65xx-WD1772-rescue:WD1772-PH U1
U 1 1 59F4FB0A
P 3350 5100
F 0 "U1" H 3350 5850 60  0000 C CNN
F 1 "WD1772-PH" H 3350 4250 60  0000 C CNN
F 2 "" H 3350 5400 60  0000 C CNN
F 3 "" H 3350 5400 60  0000 C CNN
	1    3350 5100
	1    0    0    -1  
$EndComp
Text GLabel 1850 5000 0    50   BiDi ~ 0
DATABUS
Text Label 2500 4900 0    50   ~ 0
D0
Text Label 2500 5000 0    50   ~ 0
D1
Text Label 2500 5100 0    50   ~ 0
D2
Text Label 2500 5200 0    50   ~ 0
D3
Text Label 2500 5300 0    50   ~ 0
D4
Text Label 2500 5400 0    50   ~ 0
D5
Text Label 2500 5500 0    50   ~ 0
D6
Text Label 2500 5600 0    50   ~ 0
D7
$Comp
L 65xx-WD1772-rescue:Crystal_oscillator U7
U 1 1 59F4FC82
P 12300 9150
F 0 "U7" H 12250 9400 60  0000 C CNN
F 1 "8MHz" H 12300 8900 60  0000 C CNN
F 2 "" H 12250 9150 60  0001 C CNN
F 3 "" H 12250 9150 60  0001 C CNN
	1    12300 9150
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:+5V #PWR03
U 1 1 59F4FD76
P 12950 8950
F 0 "#PWR03" H 12950 8800 50  0001 C CNN
F 1 "+5V" H 12950 9090 50  0000 C CNN
F 2 "" H 12950 8950 50  0001 C CNN
F 3 "" H 12950 8950 50  0001 C CNN
	1    12950 8950
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:GNDD #PWR04
U 1 1 59F4FDD6
P 11750 9450
F 0 "#PWR04" H 11750 9200 50  0001 C CNN
F 1 "GNDD" H 11750 9325 50  0000 C CNN
F 2 "" H 11750 9450 50  0001 C CNN
F 3 "" H 11750 9450 50  0001 C CNN
	1    11750 9450
	1    0    0    -1  
$EndComp
Text GLabel 12950 9250 2    50   Output ~ 0
8MHz
Text GLabel 4850 6350 2    50   Input ~ 0
8MHz
$Comp
L 65xx-WD1772-rescue:74HCT138 U6
U 1 1 59F50070
P 12300 7700
F 0 "U6" H 12450 8050 50  0000 C CNN
F 1 "74HCT138" H 12650 7050 50  0000 C CNN
F 2 "" H 12300 7700 60  0001 C CNN
F 3 "" H 12300 7700 60  0001 C CNN
	1    12300 7700
	1    0    0    -1  
$EndComp
Entry Wire Line
	11350 8050 11450 8150
Entry Wire Line
	11350 7950 11450 8050
Entry Wire Line
	11350 7850 11450 7950
Text GLabel 11200 7400 0    50   Input ~ 0
ADDRESSBUS
Entry Wire Line
	11350 7500 11450 7600
Text Label 11550 7600 0    50   ~ 0
A7
Text Label 11550 7950 0    50   ~ 0
A4
Text Label 11550 8050 0    50   ~ 0
A5
Text Label 11550 8150 0    50   ~ 0
A6
Text GLabel 13200 8100 2    50   Output ~ 0
~CS
$Comp
L 65xx-WD1772-rescue:+5V #PWR05
U 1 1 59F5064B
P 11500 7300
F 0 "#PWR05" H 11500 7150 50  0001 C CNN
F 1 "+5V" H 11500 7440 50  0000 C CNN
F 2 "" H 11500 7300 50  0001 C CNN
F 3 "" H 11500 7300 50  0001 C CNN
	1    11500 7300
	1    0    0    -1  
$EndComp
Text GLabel 11200 7100 0    50   Input ~ 0
~IORQ
$Comp
L 65xx-WD1772-rescue:74LS244 U2
U 1 1 59F5099E
P 3350 7050
F 0 "U2" H 3700 7450 50  0000 C CNN
F 1 "74LS244" H 3600 6750 50  0000 C CNN
F 2 "" H 3350 7050 60  0001 C CNN
F 3 "" H 3350 7050 60  0001 C CNN
	1    3350 7050
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:74LS244 U2
U 2 1 59F50A5D
P 3350 8550
F 0 "U2" H 3700 8950 50  0000 C CNN
F 1 "74LS244" H 3600 8250 50  0000 C CNN
F 2 "" H 3350 8550 60  0001 C CNN
F 3 "" H 3350 8550 60  0001 C CNN
	2    3350 8550
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:7406 U4
U 6 1 59F50BA0
P 2650 3000
F 0 "U4" H 3000 3150 50  0000 C CNN
F 1 "7406" H 2700 3000 50  0000 C CNN
F 2 "" H 2650 3000 60  0001 C CNN
F 3 "" H 2650 3000 60  0001 C CNN
	6    2650 3000
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:7406 U4
U 2 1 59F50C89
P 5750 5550
F 0 "U4" H 6100 5700 50  0000 C CNN
F 1 "7406" H 5800 5500 50  0000 C CNN
F 2 "" H 5750 5550 60  0001 C CNN
F 3 "" H 5750 5550 60  0001 C CNN
	2    5750 5550
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:7406 U4
U 3 1 59F50D30
P 5950 1850
F 0 "U4" H 6100 2150 50  0000 C CNN
F 1 "7406" H 6200 1500 50  0000 C CNN
F 2 "" H 5950 1850 60  0001 C CNN
F 3 "" H 5950 1850 60  0001 C CNN
	3    5950 1850
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:7406 U4
U 5 1 59F50D71
P 5750 4650
F 0 "U4" H 6100 4800 50  0000 C CNN
F 1 "7406" H 5800 4650 50  0000 C CNN
F 2 "" H 5750 4650 60  0001 C CNN
F 3 "" H 5750 4650 60  0001 C CNN
	5    5750 4650
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:7406 U4
U 1 1 59F50E0A
P 6700 5850
F 0 "U4" H 7050 6000 50  0000 C CNN
F 1 "7406" H 6700 5850 50  0000 C CNN
F 2 "" H 6700 5850 60  0001 C CNN
F 3 "" H 6700 5850 60  0001 C CNN
	1    6700 5850
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:7406 U4
U 4 1 59F50EAC
P 6600 5100
F 0 "U4" H 6950 5250 50  0000 C CNN
F 1 "7406" H 6650 5100 50  0000 C CNN
F 2 "" H 6600 5100 60  0001 C CNN
F 3 "" H 6600 5100 60  0001 C CNN
	4    6600 5100
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:74LS273 U3
U 1 1 59F50F7F
P 4700 2050
F 0 "U3" H 5050 2550 50  0000 C CNN
F 1 "74LS273" H 4700 1450 50  0000 C CNN
F 2 "" H 4700 2050 60  0001 C CNN
F 3 "" H 4700 2050 60  0001 C CNN
	1    4700 2050
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:Conn_02x17_Odd_Even J1
U 1 1 59F51094
P 9150 5250
F 0 "J1" H 9200 6150 50  0000 C CNN
F 1 "Floppy Connector" H 9200 4350 50  0000 C CNN
F 2 "" H 9150 5250 50  0001 C CNN
F 3 "" H 9150 5250 50  0001 C CNN
	1    9150 5250
	-1   0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:GNDD #PWR06
U 1 1 59F51703
P 9650 6350
F 0 "#PWR06" H 9650 6100 50  0001 C CNN
F 1 "GNDD" H 9650 6225 50  0000 C CNN
F 2 "" H 9650 6350 50  0001 C CNN
F 3 "" H 9650 6350 50  0001 C CNN
	1    9650 6350
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:R R2
U 1 1 59F52324
P 7600 4100
F 0 "R2" V 7680 4100 50  0000 C CNN
F 1 "1K" V 7600 4100 50  0000 C CNN
F 2 "" V 7530 4100 50  0001 C CNN
F 3 "" H 7600 4100 50  0001 C CNN
	1    7600 4100
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:R R3
U 1 1 59F5252F
P 7750 4100
F 0 "R3" V 7830 4100 50  0000 C CNN
F 1 "1K" V 7750 4100 50  0000 C CNN
F 2 "" V 7680 4100 50  0001 C CNN
F 3 "" H 7750 4100 50  0001 C CNN
	1    7750 4100
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:R R4
U 1 1 59F52577
P 7900 4100
F 0 "R4" V 7980 4100 50  0000 C CNN
F 1 "1K" V 7900 4100 50  0000 C CNN
F 2 "" V 7830 4100 50  0001 C CNN
F 3 "" H 7900 4100 50  0001 C CNN
	1    7900 4100
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:R R1
U 1 1 59F525C2
P 7450 4100
F 0 "R1" V 7530 4100 50  0000 C CNN
F 1 "1K" V 7450 4100 50  0000 C CNN
F 2 "" V 7380 4100 50  0001 C CNN
F 3 "" H 7450 4100 50  0001 C CNN
	1    7450 4100
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:+5V #PWR07
U 1 1 59F52AD2
P 7450 3800
F 0 "#PWR07" H 7450 3650 50  0001 C CNN
F 1 "+5V" H 7450 3940 50  0000 C CNN
F 2 "" H 7450 3800 50  0001 C CNN
F 3 "" H 7450 3800 50  0001 C CNN
	1    7450 3800
	1    0    0    -1  
$EndComp
Text GLabel 14550 3700 0    50   Output ~ 0
~RESET
Text GLabel 4050 1700 0    50   Input ~ 0
~RESET
Text GLabel 3350 1750 0    50   Input ~ 0
DATABUS
Text Label 4000 1850 0    50   ~ 0
D0
Text Label 4000 1950 0    50   ~ 0
D1
Entry Wire Line
	3700 1750 3800 1850
Entry Wire Line
	3700 1850 3800 1950
Text Label 8100 5950 0    50   ~ 0
~SIDE_SELECT
$Comp
L 65xx-WD1772-rescue:74HCT139 U5
U 1 1 59F549C4
P 7750 8850
F 0 "U5" H 7750 8950 50  0000 C CNN
F 1 "74HCT139" H 7750 8750 50  0000 C CNN
F 2 "" H 7750 8850 50  0001 C CNN
F 3 "" H 7750 8850 50  0001 C CNN
	1    7750 8850
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:74HCT139 U5
U 2 1 59F54A3F
P 7750 9900
F 0 "U5" H 7750 10000 50  0000 C CNN
F 1 "74HCT139" H 7750 9800 50  0000 C CNN
F 2 "" H 7750 9900 50  0001 C CNN
F 3 "" H 7750 9900 50  0001 C CNN
	2    7750 9900
	1    0    0    -1  
$EndComp
Text GLabel 6750 10150 0    50   Input ~ 0
~CS
Text GLabel 6700 9100 0    50   Input ~ 0
~CS
Entry Wire Line
	6300 8650 6400 8750
Text GLabel 6050 8300 0    50   Input ~ 0
ADDRESSBUS
Text Label 6450 8750 0    50   ~ 0
A2
Entry Wire Line
	6300 9700 6400 9800
Text Label 6550 9800 0    50   ~ 0
A2
Text GLabel 6650 8600 0    50   Input ~ 0
PHI2
Text GLabel 6650 9650 0    50   Input ~ 0
R_~W
Text GLabel 8850 10200 2    50   Output ~ 0
~CS_LATCH
Text GLabel 8850 10000 2    50   Output ~ 0
~CS_STAT
Text GLabel 8850 8750 2    50   Output ~ 0
~CS_WD1772
Text GLabel 1600 4500 0    50   Input ~ 0
~CS_WD1772
Text GLabel 1850 4600 0    50   Input ~ 0
R_~W
Entry Wire Line
	1850 4700 1950 4800
Entry Wire Line
	1850 4800 1950 4900
Text GLabel 1600 4700 0    50   Input ~ 0
ADDRESSBUS
Text Label 2000 4800 0    50   ~ 0
A0
Text Label 2000 4900 0    50   ~ 0
A1
Entry Wire Line
	2200 5000 2300 4900
Entry Wire Line
	2200 5100 2300 5000
Entry Wire Line
	2200 5200 2300 5100
Entry Wire Line
	2200 5300 2300 5200
Entry Wire Line
	2200 5400 2300 5300
Entry Wire Line
	2200 5500 2300 5400
Entry Wire Line
	2200 5600 2300 5500
Entry Wire Line
	2200 5700 2300 5600
$Comp
L 65xx-WD1772-rescue:GNDD #PWR08
U 1 1 59F57CEE
P 2550 5950
F 0 "#PWR08" H 2550 5700 50  0001 C CNN
F 1 "GNDD" H 2550 5825 50  0000 C CNN
F 2 "" H 2550 5950 50  0001 C CNN
F 3 "" H 2550 5950 50  0001 C CNN
	1    2550 5950
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:+5V #PWR09
U 1 1 59F580BC
P 4250 4350
F 0 "#PWR09" H 4250 4200 50  0001 C CNN
F 1 "+5V" H 4250 4490 50  0000 C CNN
F 2 "" H 4250 4350 50  0001 C CNN
F 3 "" H 4250 4350 50  0001 C CNN
	1    4250 4350
	1    0    0    -1  
$EndComp
Text GLabel 3350 1550 0    50   Input ~ 0
~CS_LATCH
Text GLabel 1850 5800 0    50   Input ~ 0
~RESET
Text Label 8100 5850 0    50   ~ 0
~READ_DATA
Text Label 8100 5450 0    50   ~ 0
~WRITE_DATA
Text Label 8100 5750 0    50   ~ 0
~WRITE_PROT
Text Label 8100 5650 0    50   ~ 0
~TRACK0
Text Label 8100 5550 0    50   ~ 0
~WRITE_GATE
Text Label 8100 5350 0    50   ~ 0
~STEP
Text Label 8100 5250 0    50   ~ 0
~DIRECTION
Text Label 8100 4750 0    50   ~ 0
~INDEX
$Comp
L 65xx-WD1772-rescue:GNDD #PWR010
U 1 1 59F5D524
P 4150 6000
F 0 "#PWR010" H 4150 5750 50  0001 C CNN
F 1 "GNDD" H 4150 5875 50  0000 C CNN
F 2 "" H 4150 6000 50  0001 C CNN
F 3 "" H 4150 6000 50  0001 C CNN
	1    4150 6000
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:R R5
U 1 1 59F5E24D
P 8050 4100
F 0 "R5" V 8130 4100 50  0000 C CNN
F 1 "1K" V 8050 4100 50  0000 C CNN
F 2 "" V 7980 4100 50  0001 C CNN
F 3 "" H 8050 4100 50  0001 C CNN
	1    8050 4100
	1    0    0    -1  
$EndComp
Text Label 8100 4850 0    50   ~ 0
~MOTEA
Wire Wire Line
	13350 1900 14700 1900
Wire Wire Line
	13350 1900 13350 2200
Wire Wire Line
	13450 2300 13450 2000
Wire Wire Line
	13450 2000 14700 2000
Wire Wire Line
	13550 2400 13550 2100
Wire Wire Line
	13550 2100 14700 2100
Wire Wire Line
	13650 2500 13650 2200
Wire Wire Line
	13650 2200 14700 2200
Wire Wire Line
	13750 2600 13750 2300
Wire Wire Line
	13750 2300 14700 2300
Wire Wire Line
	13850 2700 13850 2400
Wire Wire Line
	13850 2400 14700 2400
Wire Wire Line
	13950 2800 13950 2500
Wire Wire Line
	13950 2500 14700 2500
Wire Wire Line
	14050 2900 14050 2600
Wire Wire Line
	14050 2600 14700 2600
Wire Wire Line
	13350 2200 13250 2200
Wire Wire Line
	13450 2300 13250 2300
Wire Wire Line
	13250 2400 13550 2400
Wire Wire Line
	13250 2500 13650 2500
Wire Wire Line
	13250 2600 13750 2600
Wire Wire Line
	13250 2700 13850 2700
Wire Wire Line
	13250 2800 13950 2800
Wire Wire Line
	13250 2900 14050 2900
Wire Wire Line
	14700 2700 14550 2700
Wire Wire Line
	12150 2050 12000 2050
Wire Wire Line
	11350 1900 12150 1900
Wire Bus Line
	11700 2100 11350 2100
Wire Wire Line
	11850 2000 12000 2000
Wire Wire Line
	12000 2000 12000 2050
Wire Wire Line
	11800 2200 12150 2200
Wire Wire Line
	11800 2300 12150 2300
Wire Wire Line
	11800 2400 12150 2400
Wire Wire Line
	11800 2500 12150 2500
Wire Wire Line
	11800 2600 12150 2600
Wire Wire Line
	11800 2700 12150 2700
Wire Wire Line
	11800 2800 12150 2800
Wire Wire Line
	11800 2900 12150 2900
Wire Wire Line
	14150 950  14150 1100
Wire Wire Line
	14150 1400 14150 1600
Wire Wire Line
	14550 2900 14700 2900
Wire Wire Line
	14700 2800 14150 2800
Wire Wire Line
	14150 2800 14150 3150
Wire Wire Line
	14150 3150 13350 3150
Wire Wire Line
	13350 3150 13350 3400
Wire Wire Line
	13350 3400 13200 3400
Wire Wire Line
	14700 3000 14200 3000
Wire Wire Line
	14200 3000 14200 3200
Wire Wire Line
	14200 3200 13400 3200
Wire Wire Line
	13400 3200 13400 3500
Wire Wire Line
	13400 3500 13200 3500
Wire Wire Line
	14700 3200 14300 3200
Wire Wire Line
	14300 3200 14300 3300
Wire Wire Line
	14300 3300 13450 3300
Wire Wire Line
	13450 3300 13450 3600
Wire Wire Line
	13450 3600 13200 3600
Wire Wire Line
	14700 3400 13500 3400
Wire Wire Line
	13500 3400 13500 3700
Wire Wire Line
	13500 3700 13200 3700
Wire Bus Line
	13100 3300 12850 3300
Wire Wire Line
	13700 3600 13700 3800
Wire Wire Line
	13700 3800 13200 3800
Wire Wire Line
	14700 3600 13700 3600
Wire Wire Line
	14700 3800 13900 3800
Wire Wire Line
	13900 3800 13900 3900
Wire Wire Line
	13900 3900 13200 3900
Wire Wire Line
	14700 4000 13200 4000
Wire Wire Line
	14700 4200 13200 4200
Wire Wire Line
	2300 4900 2650 4900
Wire Wire Line
	2300 5000 2650 5000
Wire Wire Line
	2300 5100 2650 5100
Wire Wire Line
	2300 5200 2650 5200
Wire Wire Line
	2300 5300 2650 5300
Wire Wire Line
	2300 5400 2650 5400
Wire Wire Line
	2300 5500 2650 5500
Wire Wire Line
	2300 5600 2650 5600
Wire Wire Line
	12700 9050 12950 9050
Wire Wire Line
	12950 9050 12950 8950
Wire Wire Line
	11900 9250 11750 9250
Wire Wire Line
	11750 9250 11750 9450
Wire Wire Line
	12700 9250 12950 9250
Wire Wire Line
	11750 8150 11450 8150
Wire Wire Line
	11750 8050 11450 8050
Wire Wire Line
	11750 7950 11450 7950
Wire Bus Line
	11350 7400 11200 7400
Wire Wire Line
	11750 7600 11450 7600
Wire Wire Line
	12850 8100 13200 8100
Wire Wire Line
	11750 7700 11500 7700
Wire Wire Line
	11500 7700 11500 7300
Wire Wire Line
	11700 7100 11200 7100
Wire Wire Line
	9350 4450 9650 4450
Wire Wire Line
	9650 4450 9650 4550
Wire Wire Line
	9350 4550 9650 4550
Connection ~ 9650 4550
Wire Wire Line
	9350 4750 9650 4750
Connection ~ 9650 4750
Wire Wire Line
	9350 4850 9650 4850
Connection ~ 9650 4850
Wire Wire Line
	9350 4950 9650 4950
Connection ~ 9650 4950
Wire Wire Line
	9350 5050 9650 5050
Connection ~ 9650 5050
Wire Wire Line
	9350 5150 9650 5150
Connection ~ 9650 5150
Wire Wire Line
	9350 5250 9650 5250
Connection ~ 9650 5250
Wire Wire Line
	9350 5350 9650 5350
Connection ~ 9650 5350
Wire Wire Line
	9350 5450 9650 5450
Connection ~ 9650 5450
Wire Wire Line
	9350 5550 9650 5550
Connection ~ 9650 5550
Wire Wire Line
	9350 5650 9650 5650
Connection ~ 9650 5650
Wire Wire Line
	9350 5750 9650 5750
Connection ~ 9650 5750
Wire Wire Line
	9350 5850 9650 5850
Connection ~ 9650 5850
Wire Wire Line
	9350 5950 9650 5950
Connection ~ 9650 5950
Wire Wire Line
	9350 6050 9650 6050
Connection ~ 9650 6050
Wire Wire Line
	7750 5750 8850 5750
Wire Wire Line
	7600 5650 8850 5650
Wire Wire Line
	7450 4250 7450 4750
Wire Wire Line
	7450 3950 7450 3900
Wire Wire Line
	7600 3950 7600 3900
Wire Wire Line
	7450 3900 7600 3900
Connection ~ 7450 3900
Wire Wire Line
	7750 3900 7750 3950
Connection ~ 7600 3900
Wire Wire Line
	7900 3900 7900 3950
Connection ~ 7750 3900
Wire Wire Line
	14700 3700 14550 3700
Wire Wire Line
	4150 1700 4050 1700
Wire Bus Line
	3700 1750 3350 1750
Wire Wire Line
	3800 1850 4150 1850
Wire Wire Line
	3800 1950 4150 1950
Wire Bus Line
	3700 1850 3700 1750
Wire Wire Line
	5250 1850 5400 1850
Wire Wire Line
	7350 5950 8850 5950
Wire Wire Line
	6400 8750 6900 8750
Wire Bus Line
	6300 8300 6050 8300
Wire Wire Line
	6900 9800 6400 9800
Wire Wire Line
	6900 9100 6700 9100
Wire Wire Line
	6900 10150 6750 10150
Wire Wire Line
	6900 8600 6650 8600
Wire Wire Line
	6900 9650 6650 9650
Wire Wire Line
	8600 10200 8850 10200
Wire Wire Line
	8600 10000 8850 10000
Wire Wire Line
	8850 8750 8600 8750
Wire Wire Line
	2650 4500 1600 4500
Wire Wire Line
	2650 4600 1850 4600
Wire Wire Line
	1950 4800 2150 4800
Wire Wire Line
	1950 4900 2200 4900
Wire Bus Line
	1850 4700 1600 4700
Wire Bus Line
	1850 4800 1850 4700
Wire Bus Line
	1850 5000 2200 5000
Wire Wire Line
	2150 4800 2250 4700
Wire Wire Line
	2250 4700 2650 4700
Wire Wire Line
	2650 4800 2300 4800
Wire Wire Line
	2300 4800 2200 4900
Wire Wire Line
	2650 5700 2350 5700
Wire Wire Line
	2350 5700 2350 5800
Wire Wire Line
	2350 5800 1850 5800
Wire Wire Line
	2650 5800 2550 5800
Wire Wire Line
	2550 5800 2550 5950
Wire Wire Line
	4250 4350 4250 5800
Wire Wire Line
	4250 5800 4050 5800
Wire Wire Line
	4150 1550 3350 1550
Wire Wire Line
	7150 5100 7250 5100
Wire Wire Line
	7250 5100 7250 5250
Wire Wire Line
	7250 5250 8850 5250
Wire Wire Line
	7250 4250 7250 4850
Wire Wire Line
	6300 4650 7200 4650
Wire Wire Line
	7200 4650 7200 5350
Wire Wire Line
	7200 5350 8850 5350
Wire Wire Line
	4950 5850 6150 5850
Wire Wire Line
	4950 5100 4050 5100
Wire Wire Line
	7600 4250 7600 4950
Wire Wire Line
	7750 4250 7750 4900
Wire Wire Line
	7900 4250 7900 5850
Wire Wire Line
	7900 5850 8850 5850
Wire Wire Line
	7250 5850 7450 5850
Wire Wire Line
	7450 5850 7450 5450
Wire Wire Line
	7450 5450 8850 5450
Wire Wire Line
	7300 4750 7450 4750
Wire Wire Line
	4050 5500 4350 5500
Wire Wire Line
	5200 5550 5150 5550
Wire Wire Line
	5150 5550 5150 5200
Wire Wire Line
	5150 5200 4050 5200
Wire Wire Line
	6050 5100 5050 5100
Wire Wire Line
	5050 5100 5050 5600
Wire Wire Line
	5050 5600 4050 5600
Wire Wire Line
	4050 5700 4850 5700
Wire Wire Line
	4850 5700 4850 4650
Wire Wire Line
	4850 4650 5200 4650
Wire Wire Line
	4050 5400 4650 5400
Wire Wire Line
	4650 5400 4650 6200
Wire Wire Line
	4650 6200 7900 6200
Connection ~ 7900 5850
Wire Wire Line
	4050 5300 4650 5300
Wire Wire Line
	4650 5300 4650 4250
Wire Wire Line
	4650 4250 5150 4250
Wire Wire Line
	4050 5000 6200 5000
Wire Wire Line
	6200 5000 6200 4750
Wire Wire Line
	6200 4750 7100 4750
Wire Wire Line
	7100 4750 7100 4950
Wire Wire Line
	7100 4950 7600 4950
Connection ~ 7600 4950
Wire Wire Line
	4050 4800 4950 4800
Wire Wire Line
	4950 4800 4950 4950
Wire Wire Line
	4950 4950 6150 4950
Wire Wire Line
	6150 4950 6150 4700
Wire Wire Line
	6150 4700 7150 4700
Wire Wire Line
	7150 4700 7150 4900
Wire Wire Line
	7150 4900 7750 4900
Connection ~ 7750 4900
Wire Wire Line
	4150 6000 4150 4700
Wire Wire Line
	4150 4700 4050 4700
Wire Wire Line
	7300 4750 7300 4600
Wire Wire Line
	7300 4600 6250 4600
Wire Wire Line
	6250 4600 6250 4300
Wire Wire Line
	6250 4300 4750 4300
Wire Wire Line
	4750 4300 4750 4900
Wire Wire Line
	4750 4900 4050 4900
Connection ~ 7450 4750
Wire Wire Line
	4950 5850 4950 5100
Wire Wire Line
	8050 3900 8050 3950
Connection ~ 7900 3900
Wire Wire Line
	8050 4250 8050 4850
Wire Wire Line
	7950 5050 8850 5050
Wire Wire Line
	7950 5050 7950 4850
Wire Wire Line
	7250 4850 7950 4850
Connection ~ 7950 4850
Connection ~ 8050 4850
Text GLabel 1850 6750 0    50   Input ~ 0
~CS_STAT
Wire Wire Line
	1850 6750 2800 6750
Wire Wire Line
	4350 5500 4350 6350
Wire Wire Line
	4350 6350 4850 6350
Wire Wire Line
	4050 4600 4450 4600
Wire Wire Line
	4450 4600 4450 6450
Wire Wire Line
	4450 6450 2700 6450
Wire Wire Line
	2700 6450 2700 6950
Wire Wire Line
	2700 6950 2800 6950
Wire Wire Line
	4050 4500 4550 4500
Wire Wire Line
	4550 4500 4550 6600
Wire Wire Line
	4550 6600 2600 6600
Wire Wire Line
	2600 6600 2600 7050
Wire Wire Line
	2600 7050 2800 7050
Text GLabel 4700 6750 2    50   Output ~ 0
DATABUS
Entry Wire Line
	4350 6950 4250 7050
Entry Wire Line
	4350 6850 4250 6950
Wire Bus Line
	4350 6750 4700 6750
Wire Wire Line
	4250 6950 3900 6950
Text Label 4050 6950 2    50   ~ 0
D7
Wire Wire Line
	4250 7050 3900 7050
Text Label 4050 7050 2    50   ~ 0
D6
$Comp
L 65xx-WD1772-rescue:+5V #PWR011
U 1 1 59F5FB31
P 8100 1900
F 0 "#PWR011" H 8100 1750 50  0001 C CNN
F 1 "+5V" H 8100 2040 50  0000 C CNN
F 2 "" H 8100 1900 50  0001 C CNN
F 3 "" H 8100 1900 50  0001 C CNN
	1    8100 1900
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:GNDD #PWR012
U 1 1 59F5FCDA
P 8100 2500
F 0 "#PWR012" H 8100 2250 50  0001 C CNN
F 1 "GNDD" H 8100 2375 50  0000 C CNN
F 2 "" H 8100 2500 50  0001 C CNN
F 3 "" H 8100 2500 50  0001 C CNN
	1    8100 2500
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:Conn_01x02 J2
U 1 1 59F5FD9F
P 9500 2100
F 0 "J2" H 9500 2200 50  0000 C CNN
F 1 "Floppy Power" H 9500 1900 50  0000 C CNN
F 2 "" H 9500 2100 50  0001 C CNN
F 3 "" H 9500 2100 50  0001 C CNN
	1    9500 2100
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:CP C1
U 1 1 59F5FF47
P 8100 2200
F 0 "C1" H 8125 2300 50  0000 L CNN
F 1 "220µF/10V" H 8125 2100 50  0000 L CNN
F 2 "" H 8138 2050 50  0001 C CNN
F 3 "" H 8100 2200 50  0001 C CNN
	1    8100 2200
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:CP C2
U 1 1 59F60444
P 8750 2200
F 0 "C2" H 8775 2300 50  0000 L CNN
F 1 "100µF/25V" H 8775 2100 50  0000 L CNN
F 2 "" H 8788 2050 50  0001 C CNN
F 3 "" H 8750 2200 50  0001 C CNN
	1    8750 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 2100 9250 2100
Wire Wire Line
	9250 2100 9250 1950
Wire Wire Line
	9250 1950 8750 1950
Wire Wire Line
	8100 1900 8100 1950
Connection ~ 8100 1950
Wire Wire Line
	8100 2500 8100 2400
Wire Wire Line
	8100 2400 8750 2400
Wire Wire Line
	8750 2400 8750 2350
Connection ~ 8100 2400
Wire Wire Line
	9300 2200 9250 2200
Wire Wire Line
	9250 2200 9250 2400
Connection ~ 8750 2400
Wire Wire Line
	8750 2050 8750 1950
Connection ~ 8750 1950
$Comp
L 65xx-WD1772-rescue:+5V #PWR013
U 1 1 59F610FF
P 12900 1700
F 0 "#PWR013" H 12900 1550 50  0001 C CNN
F 1 "+5V" H 12900 1840 50  0000 C CNN
F 2 "" H 12900 1700 50  0001 C CNN
F 3 "" H 12900 1700 50  0001 C CNN
	1    12900 1700
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:GNDD #PWR014
U 1 1 59F6118C
P 12600 1550
F 0 "#PWR014" H 12600 1300 50  0001 C CNN
F 1 "GNDD" H 12600 1425 50  0000 C CNN
F 2 "" H 12600 1550 50  0001 C CNN
F 3 "" H 12600 1550 50  0001 C CNN
	1    12600 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	12600 1550 12600 1500
Wire Wire Line
	12600 1500 12750 1500
Wire Wire Line
	12750 1500 12750 1800
Wire Wire Line
	12900 1700 12900 1800
Text GLabel 11350 1900 0    50   Input ~ 0
~CS
$Comp
L 65xx-WD1772-rescue:+5V #PWR015
U 1 1 59F63090
P 4900 1400
F 0 "#PWR015" H 4900 1250 50  0001 C CNN
F 1 "+5V" H 4900 1540 50  0000 C CNN
F 2 "" H 4900 1400 50  0001 C CNN
F 3 "" H 4900 1400 50  0001 C CNN
	1    4900 1400
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:GNDD #PWR016
U 1 1 59F63207
P 4600 1300
F 0 "#PWR016" H 4600 1050 50  0001 C CNN
F 1 "GNDD" H 4600 1175 50  0000 C CNN
F 2 "" H 4600 1300 50  0001 C CNN
F 3 "" H 4600 1300 50  0001 C CNN
	1    4600 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 1500 4900 1400
Wire Wire Line
	4600 1300 4600 1250
Wire Wire Line
	4600 1250 4750 1250
Wire Wire Line
	4750 1250 4750 1500
$Comp
L 65xx-WD1772-rescue:+5V #PWR017
U 1 1 59F639C4
P 12300 7300
F 0 "#PWR017" H 12300 7150 50  0001 C CNN
F 1 "+5V" H 12300 7440 50  0000 C CNN
F 2 "" H 12300 7300 50  0001 C CNN
F 3 "" H 12300 7300 50  0001 C CNN
	1    12300 7300
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:GNDD #PWR018
U 1 1 59F639CA
P 12300 8350
F 0 "#PWR018" H 12300 8100 50  0001 C CNN
F 1 "GNDD" H 12300 8225 50  0000 C CNN
F 2 "" H 12300 8350 50  0001 C CNN
F 3 "" H 12300 8350 50  0001 C CNN
	1    12300 8350
	1    0    0    -1  
$EndComp
Wire Wire Line
	12300 7400 12300 7300
Wire Wire Line
	12300 8350 12300 8300
$Comp
L 65xx-WD1772-rescue:+5V #PWR019
U 1 1 59F63D48
P 7300 8350
F 0 "#PWR019" H 7300 8200 50  0001 C CNN
F 1 "+5V" H 7300 8490 50  0000 C CNN
F 2 "" H 7300 8350 50  0001 C CNN
F 3 "" H 7300 8350 50  0001 C CNN
	1    7300 8350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 8450 7300 8350
$Comp
L 65xx-WD1772-rescue:GNDD #PWR020
U 1 1 59F63FFA
P 7300 10350
F 0 "#PWR020" H 7300 10100 50  0001 C CNN
F 1 "GNDD" H 7300 10225 50  0000 C CNN
F 2 "" H 7300 10350 50  0001 C CNN
F 3 "" H 7300 10350 50  0001 C CNN
	1    7300 10350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 10350 7300 10300
$Comp
L 65xx-WD1772-rescue:+5V #PWR021
U 1 1 59F643C3
P 5950 1500
F 0 "#PWR021" H 5950 1350 50  0001 C CNN
F 1 "+5V" H 5950 1640 50  0000 C CNN
F 2 "" H 5950 1500 50  0001 C CNN
F 3 "" H 5950 1500 50  0001 C CNN
	1    5950 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 1600 5950 1500
$Comp
L 65xx-WD1772-rescue:GNDD #PWR022
U 1 1 59F6451C
P 5950 2200
F 0 "#PWR022" H 5950 1950 50  0001 C CNN
F 1 "GNDD" H 5950 2075 50  0000 C CNN
F 2 "" H 5950 2200 50  0001 C CNN
F 3 "" H 5950 2200 50  0001 C CNN
	1    5950 2200
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:+5V #PWR023
U 1 1 59F646F7
P 3550 8050
F 0 "#PWR023" H 3550 7900 50  0001 C CNN
F 1 "+5V" H 3550 8190 50  0000 C CNN
F 2 "" H 3550 8050 50  0001 C CNN
F 3 "" H 3550 8050 50  0001 C CNN
	1    3550 8050
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:GNDD #PWR024
U 1 1 59F648BC
P 3200 7950
F 0 "#PWR024" H 3200 7700 50  0001 C CNN
F 1 "GNDD" H 3200 7825 50  0000 C CNN
F 2 "" H 3200 7950 50  0001 C CNN
F 3 "" H 3200 7950 50  0001 C CNN
	1    3200 7950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 8150 3550 8050
Wire Wire Line
	3400 8150 3400 7900
Wire Wire Line
	3400 7900 3200 7900
Wire Wire Line
	3200 7900 3200 7950
$Comp
L 65xx-WD1772-rescue:C C3
U 1 1 59F650F8
P 8100 3300
F 0 "C3" H 8125 3400 50  0000 L CNN
F 1 "100nF" H 8125 3200 50  0000 L CNN
F 2 "" H 8138 3150 50  0001 C CNN
F 3 "" H 8100 3300 50  0001 C CNN
	1    8100 3300
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:GNDD #PWR025
U 1 1 59F6524D
P 8100 3550
F 0 "#PWR025" H 8100 3300 50  0001 C CNN
F 1 "GNDD" H 8100 3425 50  0000 C CNN
F 2 "" H 8100 3550 50  0001 C CNN
F 3 "" H 8100 3550 50  0001 C CNN
	1    8100 3550
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:+5V #PWR026
U 1 1 59F652A9
P 8100 3050
F 0 "#PWR026" H 8100 2900 50  0001 C CNN
F 1 "+5V" H 8100 3190 50  0000 C CNN
F 2 "" H 8100 3050 50  0001 C CNN
F 3 "" H 8100 3050 50  0001 C CNN
	1    8100 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 3050 8100 3100
Wire Wire Line
	8100 3450 8100 3500
$Comp
L 65xx-WD1772-rescue:C C4
U 1 1 59F656B4
P 8350 3300
F 0 "C4" H 8375 3400 50  0000 L CNN
F 1 "100nF" H 8375 3200 50  0000 L CNN
F 2 "" H 8388 3150 50  0001 C CNN
F 3 "" H 8350 3300 50  0001 C CNN
	1    8350 3300
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:C C5
U 1 1 59F6571E
P 8600 3300
F 0 "C5" H 8625 3400 50  0000 L CNN
F 1 "100nF" H 8625 3200 50  0000 L CNN
F 2 "" H 8638 3150 50  0001 C CNN
F 3 "" H 8600 3300 50  0001 C CNN
	1    8600 3300
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:C C6
U 1 1 59F6578F
P 8850 3300
F 0 "C6" H 8875 3400 50  0000 L CNN
F 1 "100nF" H 8875 3200 50  0000 L CNN
F 2 "" H 8888 3150 50  0001 C CNN
F 3 "" H 8850 3300 50  0001 C CNN
	1    8850 3300
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:C C7
U 1 1 59F657FF
P 9100 3300
F 0 "C7" H 9125 3400 50  0000 L CNN
F 1 "100nF" H 9125 3200 50  0000 L CNN
F 2 "" H 9138 3150 50  0001 C CNN
F 3 "" H 9100 3300 50  0001 C CNN
	1    9100 3300
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:C C8
U 1 1 59F65872
P 9350 3300
F 0 "C8" H 9375 3400 50  0000 L CNN
F 1 "100nF" H 9375 3200 50  0000 L CNN
F 2 "" H 9388 3150 50  0001 C CNN
F 3 "" H 9350 3300 50  0001 C CNN
	1    9350 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	9350 3150 9350 3100
Wire Wire Line
	9350 3100 9100 3100
Connection ~ 8100 3100
Wire Wire Line
	8100 3500 8350 3500
Wire Wire Line
	9350 3500 9350 3450
Connection ~ 8100 3500
Wire Wire Line
	9100 3450 9100 3500
Connection ~ 9100 3500
Wire Wire Line
	8850 3450 8850 3500
Connection ~ 8850 3500
Wire Wire Line
	8600 3450 8600 3500
Connection ~ 8600 3500
Wire Wire Line
	8350 3450 8350 3500
Connection ~ 8350 3500
Wire Wire Line
	8350 3150 8350 3100
Connection ~ 8350 3100
Wire Wire Line
	8600 3150 8600 3100
Connection ~ 8600 3100
Wire Wire Line
	8850 3150 8850 3100
Connection ~ 8850 3100
Wire Wire Line
	9100 3150 9100 3100
Connection ~ 9100 3100
$Comp
L 65xx-WD1772-rescue:+5V #PWR027
U 1 1 59F66D45
P 14500 1650
F 0 "#PWR027" H 14500 1500 50  0001 C CNN
F 1 "+5V" H 14500 1790 50  0000 C CNN
F 2 "" H 14500 1650 50  0001 C CNN
F 3 "" H 14500 1650 50  0001 C CNN
	1    14500 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	14700 1700 14500 1700
Wire Wire Line
	14500 1650 14500 1700
Wire Wire Line
	14500 1800 14700 1800
Connection ~ 14500 1700
Wire Wire Line
	14700 7900 14550 7900
Wire Wire Line
	14550 7900 14550 8000
Wire Wire Line
	14700 8000 14550 8000
Connection ~ 14550 8000
$Comp
L 65xx-WD1772-rescue:GNDD #PWR028
U 1 1 59F67693
P 14550 8200
F 0 "#PWR028" H 14550 7950 50  0001 C CNN
F 1 "GNDD" H 14550 8075 50  0000 C CNN
F 2 "" H 14550 8200 50  0001 C CNN
F 3 "" H 14550 8200 50  0001 C CNN
	1    14550 8200
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:GNDD #PWR029
U 1 1 59F64044
P 3250 6250
F 0 "#PWR029" H 3250 6000 50  0001 C CNN
F 1 "GNDD" H 3250 6125 50  0000 C CNN
F 2 "" H 3250 6250 50  0001 C CNN
F 3 "" H 3250 6250 50  0001 C CNN
	1    3250 6250
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:+5V #PWR030
U 1 1 59F6410F
P 3550 6350
F 0 "#PWR030" H 3550 6200 50  0001 C CNN
F 1 "+5V" H 3550 6490 50  0000 C CNN
F 2 "" H 3550 6350 50  0001 C CNN
F 3 "" H 3550 6350 50  0001 C CNN
	1    3550 6350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 6650 3550 6350
Wire Wire Line
	3400 6650 3400 6150
Wire Wire Line
	3400 6150 3250 6150
Wire Wire Line
	3250 6150 3250 6250
Wire Wire Line
	11750 7500 11700 7500
Wire Wire Line
	11700 7500 11700 7100
Text GLabel 14550 3900 0    50   Output ~ 0
~IORQ
Wire Wire Line
	14550 3900 14700 3900
Text Notes 9800 6150 0    50   ~ 0
SIDE_SELECT: \nhigh level = side 0 (lower side) \nlow level = side 1 (upper side)
Text Notes 9400 4700 0    50   ~ 0
Key
Text Notes 2400 4000 0    50   ~ 0
WD1770-PH has bad CPU-interface timing. \nWD1772-PH is okay.
Text Notes 9800 5050 0    50   ~ 0
Floppy cable twisted \nbetween pin 10 to 16\nfor drive A.
Wire Wire Line
	6500 1850 7350 1850
Wire Wire Line
	7350 1850 7350 5950
Text Notes 8750 4250 0    50   ~ 0
For IBM PC/AT compatible \nfloppy connector.
Wire Wire Line
	9650 4550 9650 4750
Wire Wire Line
	9650 4750 9650 4850
Wire Wire Line
	9650 4850 9650 4950
Wire Wire Line
	9650 4950 9650 5050
Wire Wire Line
	9650 5050 9650 5150
Wire Wire Line
	9650 5150 9650 5250
Wire Wire Line
	9650 5250 9650 5350
Wire Wire Line
	9650 5350 9650 5450
Wire Wire Line
	9650 5450 9650 5550
Wire Wire Line
	9650 5550 9650 5650
Wire Wire Line
	9650 5650 9650 5750
Wire Wire Line
	9650 5750 9650 5850
Wire Wire Line
	9650 5850 9650 5950
Wire Wire Line
	9650 5950 9650 6050
Wire Wire Line
	9650 6050 9650 6350
Wire Wire Line
	7450 3900 7450 3800
Wire Wire Line
	7600 3900 7750 3900
Wire Wire Line
	7750 3900 7900 3900
Wire Wire Line
	7900 5850 7900 6200
Wire Wire Line
	7600 4950 7600 5650
Wire Wire Line
	7750 4900 7750 5750
Wire Wire Line
	7450 4750 8850 4750
Wire Wire Line
	7900 3900 8050 3900
Wire Wire Line
	7950 4850 8050 4850
Wire Wire Line
	8050 4850 8850 4850
Wire Wire Line
	8100 1950 8100 2050
Wire Wire Line
	8100 2400 8100 2350
Wire Wire Line
	8750 2400 9250 2400
Wire Wire Line
	8750 1950 8100 1950
Wire Wire Line
	8100 3100 8100 3150
Wire Wire Line
	8100 3500 8100 3550
Wire Wire Line
	9100 3500 9350 3500
Wire Wire Line
	8850 3500 9100 3500
Wire Wire Line
	8600 3500 8850 3500
Wire Wire Line
	8350 3500 8600 3500
Wire Wire Line
	8350 3100 8100 3100
Wire Wire Line
	8600 3100 8350 3100
Wire Wire Line
	8850 3100 8600 3100
Wire Wire Line
	9100 3100 8850 3100
Wire Wire Line
	14500 1700 14500 1800
Wire Wire Line
	14550 8000 14550 8200
Wire Wire Line
	6300 5550 8850 5550
Text Notes 8100 6050 0    50   ~ 0
~DISK_CHANGE
$Comp
L 65xx-WD1772-rescue:74LS00-65xx-Computer U8
U 3 1 5E2673C9
P 5950 2850
F 0 "U8" H 5950 3316 50  0000 C CNN
F 1 "74LS00" H 5950 3225 50  0000 C CNN
F 2 "" H 5950 2850 60  0001 C CNN
F 3 "" H 5950 2850 60  0001 C CNN
	3    5950 2850
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:74LS00-65xx-Computer U8
U 2 1 5E26A39A
P 4600 3600
F 0 "U8" H 4600 4066 50  0000 C CNN
F 1 "74LS00" H 4600 3975 50  0000 C CNN
F 2 "" H 4600 3600 60  0001 C CNN
F 3 "" H 4600 3600 60  0001 C CNN
	2    4600 3600
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:74LS00-65xx-Computer U8
U 4 1 5E26B803
P 5950 3700
F 0 "U8" H 6150 4150 50  0000 C CNN
F 1 "74LS00" H 6200 4050 50  0000 C CNN
F 2 "" H 5950 3700 60  0001 C CNN
F 3 "" H 5950 3700 60  0001 C CNN
	4    5950 3700
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:74LS00-65xx-Computer U8
U 1 1 5E26C911
P 1550 2350
F 0 "U8" H 1750 2750 50  0000 C CNN
F 1 "74LS00" H 1800 2650 50  0000 C CNN
F 2 "" H 1550 2350 60  0001 C CNN
F 3 "" H 1550 2350 60  0001 C CNN
	1    1550 2350
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:C C9
U 1 1 5E295873
P 9650 3300
F 0 "C9" H 9675 3400 50  0000 L CNN
F 1 "100nF" H 9675 3200 50  0000 L CNN
F 2 "" H 9688 3150 50  0001 C CNN
F 3 "" H 9650 3300 50  0001 C CNN
	1    9650 3300
	1    0    0    -1  
$EndComp
$Comp
L 65xx-WD1772-rescue:+5V #PWR031
U 1 1 5E2D542A
P 1550 1800
F 0 "#PWR031" H 1550 1650 50  0001 C CNN
F 1 "+5V" H 1550 1940 50  0000 C CNN
F 2 "" H 1550 1800 50  0001 C CNN
F 3 "" H 1550 1800 50  0001 C CNN
	1    1550 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 1800 1550 2100
$Comp
L 65xx-WD1772-rescue:GNDD #PWR032
U 1 1 5E2F65A6
P 1550 2850
F 0 "#PWR032" H 1550 2600 50  0001 C CNN
F 1 "GNDD" H 1550 2725 50  0000 C CNN
F 2 "" H 1550 2850 50  0001 C CNN
F 3 "" H 1550 2850 50  0001 C CNN
	1    1550 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 2850 1550 2600
Wire Wire Line
	5250 1950 5350 1950
Wire Wire Line
	5350 1950 5350 2750
Wire Wire Line
	5350 2750 5450 2750
Wire Wire Line
	5450 3800 5150 3800
Wire Wire Line
	5150 3800 5150 4250
Wire Wire Line
	5450 2950 5150 2950
Wire Wire Line
	5150 2950 5150 3800
Connection ~ 5150 3800
Wire Wire Line
	6450 3700 6450 4250
Wire Wire Line
	6450 4250 7250 4250
Wire Wire Line
	6450 2850 7050 2850
Wire Wire Line
	7050 2850 7050 5000
Wire Wire Line
	7050 5000 7500 5000
Wire Wire Line
	7500 5000 7500 5150
Wire Wire Line
	7500 5150 7800 5150
Wire Wire Line
	8850 4950 7800 4950
Wire Wire Line
	7800 4950 7800 5150
Connection ~ 7800 5150
Wire Wire Line
	7800 5150 8850 5150
Wire Wire Line
	5100 3600 5450 3600
Wire Wire Line
	4100 3700 4000 3700
Wire Wire Line
	4000 3700 4000 3500
Wire Wire Line
	4000 3500 4100 3500
Wire Wire Line
	4000 3500 4000 2750
Wire Wire Line
	4000 2750 5350 2750
Connection ~ 4000 3500
Connection ~ 5350 2750
Text Label 8100 4950 0    50   ~ 0
~DRIVE_SELECT_B
Text Label 8100 5050 0    50   ~ 0
~DRIVE_SELECT_A
Text Label 8100 5150 0    50   ~ 0
~MOTEB
Wire Wire Line
	5950 2200 5950 2100
Wire Wire Line
	9350 3100 9650 3100
Wire Wire Line
	9650 3100 9650 3150
Connection ~ 9350 3100
Wire Wire Line
	9350 3500 9650 3500
Wire Wire Line
	9650 3500 9650 3450
Connection ~ 9350 3500
Wire Bus Line
	6300 8300 6300 9700
Wire Bus Line
	4350 6750 4350 6950
Wire Bus Line
	11350 7400 11350 8050
Wire Bus Line
	11700 2100 11700 2800
Wire Bus Line
	13100 3300 13100 4100
Wire Bus Line
	2200 5000 2200 5700
$EndSCHEMATC
