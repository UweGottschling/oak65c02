EESchema Schematic File Version 2
LIBS:65xx-IO-rescue
LIBS:65xx-Computer
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Logic_TTL_IEEE
LIBS:65xx-IO-cache
EELAYER 25 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "Oak-65XX-IO"
Date "2017-11-05"
Rev "0.1"
Comp "Uwe Gottschling"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 65C22 U6
U 1 1 59EFA190
P 9050 3800
F 0 "U6" H 9050 4900 60  0000 C CNN
F 1 "65C22" H 9050 3800 60  0000 C CNN
F 2 "" H 9050 4450 60  0000 C CNN
F 3 "" H 9050 4450 60  0000 C CNN
	1    9050 3800
	1    0    0    -1  
$EndComp
$Comp
L 65C22 U7
U 1 1 59EFA1EA
P 9050 6350
F 0 "U7" H 9050 7450 60  0000 C CNN
F 1 "65C22" H 9050 6350 60  0000 C CNN
F 2 "" H 9050 7000 60  0000 C CNN
F 3 "" H 9050 7000 60  0000 C CNN
	1    9050 6350
	1    0    0    -1  
$EndComp
$Comp
L 65C51-RESCUE-65xx-IO U5
U 1 1 59EFA243
P 8250 9850
F 0 "U5" H 8250 10600 60  0000 C CNN
F 1 "65C51" H 8250 9850 60  0000 C CNN
F 2 "" H 8250 10150 60  0000 C CNN
F 3 "" H 8250 10150 60  0000 C CNN
	1    8250 9850
	1    0    0    -1  
$EndComp
$Comp
L Crystal_oscillator U2
U 1 1 59EFA49B
P 7150 8250
F 0 "U2" H 7100 8500 60  0000 C CNN
F 1 "Crystal_oscillator_1,8432MHz" H 7150 8000 60  0000 C CNN
F 2 "" H 7100 8250 60  0001 C CNN
F 3 "" H 7100 8250 60  0001 C CNN
	1    7150 8250
	1    0    0    -1  
$EndComp
$Comp
L D_TVS_UNI D1
U 1 1 59EFA4EC
P 13650 1300
F 0 "D1" H 13650 1400 50  0000 C CNN
F 1 "P6KE6,8A" H 13650 1200 50  0000 C CNN
F 2 "" H 13650 1300 50  0001 C CNN
F 3 "" H 13650 1300 50  0001 C CNN
	1    13650 1300
	1    0    0    -1  
$EndComp
$Comp
L C64AC_DIN41612_BUS-RESCUE-65xx-IO X1
U 1 1 59EFA531
P 15300 4850
F 0 "X1" H 15200 8150 50  0000 C CNN
F 1 "C64AC_DIN41612_BUS" V 15700 4800 50  0000 C CNN
F 2 "" H 15200 4850 50  0001 C CNN
F 3 "" H 15200 4850 50  0001 C CNN
	1    15300 4850
	1    0    0    -1  
$EndComp
$Comp
L HIN232CP U1
U 1 1 59EFA5E2
P 4150 9750
F 0 "U1" H 4100 10675 50  0000 C CNN
F 1 "HIN232CP" H 4150 9900 50  0000 C CNN
F 2 "" H 4200 9900 50  0001 L CNN
F 3 "" H 4150 10050 50  0001 C CNN
	1    4150 9750
	-1   0    0    -1  
$EndComp
$Comp
L +5V #PWR01
U 1 1 59EFB08C
P 14150 1300
F 0 "#PWR01" H 14150 1150 50  0001 C CNN
F 1 "+5V" H 14150 1440 50  0000 C CNN
F 2 "" H 14150 1300 50  0001 C CNN
F 3 "" H 14150 1300 50  0001 C CNN
	1    14150 1300
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR02
U 1 1 59EFB0C4
P 13150 1300
F 0 "#PWR02" H 13150 1050 50  0001 C CNN
F 1 "GNDD" H 13150 1175 50  0000 C CNN
F 2 "" H 13150 1300 50  0001 C CNN
F 3 "" H 13150 1300 50  0001 C CNN
	1    13150 1300
	1    0    0    -1  
$EndComp
$Comp
L ATTINY26-16PU U3
U 1 1 59EFB277
P 4550 3850
F 0 "U3" H 3950 4800 50  0000 C CNN
F 1 "ATTINY26-16PU" H 5000 2900 50  0000 C CNN
F 2 "Housings_DIP:DIP-20_W7.62mm" H 4550 3850 50  0001 C CIN
F 3 "" H 4550 3850 50  0001 C CNN
	1    4550 3850
	1    0    0    -1  
$EndComp
$Comp
L 74HCT245 U10
U 1 1 59EFB954
P 12700 2450
F 0 "U10" H 13000 2950 50  0000 L CNN
F 1 "74HCT245" H 12800 1850 50  0000 L CNN
F 2 "" H 12700 2450 60  0001 C CNN
F 3 "" H 12700 2450 60  0001 C CNN
	1    12700 2450
	1    0    0    -1  
$EndComp
$Comp
L 74LS30 U9
U 1 1 59EFBBEB
P 9400 1150
F 0 "U9" H 9550 1600 50  0000 C CNN
F 1 "74LS30" H 9600 700 50  0000 C CNN
F 2 "" H 9400 1150 60  0001 C CNN
F 3 "" H 9400 1150 60  0001 C CNN
	1    9400 1150
	1    0    0    -1  
$EndComp
$Comp
L 74HCT138 U8
U 1 1 59EFBF2D
P 7200 1000
F 0 "U8" H 7350 1350 50  0000 C CNN
F 1 "74HCT138" H 7550 350 50  0000 C CNN
F 2 "" H 7200 1000 60  0001 C CNN
F 3 "" H 7200 1000 60  0001 C CNN
	1    7200 1000
	1    0    0    -1  
$EndComp
Text GLabel 14550 2700 0    60   Input ~ 0
R/~W
Text GLabel 11850 2000 0    60   Input ~ 0
R/~W
$Comp
L 74LS00 U4
U 4 1 59EFCEA0
P 10850 1150
F 0 "U4" H 11100 1450 50  0000 C CNN
F 1 "74LS00" H 11050 850 50  0000 C CNN
F 2 "" H 10850 1150 60  0001 C CNN
F 3 "" H 10850 1150 60  0001 C CNN
	4    10850 1150
	1    0    0    -1  
$EndComp
$Comp
L 74LS00 U4
U 1 1 59EFCF52
P 2600 1450
F 0 "U4" H 2850 1750 50  0000 C CNN
F 1 "74LS00" H 2800 1150 50  0000 C CNN
F 2 "" H 2600 1450 60  0001 C CNN
F 3 "" H 2600 1450 60  0001 C CNN
	1    2600 1450
	1    0    0    -1  
$EndComp
$Comp
L 74LS00 U4
U 2 1 59EFCFB3
P 3850 1450
F 0 "U4" H 4100 1750 50  0000 C CNN
F 1 "74LS00" H 4050 1150 50  0000 C CNN
F 2 "" H 3850 1450 60  0001 C CNN
F 3 "" H 3850 1450 60  0001 C CNN
	2    3850 1450
	1    0    0    -1  
$EndComp
$Comp
L 74LS00 U4
U 3 1 59EFD03D
P 5150 1450
F 0 "U4" H 5400 1750 50  0000 C CNN
F 1 "74LS00" H 5350 1150 50  0000 C CNN
F 2 "" H 5150 1450 60  0001 C CNN
F 3 "" H 5150 1450 60  0001 C CNN
	3    5150 1450
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR03
U 1 1 59EFD762
P 8500 700
F 0 "#PWR03" H 8500 550 50  0001 C CNN
F 1 "+5V" H 8500 840 50  0000 C CNN
F 2 "" H 8500 700 50  0001 C CNN
F 3 "" H 8500 700 50  0001 C CNN
	1    8500 700 
	1    0    0    -1  
$EndComp
Text GLabel 14200 3900 0    60   Input ~ 0
~IORQ
Text GLabel 6100 800  0    60   Input ~ 0
~IORQ
$Comp
L +5V #PWR04
U 1 1 59EFE4C9
P 6550 700
F 0 "#PWR04" H 6550 550 50  0001 C CNN
F 1 "+5V" H 6550 840 50  0000 C CNN
F 2 "" H 6550 700 50  0001 C CNN
F 3 "" H 6550 700 50  0001 C CNN
	1    6550 700 
	1    0    0    -1  
$EndComp
Text GLabel 7950 650  2    60   Input ~ 0
CS6551
Text GLabel 7450 9450 0    60   Input ~ 0
CS6551
$Comp
L +5V #PWR05
U 1 1 59EFEC17
P 7300 9100
F 0 "#PWR05" H 7300 8950 50  0001 C CNN
F 1 "+5V" H 7300 9240 50  0000 C CNN
F 2 "" H 7300 9100 50  0001 C CNN
F 3 "" H 7300 9100 50  0001 C CNN
	1    7300 9100
	1    0    0    -1  
$EndComp
Text GLabel 9650 9350 2    60   Input ~ 0
DATABUS
Text GLabel 11350 2100 0    60   Input ~ 0
DATABUS
Text Label 9000 10250 0    60   ~ 0
D0
Text Label 9000 10150 0    60   ~ 0
D1
Text Label 9000 10050 0    60   ~ 0
D2
Text Label 9000 9950 0    60   ~ 0
D3
Text Label 9000 9850 0    60   ~ 0
D4
Text Label 9000 9750 0    60   ~ 0
D5
Text Label 9000 9650 0    60   ~ 0
D6
Text Label 9000 9550 0    60   ~ 0
D7
Text Label 12000 2200 0    60   ~ 0
D0
Text Label 12000 2300 0    60   ~ 0
D1
Text Label 12000 2400 0    60   ~ 0
D2
Text Label 12000 2500 0    60   ~ 0
D3
Text Label 12000 2600 0    60   ~ 0
D4
Text Label 12000 2700 0    60   ~ 0
D5
Text Label 12000 2800 0    60   ~ 0
D6
Text Label 12000 2900 0    60   ~ 0
D7
Text GLabel 10650 6000 2    60   Input ~ 0
DATABUS
Text GLabel 10550 3450 2    60   Input ~ 0
DATABUS
Entry Wire Line
	10250 3550 10350 3450
Entry Wire Line
	10250 3650 10350 3550
Entry Wire Line
	10250 3750 10350 3650
Entry Wire Line
	10250 3850 10350 3750
Entry Wire Line
	10250 3950 10350 3850
Entry Wire Line
	10250 4050 10350 3950
Entry Wire Line
	10250 4150 10350 4050
Entry Wire Line
	10250 4250 10350 4150
Entry Wire Line
	11700 2100 11800 2200
Entry Wire Line
	11700 2200 11800 2300
Entry Wire Line
	11700 2300 11800 2400
Entry Wire Line
	11700 2400 11800 2500
Entry Wire Line
	11700 2500 11800 2600
Entry Wire Line
	11700 2600 11800 2700
Entry Wire Line
	11700 2700 11800 2800
Entry Wire Line
	11700 2800 11800 2900
Text Label 9950 3550 0    60   ~ 0
D0
Text Label 9950 3650 0    60   ~ 0
D1
Text Label 9950 3750 0    60   ~ 0
D2
Text Label 9950 3850 0    60   ~ 0
D3
Text Label 9950 3950 0    60   ~ 0
D4
Text Label 9950 4050 0    60   ~ 0
D5
Text Label 9950 4150 0    60   ~ 0
D6
Text Label 9950 4250 0    60   ~ 0
D7
Entry Wire Line
	10250 6100 10350 6000
Entry Wire Line
	10250 6200 10350 6100
Entry Wire Line
	10250 6300 10350 6200
Entry Wire Line
	10250 6400 10350 6300
Entry Wire Line
	10250 6500 10350 6400
Entry Wire Line
	10250 6600 10350 6500
Entry Wire Line
	10250 6700 10350 6600
Entry Wire Line
	10250 6800 10350 6700
Text Label 10000 6100 0    60   ~ 0
D0
Text Label 10000 6200 0    60   ~ 0
D1
Text Label 10000 6300 0    60   ~ 0
D2
Text Label 10000 6400 0    60   ~ 0
D3
Text Label 10000 6500 0    60   ~ 0
D4
Text Label 10000 6600 0    60   ~ 0
D5
Text Label 10000 6700 0    60   ~ 0
D6
Text Label 10000 6800 0    60   ~ 0
D7
Entry Wire Line
	9150 9550 9250 9450
Entry Wire Line
	9150 9650 9250 9550
Entry Wire Line
	9150 9750 9250 9650
Entry Wire Line
	9150 9850 9250 9750
Entry Wire Line
	9150 9950 9250 9850
Entry Wire Line
	9150 10050 9250 9950
Entry Wire Line
	9150 10150 9250 10050
Entry Wire Line
	9150 10250 9250 10150
$Comp
L Mini-DIN-6 J2
U 1 1 59F0F80E
P 6050 4300
F 0 "J2" H 6050 4550 50  0000 C CNN
F 1 "Mini-DIN-6" H 6050 4050 50  0000 C CNN
F 2 "" H 6050 4300 50  0001 C CNN
F 3 "" H 6050 4300 50  0001 C CNN
	1    6050 4300
	0    -1   -1   0   
$EndComp
$Comp
L GNDD #PWR07
U 1 1 59F1020D
P 6500 4050
F 0 "#PWR07" H 6500 3800 50  0001 C CNN
F 1 "GNDD" H 6500 3925 50  0000 C CNN
F 2 "" H 6500 4050 50  0001 C CNN
F 3 "" H 6500 4050 50  0001 C CNN
	1    6500 4050
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR08
U 1 1 59F103F2
P 6500 4600
F 0 "#PWR08" H 6500 4450 50  0001 C CNN
F 1 "+5V" H 6500 4740 50  0000 C CNN
F 2 "" H 6500 4600 50  0001 C CNN
F 3 "" H 6500 4600 50  0001 C CNN
	1    6500 4600
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 59F10B19
P 5700 2700
F 0 "R1" V 5780 2700 50  0000 C CNN
F 1 "10k" V 5700 2700 50  0000 C CNN
F 2 "" V 5630 2700 50  0001 C CNN
F 3 "" H 5700 2700 50  0001 C CNN
	1    5700 2700
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 59F10C34
P 5900 2700
F 0 "R2" V 5980 2700 50  0000 C CNN
F 1 "10k" V 5900 2700 50  0000 C CNN
F 2 "" V 5830 2700 50  0001 C CNN
F 3 "" H 5900 2700 50  0001 C CNN
	1    5900 2700
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR09
U 1 1 59F10E69
P 5700 2400
F 0 "#PWR09" H 5700 2250 50  0001 C CNN
F 1 "+5V" H 5700 2540 50  0000 C CNN
F 2 "" H 5700 2400 50  0001 C CNN
F 3 "" H 5700 2400 50  0001 C CNN
	1    5700 2400
	1    0    0    -1  
$EndComp
Text GLabel 8200 1600 2    60   Input ~ 0
6522#1
Text GLabel 8200 1700 2    60   Input ~ 0
6522#2
Text GLabel 10300 7100 2    60   Input ~ 0
6522#1
Text GLabel 10300 4550 2    60   Input ~ 0
6522#2
$Comp
L GNDD #PWR010
U 1 1 59F124BE
P 8050 2750
F 0 "#PWR010" H 8050 2500 50  0001 C CNN
F 1 "GNDD" H 8050 2625 50  0000 C CNN
F 2 "" H 8050 2750 50  0001 C CNN
F 3 "" H 8050 2750 50  0001 C CNN
	1    8050 2750
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR011
U 1 1 59F126E5
P 8000 5300
F 0 "#PWR011" H 8000 5050 50  0001 C CNN
F 1 "GNDD" H 8000 5175 50  0000 C CNN
F 2 "" H 8000 5300 50  0001 C CNN
F 3 "" H 8000 5300 50  0001 C CNN
	1    8000 5300
	1    0    0    -1  
$EndComp
Text GLabel 10300 4650 2    60   Input ~ 0
R/~W
Text GLabel 10300 7200 2    60   Input ~ 0
R/~W
$Comp
L +5V #PWR012
U 1 1 59F13523
P 8050 4800
F 0 "#PWR012" H 8050 4650 50  0001 C CNN
F 1 "+5V" H 8050 4940 50  0000 C CNN
F 2 "" H 8050 4800 50  0001 C CNN
F 3 "" H 8050 4800 50  0001 C CNN
	1    8050 4800
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR013
U 1 1 59F13921
P 7950 7350
F 0 "#PWR013" H 7950 7200 50  0001 C CNN
F 1 "+5V" H 7950 7490 50  0000 C CNN
F 2 "" H 7950 7350 50  0001 C CNN
F 3 "" H 7950 7350 50  0001 C CNN
	1    7950 7350
	1    0    0    -1  
$EndComp
Text GLabel 14550 2900 0    60   Input ~ 0
PHI2
Text GLabel 10300 6900 2    60   Input ~ 0
PHI2
Text GLabel 10300 4350 2    60   Input ~ 0
PHI2
Entry Wire Line
	13100 3300 13200 3400
Entry Wire Line
	13100 3400 13200 3500
Entry Wire Line
	13100 3500 13200 3600
Entry Wire Line
	13100 3600 13200 3700
Text GLabel 12850 3300 0    60   Input ~ 0
ADRESSBUS
Text Label 13250 3400 0    60   ~ 0
A0
Text Label 13250 3500 0    60   ~ 0
A1
Text Label 13250 3600 0    60   ~ 0
A2
Text Label 13250 3700 0    60   ~ 0
A3
Text GLabel 10550 5500 2    60   Input ~ 0
ADRESSBUS
Entry Wire Line
	10150 5900 10250 5800
Entry Wire Line
	10150 5800 10250 5700
Entry Wire Line
	10150 5700 10250 5600
Entry Wire Line
	10150 5600 10250 5500
Text Label 10000 5600 0    60   ~ 0
A0
Text Label 10000 5700 0    60   ~ 0
A1
Text Label 10000 5800 0    60   ~ 0
A2
Text Label 10000 5900 0    60   ~ 0
A3
Entry Wire Line
	10150 3350 10250 3250
Entry Wire Line
	10150 3250 10250 3150
Entry Wire Line
	10150 3150 10250 3050
Entry Wire Line
	10150 3050 10250 2950
Text Label 10000 3050 0    60   ~ 0
A0
Text Label 10000 3150 0    60   ~ 0
A1
Text Label 10000 3250 0    60   ~ 0
A2
Text Label 10000 3350 0    60   ~ 0
A3
Text GLabel 10550 2950 2    60   Input ~ 0
ADRESSBUS
Text GLabel 6750 10350 0    60   Input ~ 0
ADRESSBUS
Entry Wire Line
	7150 10550 7050 10450
Entry Wire Line
	7150 10450 7050 10350
Text Label 7300 10450 2    60   ~ 0
A0
Text Label 7300 10550 2    60   ~ 0
A1
$Comp
L GNDD #PWR014
U 1 1 59F17BB6
P 3200 10750
F 0 "#PWR014" H 3200 10500 50  0001 C CNN
F 1 "GNDD" H 3200 10625 50  0000 C CNN
F 2 "" H 3200 10750 50  0001 C CNN
F 3 "" H 3200 10750 50  0001 C CNN
	1    3200 10750
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR015
U 1 1 59F17DA4
P 7750 8900
F 0 "#PWR015" H 7750 8650 50  0001 C CNN
F 1 "GNDD" H 7750 8775 50  0000 C CNN
F 2 "" H 7750 8900 50  0001 C CNN
F 3 "" H 7750 8900 50  0001 C CNN
	1    7750 8900
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR016
U 1 1 59F18080
P 6300 8400
F 0 "#PWR016" H 6300 8150 50  0001 C CNN
F 1 "GNDD" H 6300 8275 50  0000 C CNN
F 2 "" H 6300 8400 50  0001 C CNN
F 3 "" H 6300 8400 50  0001 C CNN
	1    6300 8400
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR017
U 1 1 59F180C1
P 7700 8050
F 0 "#PWR017" H 7700 7900 50  0001 C CNN
F 1 "+5V" H 7700 8190 50  0000 C CNN
F 2 "" H 7700 8050 50  0001 C CNN
F 3 "" H 7700 8050 50  0001 C CNN
	1    7700 8050
	1    0    0    -1  
$EndComp
$Comp
L CP C3
U 1 1 59F189B0
P 5350 9150
F 0 "C3" H 5375 9250 50  0000 L CNN
F 1 "1µ/35V" H 5375 9050 50  0000 L CNN
F 2 "" H 5388 9000 50  0001 C CNN
F 3 "" H 5350 9150 50  0001 C CNN
	1    5350 9150
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR018
U 1 1 59F19158
P 5050 8600
F 0 "#PWR018" H 5050 8450 50  0001 C CNN
F 1 "+5V" H 5050 8740 50  0000 C CNN
F 2 "" H 5050 8600 50  0001 C CNN
F 3 "" H 5050 8600 50  0001 C CNN
	1    5050 8600
	1    0    0    -1  
$EndComp
$Comp
L CP C4
U 1 1 59F1919C
P 5900 9450
F 0 "C4" H 5925 9550 50  0000 L CNN
F 1 "1µ/35V" H 5925 9350 50  0000 L CNN
F 2 "" H 5938 9300 50  0001 C CNN
F 3 "" H 5900 9450 50  0001 C CNN
	1    5900 9450
	1    0    0    -1  
$EndComp
$Comp
L CP C2
U 1 1 59F19CB3
P 3050 8800
F 0 "C2" H 3075 8900 50  0000 L CNN
F 1 "1µ/35V" H 3075 8700 50  0000 L CNN
F 2 "" H 3088 8650 50  0001 C CNN
F 3 "" H 3050 8800 50  0001 C CNN
	1    3050 8800
	-1   0    0    1   
$EndComp
$Comp
L CP C1
U 1 1 59F19D6C
P 2750 9600
F 0 "C1" H 2775 9700 50  0000 L CNN
F 1 "1µ/35V" H 2775 9500 50  0000 L CNN
F 2 "" H 2788 9450 50  0001 C CNN
F 3 "" H 2750 9600 50  0001 C CNN
	1    2750 9600
	1    0    0    -1  
$EndComp
$Comp
L DB25_Male J1
U 1 1 59F1A5E1
P 1650 9150
F 0 "J1" H 1650 10500 50  0000 C CNN
F 1 "DB25_Male" H 1650 7775 50  0000 C CNN
F 2 "" H 1650 9150 50  0001 C CNN
F 3 "" H 1650 9150 50  0001 C CNN
	1    1650 9150
	-1   0    0    -1  
$EndComp
Entry Wire Line
	13100 3900 13200 4000
Entry Wire Line
	13100 3800 13200 3900
Entry Wire Line
	13100 3700 13200 3800
Entry Wire Line
	13100 4000 13200 4100
Text Label 13250 3800 0    60   ~ 0
A4
Text Label 13250 3900 0    60   ~ 0
A5
Text Label 13250 4000 0    60   ~ 0
A6
Text Label 13250 4100 0    60   ~ 0
A7
Entry Wire Line
	6200 1350 6300 1450
Entry Wire Line
	6200 1250 6300 1350
Entry Wire Line
	6200 1150 6300 1250
Entry Wire Line
	6200 1000 6300 900 
Text Label 6400 900  0    60   ~ 0
A7
Text Label 6400 1250 0    60   ~ 0
A4
Text Label 6400 1350 0    60   ~ 0
A5
Text Label 6400 1450 0    60   ~ 0
A6
Text GLabel 6100 1000 0    60   Input ~ 0
ADRESSBUS
$Comp
L +5V #PWR?
U 1 1 59F66855
P 3450 2950
F 0 "#PWR?" H 3450 2800 50  0001 C CNN
F 1 "+5V" H 3450 3090 50  0000 C CNN
F 2 "" H 3450 2950 50  0001 C CNN
F 3 "" H 3450 2950 50  0001 C CNN
	1    3450 2950
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR?
U 1 1 59F66A88
P 3500 4850
F 0 "#PWR?" H 3500 4600 50  0001 C CNN
F 1 "GNDD" H 3500 4725 50  0000 C CNN
F 2 "" H 3500 4850 50  0001 C CNN
F 3 "" H 3500 4850 50  0001 C CNN
	1    3500 4850
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR?
U 1 1 59F67403
P 10850 850
F 0 "#PWR?" H 10850 700 50  0001 C CNN
F 1 "+5V" H 10850 990 50  0000 C CNN
F 2 "" H 10850 850 50  0001 C CNN
F 3 "" H 10850 850 50  0001 C CNN
	1    10850 850 
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR?
U 1 1 59F67567
P 10850 1450
F 0 "#PWR?" H 10850 1200 50  0001 C CNN
F 1 "GNDD" H 10850 1325 50  0000 C CNN
F 2 "" H 10850 1450 50  0001 C CNN
F 3 "" H 10850 1450 50  0001 C CNN
	1    10850 1450
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR?
U 1 1 59F67686
P 7200 1700
F 0 "#PWR?" H 7200 1450 50  0001 C CNN
F 1 "GNDD" H 7200 1575 50  0000 C CNN
F 2 "" H 7200 1700 50  0001 C CNN
F 3 "" H 7200 1700 50  0001 C CNN
	1    7200 1700
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR?
U 1 1 59F6771B
P 7200 650
F 0 "#PWR?" H 7200 500 50  0001 C CNN
F 1 "+5V" H 7200 790 50  0000 C CNN
F 2 "" H 7200 650 50  0001 C CNN
F 3 "" H 7200 650 50  0001 C CNN
	1    7200 650 
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR?
U 1 1 59F6780C
P 9400 700
F 0 "#PWR?" H 9400 550 50  0001 C CNN
F 1 "+5V" H 9400 840 50  0000 C CNN
F 2 "" H 9400 700 50  0001 C CNN
F 3 "" H 9400 700 50  0001 C CNN
	1    9400 700 
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR?
U 1 1 59F6792B
P 9400 1600
F 0 "#PWR?" H 9400 1350 50  0001 C CNN
F 1 "GNDD" H 9400 1475 50  0000 C CNN
F 2 "" H 9400 1600 50  0001 C CNN
F 3 "" H 9400 1600 50  0001 C CNN
	1    9400 1600
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR?
U 1 1 59F67F59
P 12900 1700
F 0 "#PWR?" H 12900 1550 50  0001 C CNN
F 1 "+5V" H 12900 1840 50  0000 C CNN
F 2 "" H 12900 1700 50  0001 C CNN
F 3 "" H 12900 1700 50  0001 C CNN
	1    12900 1700
	1    0    0    -1  
$EndComp
$Comp
L GNDD #PWR?
U 1 1 59F67FEE
P 12550 1550
F 0 "#PWR?" H 12550 1300 50  0001 C CNN
F 1 "GNDD" H 12550 1425 50  0000 C CNN
F 2 "" H 12550 1550 50  0001 C CNN
F 3 "" H 12550 1550 50  0001 C CNN
	1    12550 1550
	1    0    0    -1  
$EndComp
Text Notes 1500 7650 0    60   ~ 0
RS-232\nDB-25
Text Notes 4050 5000 0    60   ~ 0
PS/2 Keyboard Interface
Text GLabel 9650 9050 2    60   Input ~ 0
R/~W
Text GLabel 9650 9150 2    60   Input ~ 0
PHI2
$Comp
L GNDD #PWR?
U 1 1 59F89038
P 11050 9400
F 0 "#PWR?" H 11050 9150 50  0001 C CNN
F 1 "GNDD" H 11050 9275 50  0000 C CNN
F 2 "" H 11050 9400 50  0001 C CNN
F 3 "" H 11050 9400 50  0001 C CNN
	1    11050 9400
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR?
U 1 1 59F891CA
P 11050 8700
F 0 "#PWR?" H 11050 8550 50  0001 C CNN
F 1 "+5V" H 11050 8840 50  0000 C CNN
F 2 "" H 11050 8700 50  0001 C CNN
F 3 "" H 11050 8700 50  0001 C CNN
	1    11050 8700
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 59F8935E
P 11050 9050
F 0 "C?" H 11075 9150 50  0000 L CNN
F 1 "100nF" H 11075 8950 50  0000 L CNN
F 2 "" H 11088 8900 50  0001 C CNN
F 3 "" H 11050 9050 50  0001 C CNN
	1    11050 9050
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 59F89492
P 11350 9050
F 0 "C?" H 11375 9150 50  0000 L CNN
F 1 "100nF" H 11375 8950 50  0000 L CNN
F 2 "" H 11388 8900 50  0001 C CNN
F 3 "" H 11350 9050 50  0001 C CNN
	1    11350 9050
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 59F894F5
P 11650 9050
F 0 "C?" H 11675 9150 50  0000 L CNN
F 1 "100nF" H 11675 8950 50  0000 L CNN
F 2 "" H 11688 8900 50  0001 C CNN
F 3 "" H 11650 9050 50  0001 C CNN
	1    11650 9050
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 59F8955B
P 11950 9050
F 0 "C?" H 11975 9150 50  0000 L CNN
F 1 "100nF" H 11975 8950 50  0000 L CNN
F 2 "" H 11988 8900 50  0001 C CNN
F 3 "" H 11950 9050 50  0001 C CNN
	1    11950 9050
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 59F89623
P 12250 9050
F 0 "C?" H 12275 9150 50  0000 L CNN
F 1 "100nF" H 12275 8950 50  0000 L CNN
F 2 "" H 12288 8900 50  0001 C CNN
F 3 "" H 12250 9050 50  0001 C CNN
	1    12250 9050
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 59F89697
P 12550 9050
F 0 "C?" H 12575 9150 50  0000 L CNN
F 1 "100nF" H 12575 8950 50  0000 L CNN
F 2 "" H 12588 8900 50  0001 C CNN
F 3 "" H 12550 9050 50  0001 C CNN
	1    12550 9050
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 59F8970A
P 12850 9050
F 0 "C?" H 12875 9150 50  0000 L CNN
F 1 "100nF" H 12875 8950 50  0000 L CNN
F 2 "" H 12888 8900 50  0001 C CNN
F 3 "" H 12850 9050 50  0001 C CNN
	1    12850 9050
	1    0    0    -1  
$EndComp
$Comp
L CP C?
U 1 1 59F898AA
P 13150 9050
F 0 "C?" H 13175 9150 50  0000 L CNN
F 1 "47µF/6,3V" H 13175 8950 50  0000 L CNN
F 2 "" H 13188 8900 50  0001 C CNN
F 3 "" H 13150 9050 50  0001 C CNN
	1    13150 9050
	1    0    0    -1  
$EndComp
$Comp
L CP C?
U 1 1 59F89B4A
P 13650 9050
F 0 "C?" H 13675 9150 50  0000 L CNN
F 1 "47µF/6,3V" H 13675 8950 50  0000 L CNN
F 2 "" H 13688 8900 50  0001 C CNN
F 3 "" H 13650 9050 50  0001 C CNN
	1    13650 9050
	1    0    0    -1  
$EndComp
Text Notes 3550 2800 0    60   ~ 0
http://sbc.rictor.org/ \nPC Keyboard -> AVR ATTiny26 Interface
Text GLabel 6700 9550 0    60   Input ~ 0
~RES
Text GLabel 10650 5850 2    60   Input ~ 0
~RES
Text GLabel 10550 3300 2    60   Input ~ 0
~RES
Text GLabel 14200 3700 0    60   Input ~ 0
~RES
Text GLabel 14550 3100 0    60   Input ~ 0
~IRQ
Text GLabel 10550 4750 2    60   Input ~ 0
~IRQ
Text GLabel 9450 9250 2    60   Input ~ 0
~IRQ
$Comp
L +5V #PWR06
U 1 1 59EFEE53
P 9350 8850
F 0 "#PWR06" H 9350 8700 50  0001 C CNN
F 1 "+5V" H 9350 8990 50  0000 C CNN
F 2 "" H 9350 8850 50  0001 C CNN
F 3 "" H 9350 8850 50  0001 C CNN
	1    9350 8850
	1    0    0    -1  
$EndComp
Text GLabel 10050 7300 2    60   Input ~ 0
~NMI
Text GLabel 14550 3300 0    60   Input ~ 0
~NMI
Text GLabel 6150 5100 2    60   Input ~ 0
~RES
Wire Bus Line
	6200 1000 6100 1000
Wire Bus Line
	6200 1350 6200 1000
Wire Bus Line
	13100 4000 13100 3300
Wire Wire Line
	13900 4100 13200 4100
Wire Wire Line
	13900 4200 13900 4100
Wire Wire Line
	13700 3900 13700 3800
Wire Wire Line
	13200 3900 13700 3900
Wire Wire Line
	13600 3800 13200 3800
Wire Wire Line
	13600 3600 13600 3800
Connection ~ 2750 10550
Wire Wire Line
	2150 9150 2150 10550
Wire Wire Line
	1950 9150 2150 9150
Wire Wire Line
	2200 10150 1950 10150
Wire Wire Line
	2200 9850 2200 10150
Wire Wire Line
	3100 9850 2200 9850
Wire Wire Line
	3100 9750 3100 9850
Wire Wire Line
	3350 9750 3100 9750
Wire Wire Line
	2300 9950 1950 9950
Wire Wire Line
	2300 10150 2300 9950
Wire Wire Line
	3350 10150 2300 10150
Wire Wire Line
	2400 9550 1950 9550
Wire Wire Line
	2400 10350 2400 9550
Wire Wire Line
	3350 10350 2400 10350
Wire Wire Line
	2450 9750 1950 9750
Wire Wire Line
	2450 9950 2450 9750
Wire Wire Line
	3350 9950 2450 9950
Connection ~ 3200 10550
Wire Wire Line
	2750 10550 2750 9750
Wire Wire Line
	3350 9450 2750 9450
Connection ~ 5050 8750
Wire Wire Line
	4850 8750 5050 8750
Wire Wire Line
	4850 8600 4850 8750
Wire Wire Line
	3050 8600 4850 8600
Wire Wire Line
	3050 8650 3050 8600
Wire Wire Line
	3050 9000 3050 8950
Wire Wire Line
	3350 9000 3050 9000
Wire Wire Line
	5050 9000 5050 8600
Wire Wire Line
	4950 9000 5050 9000
Wire Wire Line
	5350 8950 5350 9000
Wire Wire Line
	5200 8950 5350 8950
Wire Wire Line
	5200 9150 5200 8950
Wire Wire Line
	4950 9150 5200 9150
Wire Wire Line
	4950 9300 5350 9300
Wire Wire Line
	5900 9250 5900 9300
Wire Wire Line
	5750 9250 5900 9250
Wire Wire Line
	5750 9450 5750 9250
Wire Wire Line
	4950 9450 5750 9450
Wire Wire Line
	5900 9600 4950 9600
Wire Wire Line
	6300 8350 6300 8400
Wire Wire Line
	6750 8350 6300 8350
Wire Wire Line
	7700 8050 7700 8150
Wire Wire Line
	7700 8150 7550 8150
Wire Wire Line
	7400 9250 7550 9250
Wire Wire Line
	7400 8850 7400 9250
Wire Wire Line
	7750 8850 7400 8850
Wire Wire Line
	7750 8900 7750 8850
Wire Wire Line
	3200 10550 3200 10750
Wire Wire Line
	2150 10550 3350 10550
Wire Wire Line
	7350 9700 6800 9700
Wire Wire Line
	7350 9750 7350 9700
Wire Wire Line
	7900 8350 7550 8350
Wire Wire Line
	7900 8750 7900 8350
Wire Wire Line
	6800 8750 7900 8750
Wire Wire Line
	6800 9700 6800 8750
Wire Wire Line
	7350 9750 7550 9750
Wire Wire Line
	7150 10350 7550 10350
Wire Wire Line
	7150 10150 7150 10350
Wire Wire Line
	4950 10150 7150 10150
Wire Bus Line
	7050 10350 7050 10450
Wire Wire Line
	6100 10050 7550 10050
Wire Wire Line
	6100 10350 6100 10050
Wire Wire Line
	4950 10350 6100 10350
Wire Wire Line
	4950 9950 7550 9950
Wire Wire Line
	7250 10150 7250 9750
Wire Wire Line
	7550 10150 7250 10150
Wire Wire Line
	7250 9750 4950 9750
Wire Wire Line
	7550 10550 7150 10550
Wire Wire Line
	7150 10450 7550 10450
Wire Bus Line
	6750 10350 7050 10350
Wire Wire Line
	9750 3350 10150 3350
Wire Wire Line
	10150 3250 9750 3250
Wire Wire Line
	9750 3150 10150 3150
Wire Wire Line
	10150 3050 9750 3050
Wire Bus Line
	10250 2950 10250 3250
Wire Bus Line
	10550 2950 10250 2950
Wire Wire Line
	9750 5900 10150 5900
Wire Wire Line
	10150 5800 9750 5800
Wire Wire Line
	9750 5700 10150 5700
Wire Wire Line
	10150 5600 9750 5600
Wire Bus Line
	10250 5500 10250 5800
Wire Bus Line
	10550 5500 10250 5500
Wire Bus Line
	13100 3300 12850 3300
Wire Wire Line
	13500 3700 13200 3700
Wire Wire Line
	13500 3400 13500 3700
Wire Wire Line
	14700 3400 13500 3400
Wire Wire Line
	13450 3600 13200 3600
Wire Wire Line
	13450 3300 13450 3600
Wire Wire Line
	14300 3300 13450 3300
Wire Wire Line
	14300 3200 14300 3300
Wire Wire Line
	14700 3200 14300 3200
Wire Wire Line
	13400 3500 13200 3500
Wire Wire Line
	13400 3200 13400 3500
Wire Wire Line
	14200 3200 13400 3200
Wire Wire Line
	14200 3000 14200 3200
Wire Wire Line
	14700 3000 14200 3000
Wire Wire Line
	13350 3400 13200 3400
Wire Wire Line
	13350 3150 13350 3400
Wire Wire Line
	14150 3150 13350 3150
Wire Wire Line
	14150 2800 14150 3150
Wire Wire Line
	14700 2800 14150 2800
Wire Wire Line
	10300 6900 9750 6900
Wire Wire Line
	10300 4350 9750 4350
Wire Wire Line
	14550 2900 14700 2900
Connection ~ 8150 7400
Wire Wire Line
	9900 7550 8150 7550
Wire Wire Line
	9900 7000 9900 7550
Wire Wire Line
	9750 7000 9900 7000
Wire Wire Line
	8150 7300 8350 7300
Wire Wire Line
	8150 7550 8150 7300
Wire Wire Line
	7950 7400 8150 7400
Wire Wire Line
	7950 7350 7950 7400
Connection ~ 8250 4900
Wire Wire Line
	9850 4450 9750 4450
Wire Wire Line
	9850 4900 9850 4450
Wire Wire Line
	8250 4750 8350 4750
Wire Wire Line
	8250 4900 8250 4750
Wire Wire Line
	8050 4900 9850 4900
Wire Wire Line
	8050 4800 8050 4900
Wire Wire Line
	9750 4650 10300 4650
Wire Wire Line
	9750 7200 10300 7200
Wire Wire Line
	10300 7100 9750 7100
Wire Wire Line
	8250 2850 8350 2850
Wire Wire Line
	8250 2600 8250 2850
Wire Wire Line
	8050 2600 8250 2600
Wire Wire Line
	8050 2750 8050 2600
Wire Wire Line
	8200 5200 8200 5400
Wire Wire Line
	8000 5200 8200 5200
Wire Wire Line
	8000 5300 8000 5200
Wire Wire Line
	8200 5400 8350 5400
Wire Wire Line
	10300 4550 9750 4550
Connection ~ 7900 1000
Wire Wire Line
	7900 1700 8200 1700
Wire Wire Line
	7900 1000 7900 1700
Connection ~ 8000 900 
Wire Wire Line
	8000 1600 8200 1600
Wire Wire Line
	8000 900  8000 1600
Wire Wire Line
	7550 3650 8350 3650
Wire Wire Line
	7550 3750 7550 3650
Wire Wire Line
	5350 3750 7550 3750
Wire Wire Line
	7450 3550 8350 3550
Wire Wire Line
	7450 3650 7450 3550
Wire Wire Line
	5350 3650 7450 3650
Wire Wire Line
	7350 3450 8350 3450
Wire Wire Line
	7350 3550 7350 3450
Wire Wire Line
	5350 3550 7350 3550
Wire Wire Line
	7250 3350 8350 3350
Wire Wire Line
	7250 3450 7250 3350
Wire Wire Line
	5350 3450 7250 3450
Wire Wire Line
	7150 3250 8350 3250
Wire Wire Line
	7150 3350 7150 3250
Wire Wire Line
	5350 3350 7150 3350
Wire Wire Line
	7050 3150 8350 3150
Wire Wire Line
	7050 3250 7050 3150
Wire Wire Line
	5350 3250 7050 3250
Wire Wire Line
	6950 3150 5350 3150
Wire Wire Line
	6950 3050 6950 3150
Wire Wire Line
	8350 3050 6950 3050
Wire Wire Line
	6900 3050 5350 3050
Wire Wire Line
	6900 2950 6900 3050
Wire Wire Line
	8350 2950 6900 2950
Connection ~ 5700 2500
Wire Wire Line
	5900 2500 5700 2500
Wire Wire Line
	5900 2550 5900 2500
Wire Wire Line
	5700 2550 5700 2400
Connection ~ 5900 3900
Wire Wire Line
	5900 2850 5900 3900
Connection ~ 5700 3800
Wire Wire Line
	5700 2850 5700 3800
Wire Wire Line
	6500 4700 6500 4600
Wire Wire Line
	6050 4700 6500 4700
Wire Wire Line
	6500 3900 6500 4050
Wire Wire Line
	6050 3900 6500 3900
Wire Wire Line
	6050 4600 6050 4700
Wire Wire Line
	6050 3900 6050 4000
Wire Wire Line
	5700 4250 5350 4250
Wire Wire Line
	5700 3900 5700 4250
Wire Wire Line
	5950 3900 5700 3900
Wire Wire Line
	5950 4000 5950 3900
Wire Wire Line
	6150 3800 6150 4000
Wire Wire Line
	5650 3800 6150 3800
Wire Wire Line
	5650 4350 5350 4350
Wire Wire Line
	5650 3800 5650 4350
Wire Wire Line
	13500 1300 13150 1300
Wire Wire Line
	14150 1300 13800 1300
Wire Bus Line
	10350 6000 10650 6000
Wire Bus Line
	10350 6700 10350 6000
Wire Wire Line
	9750 6800 10250 6800
Wire Wire Line
	9750 6700 10250 6700
Wire Wire Line
	9750 6600 10250 6600
Wire Wire Line
	9750 6500 10250 6500
Wire Wire Line
	9750 6400 10250 6400
Wire Wire Line
	10250 6300 9750 6300
Wire Wire Line
	9750 6200 10250 6200
Wire Wire Line
	10250 6100 9750 6100
Wire Wire Line
	9750 3550 10250 3550
Wire Wire Line
	9750 3650 10250 3650
Wire Wire Line
	9750 3750 10250 3750
Wire Wire Line
	9750 3850 10250 3850
Wire Wire Line
	9750 3950 10250 3950
Wire Wire Line
	9750 4050 10250 4050
Wire Wire Line
	9750 4150 10250 4150
Wire Wire Line
	9750 4250 10250 4250
Wire Bus Line
	10350 3450 10550 3450
Wire Bus Line
	10350 4150 10350 3450
Wire Wire Line
	11800 2900 12150 2900
Wire Wire Line
	11800 2800 12150 2800
Wire Wire Line
	11800 2700 12150 2700
Wire Wire Line
	11800 2600 12150 2600
Wire Wire Line
	11800 2500 12150 2500
Wire Wire Line
	11800 2400 12150 2400
Wire Wire Line
	11800 2300 12150 2300
Wire Wire Line
	11800 2200 12150 2200
Wire Wire Line
	12000 2000 12000 2050
Wire Wire Line
	11850 2000 12000 2000
Wire Bus Line
	11700 2100 11350 2100
Wire Bus Line
	11700 2800 11700 2100
Wire Wire Line
	9150 9550 8950 9550
Wire Wire Line
	9150 9650 8950 9650
Wire Wire Line
	9150 9750 8950 9750
Wire Wire Line
	9150 9850 8950 9850
Wire Wire Line
	9150 9950 8950 9950
Wire Wire Line
	9150 10050 8950 10050
Wire Wire Line
	9150 10150 8950 10150
Wire Wire Line
	9150 10250 8950 10250
Wire Bus Line
	9250 9350 9650 9350
Wire Bus Line
	9250 10150 9250 9350
Wire Wire Line
	9350 8850 9350 10550
Wire Wire Line
	9350 10550 8950 10550
Wire Wire Line
	7300 9350 7300 9100
Wire Wire Line
	7550 9350 7300 9350
Wire Wire Line
	7550 9450 7450 9450
Connection ~ 7850 800 
Wire Wire Line
	7850 650  7850 800 
Wire Wire Line
	7950 650  7850 650 
Wire Wire Line
	6300 1450 6650 1450
Wire Wire Line
	6300 1350 6650 1350
Wire Wire Line
	6300 1250 6650 1250
Wire Wire Line
	6300 900  6650 900 
Wire Wire Line
	6550 1000 6550 700 
Wire Wire Line
	6650 1000 6550 1000
Wire Wire Line
	13900 4200 14700 4200
Wire Wire Line
	13200 4000 14700 4000
Wire Wire Line
	13700 3800 14700 3800
Wire Wire Line
	13600 3600 14700 3600
Wire Wire Line
	6100 800  6650 800 
Wire Wire Line
	14200 3900 14700 3900
Connection ~ 8500 1400
Wire Wire Line
	8500 1500 8900 1500
Connection ~ 8500 1300
Wire Wire Line
	8500 1400 8900 1400
Connection ~ 8500 1200
Wire Wire Line
	8500 1300 8900 1300
Connection ~ 8500 1100
Wire Wire Line
	8500 1200 8900 1200
Wire Wire Line
	8500 700  8500 1500
Wire Wire Line
	8900 1100 8500 1100
Wire Wire Line
	7750 1000 8900 1000
Wire Wire Line
	7750 900  8900 900 
Wire Wire Line
	7750 800  8900 800 
Wire Wire Line
	11850 1900 12150 1900
Wire Wire Line
	11850 1150 11850 1900
Wire Wire Line
	11350 1150 11850 1150
Connection ~ 10150 1150
Wire Wire Line
	10150 1250 10350 1250
Wire Wire Line
	10150 1050 10350 1050
Wire Wire Line
	10150 1050 10150 1250
Wire Wire Line
	9900 1150 10150 1150
Wire Wire Line
	12000 2050 12150 2050
Wire Wire Line
	14700 2700 14550 2700
Wire Wire Line
	13250 2900 14050 2900
Wire Wire Line
	13250 2800 13950 2800
Wire Wire Line
	13250 2700 13850 2700
Wire Wire Line
	13250 2600 13750 2600
Wire Wire Line
	13250 2500 13650 2500
Wire Wire Line
	13250 2400 13550 2400
Wire Wire Line
	13450 2300 13250 2300
Wire Wire Line
	13350 2200 13250 2200
Wire Wire Line
	14050 2600 14700 2600
Wire Wire Line
	14050 2900 14050 2600
Wire Wire Line
	13950 2500 14700 2500
Wire Wire Line
	13950 2800 13950 2500
Wire Wire Line
	13850 2400 14700 2400
Wire Wire Line
	13850 2700 13850 2400
Wire Wire Line
	13750 2300 14700 2300
Wire Wire Line
	13750 2600 13750 2300
Wire Wire Line
	13650 2200 14700 2200
Wire Wire Line
	13650 2500 13650 2200
Wire Wire Line
	13550 2100 14700 2100
Wire Wire Line
	13550 2400 13550 2100
Wire Wire Line
	13450 2000 14700 2000
Wire Wire Line
	13450 2300 13450 2000
Wire Wire Line
	13350 1900 13350 2200
Wire Wire Line
	13350 1900 14700 1900
Wire Wire Line
	3750 4550 3500 4550
Wire Wire Line
	3500 4550 3500 4850
Wire Wire Line
	3500 4650 3750 4650
Connection ~ 3500 4650
Wire Wire Line
	3750 3050 3450 3050
Wire Wire Line
	3450 2950 3450 3350
Wire Wire Line
	3450 3350 3750 3350
Connection ~ 3450 3050
Wire Wire Line
	12900 1700 12900 1800
Wire Wire Line
	12750 1800 12750 1500
Wire Wire Line
	12750 1500 12550 1500
Wire Wire Line
	12550 1500 12550 1550
Wire Wire Line
	7200 650  7200 700 
Wire Wire Line
	7200 1700 7200 1600
Wire Wire Line
	10850 850  10850 900 
Wire Wire Line
	10850 1400 10850 1450
Wire Wire Line
	9400 700  9400 750 
Wire Wire Line
	9400 1600 9400 1550
Wire Wire Line
	11050 9400 11050 9200
Wire Wire Line
	11050 8700 11050 8900
Wire Wire Line
	11050 8800 13650 8800
Connection ~ 11050 8800
Wire Wire Line
	11350 8800 11350 8900
Wire Wire Line
	11650 8800 11650 8900
Connection ~ 11350 8800
Wire Wire Line
	11950 8800 11950 8900
Connection ~ 11650 8800
Wire Wire Line
	12250 8800 12250 8900
Connection ~ 11950 8800
Wire Wire Line
	12550 8800 12550 8900
Connection ~ 12250 8800
Wire Wire Line
	12850 8800 12850 8900
Connection ~ 12550 8800
Wire Wire Line
	13150 8800 13150 8900
Connection ~ 12850 8800
Wire Wire Line
	13650 8800 13650 8900
Connection ~ 13150 8800
Wire Wire Line
	11050 9300 13650 9300
Wire Wire Line
	11350 9300 11350 9200
Connection ~ 11050 9300
Wire Wire Line
	11650 9300 11650 9200
Connection ~ 11350 9300
Wire Wire Line
	11950 9300 11950 9200
Connection ~ 11650 9300
Wire Wire Line
	12250 9300 12250 9200
Connection ~ 11950 9300
Wire Wire Line
	12550 9300 12550 9200
Connection ~ 12250 9300
Wire Wire Line
	12850 9300 12850 9200
Connection ~ 12550 9300
Wire Wire Line
	13150 9300 13150 9200
Connection ~ 12850 9300
Wire Wire Line
	13650 9300 13650 9200
Connection ~ 13150 9300
Wire Wire Line
	7550 9550 6700 9550
Wire Wire Line
	9750 6000 10300 6000
Wire Wire Line
	10300 6000 10300 5850
Wire Wire Line
	10300 5850 10650 5850
Wire Wire Line
	9750 3450 10300 3450
Wire Wire Line
	10300 3450 10300 3300
Wire Wire Line
	10300 3300 10550 3300
Wire Wire Line
	14700 3700 14200 3700
Wire Wire Line
	9650 9050 9000 9050
Wire Wire Line
	9000 9050 9000 9250
Wire Wire Line
	9000 9250 8950 9250
Wire Wire Line
	8950 9350 9050 9350
Wire Wire Line
	9050 9350 9050 9150
Wire Wire Line
	9050 9150 9650 9150
Wire Wire Line
	9450 9250 9100 9250
Wire Wire Line
	9100 9250 9100 9450
Wire Wire Line
	9100 9450 8950 9450
Wire Wire Line
	9750 7300 10050 7300
Wire Wire Line
	14550 3100 14700 3100
Wire Wire Line
	14550 3300 14700 3300
Wire Wire Line
	5350 4650 5400 4650
Wire Wire Line
	5400 4650 5400 5100
Wire Wire Line
	5400 5100 6150 5100
Text GLabel 6150 4800 2    60   Input ~ 0
PS2_RDY
Text GLabel 6150 4950 2    60   Input ~ 0
PS2_ACK
Wire Wire Line
	5350 4550 5500 4550
Wire Wire Line
	5500 4550 5500 4950
Wire Wire Line
	5500 4950 6150 4950
Wire Wire Line
	5350 4450 5600 4450
Wire Wire Line
	5600 4450 5600 4800
Wire Wire Line
	5600 4800 6150 4800
Text GLabel 10550 2650 2    60   Input ~ 0
PS2_RDY
Text GLabel 10550 2800 2    60   Input ~ 0
PS2_ACK
Wire Wire Line
	9750 2950 10150 2950
Wire Wire Line
	10150 2950 10150 2800
Wire Wire Line
	10150 2800 10550 2800
Wire Wire Line
	9750 2850 10050 2850
Wire Wire Line
	10050 2850 10050 2650
Wire Wire Line
	10050 2650 10550 2650
$EndSCHEMATC
