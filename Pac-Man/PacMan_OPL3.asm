


;***************************
; Restore YMF262
;
;***************************

OPL3_RESET
		lda #$05
		sta YMF262_ADDR2
		lda #$01			
		sta YMF262_DATA2	;OPL3 mode
		ldy #$B0		;key off 	
YMF262_Reset1		
		sty YMF262_ADDR1
		stz YMF262_DATA1
		sty YMF262_ADDR2
		stz YMF262_DATA2			
		iny
		cpy #$B9
		bne YMF262_Reset1
		ldy #$01
YMF262_Reset6
		sty YMF262_ADDR1
		stz YMF262_DATA1
		sty YMF262_ADDR2
		stz YMF262_DATA2	
		iny
		cpy #$F6
		bne YMF262_Reset6
		lda #$05
		sta YMF262_ADDR2
		stz YMF262_DATA2	;set to OPL2 Mode
		rts
