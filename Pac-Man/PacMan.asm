;TMS9918 Pac Man clone
;(c) Uwe Gottschling 2021

!src "../Kernel/include/label_definition.asm"
!src "../Kernel/include/Jumptable.asm"
!src "../Kernel/DOS/DOS_FAT_constant.asm"
!CPU W65C02

NameTableBase		= $1800 ; $1800 ... $1AFF
PatternBase		= $0000	; $0000 ... $17FF
SpriteAttrBase		= $1B00	; $1B00 ... $1B7F
ColorTableBase		= $2000	; $2000 ... $37FF
SpritePatternBase	= $3800 ; $3800 ... $3FFF

*=$0FFE
!word $1000

; Object structure offsets
status			= 0	;0=off, 1=chase, 2=scatter, 3=frighten, 4=eaten
type			= 1	;0=pacman, 1=shadow, 2=speedy, 3=bashful, 4=pokey
xpos			= 2	;2 bytes 
ypos			= 4	;2 bytes
direction		= 6	;00 = up, 01 = right, 02 = down, 03 = left 
pattern			= 7	;Sprite pattern
color			= 8	;
OBJ_SIZE 		= 9	

DIR_UP			= 0
DIR_RIGHT		= 1
DIR_DOWN		= 2
DIR_LEFT		= 3

NUM_OBJ			= 5
VRAM			= $40	; TMS9918 video ram register bit

ZeroPage 
ZSTART			= $80		; zero point start address
Level			= ZSTART + 1
Score			= ZSTART + 2	; 2 bytes
PTR_READ		= ZSTART + 4	; 2 bytes
PTR_WRITE		= ZSTART + 6	; 2 bytes	
SpeedPacMan		= ZSTART + 8	; 2 bytes, fixed point 8.8
SpeedGhosts		= ZSTART + 10	; 2 bytes, fixed point 8.8
SpeedGhostsAff		= ZSTART + 12	; 2 bytes, fixed point 8.8
SpeedGhostsEyes		= ZSTART + 14	; 2 bytes, fixed point 8.8
PTR_OBJ			= ZSTART + 16	; 2 bytes
SAVEIRQVECT_L		= ZSTART + 18
SAVEIRQVECT_H		= ZSTART + 19
FRAMECOUNTER		= ZSTART + 20
PTR_IRQ_READ		= ZSTART + 21	; 2 bytes
JOYSTATE		= ZSTART + 23	
TILEOFFSET		= ZSTART + 24	; 2 bytes

; TMS9918 colors

Transparent		= 0
Black			= 1
MediumGreen		= 2
LightGreen		= 3 
DarkBlue		= 4
LightBlue		= 5
DarkRed			= 6
Cyan			= 7
MediumRed		= 8
LightRed		= 9
DarkYellow		= 10
LightYellow		= 11
DarkGreen		= 12
Magenta			= 13
Gray			= 14
White			= 15

SPR_PacManClose		= 0
SPR_PacManUp1		= 4
SPR_PacManUp2		= 8
SPR_PacManRight1	= 12	;$0C
SPR_PacManRight2	= 16	;$10
SPR_PacManDown1		= 20	;$14
SPR_PacManDown2		= 24	;$18
SPR_PacManLeft1		= 28	;$1C
SPR_PacManLeft2		= 32	;$20
SPR_PacManDie1		= 36	;$24
SPR_PacManDie2		= 40	;$28
SPR_PacManDie3		= 44	;$2C
SPR_PacManDie4		= 48	;$30
SPR_PacManDie5		= 52	;$34
SPR_PacManDie6		= 56	;$38
SPR_PacManDie7		= 60	;$40
SPR_PacManDie8		= 64
SPR_GhostUp1		= 68	
SPR_GhostUp2		= 72
SPR_GhostRight1		= 76
SPR_GhostRight2		= 80
SPR_GhostDown1		= 84
SPR_GhostDown2		= 88
SPR_GhostLeft1		= 92
SPR_GhostLeft2		= 96
SPR_GhostAfraid1	= 100
SPR_GhostAfraid2	= 104
SPR_EyesUp		= 108
SPR_EyesRight		= 112
SPR_EyesDown		= 116
SPR_EyesLeft		= 120
SPR_200			= 124
SPR_400			= 128
SPR_800			= 132
SPR_1600		= 136

; Points
PacDot 			= 10
PowerPellet		= 50
GhostNo1		= 200
GhostNo2		= 400
GhostNo3		= 800
GhostNo4		= 1600

; Joytick port switches
JOY_UP		= %00000001
JOY_DOWN	= %00000010
JOY_LEFT	= %00000100
JOY_RIGHT	= %00001000
JOY_FIRE	= %00010000

Main
		jsr TMS9918Init
		jsr TMS9918Init_Sprite_Pattern
		jsr Fill_VRAM_NameTable
		jsr Fill_Color_Generator_Table
		jsr Fill_Pattern_Generator_Table
		jsr Initialize
		jsr SetIRQVector
		jsr SetObjStartPos
;		jsr DispObj	; TBD: for debugging
		jsr Main_Loop
;		jsr WaitKey
Main2
		jsr Restore_HW
		jsr TMS9918_Init_Textmode1
		rts
		jmp Main2

;***************************
; TMS9918 Sprite Pattern
;
;***************************

TMS9918Init_Sprite_Pattern
		lda #<SpritePatternBase
		sta TMS9918REG		; set start address VRAM low Byte
		nop
		nop
		nop
		nop
		lda #$40+>SpritePatternBase ; set address maker and high Byte --> Sprite Pattern Generator Sub-Block
		sta TMS9918REG		; set start address VRAM
		lda #<Sprite_Data	; Low Byte
		sta PTR_READ
		lda #>Sprite_Data	; High Byte
		sta PTR_READ+1		
		ldy #35			; number of pattern 	
TMS9918Init_Sprite_Pattern1
		ldx #32			; Byte per pattern		
TMS9918Init_Sprite_Pattern2
		lda (PTR_READ)
		jsr Inc_PTR_READ
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		sta TMS9918RAM
		dex
		bne TMS9918Init_Sprite_Pattern2
		dey
		bne TMS9918Init_Sprite_Pattern1
; Set attribute
		sta Operand1
		ldx #$00
		ldy #28			; Number of sprites
		lda #<SpriteAttrBase
		sta TMS9918REG		; set start address VRAM low Byte
		nop	
		nop
		lda #$40+>SpriteAttrBase		; set address maker and high Byte 
		sta TMS9918REG		; set start address VRAM
		lda #0
TMS9918Init_Sprite_Pattern3
		nop	
		nop
		nop	
		nop
		sta TMS9918RAM		; Y Position
		nop
		nop
		nop
		nop
		sta TMS9918RAM		; X Position
		nop
		nop
		nop
		nop
		stx TMS9918RAM		; Sprite Name 
		inx
		inx
		inx
		inx
		pha
		lda #10
		sta TMS9918RAM		; Sprite Color Code
		pla
		nop
		nop
		nop
		clc
		adc #8
		dey 
		bne TMS9918Init_Sprite_Pattern3
		rts

;***************************
; TMS9918 Copy Data to Pattern Generator Table
;
;***************************

Fill_Pattern_Generator_Table
		lda #<PatternBase
		sta TMS9918REG		; set start address VRAM low Byte
		nop
		nop
		nop
		nop
		lda #$40+>PatternBase	; set address maker and high Byte --> Pattern Generator Sub-Block
		sta TMS9918REG		; set start address VRAM
		ldx #3
Fill_Pattern_Generator_Table0
		phx
		lda #<BANK_PATTERN_0	; Low Byte
		sta PTR_READ
		lda #>BANK_PATTERN_0	; High Byte
		sta PTR_READ+1		
		ldy #0			; number of pattern 0=255 
Fill_Pattern_Generator_Table1
		ldx #8		; Byte per pattern * 3 pattern table		
Fill_Pattern_Generator_Table2
		lda (PTR_READ)
		jsr Inc_PTR_READ
		nop
		nop
		nop
		nop
		sta TMS9918RAM
		dex
		bne Fill_Pattern_Generator_Table2
		dey
		bne Fill_Pattern_Generator_Table1
		plx
		dex
		bne Fill_Pattern_Generator_Table0
		rts


;***************************
; TMS9918 Copy Data to Color Generator Table
;
;***************************

Fill_Color_Generator_Table
		lda #<ColorTableBase
		sta TMS9918REG		; set start address VRAM low Byte
		nop
		nop
		nop
		nop
		lda #$40+>ColorTableBase ; set address maker and high Byte --> Pattern Generator Sub-Block --> $2000
		sta TMS9918REG		; set start address VRAM
		ldx #3	
Fill_Color_Generator_Table0		
		phx
		lda #<BANK_COLOR_0	; Low Byte
		sta PTR_READ
		lda #>BANK_COLOR_0	; High Byte
		sta PTR_READ+1		
		ldy #0		; number of pattern 	
Fill_Color_Generator_Table1
		ldx #8		; Byte per pattern * 3 pattern table		
Fill_Color_Generator_Table2
		lda (PTR_READ)
		jsr Inc_PTR_READ
		nop
		nop
		nop
		nop
		sta TMS9918RAM
		dex
		bne Fill_Color_Generator_Table2
		dey
		bne Fill_Color_Generator_Table1
		plx
		dex
		bne Fill_Color_Generator_Table0
		rts

;***************************
; Initialize 
;
;***************************

Initialize
		stz VIA2DDRB	; input for joystick interface
		stz VIA2ACR	; input latch disable
		lda #$FF
		sta VIA2ORB 
		stz Level
		stz Score
		stz FRAMECOUNTER	

		rts		
		
;***************************
; Set new interrupt vector
;
;***************************		

SetIRQVector		
		sei
		lda IRQVector_L		;save old interrupt vector
		sta SAVEIRQVECT_L
		lda IRQVector_H
		sta SAVEIRQVECT_H
		lda #<VBLANK		;set up interrupt vector
		sta IRQVector_L	
		lda #>VBLANK
		sta IRQVector_H			
		cli	
		rts	

;------------------------------------------------------------------------------
; vertical blanking interrupt handler
;------------------------------------------------------------------------------

VBLANK
		lda TMS9918REG		;read status, clear interrupt flag
		
		lda #$43		;change frame color
		sta TMS9918REG
		lda #$87
		sta TMS9918REG	

		lda #<SpriteAttrBase
		sta TMS9918REG
		lda #VRAM OR >SpriteAttrBase
		sta TMS9918REG
		lda FRAMECOUNTER
		and #01
		bne drawbackwards
		
drawsprites
		lda #<PlayerObjStart
		sta PTR_IRQ_READ
		lda #>PlayerObjStart
		sta PTR_IRQ_READ+1
spriteloop
		ldy #status
		lda (PTR_IRQ_READ),y
		beq nextsprite		;skip dead objects
		ldy #ypos+1
		lda (PTR_IRQ_READ),y	
		sta TMS9918RAM		;y position
		ldy #xpos+1
		lda (PTR_IRQ_READ),y	
		sta TMS9918RAM		;x position
		ldy #pattern
		lda (PTR_IRQ_READ),y
		sta TMS9918RAM		;sprite name
		ldy #color
		lda (PTR_IRQ_READ),y		
		sta TMS9918RAM		;color
nextsprite
		lda PTR_IRQ_READ
		clc
		adc #<OBJ_SIZE 		;advance to next
		sta PTR_IRQ_READ
		lda PTR_IRQ_READ+1
		adc #>OBJ_SIZE
		sta PTR_IRQ_READ+1		
		lda PTR_IRQ_READ+1
		cmp #>PlayerObjEnd
		bne spriteloop		
		lda PTR_IRQ_READ
		cmp #<PlayerObjEnd
		bne spriteloop
		lda #$D0		;store end-of-sprite-list marker
		sta TMS9918RAM	
		jmp printscore
	
; on odd frames, reverse the sprite priorities to reduce fifth-sprite clipping
drawbackwards
		lda #<(PlayerObjEnd-OBJ_SIZE)
		sta PTR_IRQ_READ
		lda #>(PlayerObjEnd-OBJ_SIZE)
		sta PTR_IRQ_READ+1
bspriteloop
		ldy #status
		lda (PTR_IRQ_READ),y
		beq bnextsprite		;skip dead objects
		ldy #ypos+1
		lda (PTR_IRQ_READ),y	
		sta TMS9918RAM		;y position
		ldy #xpos+1
		lda (PTR_IRQ_READ),y	
		sta TMS9918RAM		;x position
		ldy #pattern
		lda (PTR_IRQ_READ),y
		sta TMS9918RAM		;sprite name
		ldy #color
		lda (PTR_IRQ_READ),y		
		sta TMS9918RAM		;color
bnextsprite
		lda PTR_IRQ_READ
		sec
		sbc #<OBJ_SIZE 		;advance to next
		sta PTR_IRQ_READ
		lda PTR_IRQ_READ+1
		sbc #>OBJ_SIZE
		sta PTR_IRQ_READ+1		
		lda PTR_IRQ_READ+1
		cmp #>(PlayerObjStart-OBJ_SIZE)
		bne bspriteloop	
		lda PTR_IRQ_READ
		cmp #<(PlayerObjStart-OBJ_SIZE)
		bne bspriteloop
		lda #$D0		;store end-of-sprite-list marker
		sta TMS9918RAM

printscore	; TBD		

READ_JOYSTICK
		lda VIA2ORB ;Joystick port: bit 0 = up, bit 1 = down, bit 2 = left, bit 3 = right, bit 4 = fire		
		and #$1F
		eor #$1F
		sta JOYSTATE

		lda #$11		;change frame color
		sta TMS9918REG
		lda #$87
		sta TMS9918REG	

		lda ARegister
		ldx XRegister
		ldy YRegister
	
		rti		

;***************************
; Main game loop
;
;***************************

Main_Loop
		lda #$C2+$20		;enable VDP interrupt
		sta TMS9918REG		
		lda #$81
		sta TMS9918REG		
Main_Loop3	
		jsr SetDirectionPacMan
		jsr MovePacMan
		ldx #PS2_ChkKey_FN
		jsr PS2_Function_Call	
		bcc Main_Loop_notexit
		ldx #PS2_GetKey_FN
		jsr PS2_Function_Call
		cmp #"x"		;press x for exit	
		beq Main_Loop_Exit		
		cmp #"p"		;pause key
		bne Main_Loop_notexit		
		ldx #PS2_GetKey_FN
		jsr PS2_Function_Call 
		bra Main_Loop_notexit		
Main_Loop_Exit		
		rts
Main_Loop_notexit
		wai			;wait for interrupt, only with W65C02 / W65C816 !
		inc FRAMECOUNTER
		jmp Main_Loop3

;***************************
; Read joystick input
;
;***************************		
SetDirectionPacMan	
		lda #<PlayerObjStart
		sta PTR_WRITE
		lda #>PlayerObjStart
		sta PTR_WRITE+1
		
		lda JOYSTATE
		and #JOY_LEFT
		beq SetDirectionPacMan2
		ldy #direction
		lda #DIR_LEFT
		sta (PTR_WRITE),y
		rts	
SetDirectionPacMan2		
		lda JOYSTATE
		and #JOY_RIGHT
		beq SetDirectionPacMan3		
		ldy #direction
		lda #DIR_RIGHT
		sta (PTR_WRITE),y
		rts		
SetDirectionPacMan3		
		lda JOYSTATE
		and #JOY_UP
		beq SetDirectionPacMan4		
		ldy #direction
		lda #DIR_UP
		sta (PTR_WRITE),y
		rts
SetDirectionPacMan4
		lda JOYSTATE
		and #JOY_DOWN
		beq SetDirectionPacMan5
		ldy #direction
		lda #DIR_DOWN
		sta (PTR_WRITE),y
SetDirectionPacMan5
		rts		

;***************************
; Set objects to default position
;
;***************************

MovePacMan
		lda #<PlayerObjStart
		sta PTR_WRITE
		lda #>PlayerObjStart
		sta PTR_WRITE+1
		
		ldy #direction
		lda (PTR_WRITE),y
		cmp #DIR_UP			
		bne MovePacMan2
		ldy #pattern
		lda #SPR_PacManUp1
		sta (PTR_WRITE),y
		ldy #ypos+1
		lda (PTR_WRITE),y		
		dec
		sta (PTR_WRITE),y
		bra MovePacMan5
MovePacMan2
		ldy #direction
		lda (PTR_WRITE),y
		cmp #DIR_RIGHT			
		bne MovePacMan3
		lda #SPR_PacManRight1
		ldy #pattern
		sta (PTR_WRITE),y		
		ldy #xpos+1
		lda (PTR_WRITE),y		
		inc
		sta (PTR_WRITE),y	
		bra MovePacMan5
MovePacMan3
		ldy #direction
		lda (PTR_WRITE),y
		cmp #DIR_DOWN			
		bne MovePacMan4
		lda #SPR_PacManDown1
		ldy #pattern
		sta (PTR_WRITE),y
		ldy #ypos+1
		lda (PTR_WRITE),y		
		inc
		sta (PTR_WRITE),y	
		bra MovePacMan5
MovePacMan4		
		ldy #direction
		lda (PTR_WRITE),y
		cmp #DIR_LEFT	
		bne MovePacMan5
		lda #SPR_PacManLeft1
		ldy #pattern
		sta (PTR_WRITE),y
		ldy #xpos+1
		lda (PTR_WRITE),y		
		dec
		sta (PTR_WRITE),y		
MovePacMan5
		bit #%00001100
		bne MovePacMan6
		lda #SPR_PacManClose
		ldy #pattern	
		sta (PTR_WRITE),y
		rts
MovePacMan6		
		bit #%00000100
		bne MovePacMan7
		ldy #pattern	
		lda (PTR_WRITE),y
		clc
		adc #4
		sta (PTR_WRITE),y
MovePacMan7
		rts
		
;***************************
; Check border
;
;***************************		

CalcCollisionOffset
		ldy #ypos+1
		lda (PTR_WRITE),y
		inc
		and #$F8
		sta TILEOFFSET
		stz TILEOFFSET+1
		clc
		rol TILEOFFSET		; Y*2 (Y*32/8 = Y*4)
		rol TILEOFFSET+1
		clc
		rol TILEOFFSET		; Y*4
		rol TILEOFFSET+1
		ldy #xpos+1
		lda (PTR_WRITE),y
		lsr			; X/2
		lsr 			; X/4
		lsr			; X/8
		clc
		adc TILEOFFSET
		sta TILEOFFSET
		lda #0
		adc TILEOFFSET+1
		sta TILEOFFSET+1
CalcCollisionOffset2
		rts
		

		
;***************************
; Set objects to default position
;
;***************************

SetObjStartPos
		lda #<DefPosTable		
		sta PTR_READ
		lda #>DefPosTable		
		sta PTR_READ+1		
		lda #<PlayerObjStart
		sta PTR_WRITE
		lda #>PlayerObjStart
		sta PTR_WRITE+1
		ldy #0
SetObjStartPos1
		lda (PTR_READ),y
		sta (PTR_WRITE),y		
		iny
		cpy #PlayerObjLen
		bne SetObjStartPos1				
		rts

DefPosTable
	!byte 1,0,105,124,03,SPR_PacManClose,DarkYellow	; pac-man, yellow
	!byte 1,1,105,68,03,SPR_GhostLeft1,DarkRed	; shadow, red
	!byte 1,2,105,92,02,SPR_GhostDown1,Magenta	; speedy, magenta
	!byte 1,3,90,92,00,SPR_GhostUp1,Cyan		; bashful, dark blue
	!byte 1,4,120,92,00,SPR_GhostUp1,LightRed	; pokey, orange			

;status		= 0	;0=normal, 1=fearful, 2=only eyes
;type		= 1	;0=pacman, 1=shadow, 2=speedy, 3=bashful, 4=pokey
;xpos		= 2	
;ypos		= 3
;direct		= 4	;00 = up, 01 = right, 02 = down, 03 = left 
;pattern	= 5	;Sprite pattern
;patternColor	= 6	;Sprite color	

; TMS9918 colors: 
;0 transparent
;1 black
;2 medium green
;3 light green
;4 dark blue
;5 light blue
;6 dark red
;7 cyan
;8 medium red
;9 light red
;10 dark yellow
;11 light yellow
;12 dark green
;13 magenta
;14 gray
;15 white

;***************************
; Display sprites
;
;***************************

DispObj		; tbd: delete
		lda #<SpriteAttrBase
		sta TMS9918REG
		lda #VRAM OR >SpriteAttrBase
		sta TMS9918REG

		lda #<PlayerObjStart
		sta PTR_OBJ
		lda #>PlayerObjStart
		sta PTR_OBJ+1
DispObj2
		ldy #ypos
		lda (PTR_OBJ),y	
		sta TMS9918RAM		;y position
		ldy #xpos
		lda (PTR_OBJ),y	
		sta TMS9918RAM		;x position
		ldy #pattern
		lda (PTR_OBJ),y
		sta TMS9918RAM		;sprite name
		ldy #color
		lda (PTR_OBJ),y		
		sta TMS9918RAM		;color

		lda PTR_OBJ
		clc
		adc #<OBJ_SIZE 		;advance to next
		sta PTR_OBJ
		lda PTR_OBJ+1
		adc #>OBJ_SIZE
		sta PTR_OBJ+1		
		lda PTR_OBJ+1
		cmp #>PlayerObjEnd
		bne DispObj2		
		lda PTR_OBJ
		cmp #<PlayerObjEnd
		bne DispObj2
		lda #$D0		;store end-of-sprite-list marker
		sta TMS9918RAM
		rts

;***************************
; Increment PTR_W_L/_H
;
;***************************

Inc_PTR_READ    	
		inc   PTR_READ 	          ;  increments ptr1
               	bne   Inc_PTR_READ2
               	inc   PTR_READ+1 
Inc_PTR_READ2      	
		rts 

;***************************
; Increment PTR_J_L/_H
;
;***************************

Inc_PTR_WRITE  	
		inc   PTR_WRITE          	;  increments ptr2
               	bne   Inc_PTR_WRITE2       
               	inc   PTR_WRITE+1     
Inc_PTR_WRITE2  	
		rts

;***************************
; Fill name table
;
;***************************

Fill_VRAM_NameTable
		lda #<NameTableBase
		sta TMS9918REG		; set start address VRAM low Byte
		nop
		nop
		nop
		nop
		lda #$40+>NameTableBase	; set address maker and high byte --> name block
		sta TMS9918REG		; set start address VRAM
		lda #<Screen_0_0	; Low Byte
		sta PTR_READ
		lda #>Screen_0_0	; High Byte
		sta PTR_READ+1		
		ldy #32		; number of bytes
Fill_VRAM_NameTable1
		ldx #24		; number of bytes
Fill_VRAM_NameTable2
		lda (PTR_READ)
		jsr Inc_PTR_READ
		nop
		nop
		nop
		nop
		nop
		nop
		nop
		sta TMS9918RAM
		dex
		bne Fill_VRAM_NameTable2
		dey
		bne Fill_VRAM_NameTable1
		rts

;Fill_VRAM_NameTable
;		lda #$00
;		sta TMS9918REG		; set start address VRAM low Byte
;		nop
;		nop
;		nop
;		nop
;		lda #$44		; set address maker and high byte --> $4000
;		sta TMS9918REG		; set start address VRAM
;		lda #$00
;		ldx #24
;Fill_VRAM_NameTable_1
;		ldy #32
;Fill_VRAM_NameTable_2
;		nop
;		nop
;		nop
;		nop
;		nop
;		nop
;		nop
;		sta TMS9918RAM			
;		inc
;		dey
;		bne Fill_VRAM_NameTable_2
;		dex
;		bne Fill_VRAM_NameTable_1
;		rts

;***************************
; Wait V-Sync
;
;***************************

;Wait_V_Sync
;		lda TMS9918REG
;		nop
;		nop
;Wait_V_Sync1
;		lda TMS9918REG
;		and #$80
;		beq Wait_V_Sync1
;		rts

;***************************
; Restore interrupt vector
; Restore YMF262
;***************************
		
Restore_HW
		sei	
		lda SAVEIRQVECT_L
		sta IRQVector_L
		lda SAVEIRQVECT_H
		sta IRQVector_H
		lda #$42+$80		;TBD: disable VDC interrupt
		sta TMS9918REG
		lda #$81
		sta TMS9918REG
		lda #$11		;change frame color
		sta TMS9918REG
		lda #$87
		sta TMS9918REG	
		jsr OPL3_RESET	
		rts
		
;***************************
;
;
;***************************		

WaitKey
		ldx #PS2_GetKey_FN
		jsr PS2_Function_Call
		cmp #"x"
		bne WaitKey
		rts

;***************************
; Init Graphic Mode
;
;***************************

TMS9918Init
	lda #TMS9918_Set_Screen2	; 32 x 24 characters/tiles 256x192 pixel
	ldx #VDP_Set_Video_Mode_FN	; Name table --> $800 ... $AFF
	jsr VDP_Function_Call
	rts

; Register 2 Name table base --> value*$400 --> value*$400 --> $1800 ... $1AFF
; Register 3 Color table base --> value*$40, bit 0 ... 6 must be 1 --> $2000 ... $37FF
; Register 4 Pattern generator base --> value*$800, bit 0 ... 1 must be 1 --> $0000 ... $17FF
; Register 5 Sprite attribute table base address --> value*$80 --> $1B00 ... $1B7F
; Register 6 Sprite pattern generator base address --> value*$800 --> $3800 ... $3FFF

TMS9918_Init_Textmode1
 	lda #TMS9918_Set_Screen0
	ldx #VDP_Set_Video_Mode_FN
	jsr VDP_Function_Call	
	ldx #VDP_Init_Pattern_FN
	jsr VDP_Function_Call	
	rts	

Pattern_Color_Table
!by $90	; #1
!by $90	; #2
!by $90	; #3
!by $90	; #4
!by $40	; #5
!by $40	; #6
!by $F0	; #7
!by $F0	; #8
!by $40	; #9
!by $40	; #10
!by $40	; #11
!by $40	; #12
!by $40	; #13
!by $40	; #14
!by $40	; #15
!by $40	; #16
!by $40	; #17
!by $40	; #18
!by $40	; #19
!by $40	; #20
!by $40	; #21
!by $40	; #22
!by $40	; #23
!by $40	; #24
!by $40	; #25
!by $40	; #26
!by $40	; #27
!by $40	; #28
!by $40	; #29
!by $40	; #30
!by $40	; #31
!by $40	; #32

!source "PacManSprites.asm"
!source "PacManPattern.asm"
!source "PacManScreen.asm"
!source "PacMan_OPL3.asm"

PlayerObjStart	= *
PlayerObj	!fill OBJ_SIZE*NUM_OBJ
PlayerObjEnd 	= *
PlayerObjLen	=PlayerObjEnd-PlayerObjStart


